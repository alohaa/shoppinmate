//
//  SPMSCategoryTypeDataService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/13/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface SPMSCategoryTypeDataService : NSObject

+ (SPMSCategoryTypeDataService*)sharedService;
- (RACSignal*)fetchCategoryTypeWithBaseUrl:(NSURL*)baseUrl
                                    client:(NSURLSession*)client;
- (RACSignal*)fetchCategoryTypeListWithPageNumber:(NSUInteger)pageNumber
                                          baseUrl:(NSURL*)baseUrl
                                           client:(NSURLSession*)client;
- (RACSignal*)categoryTypeForSignal:(RACSignal*)signal;

@end
