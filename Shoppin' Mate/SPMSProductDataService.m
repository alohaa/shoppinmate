//
//  SPMSProductDataService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSProductDataService.h"

#import "Product.h"
#import "NSDictionary+Verified.h"
#import "ReactiveCocoa.h"
#import "RACEXTScope.h"

@interface SPMSProductDataService ()

@end

@implementation SPMSProductDataService
+ (SPMSProductDataService*)sharedService {
  static SPMSProductDataService* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

#pragma mark - Get Product Detail
- (RACSignal*)fetchProductDetailWithProductId:(NSString*)productId
                                      baseUrl:(NSURL*)baseUrl
                                       client:(NSURLSession*)client {
  NSString* relativeString = @"api/product/detail";
  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSMutableURLRequest* request = [NSMutableURLRequest
          requestWithURL:[NSURL URLWithString:relativeString
                                relativeToURL:baseUrl]];
      NSString* paramString =
          [NSString stringWithFormat:@"productId=%@", productId];
      [request
          setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
      [request setHTTPMethod:@"GET"];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[dict verifiedObjectForKey:@"Result"]];
                    [subscriber sendCompleted];
                  }
                }
            }];
      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

#pragma mark - Get Product List
- (RACSignal*)fetchProductListWithPageNumber:(NSUInteger)pageNumber
                                     baseUrl:(NSURL*)baseUrl
                                      client:(NSURLSession*)client {
  NSString* relativeString = @"api/product/list";
  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSMutableURLRequest* request = [NSMutableURLRequest
          requestWithURL:[NSURL URLWithString:relativeString
                                relativeToURL:baseUrl]];
      NSString* paramString = [NSString
          stringWithFormat:@"CurrentPage=%lu", (unsigned long)pageNumber];
      [request
          setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
      [request setHTTPMethod:@"POST"];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[[dict verifiedObjectForKey:@"Result"]
                                             verifiedObjectForKey:@"Data"]];
                    [subscriber sendCompleted];
                  }
                }
            }];
      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

#pragma mark - Get Shelf's Product List
- (RACSignal*)fetchProductListOfShelfWithFurnitureId:(NSUInteger)furnitureID
                                          pageNumber:(NSUInteger)pageNumber
                                             baseUrl:(NSURL*)baseUrl
                                              client:(NSURLSession*)client {
  NSString* relativeString =
      [NSString stringWithFormat:@"api/shelf/%lu/products/%lu",
                                 (unsigned long)furnitureID,
                                 (unsigned long)pageNumber];
  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSMutableURLRequest* request = [NSMutableURLRequest
          requestWithURL:[NSURL URLWithString:relativeString
                                relativeToURL:baseUrl]];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[[dict verifiedObjectForKey:@"Result"]
                                             verifiedObjectForKey:@"Data"]];
                    [subscriber sendCompleted];
                  }
                }
            }];
      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

- (RACSignal*)productListForSignal:(RACSignal*)signal {
  return [signal map:^id(NSArray* dicts) {
      NSMutableArray* products = [[NSMutableArray alloc] init];
      for (NSDictionary* productDic in dicts) {
        Product* new_product = [Product productForDictionary:productDic];
        if (new_product) {
          [products addObject:new_product];
        }
      }
      return products;
  }];
}

@end
