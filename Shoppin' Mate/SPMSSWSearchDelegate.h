//
//  SPMSSWSearchDelegate.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SPMSSWSearchDelegate <NSObject>

- (void)searchCompletedWithResults:(NSArray *)results;

@end
