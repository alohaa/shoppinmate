//
//  AFNetworkReachabilityManager+RAC.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/27/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "AFNetworkReachabilityManager+RAC.h"
#import <ReactiveCocoa.h>
#import <objc/runtime.h>
#import "RACEXTScope.h"

@implementation AFNetworkReachabilityManager (RAC)

- (RACSignal*)rac_reachabilityStatusChangeSignal {
  RACSignal* signal = objc_getAssociatedObject(self, _cmd);

  if (!signal) {
    @weakify(self)

        // Creates multicast signal (messages will arrive on all subscribers)
        // which listens for status changes.
        signal = [[[RACSignal
            createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
                @strongify(self)[self setReachabilityStatusChangeBlock
                                 : ^(AFNetworkReachabilityStatus status) {
                                     [subscriber sendNext:@(status)];
                                 }];
                return nil;
            }] publish] autoconnect];

    objc_setAssociatedObject(
        self, _cmd, signal, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  }

  return signal;
}

@end
