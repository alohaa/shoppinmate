//
//  SPMSDetailPromotionViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMSDetailPromotionViewController
    : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(retain, nonatomic) NSMutableArray* productList;

@end
