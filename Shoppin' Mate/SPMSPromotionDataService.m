//
//  SPMSPromotionDataService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSPromotionDataService.h"
#import "NSDictionary+Verified.h"
#import <ObjectiveSugar.h>

@implementation SPMSPromotionDataService

#pragma mark - Singleton Object
+ (SPMSPromotionDataService*)sharedService {
  static SPMSPromotionDataService* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

#pragma mark - Get Promotion List
- (RACSignal*)fetchPromotionListWithEndDate:(NSDate*)endDate
                                 pageNumber:(NSUInteger)pageNumber
                                    baseUrl:(NSURL*)baseUrl
                                     client:(NSURLSession*)client {
  NSString* relativeString = @"api/promotion/list";

  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  [formatter setDateFormat:DATE_FORMAT_PROMOTION_DATE];
  NSString* formattedEndDate = [formatter stringFromDate:endDate];

  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSString* params =
          [NSString stringWithFormat:@"CurrentPage=%lu&EndDate=%@",
                                     (unsigned long)pageNumber,
                                     formattedEndDate];
      NSURL* relativeUrl =
          [NSURL URLWithString:relativeString relativeToURL:baseUrl];
      NSMutableURLRequest* request =
          [NSMutableURLRequest requestWithURL:relativeUrl];
      [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
      [request setHTTPMethod:@"POST"];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[[dict verifiedObjectForKey:@"Result"]
                                             verifiedObjectForKey:@"Data"]];
                    [subscriber sendCompleted];
                  }
                }
            }];

      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

#pragma mark - Get Total Number of Available Promotion
- (RACSignal*)getTotalNumberofAvailablePromotionWithEndDate:(NSDate*)
              endDate baseUrl:(NSURL*)baseUrl client:(NSURLSession*)client {
  NSString* relativeString = @"api/promotion/count";

  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  [formatter setDateFormat:DATE_FORMAT_PROMOTION_DATE];
  NSString* formattedEndDate = [formatter stringFromDate:endDate];

  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSString* params =
          [NSString stringWithFormat:@"EndDate=%@", formattedEndDate];
      NSURL* relativeUrl =
          [NSURL URLWithString:relativeString relativeToURL:baseUrl];
      NSMutableURLRequest* request =
          [NSMutableURLRequest requestWithURL:relativeUrl];
      [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
      [request setHTTPMethod:@"POST"];
      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[dict verifiedObjectForKey:@"Result"]];
                    [subscriber sendCompleted];
                  }
                }
            }];

      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

- (RACSignal*)promotionForSignal:(RACSignal*)signal {
  return [signal map:^id(NSArray* dicts) {
      NSMutableArray* promotions = [[NSMutableArray alloc] init];
      for (NSDictionary* promotionDic in dicts) {
        Promotion* new_promotion =
            [Promotion promotionForDictionary:promotionDic];
        [promotions addObject:new_promotion];
      }
      return promotions;
  }];
}

@end
