//
//  SPMSFilterSearchProductViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ASValueTrackingSlider.h>

@interface SPMSFilterSearchProductViewController
    : UIViewController<ASValueTrackingSliderDataSource>

@property(strong, nonatomic) IBOutlet ASValueTrackingSlider* priceRangeSlider;
@property(strong, nonatomic) IBOutlet UILabel* priceRangeLabel;
@property(strong, nonatomic) IBOutlet UIButton* applyFiltersButton;
- (IBAction)viewMoreCategoryBTNTapped:(id)sender;
- (IBAction)viewMoreFloorBTNTapped:(id)sender;

@property(strong, nonatomic) NSMutableSet* selectedCategories;
@property(strong, nonatomic) NSMutableSet* selectedMapIds;
@property(assign, nonatomic) NSUInteger seletedCategoryCount;
@property(assign, nonatomic) NSUInteger seletedFloorCount;
@property(assign, nonatomic) CGFloat maxPrice;
@property(assign, nonatomic) BOOL isHasPromotion;
@property(nonatomic) BOOL isAppliedFilter;

@end
