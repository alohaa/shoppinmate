//
//  SPMSPromotionListTableViewCell.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSPromotionListTableViewCell.h"
#import "Product.h"
#import <UIImageView+WebCache.h>

@implementation SPMSPromotionListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString*)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setPromotion:(Promotion*)promotion {
  self.promotionNameLabel.text = promotion.promotionName;
  if (promotion.promotionDescription) {
    self.promotionDescriptionLabel.text = promotion.promotionDescription;
  } else {
    self.promotionDescriptionLabel.text = @"No Description";
  }

  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
  [formatter setDateStyle:NSDateFormatterShortStyle];
  [formatter setTimeStyle:NSDateFormatterShortStyle];

  self.startDateLabel.text = [NSString
      stringWithFormat:@"Start: %@",
                       [formatter stringFromDate:promotion.startDate]];
  self.endDateLabel.text =
      [NSString stringWithFormat:@"End: %@",
                                 [formatter stringFromDate:promotion.endDate]];

  if (1 <= [promotion.products count]) {
    [promotion.products eachWithIndex:^(id object, NSUInteger index) {
        if (index == 0) {
          Product* product = (Product*)object;
          NSString* imageURL = product.imageURL;
          [self.promotionImageView
              sd_setImageWithURL:[NSURL URLWithString:imageURL]
                placeholderImage:[UIImage imageNamed:@"Photo"]];
        }
    }];
  } else {
    [self.promotionImageView setImage:[UIImage imageNamed:@"Photo"]];
  }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

@end
