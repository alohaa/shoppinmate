//
//  SPMSShoppingListDataService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/21/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSShoppingListDataService.h"
#import "NSDictionary+Verified.h"
#import "SPMSOAuthClient.h"
#import "SPMSOAuthAccessToken.h"
#import "Product.h"
#import <ObjectiveSugar.h>

@interface SPMSShoppingListDataService ()

@end

@implementation SPMSShoppingListDataService

#pragma mark - Singleton Object
+ (SPMSShoppingListDataService*)sharedService {
  static SPMSShoppingListDataService* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

#pragma mark - Check Product

#pragma mark - Get Shopping List
- (RACSignal*)fetchShoppingListsWithPageNumber:(NSUInteger)pageNumber
                                       baseUrl:(NSURL*)baseUrl
                                        client:(NSURLSession*)client {
  NSString* relativeString = @"api/shopping/list";

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* accessToken =
              [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
          NSString* headerParam =
              [NSString stringWithFormat:@"Bearer %@", accessToken];
          NSString* params = [NSString
              stringWithFormat:@"CurrentPage=%lu", (unsigned long)pageNumber];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
          [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      [subscriber sendError:error];
                    } else if (!data) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey :
                            @"No data was received from the server."
                            };
                      NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                           code:ERROR_CODE
                                                       userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        NSUInteger responseCode =
                            [(NSHTTPURLResponse*)response statusCode];
                        if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                          NSDictionary* userInfo = @{
                            NSLocalizedDescriptionKey : @"Authorization has "
                            @"been denied. Please " @"login again."
                          };
                          NSError* error = [NSError
                              errorWithDomain:ERROR_DOMAIN
                                         code:HTTP_CODE_NO401_UNAUTHORISED
                                     userInfo:userInfo];
                          [subscriber sendError:error];
                        } else {
                          [subscriber
                              sendNext:[[dict verifiedObjectForKey:@"Result"]
                                           verifiedObjectForKey:@"Data"]];
                          [subscriber sendCompleted];
                        }
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

- (RACSignal*)shoppingListForSignal:(RACSignal*)signal {
  return [[signal catch:^RACSignal * (NSError * error) {
      // If error happened, return nil
      return [RACSignal error:error];
  }] map:^id(NSArray* dicts) {
      if ([dicts isKindOfClass:[NSArray class]] ||
          [dicts isKindOfClass:[NSDictionary class]]) {
        NSMutableArray* shoppingLists = [[NSMutableArray alloc] init];
        for (id object in dicts) {
          if ([object isKindOfClass:[NSDictionary class]]) {
            for (NSDictionary* shoppingListDic in dicts) {
              ShoppingList* new_shoppingList =
                  [ShoppingList shoppingListForDictionary:shoppingListDic];
              [shoppingLists addObject:new_shoppingList];
            }
          } else {
            NSDictionary* dataDict = (NSDictionary*)dicts;
            ShoppingList* new_shoppingList =
                [ShoppingList shoppingListForDictionary:dataDict];
            [shoppingLists addObject:new_shoppingList];
          }
          break;
        }
        return shoppingLists;
      } else {
        return nil;
      }
  }];
}

#pragma mark - Add Product To Shopping List
- (RACSignal*)addProduct:(Product*)product
          toShoppingList:(ShoppingList*)shoppingList
                 baseUrl:(NSURL*)baseUrl
                  client:(NSURLSession*)client {
  NSString* relativeString = @"api/shopping/addproduct";

  NSString* productInListID = product.productID;
  int shoppingListID = [shoppingList.shoppingListID intValue];

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* accessToken =
              [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
          NSString* headerParam =
              [NSString stringWithFormat:@"Bearer %@", accessToken];
          NSString* params =
              [NSString stringWithFormat:@"ShoppingListId=%lu&ProductId=%@",
                                         (unsigned long)shoppingListID,
                                         productInListID];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
          [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      NSError* aError = [self createErrorWithData:request];
                      [subscriber sendError:aError];
                    } else if (!data) {
                      NSError* error =
                          [self createErrorWithData:
                                    @"No data was received from the server."];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        NSUInteger responseCode =
                            [(NSHTTPURLResponse*)response statusCode];
                        if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                          NSDictionary* userInfo = @{
                            NSLocalizedDescriptionKey : @"Authorization has "
                            @"been denied. Please " @"login again."
                          };
                          NSError* error = [NSError
                              errorWithDomain:ERROR_DOMAIN
                                         code:HTTP_CODE_NO401_UNAUTHORISED
                                     userInfo:userInfo];
                          [subscriber sendError:error];
                        } else {
                          [subscriber
                              sendNext:[dict verifiedObjectForKey:@"Success"]];
                          [subscriber sendCompleted];
                        }
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

#pragma mark - Remove Product from Shopping List
- (RACSignal*)removeProduct:(Product*)product
           fromShoppingList:(ShoppingList*)shoppingList
                    baseUrl:(NSURL*)baseUrl
                     client:(NSURLSession*)client {
  NSString* relativeString = @"api/shopping/removeproduct";

  NSString* productID = product.productID;
  int shoppingListID = [shoppingList.shoppingListID intValue];

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* accessToken =
              [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
          NSString* headerParam =
              [NSString stringWithFormat:@"Bearer %@", accessToken];
          NSString* params =
              [NSString stringWithFormat:@"ShoppingListId=%lu&ProductId=%@",
                                         (unsigned long)shoppingListID,
                                         productID];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
          [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      NSError* aError = [self createErrorWithData:request];
                      [subscriber sendError:aError];
                    } else if (!data) {
                      NSError* error =
                          [self createErrorWithData:
                                    @"No data was received from the server."];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        NSUInteger responseCode =
                            [(NSHTTPURLResponse*)response statusCode];
                        if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                          NSDictionary* userInfo = @{
                            NSLocalizedDescriptionKey : @"Authorization has "
                            @"been denied. Please " @"login again."
                          };
                          NSError* error = [NSError
                              errorWithDomain:ERROR_DOMAIN
                                         code:HTTP_CODE_NO401_UNAUTHORISED
                                     userInfo:userInfo];
                          [subscriber sendError:error];
                        } else {
                          [subscriber
                              sendNext:[dict verifiedObjectForKey:@"Success"]];
                          [subscriber sendCompleted];
                        }
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

#pragma mark - Add New Shopping List
- (RACSignal*)addNewShoppingListWithName:(NSString*)shoppingListName
                                 baseUrl:(NSURL*)baseUrl
                                  client:(NSURLSession*)client {
  NSString* relativeString = @"api/shopping/create";

  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSString* accessToken =
          [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
      NSString* headerParam =
          [NSString stringWithFormat:@"Bearer %@", accessToken];
      NSString* params =
          [NSString stringWithFormat:@"ShoppingListName=%@", shoppingListName];
      NSURL* relativeUrl =
          [NSURL URLWithString:relativeString relativeToURL:baseUrl];
      NSMutableURLRequest* request =
          [NSMutableURLRequest requestWithURL:relativeUrl];
      [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
      [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
      [request setHTTPMethod:@"POST"];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  NSError* aError = [self createErrorWithData:request];
                  [subscriber sendError:aError];
                } else if (!data) {
                  NSError* error =
                      [self createErrorWithData:
                                @"No data was received from the server."];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    NSUInteger responseCode =
                        [(NSHTTPURLResponse*)response statusCode];
                    if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey : @"Authorization has "
                        @"been denied. Please " @"login again."
                      };
                      NSError* error =
                          [NSError errorWithDomain:ERROR_DOMAIN
                                              code:HTTP_CODE_NO401_UNAUTHORISED
                                          userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      [subscriber
                          sendNext:[dict verifiedObjectForKey:@"Result"]];
                      [subscriber sendCompleted];
                    }
                  }
                }
            }];

      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

#pragma mark - Delete Shopping List
- (RACSignal*)deleteShoppingListWithShoppingListId:(NSNumber*)shoppingListId
                                           baseUrl:(NSURL*)baseUrl
                                            client:(NSURLSession*)client {
  NSString* relativeString = @"api/shopping/delete";

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* accessToken =
              [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
          NSString* headerParam =
              [NSString stringWithFormat:@"Bearer %@", accessToken];
          NSString* params =
              [NSString stringWithFormat:@"ShoppingListId=%@", shoppingListId];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
          [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      NSError* aError = [self createErrorWithData:request];
                      [subscriber sendError:aError];
                    } else if (!data) {
                      NSError* error =
                          [self createErrorWithData:
                                    @"No data was received from the server."];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        NSUInteger responseCode =
                            [(NSHTTPURLResponse*)response statusCode];
                        if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                          NSDictionary* userInfo = @{
                            NSLocalizedDescriptionKey : @"Authorization has "
                            @"been denied. Please " @"login again."
                          };
                          NSError* error = [NSError
                              errorWithDomain:ERROR_DOMAIN
                                         code:HTTP_CODE_NO401_UNAUTHORISED
                                     userInfo:userInfo];
                          [subscriber sendError:error];
                        } else {
                          [subscriber
                              sendNext:[dict verifiedObjectForKey:@"Result"]];
                          [subscriber sendCompleted];
                        }
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

#pragma mark - Edit Shopping List
- (RACSignal*)editShoppingListWithShoppingListId:(NSNumber*)shoppingListId
                                shoppingListName:(NSString*)shoppingListName
                                         baseUrl:(NSURL*)baseUrl
                                          client:(NSURLSession*)client {
  NSString* relativeString = @"api/shopping/edit";

  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSString* accessToken =
          [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
      NSString* headerParam =
          [NSString stringWithFormat:@"Bearer %@", accessToken];
      NSString* params =
          [NSString stringWithFormat:@"ShoppingListId=%@&ShoppingListName=%@",
                                     shoppingListId,
                                     shoppingListName];
      NSURL* relativeUrl =
          [NSURL URLWithString:relativeString relativeToURL:baseUrl];
      NSMutableURLRequest* request =
          [NSMutableURLRequest requestWithURL:relativeUrl];
      [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
      [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
      [request setHTTPMethod:@"POST"];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  NSError* aError = [self createErrorWithData:request];
                  [subscriber sendError:aError];
                } else if (!data) {
                  NSError* error =
                      [self createErrorWithData:
                                @"No data was received from the server."];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    NSUInteger responseCode =
                        [(NSHTTPURLResponse*)response statusCode];
                    if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey : @"Authorization has "
                        @"been denied. Please " @"login again."
                      };
                      NSError* error =
                          [NSError errorWithDomain:ERROR_DOMAIN
                                              code:HTTP_CODE_NO401_UNAUTHORISED
                                          userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      [subscriber
                          sendNext:[dict verifiedObjectForKey:@"Success"]];
                      [subscriber sendCompleted];
                    }
                  }
                }
            }];

      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

#pragma mark - Generate Error containing Request
- (NSError*)createErrorWithData:(id)data {
  NSDictionary* userInfo = @{NSLocalizedDescriptionKey : data};
  NSError* error =
      [NSError errorWithDomain:ERROR_DOMAIN code:ERROR_CODE userInfo:userInfo];
  return error;
}

@end
