//
//  SPMSPromotionDataService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "Product.h"
#import "Promotion.h"

@interface SPMSPromotionDataService : NSObject

+ (SPMSPromotionDataService*)sharedService;
- (RACSignal*)fetchPromotionListWithEndDate:(NSDate*)endDate
                                 pageNumber:(NSUInteger)pageNumber
                                    baseUrl:(NSURL*)baseUrl
                                     client:(NSURLSession*)client;
- (RACSignal*)getTotalNumberofAvailablePromotionWithEndDate:(NSDate*)endDate
                                                    baseUrl:(NSURL*)baseUrl
                                                     client:
                                                         (NSURLSession*)client;
- (RACSignal*)promotionForSignal:(RACSignal*)signal;

@end
