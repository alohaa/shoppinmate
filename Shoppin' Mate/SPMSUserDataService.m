//
//  SPMSUserDataService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSUserDataService.h"

#import "User.h"
#import "NSDictionary+Verified.h"
#import "SPMSOAuthClient.h"
#import "SPMSOAuthAccessToken.h"
#import "NSMutableURLRequest+Multipart.h"
#import "SPMSDataPersistentOperationQueue.h"

@implementation SPMSUserDataService

+ (SPMSUserDataService*)sharedService {
  static SPMSUserDataService* sharedService;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

#pragma mark - Check Existed Account with Email
- (RACSignal*)checkExistedAccountWithEmail:(NSString*)email
                                   baseUrl:(NSURL*)baseUrl
                                    client:(NSURLSession*)client {
  NSString* relativeString = @"api/user/checkemail";

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* params = [NSString stringWithFormat:@"Email=%@", email];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      [subscriber sendError:error];
                    } else if (!data) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey :
                            @"No data was received from the server."
                            };
                      NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                           code:ERROR_CODE
                                                       userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        id result = [dict verifiedObjectForKey:@"Result"];
                        if (result) {
                          [subscriber sendNext:@(YES)];
                        } else {
                          [subscriber sendNext:@(NO)];
                        }
                        [subscriber sendCompleted];
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return signal;
}

#pragma mark - Registration
- (RACSignal*)registerAccountWithEmail:(NSString*)email
                              password:(NSString*)password
                              fullName:(NSString*)fullName
                           avatarImage:(UIImage*)avatarImage
                               baseUrl:(NSURL*)baseUrl
                                client:(NSURLSession*)client {
  NSString* relativeString = @"api/user/register";

  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSData* imageData;
      NSDictionary* parameters;
      if (avatarImage) {
        imageData = UIImageJPEGRepresentation(avatarImage, 0.6);
        if (imageData) {
          parameters = @{
            @"Image" : imageData,
            @"Email" : email,
            @"FullName" : fullName,
            @"Password" : password
          };
        }
      } else {
        parameters = @{
          @"Email" : email,
          @"FullName" : fullName,
          @"Password" : password
        };
      }

      NSURL* relativeUrl =
          [NSURL URLWithString:relativeString relativeToURL:baseUrl];
      NSMutableURLRequest* request =
          [NSMutableURLRequest requestWithURL:relativeUrl];
      [NSMutableURLRequest addMultipartDataWithParameters:parameters
                                             toURLRequest:request];
      [request setHTTPMethod:@"POST"];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[dict verifiedObjectForKey:@"Result"]];
                    [subscriber sendCompleted];
                  }
                }
            }];

      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

#pragma mark - Get User Profile
- (RACSignal*)fetchUserProfileWithToken:(NSString*)tokenString
                                baseUrl:(NSURL*)baseUrl
                                 client:(NSURLSession*)client {
  NSString* relativeString = @"api/user/profile";

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* headerParam =
              [NSString stringWithFormat:@"Bearer %@", tokenString];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [request addValue:headerParam forHTTPHeaderField:@"Authorization"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      [subscriber sendError:error];
                    } else if (!data) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey :
                            @"No data was received from the server."
                            };
                      NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                           code:ERROR_CODE
                                                       userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        NSInteger responseCode =
                            [(NSHTTPURLResponse*)response statusCode];
                        if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                          NSDictionary* userInfo = @{
                            NSLocalizedDescriptionKey : @"Authorization has "
                            @"been denied. Please " @"login again."
                          };
                          NSError* error = [NSError
                              errorWithDomain:ERROR_DOMAIN
                                         code:HTTP_CODE_NO401_UNAUTHORISED
                                     userInfo:userInfo];
                          [subscriber sendError:error];
                        } else {
                          [subscriber
                              sendNext:[dict verifiedObjectForKey:@"Result"]];
                          [subscriber sendCompleted];
                        }
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

- (RACSignal*)userForSignal:(RACSignal*)signal {
  return [signal map:^id(NSDictionary* response) {
      NSManagedObjectContext* localContext =
          [NSManagedObjectContext MR_contextForCurrentThread];
      User* user = [User MR_createInContext:localContext];
      [localContext performBlockAndWait:^{
          [user MR_importValuesForKeysWithObject:response];
          // Save to Core Data
          [localContext MR_saveToPersistentStoreAndWait];
      }];
      return user;
  }];
}

#pragma mark - Update User Profile
- (RACSignal*)updateAccountWithFullName:(NSString*)fullName
                            avatarImage:(UIImage*)avatarImage
                                 gender:(NSNumber*)gender
                            dateOfBirth:(NSDate*)dateOfBirth
                                baseUrl:(NSURL*)baseUrl
                                 client:(NSURLSession*)client {
  NSString* relativeString = @"api/user/update";

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSData* imageData;
          NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
          if (avatarImage) {
            imageData = UIImageJPEGRepresentation(avatarImage, 0.6);
            if (imageData) {
              [parameters setValue:imageData forKey:@"Image"];
            }
          }
          if (fullName) {
            [parameters setValue:fullName forKey:@"FullName"];
          }
          if (gender) {
            NSString* genderString = [gender boolValue] ? @"True" : @"False";
            [parameters setValue:genderString forKey:@"Gender"];
          }
          if (dateOfBirth) {
            [parameters setValue:dateOfBirth forKey:@"DateOfBirth"];
          }

          NSString* accessToken =
              [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
          NSString* headerParam =
              [NSString stringWithFormat:@"Bearer %@", accessToken];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [NSMutableURLRequest addMultipartDataWithParameters:parameters
                                                 toURLRequest:request];
          [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      NSError* aError = [self createErrorWithData:request];
                      [subscriber sendError:aError];
                    } else if (!data) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey :
                            @"No data was received from the server."
                            };
                      NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                           code:ERROR_CODE
                                                       userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        NSInteger responseCode =
                            [(NSHTTPURLResponse*)response statusCode];
                        if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                          NSDictionary* userInfo = @{
                            NSLocalizedDescriptionKey : @"Authorization has "
                            @"been denied. Please " @"login again."
                          };
                          NSError* error = [NSError
                              errorWithDomain:ERROR_DOMAIN
                                         code:HTTP_CODE_NO401_UNAUTHORISED
                                     userInfo:userInfo];
                          [subscriber sendError:error];
                        } else {
                          [subscriber
                              sendNext:[dict verifiedObjectForKey:@"Result"]];
                          [subscriber sendCompleted];
                        }
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

- (RACSignal*)userForUpdateProfileSignal:(RACSignal*)signal {
  return [[signal catch:^RACSignal * (NSError * error) {
      if ([[error localizedDescription]
              isKindOfClass:[NSMutableURLRequest class]]) {
        NSMutableURLRequest* request =
            (NSMutableURLRequest*)[error localizedDescription];
        // If error, Add to Offline Persistent Operation Queue
        [[SPMSDataPersistentOperationQueue sharedService]
            enqueueWithRequest:request];
      }
      // Return Error to display Error Message
      return [RACSignal error:error];
  }] map:^id(NSDictionary* response) {
      NSManagedObjectContext* localContext =
          [NSManagedObjectContext MR_contextForCurrentThread];
      NSString* userId = [response verifiedObjectForKey:@"Id"];
      User* user = [User MR_findFirstByAttribute:@"userID" withValue:userId];
      if (user) {
        [localContext performBlockAndWait:^{
            user.fullName = [response verifiedObjectForKey:@"FullName"];
            user.gender = [response verifiedObjectForKey:@"Gender"];
            NSString* dobString =
                [response verifiedObjectForKey:@"DateOfBirth"];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.locale =
                [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter setDateFormat:DATE_FORMAT];
            user.dateOfBirth = [dateFormatter dateFromString:dobString];
            user.imageURL = [response verifiedObjectForKey:@"Image"];
            // Save to Core Data
            [localContext MR_saveToPersistentStoreAndWait];
        }];
      }

      return user;
  }];
}

#pragma mark - Check Existed Account with Email
- (RACSignal*)resetPasswordWithEmail:(NSString*)email
                             baseUrl:(NSURL*)baseUrl
                              client:(NSURLSession*)client {
  NSString* relativeString = @"api/user/resetpassword";

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* params = [NSString stringWithFormat:@"Email=%@", email];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      [subscriber sendError:error];
                    } else if (!data) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey :
                            @"No data was received from the server."
                            };
                      NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                           code:ERROR_CODE
                                                       userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        id result = [dict verifiedObjectForKey:@"Success"];
                        if (result) {
                          [subscriber sendNext:@(YES)];
                        } else {
                          [subscriber sendNext:@(NO)];
                        }
                        [subscriber sendCompleted];
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return signal;
}

#pragma mark - Generate Error containing Request
- (NSError*)createErrorWithData:(id)data {
  NSDictionary* userInfo = @{NSLocalizedDescriptionKey : data};
  NSError* error =
      [NSError errorWithDomain:ERROR_DOMAIN code:ERROR_CODE userInfo:userInfo];
  return error;
}

@end
