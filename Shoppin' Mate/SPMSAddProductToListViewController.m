//
//  SPMSAddProductToListViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/22/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSAddProductToListViewController.h"
#import "SPMSShoppingListsTableViewCell.h"
#import "UIView+Border.h"
#import "SPMSWebDataService.h"
#import "ShoppingList.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "NSDictionary+Verified.h"
#import "RACEXTScope.h"
#import <TSMessage.h>
#import <SVPullToRefresh.h>
#import <SVProgressHUD.h>

@interface SPMSAddProductToListViewController () {
  SPMSPaddingTextField* addNewShoppingListTextField;
  UIButton* doneBTN;
}

@property(strong, nonatomic) NSMutableArray* listOfShoppingList;
@property(strong, nonatomic) NSMutableArray* selectedShoppingLists;
@property(strong, nonatomic) NSMutableArray* hadInShoppingLists;
@property(strong, nonatomic) NSMutableArray* removeProductShoppingLists;
@property(assign, nonatomic) NSUInteger cellCount;
@property(assign, nonatomic) NSUInteger pageNumber;
@property(nonatomic) BOOL isCreatingNewList;
@property(nonatomic) BOOL isManipulatingList;

@end

@implementation SPMSAddProductToListViewController
#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.listOfShoppingList = [[NSMutableArray alloc] init];
    self.selectedShoppingLists = [[NSMutableArray alloc] init];
    self.hadInShoppingLists = [[NSMutableArray alloc] init];
    self.removeProductShoppingLists = [[NSMutableArray alloc] init];
    self.cellCount = 0;
    self.pageNumber = 1;
    self.isCreatingNewList = NO;
    self.isManipulatingList = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setProductInfo:)
                                                 name:kAddToListNotification
                                               object:nil];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetDelegate = self;
  self.tableView.emptyDataSetSource = self;
  // Config Sidebar
  [self configNavigationBar];
  [self configureAddNewShoppingListTextField];
  // Register Custom UITableViewCell for SearchDisplayController tableview
  [self.tableView registerClass:[SPMSShoppingListsTableViewCell class]
         forCellReuseIdentifier:@"shoppingListsCell"];
  [self.tableView setTableFooterView:[UIView new]];

  // Config PullToRefresh
  [self configPullToRefresh];
  // Reload Data
  [self loadShoppingListsWithPageNumber:self.pageNumber];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  // register for keyboard notifications
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWillShow)
             name:UIKeyboardWillShowNotification
           object:nil];

  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWillHide)
             name:UIKeyboardWillHideNotification
           object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:kAddToListNotification
                                                object:nil];
  // unregister for keyboard notifications while not visible.
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillShowNotification
              object:nil];

  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillHideNotification
              object:nil];
}

#pragma mark - Load Data
- (void)loadShoppingListsWithPageNumber:(NSUInteger)page {
  if (self.pageNumber == 1) {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  }
  @weakify(self);
  [[[[SPMSWebDataService sharedService] getShoppingListsWithPageNumber:page]
      deliverOn:RACScheduler.mainThreadScheduler] subscribeNext:^(NSArray*
                                                                      list) {
      @strongify(self);
      [self.listOfShoppingList addObjectsFromArray:list];
      self.cellCount = [self.listOfShoppingList count];
      [self.tableView reloadData];
  } error:^(NSError* error) {
      [SVProgressHUD
          showErrorWithStatus:
              [NSString stringWithFormat:@"%@", [error localizedDescription]]];
  } completed:^{
      self.pageNumber = self.pageNumber + 1;
      [self reloadHadInShoppingLists];
      [self.tableView reloadData];
      [SVProgressHUD dismiss];
  }];
}

- (void)setProductInfo:(NSNotification*)notification {
  Product* aProduct =
      [[notification userInfo] verifiedObjectForKey:@"productInList"];
  if (self.product != aProduct) {
    self.product = aProduct;
    [self reloadHadInShoppingLists];
    [self.tableView reloadData];
  }
}

- (void)reloadHadInShoppingLists {
  [self.hadInShoppingLists removeAllObjects];
  [self.hadInShoppingLists
      addObjectsFromArray:[self getShoppingListsContainProduct:self.product]];
}

- (NSArray*)getShoppingListsContainProduct:(Product*)aProduct {
  NSMutableArray* containerArray = [[NSMutableArray alloc] init];
  if ((0 < [self.listOfShoppingList count]) && aProduct) {
    [self.listOfShoppingList each:^(ShoppingList* aShoppingList) {
        if (0 < [aShoppingList.products count]) {
          [aShoppingList.products each:^(Product* productOfShoppingList) {
              if ([productOfShoppingList.productID
                      isEqualToString:aProduct.productID]) {
                [containerArray addObject:aShoppingList];
                return;
              }
          }];
        }
    }];
  }
  return containerArray;
}

- (NSArray*)getSelectedShoppingLists {
  NSMutableArray* selecteds = [[NSMutableArray alloc] init];
  for (NSIndexPath* indexPath in self.selectedShoppingLists) {
    ShoppingList* aShoppingList =
        [self.listOfShoppingList objectAtIndex:indexPath.row];
    if (![self.hadInShoppingLists containsObject:aShoppingList]) {
      [selecteds addObject:aShoppingList];
    }
  }
  return selecteds;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return self.cellCount;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"shoppingListsCell";
  SPMSShoppingListsTableViewCell* cell = (SPMSShoppingListsTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];

  return cell;
}

- (void)configureCell:(SPMSShoppingListsTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  ShoppingList* shoppingList =
      (ShoppingList*)[self.listOfShoppingList objectAtIndex:indexPath.row];

  [cell setCellDataWithShoppingList:shoppingList
                 hadInShoppingLists:self.hadInShoppingLists];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  SPMSShoppingListsTableViewCell* cell = (SPMSShoppingListsTableViewCell*)
      [tableView cellForRowAtIndexPath:indexPath];
  if (cell.accessoryType == UITableViewCellAccessoryNone) {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self.selectedShoppingLists addObject:indexPath];
  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
    [self.selectedShoppingLists removeObject:indexPath];
    ShoppingList* aShoppingList =
        [self.listOfShoppingList objectAtIndex:indexPath.row];
    if ([self.hadInShoppingLists containsObject:aShoppingList]) {
      [self.removeProductShoppingLists addObject:indexPath];
    }
  }
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 70.0f;
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No Shopping List";

  NSMutableParagraphStyle* paragraphStyle =
      [[NSMutableParagraphStyle alloc] init];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"Try to add new Shopping List";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"AddToList_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

#pragma mark - Actions
- (void)dimissAddToListView:(id)sender {
  [self.presentingViewController
      dismissViewControllerAnimated:YES
                         completion:^{
                             self.listOfShoppingList = nil;
                             self.selectedShoppingLists = nil;
                             self.hadInShoppingLists = nil;
                             self.removeProductShoppingLists = nil;
                             self.product = nil;
                         }];
}

- (void)doneButtonAction {
  NSMutableArray* signalArray = [[NSMutableArray alloc] init];
  // Add Product to Selected Shopping List
  RACSignal* addProductSignal = [self addProductToSelectedLists];
  if (addProductSignal) {
    [signalArray addObject:addProductSignal];
  }
  // Remove Product from Shopping List
  RACSignal* removeProductSignal = [self removeProductFromLists];
  if (removeProductSignal) {
    [signalArray addObject:removeProductSignal];
  }
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  [[[RACSignal merge:signalArray] deliverOn:RACScheduler.mainThreadScheduler]
      subscribeNext:^(NSNumber* value) {
          if ([value boolValue] && self.isManipulatingList) {
            [self dimissAddToListView:nil];
          }
      }
      error:^(NSError* error) {
          [TSMessage
              showNotificationInViewController:
                  self title:NSLocalizedString(
                                 @"Add/Remove Product Failed. "
                                 @"\nAdd to Offline Queue to " @"retry later",
                                 nil) subtitle:nil
                                         image:nil
                                          type:TSMessageNotificationTypeError
                                      duration:
                                          TSMessageNotificationDurationAutomatic
                                      callback:nil
                                   buttonTitle:nil
                                buttonCallback:nil
                                    atPosition:TSMessageNotificationPositionTop
                          canBeDismissedByUser:YES];
          self.isManipulatingList = NO;
          [SVProgressHUD dismiss];
      }
      completed:^{
          self.isManipulatingList = NO;
          [SVProgressHUD dismiss];
      }];
}

- (void)addNewListAction {
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  [[[[SPMSWebDataService sharedService]
      addNewShoppingListWithName:addNewShoppingListTextField.text]
      deliverOn:RACScheduler
                    .mainThreadScheduler] subscribeNext:^(NSArray*
                                                              resultArray) {
      if (0 < [resultArray count]) {
        ShoppingList* newShoppingList = [resultArray firstObject];
        self.cellCount++;
        [self.listOfShoppingList insertObject:newShoppingList atIndex:0];
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        if (0 < self.cellCount) {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.tableView
                  scrollToRowAtIndexPath:indexPath
                        atScrollPosition:UITableViewScrollPositionTop
                                animated:YES];
          });
        }
        // Remove Keyboard
        [addNewShoppingListTextField resignFirstResponder];
        // Update Tableview
        [self.tableView insertRowsAtIndexPaths:@[ indexPath ]
                              withRowAnimation:UITableViewRowAnimationFade];
        [TSMessage
            showNotificationInViewController:self
                                       title:NSLocalizedString(
                                                 @"New Shopping List Added",
                                                 nil)
                                    subtitle:nil
                                       image:nil
                                        type:TSMessageNotificationTypeSuccess
                                    duration:
                                        TSMessageNotificationDurationAutomatic
                                    callback:nil
                                 buttonTitle:nil
                              buttonCallback:nil
                                  atPosition:TSMessageNotificationPositionTop
                        canBeDismissedByUser:YES];
      }
  } error:^(NSError* error) {
      [TSMessage
          showNotificationInViewController:self
                                     title:NSLocalizedString(
                                               @"Add new Shopping List "
                                               @"Failed. \nAdd to Offline "
                                               @"Queue to retry later",
                                               nil)
                                  subtitle:nil
                                     image:nil
                                      type:TSMessageNotificationTypeError
                                  duration:
                                      TSMessageNotificationDurationAutomatic
                                  callback:nil
                               buttonTitle:nil
                            buttonCallback:nil
                                atPosition:TSMessageNotificationPositionTop
                      canBeDismissedByUser:YES];
      self.isCreatingNewList = NO;
      addNewShoppingListTextField.text = @"";
      [SVProgressHUD dismiss];
  } completed:^{
      [self.tableView reloadData];
      addNewShoppingListTextField.text = @"";
      self.isCreatingNewList = NO;
      [SVProgressHUD dismiss];
  }];
}

- (RACSignal*)addProductToSelectedLists {
  if (!self.product || ([[self getSelectedShoppingLists] count] == 0)) {
    return nil;
  }
  NSArray* selecteds = [self getSelectedShoppingLists];
  return [[SPMSWebDataService sharedService] addProduct:self.product
                                    toShoppingListArray:selecteds];
}

- (RACSignal*)removeProductFromLists {
  if (!self.product || ([self.removeProductShoppingLists count] == 0)) {
    return nil;
  }
  NSMutableArray* array = [[NSMutableArray alloc] init];
  for (NSIndexPath* indexPath in self.removeProductShoppingLists) {
    ShoppingList* aShoppingList =
        [self.listOfShoppingList objectAtIndex:indexPath.row];
    [array addObject:aShoppingList];
  }
  return [[SPMSWebDataService sharedService] removeProduct:self.product
                                     fromShoppingListArray:array];
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh {
  __weak SPMSAddProductToListViewController* weakSelf = self;

  [self.tableView.pullToRefreshView setTitle:@"Getting New Shopping Lists"
                                    forState:SVPullToRefreshStateLoading];

  // Setup infinite scrolling
  [self.tableView
      addInfiniteScrollingWithActionHandler:^{ [weakSelf insertRowAtBottom]; }];
}

- (void)insertRowAtBottom {
  __weak SPMSAddProductToListViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      [self loadShoppingListsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.infiniteScrollingView stopAnimating];
  });
}

#pragma mark - Buttons
- (void)configureAddNewShoppingListTextField {
  addNewShoppingListTextField = [[SPMSPaddingTextField alloc]
      initWithFrame:CGRectMake(
                        0, self.view.bounds.size.height - 50 - 64, 320, 50)];
  addNewShoppingListTextField.delegate = self;
  [addNewShoppingListTextField
      setBackgroundColor:
          [UIColor colorWithRed:0.925 green:0.941 blue:0.945 alpha:1.000]];
  addNewShoppingListTextField.placeholder = @"Add New Shopping List";
  UIButton* addNewButton =
      [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
  [addNewButton setImage:[UIImage imageNamed:@"AddToList_AddNewList_BTN"]
                forState:UIControlStateNormal];
  [addNewButton
      setImage:[UIImage imageNamed:@"AddToList_AddNewList_Highlighted_BTN"]
      forState:UIControlStateHighlighted];
  [addNewButton
      setImage:[UIImage imageNamed:@"AddToList_AddNewList_Highlighted_BTN"]
      forState:UIControlStateSelected];
  [addNewButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
  RAC(addNewButton, enabled) = [RACSignal
      combineLatest:@[
                      addNewShoppingListTextField.rac_textSignal,
                      RACObserve(self, isCreatingNewList)
                    ]
             reduce:^(NSString* newShoppingListName, NSNumber* isCreating) {
                 return
                     @(0 < newShoppingListName.length && !isCreating.boolValue);
             }];
  [[addNewButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      subscribeNext:^(id x) {
          self.isCreatingNewList = YES;
          [self addNewListAction];
      }];
  [addNewShoppingListTextField setRightView:addNewButton];
  [addNewShoppingListTextField setRightViewMode:UITextFieldViewModeAlways];
  [addNewShoppingListTextField
      addTopBorderWithHeight:0.5f
                    andColor:[UIColor colorWithRed:0.741
                                             green:0.765
                                              blue:0.780
                                             alpha:1.000]];
  [self.view addSubview:addNewShoppingListTextField];
}

- (void)setDoneButton {
  // "Done" Button
  doneBTN = [UIButton buttonWithType:UIButtonTypeSystem];
  doneBTN.frame = CGRectMake(0, 0, 50, 30);
  [doneBTN setTitle:@"Done" forState:UIControlStateNormal];
  RAC(doneBTN, enabled) =
      [RACSignal combineLatest:@[ RACObserve(self, isManipulatingList) ]
                        reduce:^(NSNumber* isManipulating) {
                            return @(!isManipulating.boolValue);
                        }];
  [[doneBTN rac_signalForControlEvents:UIControlEventTouchUpInside]
      subscribeNext:^(id x) {
          self.isManipulatingList = YES;
          [self doneButtonAction];
      }];

  UIBarButtonItem* doneBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:doneBTN];

  [self.navigationItem setRightBarButtonItem:doneBTNItem];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  self.navigationItem.title = @"Add to List";
  [self.navigationController.navigationBar setTranslucent:NO];

  // "Cancel" Button
  UIButton* cancelBTN = [UIButton buttonWithType:UIButtonTypeSystem];
  cancelBTN.frame = CGRectMake(0, 0, 50, 30);
  [cancelBTN setTitle:@"Cancel" forState:UIControlStateNormal];
  [cancelBTN addTarget:self
                action:@selector(dimissAddToListView:)
      forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* cancelBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:cancelBTN];

  [self.navigationItem setLeftBarButtonItem:cancelBTNItem];
  [self setDoneButton];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  [textField resignFirstResponder];
  return NO;
}

#pragma mark - Keyboard Helpers
// method to move the view up/down whenever the keyboard is shown/dismissed
- (void)setViewMovedUp:(BOOL)movedUp {
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationDuration:0.3];  // if you want to slide up the view

  CGRect rect = self.view.frame;
  if (movedUp) {
    // 1. move the view's origin up so that the text field that will be hidden
    // come above the keyboard
    // 2. increase the size of the view so that the area behind the keyboard is
    // covered up.
    rect.origin.y -= kOFFSET_FOR_KEYBOARD;
    rect.size.height += kOFFSET_FOR_KEYBOARD;
  } else {
    // revert back to the normal state.
    rect.origin.y += kOFFSET_FOR_KEYBOARD;
    rect.size.height -= kOFFSET_FOR_KEYBOARD;
  }
  self.view.frame = rect;

  [UIView commitAnimations];
}

- (void)keyboardWillShow {
  // Animate the current view out of the way
  if (self.view.frame.origin.y >= 0) {
    [self setViewMovedUp:YES];
  } else if (self.view.frame.origin.y < 0) {
    [self setViewMovedUp:NO];
  }
}

- (void)keyboardWillHide {
  if (self.view.frame.origin.y >= 0) {
    [self setViewMovedUp:YES];
  } else if (self.view.frame.origin.y < 0) {
    [self setViewMovedUp:NO];
  }
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:kAddToListNotification
                                                object:nil];
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
  addNewShoppingListTextField.delegate = nil;
}

@end
