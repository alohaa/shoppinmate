//
//  NALine.h
//  ShopinMateClient
//
//  Created by Dat Truong on 6/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NAMapViewDelegate.h"

@class NAMapView;

@interface NALine : UIView

/// Start point of line
@property(nonatomic, assign) CGPoint startPoint;
/// End point of line
@property(nonatomic, assign) CGPoint endPoint;
/// Associated view, displayed on an NAMapView.
@property(nonatomic, strong) UIView* view;

/// Create an line with two points.
+ (id)lineWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;
/// Create an line with two points.
- (id)initWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

/// Callback invoked when adding an annotation to a map view.
- (void)addToMapView:(NAMapView*)mapView animated:(BOOL)animate;

/// Remove this line from its map view.
//- (void)removeFromMapView;
/// Update the line position when zooming in or out.
- (void)updatePosition;

/// Override to return a custom view when the annotation is being added to a map
/// view.
- (UIView*)createViewOnMapView:(NAMapView*)mapView;

/// A delegate to invoke map-specific events.
@property(nonatomic, weak) NSObject<NAMapViewDelegate>* mapViewDelegate;
/// Map view to which the annotation currently belongs.
@property(nonatomic, readonly, weak) NAMapView* mapView;

@end
