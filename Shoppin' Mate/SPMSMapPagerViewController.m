//
//  SPMSMapPagerViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/1/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSMapPagerViewController.h"
#import "SPMSMapViewController.h"
#import "SMDBeacon.h"
#import "Beacon.h"
#import "Map.h"
#import "SPMSBeaconAnnotation.h"
#import "SMDPathFinder.h"
#import "SPMSWebDataService.h"
#import "SPMSCurrencyFormatter.h"
#import "Product.h"
#import "ChameleonFramework/Chameleon.h"
#import "SPMSSearchProductViewController.h"
#import "SPMSFilterSearchProductViewController.h"
#import <SVProgressHUD.h>

@interface SPMSMapPagerViewController ()<ViewPagerDelegate, ViewPagerDataSource>

@property(strong, nonatomic) NSMutableArray* beaconsList;
@property(strong, nonatomic) NSMutableOrderedSet* floorList;
@property(strong, nonatomic) NSMutableDictionary* graph;
@property(assign, nonatomic) SPMSMapViewController* currentMapVC;

@end

@implementation SPMSMapPagerViewController

- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.productList = [[NSMutableArray alloc] init];
    self.beaconsList = [[NSMutableArray alloc] init];
    self.floorList = [[NSMutableOrderedSet alloc] init];
    self.graph = [[NSMutableDictionary alloc] init];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.dataSource = self;
  self.delegate = self;
  [self configNavigationBar];
  [self addLocateBeaconBTN];
  [self addBeaconPointsAndGraph];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  if (self == [self.navigationController.viewControllers objectAtIndex:0]) {
    [self addMenuBTN];
  }
}

#pragma mark - Find Shortest Path on Map
- (void)addBeaconPointsAndGraph {
  // If there are products from Shopping List, find and create corresponding
  // beacons and path's graph
  if (0 < [self.productList count]) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             0),
                   ^{
        // Create Graph Dictionary
        [self.productList each:^(Product* product) {
            Map* map =
                [Map MR_findFirstByAttribute:@"mapId" withValue:product.mapId];
            [self.floorList addObject:map.level];
        }];
        // Add Beacon each Floor
        [[self.floorList array] each:^(NSNumber* floorNumber) {
            NSPredicate* bPredicate =
                [NSPredicate predicateWithFormat:@"SELF.mapId LIKE[c] %@",
                                                 [floorNumber stringValue]];
            // Filter Beacons from ProductList which has corresponding floor
            // level
            NSArray* filteredProductArray =
                [self.productList filteredArrayUsingPredicate:bPredicate];
            Map* map = [Map MR_findFirstByAttribute:@"mapId"
                                          withValue:[floorNumber stringValue]];
            NSMutableOrderedSet* beaconSet = [[NSMutableOrderedSet alloc] init];
            NSMutableOrderedSet* productSet =
                [[NSMutableOrderedSet alloc] init];
            if (map && 0 < [filteredProductArray count]) {
              [filteredProductArray each:^(Product* product) {
                  [[map.beacons array] each:^(Beacon* beacon) {
                      NSDictionary* graph = [NSKeyedUnarchiver
                          unarchiveObjectWithData:beacon.graphData];
                      [self.graph setObject:graph
                                     forKey:[beacon.pointId stringValue]];
                      if ([beacon.pointId intValue] ==
                          [product.pointId intValue]) {
                        if (![beaconSet containsObject:beacon]) {
                          [productSet addObject:product];
                          [beaconSet addObject:beacon];
                        }
                        return;
                      }
                  }];
              }];
              if (0 < [[beaconSet array] count]) {
                // Create Beacons
                NSMutableArray* beaconArray = [[NSMutableArray alloc] init];
                [[beaconSet array]
                    eachWithIndex:^(Beacon* beacon, NSUInteger index) {
                        SMDBeacon* smdBeacon =
                            [[SMDBeacon alloc] initWithBeaconData:beacon];
                        // Set Product's Info to corresponding Beacon
                        Product* aProduct = [productSet objectAtIndex:index];
                        smdBeacon.annotation.productImageURLString =
                            aProduct.imageURL;
                        smdBeacon.annotation.title = aProduct.productName;
                        NSNumberFormatter* currencyFormatter =
                            [SPMSCurrencyFormatter formatter];
                        smdBeacon.annotation.subtitle =
                            [currencyFormatter stringFromNumber:aProduct.price];
                        smdBeacon.annotation.color = RandomColorWithShade(dark);
                        [beaconArray addObject:smdBeacon];

                        // Send Visited Beacon to Server
                        [[[[SPMSWebDataService sharedService]
                            sendVisitedBeaconWithPointId:beacon.pointId]
                            deliverOn:[RACScheduler
                                          schedulerWithPriority:
                                              RACSchedulerPriorityBackground]]
                            subscribeNext:^(id x) {}];
                    }];
                [self.beaconsList addObject:beaconArray];
              }
            }
        }];

        dispatch_async(dispatch_get_main_queue(), ^{ [self reloadData]; });
    });
  } else {
    // If there is no product, add all map from Core Data
    NSArray* floorArray = [Map MR_findAll];
    if (floorArray) {
      [floorArray each:^(Map* aMap) { [self.floorList addObject:aMap.level]; }];
    }
  }
}

- (void)rebuildMapDataAndMapView {
  [self.floorList removeAllObjects];
  [self.graph removeAllObjects];
  [self.beaconsList removeAllObjects];
  [self addBeaconPointsAndGraph];
  [self.currentMapVC.view setNeedsDisplay];
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController*)viewPager {
  return [self.floorList count];
}
- (UIView*)viewPager:(ViewPagerController*)viewPager
    viewForTabAtIndex:(NSUInteger)index {
  NSNumber* floorNumber = [self.floorList objectAtIndex:index];
  UILabel* label = [UILabel new];
  label.backgroundColor = [UIColor clearColor];
  label.font = [UIFont systemFontOfSize:12.0];
  label.text = [NSString stringWithFormat:@"Floor #%@", floorNumber];
  label.textAlignment = NSTextAlignmentCenter;
  label.textColor = [UIColor blackColor];
  [label sizeToFit];

  return label;
}

- (UIViewController*)viewPager:(ViewPagerController*)viewPager
    contentViewControllerForTabAtIndex:(NSUInteger)index {
  SPMSMapViewController* mapViewController = [self.storyboard
      instantiateViewControllerWithIdentifier:@"mapViewController"];
  NSArray* path;
  NSArray* beacons;
  if (0 < [self.beaconsList count] && index <= [self.beaconsList count]) {
    beacons = [self.beaconsList objectAtIndex:index];
    SMDPathFinder* pathFinder =
        [[SMDPathFinder alloc] initWithGraphDict:self.graph];
    path = [pathFinder pathFromSelectedBeacons:beacons];
  }

  NSNumber* floor = [self.floorList objectAtIndex:index];
  mapViewController.floor = floor;
  if (beacons) {
    mapViewController.beacons = [NSMutableArray arrayWithArray:beacons];
  }
  if (path) {
    mapViewController.path = path;
  }

  self.currentMapVC = mapViewController;

  return mapViewController;
}

#pragma mark - ViewPagerDelegate
- (CGFloat)viewPager:(ViewPagerController*)viewPager
      valueForOption:(ViewPagerOption)option
         withDefault:(CGFloat)value {
  switch (option) {
    case ViewPagerOptionStartFromSecondTab:
      return 0.0;
    case ViewPagerOptionCenterCurrentTab:
      return 1.0;
    case ViewPagerOptionTabLocation:
      return 0.0;
    case ViewPagerOptionTabHeight:
      return 49.0;
    case ViewPagerOptionTabOffset:
      return 36.0;
    case ViewPagerOptionTabWidth:
      return UIInterfaceOrientationIsLandscape(self.interfaceOrientation)
                 ? 128.0
                 : 96.0;
    case ViewPagerOptionFixFormerTabsPositions:
      return 1.0;
    case ViewPagerOptionFixLatterTabsPositions:
      return 1.0;
    default:
      return value;
  }
}
- (UIColor*)viewPager:(ViewPagerController*)viewPager
    colorForComponent:(ViewPagerComponent)component
          withDefault:(UIColor*)color {
  switch (component) {
    case ViewPagerIndicator:
      return [[UIColor redColor] colorWithAlphaComponent:0.64];
    case ViewPagerTabsView:
      return [[UIColor lightGrayColor] colorWithAlphaComponent:0.32];
    case ViewPagerContent:
      return [[UIColor darkGrayColor] colorWithAlphaComponent:0.32];
    default:
      return color;
  }
}

#pragma mark - Action
- (void)locateBTNTapped:(id)sender {
  if (self.currentMapVC) {
    [self.currentMapVC detectBeaconButtonTapped:nil];
  }
}

- (void)showSearchProductVC:(id)sender {
  [self performSegueWithIdentifier:@"searchProduct" sender:self];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)addMenuBTN {
  // "Show Menu" Button
  UIButton* showMenuBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  showMenuBTN.frame = CGRectMake(0, 0, 30, 30);
  [showMenuBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_ShowMenu_ICO"]
               forState:UIControlStateNormal];
  [showMenuBTN addTarget:self
                  action:@selector(presentLeftMenuViewController:)
        forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showMenuBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:showMenuBTN];

  // "Refresh Map" Button
  UIButton* refreshBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  refreshBTN.frame = CGRectMake(0, 0, 60, 30);
  [refreshBTN setImage:[UIImage imageNamed:@"Map_ReloadMap_ICO"]
              forState:UIControlStateNormal];
  [refreshBTN addTarget:self
                 action:@selector(loadMaps)
       forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* refreshBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:refreshBTN];

  [self.navigationItem
      setLeftBarButtonItems:@[ showMenuBTNItem, refreshBTNItem ]];
}

- (void)addLocateBeaconBTN {
  // "Locate Beacon" Button
  UIButton* locateBeaconBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  locateBeaconBTN.frame = CGRectMake(0, 0, 60, 30);
  [locateBeaconBTN setImage:[UIImage imageNamed:@"Map_Beacon_ICO"]
                   forState:UIControlStateNormal];
  [locateBeaconBTN addTarget:self
                      action:@selector(locateBTNTapped:)
            forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* locateBeaconBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:locateBeaconBTN];

  // "Search Product" Button
  UIButton* searchBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  searchBTN.frame = CGRectMake(5, 0, 30, 30);
  [searchBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_Search_ICO"]
             forState:UIControlStateNormal];
  [searchBTN addTarget:self
                action:@selector(showSearchProductVC:)
      forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* searchBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:searchBTN];

  [self.navigationItem
      setRightBarButtonItems:@[ searchBTNItem, locateBeaconBTNItem ]];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"searchProduct"]) {
    if ([self.floorList count] > 0) {
      SPMSSearchProductViewController* searchVC =
          [segue destinationViewController];
      NSMutableSet* mapIds = [[NSMutableSet alloc] init];
      [[self.floorList array]
          each:^(NSNumber* level) { [mapIds addObject:[level stringValue]]; }];
      searchVC.filterVC.selectedMapIds = mapIds;
      searchVC.filterVC.seletedFloorCount = [self.floorList count];
    }
  }
}
- (void)loadMaps {
  [SVProgressHUD showWithStatus:@"Updating Map..."
                       maskType:SVProgressHUDMaskTypeBlack];
  [[[[SPMSWebDataService sharedService] getMaps]
      deliverOn:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
      [self rebuildMapDataAndMapView];
      [SVProgressHUD showSuccessWithStatus:@"Success"];
  } error:^(NSError* error) {
      [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
  }];
}

@end
