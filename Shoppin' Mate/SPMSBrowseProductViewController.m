//
//  SPMSBrowseProductViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSBrowseProductViewController.h"

#import "Product.h"
#import "SPMSBrowseProductTableViewCell.h"
#import <SVPullToRefresh.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSSearchProductViewController.h"
#import "SPMSDetailProductViewController.h"
#import "RACEXTScope.h"
#import "SPMSWebDataService.h"
#import <SVProgressHUD.h>
#import <TSMessages/TSMessage.h>
#import "Reachability.h"

@interface SPMSBrowseProductViewController ()

@property(strong, nonatomic) NSMutableArray* productList;
@property(assign, nonatomic) NSUInteger cellCount;
@property(assign, nonatomic) NSUInteger pageNumber;

@end

@implementation SPMSBrowseProductViewController
#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.productList = [[NSMutableArray alloc] init];
    self.cellCount = 0;
    self.pageNumber = 1;
  }
  return self;
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
  [super viewDidLoad];
  // Add Observer
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(reachabilityDidChange:)
             name:kReachabilityChangedNotification
           object:nil];

  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetSource = self;
  self.tableView.emptyDataSetDelegate = self;

  // Config Sidebar
  [self configNavigationBar];
  // Config TableView layout
  [self configTableViewLayout];

  // Reload Data
  [self loadProductsWithPageNumber:self.pageNumber];

  // Config PullToRefresh
  [self configPullToRefresh];
}

#pragma mark - Load Data
- (void)loadProductsWithPageNumber:(NSUInteger)page {
  if (self.pageNumber == 1) {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  }

  @weakify(self);
  [[[[SPMSWebDataService sharedService] getProductListWithPageNumber:page]
      deliverOn:RACScheduler
                    .mainThreadScheduler] subscribeNext:^(NSArray* products) {
      @strongify(self);
      [self.productList addObjectsFromArray:products];
      self.cellCount = [self.productList count];
      [self.tableView reloadData];
  } error:^(NSError* error) {
      [SVProgressHUD
          showErrorWithStatus:
              [NSString stringWithFormat:@"%@", [error localizedDescription]]];
  } completed:^{
      self.pageNumber = self.pageNumber + 1;
      [SVProgressHUD dismiss];
  }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return self.cellCount;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"BrowseProductTableViewCell";
  SPMSBrowseProductTableViewCell* cell = (SPMSBrowseProductTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];

  return cell;
}

- (void)configureCell:(SPMSBrowseProductTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  Product* product = (Product*)[self.productList objectAtIndex:indexPath.row];
  cell.product = product;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  Product* product = [self.productList objectAtIndex:indexPath.row];
  [self performSegueWithIdentifier:@"viewDetailProduct" sender:product];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 280.0f;
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No products loaded";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"Product is loading or something goes wrong!";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"BrowseProduct_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView*)scrollView {
}

- (void)emptyDataSetDidTapButton:(UIScrollView*)scrollView {
}

#pragma mark - Add Product to List
- (IBAction)addProductToListAction:(id)sender {
  CGPoint buttonPosition =
      [sender convertPoint:CGPointZero toView:self.tableView];
  NSIndexPath* indexPath =
      [self.tableView indexPathForRowAtPoint:buttonPosition];
  if (indexPath) {
    Product* product = (Product*)[self.productList objectAtIndex:indexPath.row];
    [self showAddToListViewWithProduct:product animated:YES];
  }
}

- (void)showAddToListViewWithProduct:(Product*)product animated:(BOOL)animated {
  UIStoryboard* storyboard =
      [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  UIViewController* navListViewController = [storyboard
      instantiateViewControllerWithIdentifier:@"addToListNavViewController"];

  [navListViewController
      setModalPresentationStyle:UIModalPresentationFullScreen];
  [self presentViewController:navListViewController
                     animated:animated
                   completion:^{
                       [[NSNotificationCenter defaultCenter]
                           postNotificationName:kAddToListNotification
                                         object:nil
                                       userInfo:[[NSDictionary alloc]
                                                    initWithObjectsAndKeys:
                                                        product,
                                                        @"productInList",
                                                        nil]];
                   }];
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh {
  __weak SPMSBrowseProductViewController* weakSelf = self;

  // Setup pull-to-refresh
  [self.tableView
      addPullToRefreshWithActionHandler:^{ [weakSelf insertRowAtTop]; }];

  [self.tableView.pullToRefreshView setTitle:@"Getting New Products"
                                    forState:SVPullToRefreshStateLoading];

  // Setup infinite scrolling
  [self.tableView
      addInfiniteScrollingWithActionHandler:^{ [weakSelf insertRowAtBottom]; }];
}

- (void)insertRowAtTop {
  __weak SPMSBrowseProductViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      weakSelf.pageNumber = 1;
      [weakSelf.productList removeAllObjects];
      [weakSelf loadProductsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.pullToRefreshView stopAnimating];
  });
}

- (void)insertRowAtBottom {
  __weak SPMSBrowseProductViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      [self loadProductsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.infiniteScrollingView stopAnimating];
  });
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
  [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
  self.tableView.backgroundColor = [UIColor clearColor];
  self.tableView.opaque = NO;
  self.tableView.backgroundView = nil;
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  // "Show Menu" Button
  UIButton* showMenuBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  showMenuBTN.frame = CGRectMake(0, 0, 30, 30);
  [showMenuBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_ShowMenu_ICO"]
               forState:UIControlStateNormal];
  [showMenuBTN addTarget:self
                  action:@selector(presentLeftMenuViewController:)
        forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showMenuBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:showMenuBTN];

  [self.navigationItem setLeftBarButtonItem:showMenuBTNItem];
  // "Search Product" Button
  UIButton* searchBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  searchBTN.frame = CGRectMake(5, 0, 30, 30);
  [searchBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_Search_ICO"]
             forState:UIControlStateNormal];
  [searchBTN addTarget:self
                action:@selector(showSearchProductVC:)
      forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* searchBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:searchBTN];

  [self.navigationItem setRightBarButtonItem:searchBTNItem];
  [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark - Action
- (void)showSearchProductVC:(id)sender {
  [self performSegueWithIdentifier:@"searchProduct" sender:self];
}

- (void)reachabilityDidChange:(NSNotification*)notification {
  Reachability* reachability = (Reachability*)[notification object];

  if ([reachability isReachable]) {
    [TSMessage dismissActiveNotification];
  } else {
    [TSMessage
        showNotificationInViewController:self
                                   title:NSLocalizedString(
                                             @"No Internet Connection", nil)
                                subtitle:nil
                                   image:nil
                                    type:TSMessageNotificationTypeError
                                duration:TSMessageNotificationDurationEndless
                                callback:nil
                             buttonTitle:nil
                          buttonCallback:nil
                              atPosition:TSMessageNotificationPositionTop
                    canBeDismissedByUser:NO];
  }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
// preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewDetailProduct"]) {
    Product* aProduct = (Product*)sender;
    SPMSDetailProductViewController* detailVC =
        [segue destinationViewController];
    [detailVC setDetailProduct:aProduct];
  }
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kReachabilityChangedNotification
              object:nil];
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
}
@end
