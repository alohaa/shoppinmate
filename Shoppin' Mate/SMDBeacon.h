//
//  SMDBeacon.h
//  ShopinMateClient
//
//  Created by Dat Truong on 2014-06-08.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Beacon.h"

@class CLBeacon;
@class SPMSBeaconAnnotation;

@interface SMDBeacon : NSObject

@property(strong, nonatomic) NSNumber* pointId;
@property(strong, nonatomic) NSString* alias;
@property(strong, nonatomic) SPMSBeaconAnnotation* annotation;
@property(strong, nonatomic) CLBeacon* beacon;
@property NSUInteger major;
@property NSUInteger minor;
@property double accuracy;

- (instancetype)initWithPoint:(CGPoint)point;
- (instancetype)initWithBeaconData:(Beacon*)beacon;

@end
