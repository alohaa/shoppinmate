//
//  Map.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/31/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Beacon, Shelf;

@interface Map : NSManagedObject

@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSString * imageLink;
@property (nonatomic, retain) NSDate * lastUpdatedDate;
@property (nonatomic, retain) NSNumber * level;
@property (nonatomic, retain) NSString * mapId;
@property (nonatomic, retain) NSString * mapName;
@property (nonatomic, retain) NSOrderedSet *beacons;
@property (nonatomic, retain) NSSet *shelves;
@end

@interface Map (CoreDataGeneratedAccessors)

- (void)insertObject:(Beacon *)value inBeaconsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromBeaconsAtIndex:(NSUInteger)idx;
- (void)insertBeacons:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeBeaconsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInBeaconsAtIndex:(NSUInteger)idx withObject:(Beacon *)value;
- (void)replaceBeaconsAtIndexes:(NSIndexSet *)indexes withBeacons:(NSArray *)values;
- (void)addBeaconsObject:(Beacon *)value;
- (void)removeBeaconsObject:(Beacon *)value;
- (void)addBeacons:(NSOrderedSet *)values;
- (void)removeBeacons:(NSOrderedSet *)values;
- (void)addShelvesObject:(Shelf *)value;
- (void)removeShelvesObject:(Shelf *)value;
- (void)addShelves:(NSSet *)values;
- (void)removeShelves:(NSSet *)values;

@end
