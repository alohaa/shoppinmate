//
//  SMDPathFinder.h
//  ShopinMateClient
//
//  Created by Dat Truong on 7/5/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMDPathFinder : NSObject

@property (strong, nonatomic) NSDictionary *graphDict;

- (instancetype)initWithGraphDict:(NSDictionary *)dict;
- (NSArray *)pathFromSelectedBeacons:(NSArray *)beacons;

@end
