//
//  SPMSCategoryListTableViewCell.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/6/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSCategoryListTableViewCell.h"

@implementation SPMSCategoryListTableViewCell

- (void)setCategory:(CategoryType*)category {
  self.categoryNameLabel.text = category.categoryName;
}

@end
