//
//  Product.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/7/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Promotion;

@interface Product : NSObject

@property(nonatomic, strong) NSString* categoryName;
@property(nonatomic, strong) NSDate* createdDate;
@property(nonatomic, strong) NSNumber* discount;
@property(nonatomic, strong) NSNumber* hasPromotion;
@property(nonatomic, strong) NSString* imageURL;
@property(nonatomic, strong) NSNumber* price;
@property(nonatomic, strong) NSString* productDescription;
@property(nonatomic, strong) NSString* productID;
@property(nonatomic, strong) NSString* productName;
@property(nonatomic, strong) NSNumber* mapId;
@property(nonatomic, strong) NSNumber* pointId;
@property(nonatomic, strong) NSNumber* beaconXCoordinator;
@property(nonatomic, strong) NSNumber* beaconYCoordinator;
@property(nonatomic, strong) Promotion* promotion;

+ (Product*)productForDictionary:(NSDictionary*)dictionary;
+ (NSMutableSet*)productsForDictionary:(NSDictionary*)dicts;

@end
