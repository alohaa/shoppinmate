//
//  SPMSLoginViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface SPMSLoginViewController
    : UIViewController<UITextFieldDelegate, FBLoginViewDelegate>

@property(strong, nonatomic) IBOutlet UIImageView* bgImage;
@property(strong, nonatomic) IBOutlet UITextField* emailTextField;
@property(strong, nonatomic) IBOutlet UITextField* passwordTextField;
@property(strong, nonatomic) IBOutlet UIButton* signInByEmailButton;
@property(strong, nonatomic) IBOutlet UIButton* loginViaFBButton;
@property(strong, nonatomic)
    IBOutlet UIImageView* emailValidateStatusIndicatorImageView;
@property(strong, nonatomic)
    IBOutlet UIImageView* passwordValidateStatusIndicatorImageView;

- (IBAction)backgroundTappedAction:(id)sender;
@end
