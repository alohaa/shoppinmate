//
//  SPMSResetPasswordViewController.h
//  Shoppin' Mate
//
//  Created by Hiro on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMSResetPasswordViewController
    : UIViewController<UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet UITextField* emailTextField;
@property(strong, nonatomic) IBOutlet UIButton* resetButton;
@property(strong, nonatomic)
    IBOutlet UIImageView* emailValidateStatusIndicatorImageView;
- (IBAction)backToLoginButtonAction:(id)sender;

- (IBAction)resetButtonPressed:(UIButton*)sender;
@end
