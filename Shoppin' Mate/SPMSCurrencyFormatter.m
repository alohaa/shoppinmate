//
//  SPMSCurrencyFormatter.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/20/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSCurrencyFormatter.h"

@implementation SPMSCurrencyFormatter

#pragma mark - Singleton Instance
+ (NSNumberFormatter*)formatter {
  NSMutableDictionary* threadDictionary =
      [[NSThread currentThread] threadDictionary];
  NSNumberFormatter* numberFormatter =
      [threadDictionary objectForKey:@"SPMSCurrencyNumberFormatter"];
  if (numberFormatter == nil) {
    numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"#,##0 VND"];
    [numberFormatter setNegativeFormat:@"(0 VND)"];
    [threadDictionary setObject:numberFormatter
                         forKey:@"SPMSCurrencyNumberFormatter"];
  }
  return numberFormatter;
}

@end
