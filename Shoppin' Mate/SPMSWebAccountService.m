//
//  SPMSWebAccountService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/15/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSWebAccountService.h"
#import "NSDictionary+Verified.h"

#import "User.h"
#import "SPMSOAuthClient.h"
#import "SPMSOAuthAccessToken.h"
#import <SVProgressHUD.h>
@implementation SPMSWebAccountService

#pragma mark - Singleton Class Init
+ (id)sharedService {
  static SPMSWebAccountService* sharedService = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{ sharedService = [[self alloc] init]; });
  return sharedService;
}

#pragma mark - Login
/**
 *  Sign-in with Email
 *
 *  @param emailString    User's Email
 *  @param passwordString User Account's Password
 *  @param completeBlock  Complete Block
 */
- (RACSignal*)signInWithEmail:(NSString*)emailString
                     password:(NSString*)passwordString {
  RACSignal* authenticateSignal =
      [[SPMSOAuthClient sharedInstance] authenticateWithEmail:emailString
                                                     password:passwordString];
  return
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          [[authenticateSignal catch:^RACSignal * (NSError * error) {
              return [RACSignal error:error];
          }] subscribeNext:^(id success) {
              BOOL isSucceed = [success boolValue];
              if (isSucceed) {
                [[NSUserDefaults standardUserDefaults]
                    setBool:YES
                     forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [subscriber sendNext:success];
              }
          } error:^(NSError* error) { [subscriber sendError:error]; }
              completed:^{ [subscriber sendCompleted]; }];
          return [RACDisposable disposableWithBlock:^{//
                                                    }];
      }];
}

#pragma mark - Logout
- (void)logout {
  // Remove Access Token
  [[SPMSOAuthClient sharedInstance] setAccessToken:nil];

  // Truncate all User Information in "User" Entity in Core Date
  [User MR_truncateAll];
  [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

  [[NSUserDefaults standardUserDefaults] setBool:NO
                                          forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Validation
/**
 *  Validate User's Email String
 *
 *  @param emailString User's Email
 *
 *  @return BOOL
 */
- (BOOL)isValidEmail:(NSString*)emailString {
  if (emailString != nil && [emailString length] == 0) {
    return NO;
  }

  NSString* regExPattern = REGEX_EMAIL;

  NSRegularExpression* regEx = [[NSRegularExpression alloc]
      initWithPattern:regExPattern
              options:NSRegularExpressionCaseInsensitive
                error:nil];
  NSUInteger regExMatches =
      [regEx numberOfMatchesInString:emailString
                             options:0
                               range:NSMakeRange(0, [emailString length])];

  if (regExMatches == 0) {
    return NO;
  } else {
    return YES;
  }
}
/**
 *  Validate User Account's Password String
 *
 *  @param passwordString User Account's Password
 *
 *  @return BOOL
 */
- (BOOL)isValidPassword:(NSString*)passwordString {
  if ([passwordString length] <= 3) {
    return NO;
  } else
    return YES;
}

- (BOOL)isValidFullname:(NSString*)fullnameString {
  if ([fullnameString length] <= 3) {
    return NO;
  } else
    return YES;
}

@end
