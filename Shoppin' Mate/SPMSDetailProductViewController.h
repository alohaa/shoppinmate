//
//  SPMSDetailViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface SPMSDetailProductViewController
    : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(strong, nonatomic) Product* product;
@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(strong, nonatomic) IBOutlet UIImageView* detailProductImageView;
@property(strong, nonatomic) IBOutlet UILabel* detailProductCategoryLabel;
@property(strong, nonatomic) IBOutlet UILabel* detailProductNameLabel;
@property(strong, nonatomic) IBOutlet UILabel* detailProductPriceLabel;
@property(strong, nonatomic)
    IBOutlet UITextView* detailProductDescriptionTextView;
@property(strong, nonatomic) IBOutlet UIButton* addToShoppingListButton;
@property(strong, nonatomic) IBOutlet UIButton* viewProductLocationButton;

- (void)setDetailProduct:(Product*)product;

@end
