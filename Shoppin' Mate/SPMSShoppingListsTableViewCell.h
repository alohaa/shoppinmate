//
//  SPMSShoppingListsTableViewCell.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/22/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingList.h"
#import <MCSwipeTableViewCell.h>

@interface SPMSShoppingListsTableViewCell : MCSwipeTableViewCell

@property(assign, nonatomic) ShoppingList* shoppingList;
@property(assign, nonatomic) NSMutableArray* hadInShoppingLists;

@property(strong, nonatomic) UIImageView* shoppingListImageView;
@property(strong, nonatomic) UILabel* shoppingListNameLabel;
@property(strong, nonatomic) UILabel* productCountLabel;

- (void)setCellDataWithShoppingList:(ShoppingList*)shoppingList
                 hadInShoppingLists:(NSMutableArray*)hadInShoppingLists;

@end
