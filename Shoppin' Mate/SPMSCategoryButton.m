//
//  SPMSCategoryButton.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSCategoryButton.h"

@implementation SPMSCategoryButton

- (id)initWithTitleString:(NSString*)title
                     font:(UIFont*)font
                        x:(CGFloat)x
                        y:(CGFloat)y {
  NSDictionary* attributes = @{NSFontAttributeName : font};
  CGSize stringSize = [title sizeWithAttributes:attributes];
  CGRect frame =
      CGRectMake(x, y, stringSize.width + 20, stringSize.height + 12);
  self = [super initWithFrame:frame];
  if (self) {
    self.titleLabel.font = font;
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitle:title forState:UIControlStateSelected];
    [self setTitleColor:[UIColor colorWithWhite:0.592 alpha:1.000]
               forState:UIControlStateNormal];
    [self setTitleColor:
              [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]
               forState:UIControlStateSelected];
    [self setBackgroundColor:
              [UIColor colorWithRed:0.898 green:0.898 blue:0.902 alpha:1.000]];
    self.userInteractionEnabled = YES;
    // Add Toggle-State
    [self addTarget:self
                  action:@selector(changeButtonState:)
        forControlEvents:UIControlEventTouchUpInside];
  }
  return self;
}

- (void)changeButtonState:(id)sender {
  BOOL buttonState = ![sender isSelected];
  [sender setSelected:buttonState];
}

@end
