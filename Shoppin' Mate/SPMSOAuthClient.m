//
//  SPMSOAuthClient.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/15/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSOAuthClient.h"
#import "SPMSOAuthAccessToken.h"
#import "SPMSWebDataService.h"
#import "NSDictionary+Verified.h"
#import "SPMSFacebookService.h"
#import "SPMSWebAccountService.h"

@interface SPMSOAuthClient ()

@property(nonatomic, copy) NSURL* baseUrl;
@property(nonatomic, copy) NSURLSession* client;

@end

@implementation SPMSOAuthClient
@synthesize persistent = _persistent;
#pragma mark - Singleton Instance
+ (id)sharedInstance {
  static SPMSOAuthClient* sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{ sharedInstance = [[self alloc] init]; });
  return sharedInstance;
}

#pragma mark - Initialize
- (id)init {
  if (self = [super init]) {
    self.persistent = YES;
    self.accessToken = [self accessToken];
    self.client = [[SPMSWebDataService sharedService] client];
    self.baseUrl = [[SPMSWebDataService sharedService] baseUrl];
    if (self.accessToken) {
      self.isAuthenticated = YES;
    } else {
      self.isAuthenticated = NO;
    }
  }
  return self;
}

- (id)initWithPersistent:(BOOL)shouldPersist {
  self = [super init];
  if (self) {
    self.persistent = shouldPersist;
    self.client = [[SPMSWebDataService sharedService] client];
    self.baseUrl = [[SPMSWebDataService sharedService] baseUrl];
    self.isAuthenticated = NO;
  }
  return self;
}

#pragma mark - Check Login
- (BOOL)isHadAccessToken {
  return self.isAuthenticated;
}

#pragma mark - Request Token
- (RACSignal*)authenticateWithEmail:(NSString*)email
                           password:(NSString*)password {
  RACSignal* authenticateSignal =
      [self getAccessTokenWithEmail:email password:password];
  RACSignal* getTokenSignal = [self tokenForSignal:authenticateSignal];
  return
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          [getTokenSignal subscribeNext:^(SPMSOAuthAccessToken* token) {
              // If get Access Token from Server successfully, get User Profile
              // and save into Core Data
              if (token && [token isKindOfClass:[SPMSOAuthAccessToken class]]) {
                NSString* tokenString = [token accessToken];
                [[[[SPMSWebDataService sharedService]
                    getUserProfileWithToken:tokenString]
                    deliverOn:RACScheduler.mainThreadScheduler]
                    subscribeNext:^(id x) {
                        [self setAccessToken:token];
                        [subscriber sendNext:@(YES)];
                        [subscriber sendCompleted];
                    }];
              } else {
                NSError* error = (NSError*)token;
                [subscriber sendError:error];
              }
          } error:^(NSError* error) { [subscriber sendError:error]; }];
          return [RACDisposable disposableWithBlock:^{//
                                                    }];
      }];
}

- (RACSignal*)authenticateWithFacebookToken:(NSString*)token {
  RACSignal* authenticateSignal = [self getAccessTokenWithFacebookToken:token];
  RACSignal* getTokenSignal = [self tokenForSignal:authenticateSignal];
  return
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          [getTokenSignal subscribeNext:^(SPMSOAuthAccessToken* token) {
              // If get Access Token from Server successfully, get User Profile
              // and save into Core Data
              if (token) {
                NSString* tokenString = [token accessToken];
                [[[[SPMSWebDataService sharedService]
                    getUserProfileWithToken:tokenString]
                    deliverOn:RACScheduler.mainThreadScheduler]
                    subscribeNext:^(id x) {
                        [self setAccessToken:token];
                        [subscriber sendNext:@(YES)];
                        [subscriber sendCompleted];
                    }];
              } else {
                NSError* error =
                    [NSError errorWithDomain:ERROR_DOMAIN
                                        code:ERROR_CODE
                                    userInfo:@{
                                      @"Error" : @"Cannot get Access Token"
                                    }];
                [subscriber sendError:error];
              }
          } error:^(NSError* error) { [subscriber sendError:error]; }];
          return [RACDisposable disposableWithBlock:^{//
                                                    }];
      }];
}

- (RACSignal*)getAccessTokenWithEmail:(NSString*)email
                             password:(NSString*)password {
  NSString* relativeString = @"api/token";

  return
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {

          NSMutableURLRequest* request = [NSMutableURLRequest
              requestWithURL:[NSURL URLWithString:relativeString
                                    relativeToURL:self.baseUrl]];

          NSString* paramString = [NSString
              stringWithFormat:@"grant_type=password&username=%@&password=%@",
                               email,
                               password];
          [request
              setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [self.client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      [subscriber sendError:error];
                    } else if (!data) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey :
                            @"No data was received from the server."
                            };
                      NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                           code:ERROR_CODE
                                                       userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        [subscriber sendNext:dict];
                        [subscriber sendCompleted];
                      }
                    }
                }];
          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
}

- (RACSignal*)getAccessTokenWithFacebookToken:(NSString*)token {
  NSString* relativeString = @"api/user/token";

  return [RACSignal createSignal:^RACDisposable *
                                 (id<RACSubscriber> subscriber) {

      NSMutableURLRequest* request = [NSMutableURLRequest
          requestWithURL:[NSURL URLWithString:relativeString
                                relativeToURL:self.baseUrl]];

      NSString* paramString = [NSString stringWithFormat:@"Token=%@", token];
      [request
          setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
      [request setHTTPMethod:@"POST"];

      NSURLSessionDataTask* task = [self.client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[dict verifiedObjectForKey:@"Result"]];
                    [subscriber sendCompleted];
                  }
                }
            }];
      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
}

- (RACSignal*)tokenForSignal:(RACSignal*)signal {
  return [signal map:^id(NSDictionary* response) {
      if (![response verifiedObjectForKey:@"error"]) {
        SPMSOAuthAccessToken* token =
            [SPMSOAuthAccessToken tokenWithResponseBody:response];
        return token;
      } else {
        NSString* errorDesc =
            [response verifiedObjectForKey:@"error_description"];
        NSDictionary* userInfo = @{NSLocalizedDescriptionKey : errorDesc};
        NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                             code:ERROR_CODE
                                         userInfo:userInfo];
        return error;
      }
  }];
}

#pragma mark - Refresh Token
- (RACSignal*)doRequestAndRefreshTokenIfNecessary:(RACSignal*)requestSignal {
  return [requestSignal catch:^(NSError* error) {
      // Catch the error, refresh the token, and then do the request again.
      BOOL hasRefreshToken =
          [[[SPMSOAuthClient sharedInstance] accessToken] refreshToken] != nil;

      BOOL httpCode401AccessDenied = error.code == HTTP_CODE_NO401_UNAUTHORISED;
      if (httpCode401AccessDenied && hasRefreshToken) {
        return [[[[self refreshToken] ignoreValues]
            concat:requestSignal] catch:^RACSignal * (NSError * error) {
            BOOL invalidGrantError = error.code == ERROR_CODE_INVALID_GRANT;
            if (invalidGrantError) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  // Logout to ask for new login
                  if ([[SPMSFacebookService
                              sharedFBServiceInstance] isLoggedIn]) {
                    // Logout from FB Account
                    [[[[SPMSFacebookService sharedFBServiceInstance] logoutFB]
                        deliverOn:RACScheduler.mainThreadScheduler]
                        subscribeCompleted:^{//
                                           }];
                  } else {
                    // Logout from Email Account
                    [[SPMSWebAccountService sharedService] logout];
                  }
              });
            }
            return [RACSignal error:error];
        }];
      }
      return requestSignal;
  }];
}

- (RACSignal*)refreshToken {
  if (accessToken == nil) {
    return nil;
  }

  NSString* relativeString = @"api/token";

  NSString* token = accessToken.accessToken;
  NSString* token_type = accessToken.tokenType;
  NSString* refresh_token = accessToken.refreshToken;
  NSString* header_Authorization_Param =
      [NSString stringWithFormat:@"%@ %@", token_type, token];

  RACSignal* refreshTokenSignal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {

          NSMutableURLRequest* request = [NSMutableURLRequest
              requestWithURL:[NSURL URLWithString:relativeString
                                    relativeToURL:self.baseUrl]];

          NSString* paramString = [NSString
              stringWithFormat:@"grant_type=refresh_token&refresh_token=%@",
                               refresh_token];
          [request
              setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
          [request addValue:header_Authorization_Param
              forHTTPHeaderField:@"Authorization"];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [self.client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      [subscriber sendError:error];
                    } else if (!data) {
                      NSDictionary* userInfo = @{
                        NSLocalizedDescriptionKey :
                            @"No data was received from the server."
                            };
                      NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                           code:ERROR_CODE
                                                       userInfo:userInfo];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else if ([dict verifiedObjectForKey:@"error"] &&
                                 [[dict verifiedObjectForKey:@"error"]
                                     isEqualToString:@"invalid_grant"]) {
                        NSDictionary* userInfo = @{
                          NSLocalizedDescriptionKey :
                              @"Invalid Grant. Logout the account!"
                              };
                        NSError* error =
                            [NSError errorWithDomain:ERROR_DOMAIN
                                                code:ERROR_CODE_INVALID_GRANT
                                            userInfo:userInfo];
                        [subscriber sendError:error];
                      } else {
                        RACSignal* signal = [RACSignal return:dict];
                        RACSignal* getTokenSignal =
                            [self tokenForSignal:signal];
                        [getTokenSignal
                            subscribeNext:^(SPMSOAuthAccessToken* token) {
                                [self setAccessToken:token];
                                [subscriber sendCompleted];
                            }];
                      }
                    }
                }];
          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];

  return [refreshTokenSignal concat];
}

#pragma mark - Accessors
- (void)setPersistent:(BOOL)shouldPersist {
  if (_persistent == shouldPersist) {
    return;
  }

  if (shouldPersist && accessToken) {
    [self.accessToken storeInDefaultKeychain];
  }

  if (_persistent && !shouldPersist) {
    [self.accessToken removeFromDefaultKeychain];
  }
  [self willChangeValueForKey:@"persistent"];
  _persistent = shouldPersist;
  [self didChangeValueForKey:@"persistent"];
}

- (SPMSOAuthAccessToken*)accessToken {
  if (accessToken) {
    return accessToken;
  }

  if (_persistent) {
    // Get Saved Token from Keychain
    accessToken = [SPMSOAuthAccessToken tokenFromDefaultKeychain];
    // Delegate
    return accessToken;
  } else {
    return nil;
  }
}

- (void)setAccessToken:(SPMSOAuthAccessToken*)value {
  if (self.accessToken == value) {
    return;
  }

  if (!value) {
    [self.accessToken removeFromDefaultKeychain];
    self.isAuthenticated = NO;
  }

  [self willChangeValueForKey:@"accessToken"];
  accessToken = value;
  [self didChangeValueForKey:@"accessToken"];

  if (_persistent) {
    // Save Token to Keychain
    [accessToken storeInDefaultKeychain];
    self.isAuthenticated = YES;
  }
}

#pragma mark - NSCoding
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super init]) {
    accessToken = [aDecoder decodeObjectForKey:@"token"];
  }
  return self;
}

@end
