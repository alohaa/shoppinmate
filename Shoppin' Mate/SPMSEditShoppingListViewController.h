//
//  SPMSEditShoppingListViewController.h
//  Shoppin' Mate
//
//  Created by Hiro on 8/9/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMSEditShoppingListViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *shoppingListNameTextField;
@property (strong, nonatomic) NSString* shoppingListName;
@property (strong, nonatomic) NSNumber* shoppingListID;
- (IBAction)saveButtonTapped:(id)sender;
@end
