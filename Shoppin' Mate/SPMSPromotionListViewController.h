//
//  SPMSPromotionListViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu.h>
#import <UIScrollView+EmptyDataSet.h>

@interface SPMSPromotionListViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       DZNEmptyDataSetSource,
                       DZNEmptyDataSetDelegate>

@property(strong, nonatomic) IBOutlet UITableView* tableView;

@end
