//
//  SPMSSidebarViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSSidebarViewController.h"

#import "SPMSLoginViewController.h"

@interface SPMSSidebarViewController ()

@end

@implementation SPMSSidebarViewController

- (void)awakeFromNib {
  self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
  self.contentViewShadowColor = [UIColor blackColor];
  self.contentViewShadowOffset = CGSizeMake(0, 0);
  self.contentViewShadowOpacity = 0.6;
  self.contentViewShadowRadius = 12;
  self.contentViewShadowEnabled = YES;
  self.contentViewScaleValue = 0.5f;

  self.contentViewController = [self.storyboard
      instantiateViewControllerWithIdentifier:@"navigationViewController"];
  self.leftMenuViewController = [self.storyboard
      instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
  self.backgroundImage = [UIImage imageNamed:@"SidebarNav_BG"];
  self.delegate = self;
}

#pragma mark - Life Cycle
- (void)viewDidAppear:(BOOL)animated {
}

#pragma mark - User Authentication

#pragma mark - RESideMenu Delegate

- (void)sideMenu:(RESideMenu*)sideMenu
    willShowMenuViewController:(UIViewController*)menuViewController {
}

- (void)sideMenu:(RESideMenu*)sideMenu
    didShowMenuViewController:(UIViewController*)menuViewController {
}

- (void)sideMenu:(RESideMenu*)sideMenu
    willHideMenuViewController:(UIViewController*)menuViewController {
}

- (void)sideMenu:(RESideMenu*)sideMenu
    didHideMenuViewController:(UIViewController*)menuViewController {
}

@end
