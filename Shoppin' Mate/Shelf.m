//
//  Shelf.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Shelf.h"
#import "Map.h"


@implementation Shelf

@dynamic furnitureId;
@dynamic height;
@dynamic pointId;
@dynamic width;
@dynamic xCoordinate;
@dynamic yCoordinate;
@dynamic name;
@dynamic map;

@end
