//
//  SPMSFloorListViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/7/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSFloorListViewController.h"
#import <SVProgressHUD.h>
#import "SPMSCategoryListTableViewCell.h"
#import <ObjectiveSugar.h>
#import "Map.h"
#import "NSDictionary+Verified.h"

@interface SPMSFloorListViewController ()

@property(strong, nonatomic) NSMutableArray* floorList;

@end

@implementation SPMSFloorListViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.floorList = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(setselectedMapIdsInfo:)
               name:kSendSelectedFloorFromFilterNotification
             object:nil];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetDelegate = self;
  self.tableView.emptyDataSetSource = self;

  // Config Sidebar
  [self configNavigationBar];
  // Config TableView layout
  [self configTableViewLayout];

  // Reload Data
  [self loadFloors];
}

#pragma mark - Load Data
- (void)loadFloors {
  NSArray* maps = [Map MR_findAll];
  if (maps) {
    [self.floorList addObjectsFromArray:maps];
  }
}

- (void)setselectedMapIdsInfo:(NSNotification*)notification {
  NSMutableArray* selectedMapIdArray =
      [[notification userInfo] verifiedObjectForKey:@"selectedMapIds"];
  if (selectedMapIdArray && (self.selectedMapIds != selectedMapIdArray)) {
    self.selectedMapIds = selectedMapIdArray;
    [self.tableView reloadData];
  }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return [self.floorList count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"floorCell";
  SPMSCategoryListTableViewCell* cell = (SPMSCategoryListTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (void)configureCell:(SPMSCategoryListTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  Map* map = (Map*)[self.floorList objectAtIndex:indexPath.row];

  cell.accessoryType = UITableViewCellAccessoryNone;
  if (0 < [self.selectedMapIds count]) {
    [self.selectedMapIds each:^(NSString* selectedMapId) {
        if ([map.mapId isEqualToString:selectedMapId]) {
          cell.accessoryType = UITableViewCellAccessoryCheckmark;
          return;
        }
    }];
  }
  cell.categoryNameLabel.text =
      [NSString stringWithFormat:@"Floor #%@", map.mapId];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  SPMSCategoryListTableViewCell* cell = (SPMSCategoryListTableViewCell*)
      [tableView cellForRowAtIndexPath:indexPath];
  Map* map = (Map*)[self.floorList objectAtIndex:indexPath.row];

  [[NSNotificationCenter defaultCenter]
      postNotificationName:kSelectFloorInFloorListVCNotification
                    object:nil
                  userInfo:[[NSDictionary alloc]
                               initWithObjectsAndKeys:map.mapId,
                                                      @"selectedMapId",
                                                      nil]];

  if (cell.accessoryType == UITableViewCellAccessoryNone) {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 60.0f;
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No Floor";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"There is no floor";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"BrowseProduct_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView*)scrollView {
}

- (void)emptyDataSetDidTapButton:(UIScrollView*)scrollView {
}

#pragma mark - Actions
- (void)dimissView:(id)sender {
  [self.presentingViewController dismissViewControllerAnimated:YES
                                                    completion:nil];
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
  [self.tableView setBackgroundColor:[UIColor whiteColor]];
  // A little trick for removing the cell separators
  self.tableView.tableFooterView = [UIView new];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
  // "Cancel" Button
  UIButton* cancelBTN = [UIButton buttonWithType:UIButtonTypeSystem];
  cancelBTN.frame = CGRectMake(0, 0, 50, 30);
  [cancelBTN setTitle:@"Done" forState:UIControlStateNormal];
  [cancelBTN addTarget:self
                action:@selector(dimissView:)
      forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* cancelBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:cancelBTN];

  [self.navigationItem setLeftBarButtonItem:cancelBTNItem];
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kSendSelectedFloorFromFilterNotification
              object:nil];
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
}

@end
