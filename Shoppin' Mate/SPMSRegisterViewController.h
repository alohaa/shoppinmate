//
//  SPMSRegisterViewController.h
//  Shoppin' Mate
//
//  Created by Hiro on 7/16/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMSRegisterViewController : UIViewController<UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet UIButton* registerButton;
@property(strong, nonatomic) IBOutlet UITextField* emailTextField;
@property(strong, nonatomic) IBOutlet UITextField* passwordTextField;
@property(strong, nonatomic) IBOutlet UITextField* fullnameTextField;
@property(strong, nonatomic) IBOutlet UIImageView* profileImage;
@property(strong, nonatomic)
    IBOutlet UIImageView* emailValidateStatusIndicatorImageView;
@property(strong, nonatomic)
    IBOutlet UIImageView* passwordValidateStatusIndicatorImageView;
@property(strong, nonatomic)
    IBOutlet UIImageView* fullNameValidateStatusIndicatorImageView;
- (IBAction)registerButtonTapped:(UIButton*)sender;
- (IBAction)backgroundTapped:(id)sender;
- (IBAction)imageTappedAction:(UIButton*)sender;
- (IBAction)backToLoginViewAction:(id)sender;

@end
