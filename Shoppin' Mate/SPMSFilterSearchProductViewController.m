//
//  SPMSFilterSearchProductViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSFilterSearchProductViewController.h"
#import "SPMSCategoryButton.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <UIViewController+KNSemiModal.h>
#import "SPMSWebDataService.h"
#import "SPMSCurrencyFormatter.h"
#import "CategoryType.h"
#import "Map.h"
#import "SPMSCategoryListViewController.h"
#import "NSDictionary+Verified.h"

#define MAX_HEIGHT_FOR_CATEGORY_SECTION 180
#define MAX_HEIGHT_FOR_FLOOR_SECTION 244
#define X_COORDINATE_GAP 10.0f
#define Y_COORDINATE_GAP 40.0f

@interface SPMSFilterSearchProductViewController ()

@property(strong, nonatomic) NSArray* categories;
@property(strong, nonatomic) NSString* selectedCategoryName;
@property(strong, nonatomic) NSString* selectedMapId;

@end

@implementation SPMSFilterSearchProductViewController
#pragma mark - Initialization
- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    self.selectedCategories = [[NSMutableSet alloc] init];
    self.selectedMapIds = [[NSMutableSet alloc] init];
    self.maxPrice = 0.0f;
    self.seletedCategoryCount = 0;
    self.seletedFloorCount = 0;
    self.isHasPromotion = NO;
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(setSelectedCategoryInfo:)
               name:kSelectCategoryInCategoryListVCNotification
             object:nil];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(setSelectedMapIdInfo:)
               name:kSelectFloorInFloorListVCNotification
             object:nil];
  }
  return self;
}

- (id)init {
  self = [self initWithNibName:@"SPMSFilterSearchProductView" bundle:nil];
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self getListOfCategory];
  [self getListOfFloor];
  [self configurePriceRangeSlider];
  [self addPromotionButtonGroup];
  [self configureButtons];
  [self configNavigationBar];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  self.isAppliedFilter = NO;
}

#pragma mark - Load Data
- (void)getListOfCategory {
  [[[[SPMSWebDataService sharedService] fetchCategoryTypeListWithPageNumber:1]
      deliverOn:[RACScheduler mainThreadScheduler]]
      subscribeNext:^(NSArray* results) {
          self.categories = [[NSArray alloc] initWithArray:results];
          [self addCategoryButtonGroupWithDataArray:self.categories];
      }];
}

- (void)getListOfFloor {
  NSArray* floorList = [Map MR_findAll];
  if (floorList) {
    [self addFloorButtonGroupWithDataArray:floorList];
  }
}

- (void)setSelectedCategoryInfo:(NSNotification*)notification {
  NSString* selectedCategoryName =
      [[notification userInfo] verifiedObjectForKey:@"selectedCategory"];
  if (selectedCategoryName) {
    self.selectedCategoryName = selectedCategoryName;
  }
}

- (void)setSelectedMapIdInfo:(NSNotification*)notification {
  NSString* selectedMapId =
      [[notification userInfo] verifiedObjectForKey:@"selectedMapId"];
  if (selectedMapId) {
    self.selectedMapId = selectedMapId;
  }
}

#pragma mark - Add Views
- (void)addCategoryButtonGroupWithDataArray:(NSArray*)categories {
  CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
  CGFloat buttonXCoordinate = 20;
  CGFloat buttonYCoordinate = 36;

  for (int i = 0; i < [categories count]; i++) {
    if (buttonYCoordinate <=
        MAX_HEIGHT_FOR_CATEGORY_SECTION - Y_COORDINATE_GAP) {
      CategoryType* category = [categories objectAtIndex:i];
      NSString* categoryString = category.categoryName;
      SPMSCategoryButton* button = [[SPMSCategoryButton alloc]
          initWithTitleString:categoryString
                         font:[UIFont
                                  fontWithName:@"HelveticaNeue-CondensedBold"
                                          size:16]
                            x:buttonXCoordinate
                            y:buttonYCoordinate];
      // Configure Button
      [[button rac_signalForControlEvents:UIControlEventTouchUpInside]
          subscribeNext:^(id x) {
              BOOL isButtonSelected = [button isSelected];
              [self
                  updateSelectedCategoryListWithCategoryName:categoryString
                                            isButtonSelected:isButtonSelected];
          }];

      [[RACObserve(self, selectedCategoryName)
          filter:^BOOL(NSString* categoryName) {
              return (categoryName != nil);
          }] subscribeNext:^(NSString* catName) {
          if ([button.titleLabel.text isEqualToString:catName]) {
            BOOL currentBTNState = button.isSelected;
            [button setSelected:!currentBTNState];
            [self updateSelectedCategoryListWithCategoryName:catName
                                            isButtonSelected:!currentBTNState];
          } else {
            [self updateSelectedCategoryListWithCategoryName:catName
                                            isButtonSelected:YES];
          }
      }];

      [self.view addSubview:button];

      int idx = i + 1;
      if (idx < [categories count]) {
        CategoryType* aCategory = [categories objectAtIndex:idx];
        NSString* string = aCategory.categoryName;
        CGFloat newX = buttonXCoordinate + button.frame.size.width +
                       [self getButtonWidthWithString:string] +
                       X_COORDINATE_GAP;

        if (screenWidth - 20 <= newX) {
          buttonXCoordinate = 20;
          buttonYCoordinate += Y_COORDINATE_GAP;
        } else {
          buttonXCoordinate =
              buttonXCoordinate + button.frame.size.width + X_COORDINATE_GAP;
        }
      }
    }
  }
}

- (void)addFloorButtonGroupWithDataArray:(NSArray*)floors {
  CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
  CGFloat buttonXCoordinate = 20;
  CGFloat buttonYCoordinate = MAX_HEIGHT_FOR_CATEGORY_SECTION + 24.0f;

  for (int i = 0; i < [floors count]; i++) {
    if (buttonYCoordinate <= MAX_HEIGHT_FOR_FLOOR_SECTION - Y_COORDINATE_GAP) {
      Map* map = [floors objectAtIndex:i];
      NSString* mapId = map.mapId;
      SPMSCategoryButton* button = [[SPMSCategoryButton alloc]
          initWithTitleString:mapId
                         font:[UIFont
                                  fontWithName:@"HelveticaNeue-CondensedBold"
                                          size:16]
                            x:buttonXCoordinate
                            y:buttonYCoordinate];
      if (0 < [self.selectedMapIds count]) {
        [self.selectedMapIds each:^(NSString* mapId) {
            if ([map.mapId isEqualToString:mapId]) {
              [button setSelected:YES];
              return;
            }
        }];
      }

      // Configure Button
      [[button rac_signalForControlEvents:UIControlEventTouchUpInside]
          subscribeNext:^(id x) {
              BOOL isButtonSelected = [button isSelected];
              [self updateSelectedFloorListWithMapId:mapId
                                    isButtonSelected:isButtonSelected];
          }];

      [[RACObserve(self, selectedMapId) filter:^BOOL(NSString* mapId) {
          return (mapId != nil);
      }] subscribeNext:^(NSString* mapId) {
          if ([button.titleLabel.text isEqualToString:mapId]) {
            BOOL currentBTNState = button.isSelected;
            [button setSelected:!currentBTNState];
            [self updateSelectedFloorListWithMapId:mapId
                                  isButtonSelected:!currentBTNState];
          } else {
            [self updateSelectedFloorListWithMapId:mapId isButtonSelected:YES];
          }
      }];

      [self.view addSubview:button];

      int idx = i + 1;
      if (idx < [floors count]) {
        Map* aMap = [floors objectAtIndex:idx];
        NSString* string = aMap.mapId;
        CGFloat newX = buttonXCoordinate + button.frame.size.width +
                       [self getButtonWidthWithString:string] +
                       X_COORDINATE_GAP;

        if (screenWidth - 20 <= newX) {
          buttonXCoordinate = 20;
          buttonYCoordinate += Y_COORDINATE_GAP;
        } else {
          buttonXCoordinate =
              buttonXCoordinate + button.frame.size.width + X_COORDINATE_GAP;
        }
      }
    }
  }
}

- (void)configurePriceRangeSlider {
  self.priceRangeSlider.dataSource = self;
  NSNumberFormatter* currencyFormatter = [[NSNumberFormatter alloc] init];
  [currencyFormatter setPositiveSuffix:@"k VND"];
  [currencyFormatter setNegativeSuffix:@"k VND"];

  [self.priceRangeSlider setNumberFormatter:currencyFormatter];
  self.priceRangeSlider.minimumValue = 0.0;
  self.priceRangeSlider.maximumValue = MAX_PRODUCT_PRICE_RANGE;
  self.priceRangeSlider.popUpViewCornerRadius = 16.0;
  self.priceRangeSlider.font =
      [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:16];
  self.priceRangeSlider.textColor = [UIColor colorWithWhite:0.0 alpha:0.5];

  UIColor* coldBlue =
      [UIColor colorWithHue:0.6 saturation:0.7 brightness:1.0 alpha:1.0];
  UIColor* blue =
      [UIColor colorWithHue:0.55 saturation:0.75 brightness:1.0 alpha:1.0];
  UIColor* green =
      [UIColor colorWithHue:0.3 saturation:0.65 brightness:0.8 alpha:1.0];
  UIColor* yellow =
      [UIColor colorWithHue:0.15 saturation:0.9 brightness:0.9 alpha:1.0];
  UIColor* red =
      [UIColor colorWithHue:0.0 saturation:0.8 brightness:1.0 alpha:1.0];

  [self.priceRangeSlider
      setPopUpViewAnimatedColors:@[ coldBlue, blue, green, yellow, red ]
                   withPositions:@[ @0, @100, @500, @2000, @4000 ]];
  [[self.priceRangeSlider rac_signalForControlEvents:UIControlEventValueChanged]
      subscribeNext:^(id x) {
          // Change maxPrice Value
          self.maxPrice = [self.priceRangeSlider value];
          NSNumberFormatter* currencyFormatter =
              [SPMSCurrencyFormatter formatter];
          NSString* priceRangeLabelString = [currencyFormatter
              stringFromNumber:[NSNumber
                                   numberWithFloat:(self.maxPrice * 1000)]];
          if (self.maxPrice < MAX_PRODUCT_PRICE_RANGE) {
            // Change PriceRangeLabel Text
            [self.priceRangeLabel
                setText:[NSString stringWithFormat:@"PRICE RANGE (0 VND - %@)",
                                                   priceRangeLabelString]];
          } else {
            // Change PriceRangeLabel Text
            [self.priceRangeLabel
                setText:[NSString stringWithFormat:@"PRICE RANGE (>= %@)",
                                                   priceRangeLabelString]];
          }
      }];
}

- (void)addPromotionButtonGroup {
  SPMSCategoryButton* button = [[SPMSCategoryButton alloc]
      initWithTitleString:@"Discount"
                     font:[UIFont fontWithName:@"HelveticaNeue-CondensedBold"
                                          size:16]
                        x:20
                        y:354];
  // Configure Button
  [[button rac_signalForControlEvents:UIControlEventTouchUpInside]
      subscribeNext:^(id x) {
          BOOL isButtonSelected = [button isSelected];
          if (isButtonSelected) {
            self.isHasPromotion = YES;
          } else {
            self.isHasPromotion = NO;
          }
      }];
  [self.view addSubview:button];
}

- (void)updateSelectedCategoryListWithCategoryName:(NSString*)categoryName
                                  isButtonSelected:(BOOL)isButtonSelected {
  if (isButtonSelected &&
      !([self.selectedCategories containsObject:categoryName])) {
    [self.selectedCategories addObject:categoryName];
  } else if ([self.selectedCategories containsObject:categoryName]) {
    [self.selectedCategories removeObject:categoryName];
  }
  self.seletedCategoryCount = [self.selectedCategories count];
}

- (void)updateSelectedFloorListWithMapId:(NSString*)mapId
                        isButtonSelected:(BOOL)isButtonSelected {
  if (isButtonSelected && !([self.selectedMapIds containsObject:mapId])) {
    [self.selectedMapIds addObject:mapId];
  } else if ([self.selectedMapIds containsObject:mapId]) {
    [self.selectedMapIds removeObject:mapId];
  }
  self.seletedFloorCount = [self.selectedMapIds count];
}

#pragma mark - Button
- (void)configureButtons {
  // "Aplly Filter" Button
  [[self.applyFiltersButton
      rac_signalForControlEvents:UIControlEventTouchUpInside]
      subscribeNext:^(id x) {
          [self dismissSemiModalView];
          self.isAppliedFilter = YES;
      }];
}

#pragma mark - ASValueTrackingSliderDataSource
- (NSString*)slider:(ASValueTrackingSlider*)slider stringForValue:(float)value;
{
  value = roundf(value);
  NSString* s;
  if (value < 100.0) {
    s = @"❄️Brrr!⛄️";
  } else if (000.0 < value && value < 2000.0) {
    s = [NSString
        stringWithFormat:@"😎 %@ 😎",
                         [slider.numberFormatter stringFromNumber:@(value)]];
  } else if (8000.0 <= value) {
    s = @"YOLO!";
  }
  return s;
}

#pragma mark - Actions

- (IBAction)viewMoreCategoryBTNTapped:(id)sender {
  [self showCategoryListViewWithAnimated:YES];
}

- (IBAction)viewMoreFloorBTNTapped:(id)sender {
  [self showFloorListViewWithAnimated:YES];
}

- (void)showCategoryListViewWithAnimated:(BOOL)animated {
  UIStoryboard* storyboard =
      [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  UIViewController* navListViewController = [storyboard
      instantiateViewControllerWithIdentifier:@"categoryListNavViewController"];

  [navListViewController
      setModalPresentationStyle:UIModalPresentationFullScreen];
  [self presentViewController:navListViewController
                     animated:animated
                   completion:^{
                       [[NSNotificationCenter defaultCenter]
                           postNotificationName:
                               kSendSelectedCategoryFromFilterVCNotification
                                         object:nil
                                       userInfo:[[NSDictionary alloc]
                                                    initWithObjectsAndKeys:
                                                        self.selectedCategories,
                                                        @"selectedCategories",
                                                        nil]];
                   }];
}

- (void)showFloorListViewWithAnimated:(BOOL)animated {
  UIStoryboard* storyboard =
      [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  UIViewController* navListViewController = [storyboard
      instantiateViewControllerWithIdentifier:@"floorListNavViewController"];

  [navListViewController
      setModalPresentationStyle:UIModalPresentationFullScreen];
  [self presentViewController:navListViewController
                     animated:animated
                   completion:^{
                       [[NSNotificationCenter defaultCenter]
                           postNotificationName:
                               kSendSelectedFloorFromFilterNotification
                                         object:nil
                                       userInfo:[[NSDictionary alloc]
                                                    initWithObjectsAndKeys:
                                                        self.selectedMapIds,
                                                        @"selectedMapIds",
                                                        nil]];
                   }];
}

#pragma mark - View Helpers
- (CGFloat)getButtonWidthWithString:(NSString*)string {
  NSDictionary* attributes = @{
    NSFontAttributeName :
        [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:16]
        };
  CGSize stringSize = [string sizeWithAttributes:attributes];
  return stringSize.width + 20;
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kSelectCategoryInCategoryListVCNotification
              object:nil];
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kSelectFloorInFloorListVCNotification
              object:nil];
}

@end
