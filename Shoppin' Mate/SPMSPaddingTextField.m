//
//  SPMSPaddingTextField.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/22/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSPaddingTextField.h"

@implementation SPMSPaddingTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds;
{
    return CGRectInset([super textRectForBounds:bounds], 20.f, 0.f);
}

- (CGRect)editingRectForBounds:(CGRect)bounds;
{
    return CGRectInset([super editingRectForBounds:bounds], 20.f, 0.f);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
