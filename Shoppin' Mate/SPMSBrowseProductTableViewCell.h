//
//  HomeFeedTableViewCell.h
//  Ohyeap
//
//  Created by El Desperado on 7/15/13.
//  Copyright (c) 2013 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface SPMSBrowseProductTableViewCell : UITableViewCell

@property(assign, nonatomic) Product* product;

@property(strong, nonatomic) IBOutlet UIImageView* productImageView;
@property(strong, nonatomic) IBOutlet UILabel* productNameLabel;
@property(strong, nonatomic) IBOutlet UILabel* productDescLabel;
@property(strong, nonatomic) IBOutlet UILabel* productPriceLabel;

@end
