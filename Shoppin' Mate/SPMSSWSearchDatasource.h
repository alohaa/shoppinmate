//
//  SPMSSWSearchDatasource.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPMSScoringMatrix.h"

@protocol SPMSSWSearchDatasource <NSObject>

- (void)rebuildDatabase;

@optional
- (NSInteger)customCostForEvent:(ScoringEvent)event;

@end
