//
//  CategoryType.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "CategoryType.h"
#import <RPJSONValidator.h>
#import <RPValidatorPredicate.h>
#import "NSDictionary+Verified.h"

@implementation CategoryType

+ (CategoryType*)categoryTypeForDictionary:(NSDictionary*)dictionary {
  if (![self isValidJSONWithDictionary:dictionary]) {
    return nil;
  }
  CategoryType* aCategory = [[CategoryType alloc] init];
  aCategory.categoryID = [dictionary verifiedObjectForKey:@"CategoryId"];
  aCategory.categoryName = [dictionary verifiedObjectForKey:@"CategoryName"];
  return aCategory;
}

+ (BOOL)isValidJSONWithDictionary:(NSDictionary*)dictionary {
  NSError* error;
  [RPJSONValidator
      validateValuesFrom:dictionary
        withRequirements:@{
          @"CategoryId" : [RPValidatorPredicate.isNumber.isNotNull
              valueIsGreaterThanOrEqualTo:[NSNumber numberWithInt:0]],
          @"CategoryName" : RPValidatorPredicate.isOptional.isString
        } error:&error];
  if (error) {
    return NO;
  }
  return YES;
}

@end
