//
//  Promotion.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Promotion.h"
#import "Product.h"
#import "NSDictionary+Verified.h"
#import <RPJSONValidator.h>
#import <RPValidatorPredicate.h>

@implementation Promotion

+ (Promotion*)promotionForDictionary:(NSDictionary*)dictionary {
  if (![self isValidJSONWithDictionary:dictionary]) {
    return nil;
  }
  Promotion* aPromotion = [[Promotion alloc] init];
  aPromotion.content = [dictionary verifiedObjectForKey:@"Content"];
  NSDateFormatter* dateFormatter = [self dateFormatter];
  aPromotion.createdDate = [dateFormatter
      dateFromString:[dictionary verifiedObjectForKey:@"CreatedDate"]];
  aPromotion.endDate = [dateFormatter
      dateFromString:[dictionary verifiedObjectForKey:@"EndDate"]];
  aPromotion.startDate = [dateFormatter
      dateFromString:[dictionary verifiedObjectForKey:@"StartDate"]];
  aPromotion.promotionDescription =
      [dictionary verifiedObjectForKey:@"Description"];
  aPromotion.promotionID = [dictionary verifiedObjectForKey:@"PromotionId"];
  aPromotion.promotionName = [dictionary verifiedObjectForKey:@"PromotionName"];
  aPromotion.title = [dictionary verifiedObjectForKey:@"Title"];
  aPromotion.products = [Product
      productsForDictionary:[dictionary verifiedObjectForKey:@"Products"]];
  return aPromotion;
}

+ (BOOL)isValidJSONWithDictionary:(NSDictionary*)dictionary {
  NSError* error;
  [RPJSONValidator
      validateValuesFrom:dictionary
        withRequirements:@{
          @"Content" : RPValidatorPredicate.isString.isOptional,
          @"CreatedDate" : RPValidatorPredicate.isNotNull,
          @"Description" : RPValidatorPredicate.isString.isOptional,
          @"PromotionId" : [RPValidatorPredicate.isNotNull.isNumber
              valueIsGreaterThanOrEqualTo:[NSNumber numberWithInt:0]],
          @"PromotionName" : RPValidatorPredicate.isOptional.isString,
          @"Title" : RPValidatorPredicate.isOptional.isString,
          @"Products" : RPValidatorPredicate.isArray
        } error:&error];
  if (error) {
    return NO;
  }
  return YES;
}

+ (NSDateFormatter*)dateFormatter {
  NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.locale =
      [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  dateFormatter.dateFormat = DATE_FORMAT;
  return dateFormatter;
}
@end
