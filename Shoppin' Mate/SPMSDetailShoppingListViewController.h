//
//  SPMSDetailShoppingListViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIScrollView+EmptyDataSet.h>
#import <MCSwipeTableViewCell.h>
#import "ShoppingList.h"

@interface SPMSDetailShoppingListViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       DZNEmptyDataSetDelegate,
                       DZNEmptyDataSetSource, MCSwipeTableViewCellDelegate>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(strong, nonatomic) NSMutableArray* productList;
@property(strong, nonatomic) NSNumber* shoppingListID;
@property (assign, nonatomic) ShoppingList *shoppingList;
@property(strong, nonatomic) NSString* shoppingListName;


@end
