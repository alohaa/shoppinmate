//
//  SMDPathFinder.m
//  ShopinMateClient
//
//  Created by Dat Truong on 7/5/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "SMDPathFinder.h"
#import "SMDBeacon.h"
#import "SPMSBeaconAnnotation.h"

#import "NSMutableArray+Queue.h"
#import "NSMutableArray+Stack.h"

#import "PESGraph.h"
#import "PESGraphNode.h"
#import "PESGraphEdge.h"
#import "PESGraphRoute.h"
#import "PESGraphRouteStep.h"
#import "PESGraph+Helpers.h"

@interface SMDPathFinder ()

@property PESGraph* graph;

@end

@implementation SMDPathFinder

- (instancetype)initWithGraphDict:(NSDictionary*)dict {
  self = [super init];
  if (self) {
    if ([[dict allKeys] count] > 1) {
        [self setGraphDict:dict];
    }
  }
  return self;
}

- (void)setGraphDict:(NSDictionary*)graphDict {
  _graphDict = graphDict;
  _graph = [self graphFromGraphDict:_graphDict];
}

#pragma mark - Path Finding

+ (float) lengthFrom:(CGPoint) a to:(CGPoint) b
{
    float length;
    
    length = sqrtf( powf((a.x - b.x), 2.0) +  powf((a.y - b.y), 2.0));
    
    return length;
}

- (NSArray*)nodesFromBeacons:(NSArray*)beacons {
  NSArray* nodes;

  NSMutableArray* processingNodes;
  processingNodes = [NSMutableArray array];
    
    NSArray *sortedBeacons;
    sortedBeacons = [[beacons mutableCopy] sortedArrayUsingComparator:^NSComparisonResult(SMDBeacon *beacon1, SMDBeacon *beacon2) {
        CGPoint point1, point2;
        float length1, length2;
        point1 = beacon1.annotation.point;
        point2 = beacon2.annotation.point;
        length1 = [SMDPathFinder lengthFrom:point1 to:CGPointZero];
        length2 = [SMDPathFinder lengthFrom:point2 to:CGPointZero];
        if (length1 < length2) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
        return NSOrderedSame;
    }];

  for (SMDBeacon* beacon in sortedBeacons) {
    NSString* alias;
    PESGraphNode* node;
    alias = beacon.alias;
    node = [self.graph nodeInGraphWithIdentifier:alias];
    if (node) {
      [processingNodes addObject:node];
    }
  }
  nodes = [processingNodes copy];

  return nodes;
}

// Array of Strings
- (NSArray*)pathFromSelectedBeacons:(NSArray*)beacons {
  NSArray* path;

  NSMutableArray* processingPath;
  processingPath = [NSMutableArray array];

  NSArray* route;
  route = [self routeFromSelectedBeacons:beacons];
  for (PESGraphNode* node in route) {
    NSString* alias;
    alias = node.identifier;
    [processingPath addObject:alias];
  }

  path = [processingPath copy];

  return path;
}

// Array of nodes
- (NSArray *)routeFromSelectedBeacons:(NSArray *)beacons {
    NSArray *route;

    NSMutableArray *processingRoute; // currently processing route
    NSMutableArray *selectedNodes;   // nodes from selected beacons
    processingRoute = [NSMutableArray array];
    selectedNodes =
        [NSMutableArray arrayWithArray:[self nodesFromBeacons:beacons]];

    if ([selectedNodes count] > 0) {
        PESGraphNode *currentNode;
        currentNode = [selectedNodes firstObject];

        // end when reach the final node
        while (selectedNodes.count > 1) {
            [selectedNodes removeObject:currentNode];
            
            // first node would be the min distance node
            float minDistance;
            minDistance = MAXFLOAT;
            PESGraphNode *minDistanceNode;
            minDistanceNode = selectedNodes[0];
            PESGraphRoute *minRoute;

            // compare all node in selected nodes to find min route
            for (PESGraphNode *nextNode in selectedNodes) {
                PESGraphRoute *route;
                route = [[self graph] shortestRouteFromNode:currentNode
                                                     toNode:nextNode];

                float tempDistance;
                tempDistance = [route length];

                if (tempDistance < minDistance) {
                    minDistance = tempDistance;
                    minDistanceNode = nextNode;
                    minRoute = route;
                }
            }

            // add every step to processing route
//            NSLog(@"From %@ to %@", currentNode.identifier, minDistanceNode.identifier);
            for (PESGraphRouteStep *step in minRoute.steps) {
                PESGraphNode *nodeToAdd;
                nodeToAdd = step.node;
                [processingRoute addObject:nodeToAdd];
//                NSLog(@"step %@", nodeToAdd.identifier);
            }
            
            // the min distance node now become current node
            currentNode = minDistanceNode;
        } // end while count selectedNote > 1
        
        route = [processingRoute copy];
    }

    return route;
}

#pragma mark - Graph Creation

- (PESGraphNode*)nodeWithAlias:(NSString*)alias {
  PESGraphNode* node;

  node = [self.graph nodeInGraphWithIdentifier:alias];
  if (!node) {
    node = [PESGraphNode nodeWithIdentifier:alias];
  }

  return node;
}

- (PESGraph*)graphFromGraphDict:(NSDictionary*)dict {
  PESGraph* graph;

  graph = [[PESGraph alloc] init];
  for (NSString* startNodeAlias in dict) {
    PESGraphNode* startNode;
    startNode = [self nodeWithAlias:startNodeAlias];

    NSDictionary* weightsDict;
    weightsDict = [dict objectForKey:startNodeAlias];
    if ([weightsDict isKindOfClass:[NSDictionary class]]) {
      if ([[weightsDict allKeys] count] > 0) {
        for (NSString* endNodeAlias in weightsDict) {
          PESGraphNode* endNode;
          endNode = [self nodeWithAlias:endNodeAlias];

          NSNumber* distance;
          distance = [weightsDict objectForKey:endNodeAlias];

          PESGraphEdge* edge;
          NSString* edgeName;
          edgeName = [NSString
              stringWithFormat:@"%@-%@", startNodeAlias, endNodeAlias];
          edge = [PESGraphEdge edgeWithName:edgeName andWeight:distance];
          [graph addBiDirectionalEdge:edge fromNode:startNode toNode:endNode];
        }
      }
    }
  }

  return graph;
}
@end
