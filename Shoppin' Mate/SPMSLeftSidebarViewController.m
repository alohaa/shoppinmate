//
//  SPMSLeftSidebarViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSLeftSidebarViewController.h"

#import <UIViewController+RESideMenu.h>
#import "SPMSLoginViewController.h"
#import "SPMSBrowseProductViewController.h"
#import "SPMSPromotionListViewController.h"
#import "SPMSUserProfileViewController.h"
#import "SPMSShoppingListsViewController.h"
#import "SPMSMapPagerViewController.h"
#import "SPMSLeftSidebarTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSFacebookService.h"
#import "User.h"
#import <UIImageView+WebCache.h>
#import "SPMSFacebookService.h"
#import "SPMSWebAccountService.h"
#import <MLPAccessoryBadge.h>
#import "SPMSResetPasswordViewController.h"
#import "SPMSRegisterViewController.h"
#import "SPMSUpdateProfileViewController.h"
#import "SPMSMapViewController.h"

@interface SPMSLeftSidebarViewController () {
  MLPAccessoryBadge* promotionBadgeView;
}
@end

@implementation SPMSLeftSidebarViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self reloadUserProfileView:nil];
  [self configTableViewLayout];
  [self configPushToUserProfile];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(reloadUserProfileView:)
             name:kUpdatedUserProfileNotification
           object:nil];

  // Check whether the user is logged-in or not to display corresponding view
  RACSignal* loggedInSignal = [[NSUserDefaults standardUserDefaults]
      rac_channelTerminalForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];

  [loggedInSignal
      subscribeNext:^(id value) { [self reloadUserProfileView:nil]; }];

  // Check whether the user is logged-in or not to display corresponding view
  RACSignal* promotionCountSignal = [[NSUserDefaults standardUserDefaults]
      rac_channelTerminalForKey:NSUSERDEFAULTS_PROMOTION_COUNT_KEY];
  [promotionCountSignal subscribeNext:^(NSNumber* number) {
      if (!number || [number intValue] == 0) {
        [promotionBadgeView setHidden:YES];
      } else {
        [promotionBadgeView setHidden:NO];
        [promotionBadgeView setText:[NSString stringWithFormat:@"%@", number]];
      }
  }];
}

- (void)logout {
  if ([[SPMSFacebookService sharedFBServiceInstance] isLoggedIn]) {
    // Logout from FB Account
    [[[[SPMSFacebookService sharedFBServiceInstance] logoutFB]
        deliverOn:RACScheduler.mainThreadScheduler] subscribeCompleted:^{//
                                                                       }];
  } else {
    // Logout from Email Account
    [[SPMSWebAccountService sharedService] logout];
  }
}

#pragma mark - Reload Profile View
- (void)reloadUserProfileView:(id)sender {
  // Check for login
  BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults]
      boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  if (!isUserLoggedIn) {
    // Hide User Avatar and Info, display "Ask to Login" Image
    [self configAskToLoginImageView];
  } else {
    [self configUserAvatarImageView];
  }
  [self.tableView reloadData];
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  switch (indexPath.row) {
    case 0: {
      // Navigate to Browse Product Screen
      if ([self presentingViewController] !=
          [SPMSBrowseProductViewController class]) {
        [self.sideMenuViewController
            setContentViewController:
                [[UINavigationController alloc]
                    initWithRootViewController:
                        [self.storyboard
                            instantiateViewControllerWithIdentifier:
                                @"browseProductController"]]
                            animated:YES];
      }
      [self.sideMenuViewController hideMenuViewController];
      break;
    }
    case 1: {
      // Navigate to Browse Product Screen
      if ([self presentingViewController] !=
          [SPMSPromotionListViewController class]) {
        [self.sideMenuViewController
            setContentViewController:
                [[UINavigationController alloc]
                    initWithRootViewController:
                        [self.storyboard
                            instantiateViewControllerWithIdentifier:
                                @"promotionViewController"]]
                            animated:YES];
      }
      [self.sideMenuViewController hideMenuViewController];
      break;
    }
    case 2: {
      // Navigate to Browse Product Screen
      if ([self presentingViewController] !=
          [SPMSShoppingListsViewController class]) {
        [self.sideMenuViewController
            setContentViewController:
                [[UINavigationController alloc]
                    initWithRootViewController:
                        [self.storyboard
                            instantiateViewControllerWithIdentifier:
                                @"shoppingListsViewController"]]
                            animated:YES];
      }
      [self.sideMenuViewController hideMenuViewController];
      break;
    }

    case 3: {
      // Navigate to Browse Product Screen
      if ([self presentingViewController] !=
          [SPMSMapPagerViewController class]) {
        [self.sideMenuViewController
            setContentViewController:
                [[UINavigationController alloc]
                    initWithRootViewController:
                        [self.storyboard
                            instantiateViewControllerWithIdentifier:
                                @"mapPagerViewController"]]
                            animated:YES];
      }
      [self.sideMenuViewController hideMenuViewController];
      break;
    }
    case 4: {
      // Logout
      [self logout];
      break;
    }
    default:
      break;
  }
}

#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)sectionIndex {
  NSInteger numberOfRows = 5;

  BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults]
      boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  if (!isUserLoggedIn) {
    numberOfRows = 4;
  }
  return numberOfRows;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  static NSString* cellIdentifier = @"SidebarCell";
  SPMSLeftSidebarTableViewCell* cell = (SPMSLeftSidebarTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

  if (cell == nil) {
    cell = [[SPMSLeftSidebarTableViewCell alloc]
          initWithStyle:UITableViewCellStyleDefault
        reuseIdentifier:cellIdentifier];
  }

  NSArray* titles = @[
    @"Browse Product",
    @"Promotion",
    @"Shopping List",
    @"Map",
    @"Log Out"
  ];
  NSArray* images = @[
    @"LeftSidebarNav_BrowseProduct_ICO",
    @"LeftSidebarNav_Promotion_ICO",
    @"LeftSidebarNav_ShoppingList_ICO",
    @"LeftSidebarNav_Map_ICO",
    @"IconEmpty"
  ];
  cell.menuTitleLabel.text = titles[indexPath.row];
  cell.menuIconImageView.image = [UIImage imageNamed:images[indexPath.row]];

  if (indexPath.row == 1) {
    promotionBadgeView = [MLPAccessoryBadge new];
    [promotionBadgeView setBackgroundColor:[UIColor clearColor]];
    [promotionBadgeView setStrokeColor:[UIColor whiteColor]];
    [promotionBadgeView setStrokeWidth:2.0f];
    [promotionBadgeView setShadowAlpha:0];
    [promotionBadgeView setGradientAlpha:0];
    [cell.badgeView addSubview:promotionBadgeView];
    [cell.badgeView sizeToFit];
  }
  return cell;
}

- (void)tableView:(UITableView*)tableView
      willDisplayCell:(UITableViewCell*)cell
    forRowAtIndexPath:(NSIndexPath*)indexPath {
  cell.backgroundColor = [UIColor clearColor];
  cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:21];
  cell.textLabel.textColor = [UIColor whiteColor];
  cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
  cell.selectedBackgroundView = [[UIView alloc] init];
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
  [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
  self.tableView.backgroundColor = [UIColor clearColor];
  self.tableView.opaque = NO;
  self.tableView.backgroundView = nil;
  self.tableView.bounces = NO;
  self.tableView.scrollsToTop = NO;
}

#pragma mark - Layout
- (void)configUserAvatarImageView {
  self.userAvatarImageView.hidden = NO;
  self.userFullNameLabel.hidden = NO;
  self.viewAndEditProfileLabel.hidden = NO;
  self.askToLoginImageView.hidden = YES;

  // Create Circular User Avatar Image View
  self.userAvatarImageView.layer.cornerRadius =
      self.userAvatarImageView.frame.size.width / 2;
  self.userAvatarImageView.clipsToBounds = YES;

  // Add Border
  self.userAvatarImageView.layer.borderWidth = 3.0f;
  self.userAvatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;

  // Set Data
  if ([User MR_findFirst] != nil) {
    User* user = [User MR_findFirst];
    self.userFullNameLabel.text = user.fullName;
    [self.userAvatarImageView
        sd_setImageWithURL:[NSURL URLWithString:user.imageURL]
          placeholderImage:[UIImage imageNamed:@"UserDefault"]
                   options:SDWebImageRefreshCached];
  }
}

- (void)configAskToLoginImageView {
  self.userAvatarImageView.hidden = YES;
  self.userFullNameLabel.hidden = YES;
  self.viewAndEditProfileLabel.hidden = YES;
  self.askToLoginImageView.hidden = NO;

  [self.askToLoginImageView
      setImage:[UIImage imageNamed:@"LeftSidebarNav_AskToLogin_IMG"]];
  self.askToLoginImageView.userInteractionEnabled = YES;
  UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]
      initWithTarget:self
              action:@selector(showLoginViewAnimated:)];
  [self.askToLoginImageView addGestureRecognizer:tapGesture];
}

- (void)configPushToUserProfile {
  self.userAvatarImageView.userInteractionEnabled = YES;
  self.viewAndEditProfileLabel.userInteractionEnabled = YES;
  self.userProfileView.userInteractionEnabled = YES;
  UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc]
      initWithTarget:self
              action:@selector(showUserProfileView:)];
  [self.userAvatarImageView addGestureRecognizer:gesture];
  [self.viewAndEditProfileLabel addGestureRecognizer:gesture];
  [self.userProfileView addGestureRecognizer:gesture];
}

#pragma mark - Push View
- (void)showLoginViewAnimated:(BOOL)animated {
  UIStoryboard* storyboard =
      [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  UIViewController* loginViewController =
      [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
  [loginViewController setModalPresentationStyle:UIModalPresentationFullScreen];
  [self presentViewController:loginViewController
                     animated:animated
                   completion:^{
                       [self.sideMenuViewController hideMenuViewController];
                   }];
}

- (void)showUserProfileView:(id)sender {
  if ([self presentingViewController] !=
      [SPMSUserProfileViewController class]) {
    [self.sideMenuViewController
        setContentViewController:
            [[UINavigationController alloc]
                initWithRootViewController:
                    [self.storyboard instantiateViewControllerWithIdentifier:
                                         @"userProfileViewController"]]
                        animated:YES];
  }
  [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - Other
- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kUpdatedUserProfileNotification
              object:nil];
}
@end
