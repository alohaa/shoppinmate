//
//  SPMSCurrencyFormatter.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/20/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPMSCurrencyFormatter : NSObject

+ (NSNumberFormatter*)formatter;

@end
