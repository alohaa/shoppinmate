//
//  SPMSAddProductToListViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/22/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPMSPaddingTextField.h"
#import "Product.h"
#import <UIScrollView+EmptyDataSet.h>

@interface SPMSAddProductToListViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       UITextFieldDelegate,
                       DZNEmptyDataSetDelegate,
                       DZNEmptyDataSetSource>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(strong, nonatomic) Product* product;

@end
