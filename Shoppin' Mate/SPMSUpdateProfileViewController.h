//
//  SPMSUpdateProfileViewController.h
//  Shoppin' Mate
//
//  Created by Hiro on 7/21/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface SPMSUpdateProfileViewController : UIViewController

@property(strong, nonatomic) NSArray* gender;
@property(strong, nonatomic) IBOutlet UITextField* fullNameTextField;
@property(strong, nonatomic) IBOutlet UIImageView* imageProfile;

@property(strong, nonatomic) IBOutlet UITextField* dateOfBirthTextField;
@property(strong, nonatomic) IBOutlet UITextField* genderTextField;

- (IBAction)updateButtonPressed:(id)sender;
- (IBAction)imageTappedAction:(id)sender;

@property(assign, nonatomic) NSString* userFullname;
@property(assign, nonatomic) NSString* userDOB;
@property(assign, nonatomic) NSString* userGender;
@property(assign, nonatomic) UIImage* userAvatarImage;

@end
