//
//  SPMSCategoryListTableViewCell.h
//  Shoppin' Mate
//
//  Created by El Desperado on 8/6/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryType.h"

@interface SPMSCategoryListTableViewCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UILabel* categoryNameLabel;
@property(assign, nonatomic) CategoryType* category;

@end
