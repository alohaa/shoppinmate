//
//  SPMSSWCoreDataSearchObject.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSSWAbstractSearchObject.h"

@interface SPMSSWCoreDataSearchObject : SPMSSWAbstractSearchObject

@property (strong, nonatomic) NSManagedObjectID *objectID;

@end
