//
//  SPMSShelfAnnotationView.h
//  Shoppin' Mate
//
//  Created by El Desperado on 8/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAMapView.h"
#import "SPMSShelfAnnotation.h"

@interface SPMSShelfAnnotationView : UIButton

/// Associated NAPinAnnotation.
@property(readwrite, nonatomic, weak) SPMSShelfAnnotation* annotation;

- (id)initWithAnnotation:(SPMSShelfAnnotation*)annotation
               onMapView:(NAMapView*)mapView;
/// Update the pin position when the map is zoomed in or zoomed out.
- (void)updatePosition;

@end
