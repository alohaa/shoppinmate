//
//  SPMSBeaconAnnotation.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSBeaconAnnotation.h"
#import "SPMSBeaconAnnotationView.h"

@interface SPMSBeaconAnnotation ()
@property(nonatomic, readonly, weak) SPMSBeaconAnnotationView* view;
@end

@implementation SPMSBeaconAnnotation

- (id)initWithPoint:(CGPoint)point {
  self = [super initWithPoint:point];
  if (self) {
    self.title = nil;
    self.subtitle = nil;
    self.rightCalloutAccessoryView = nil;
    self.radius = 40.0f;
    self.color =
        [UIColor colorWithRed:0.180 green:0.800 blue:0.443 alpha:1.000];
    self.opacity = 1.0f;
  }
  return self;
}

- (UIView*)createViewOnMapView:(NAMapView*)mapView {
  return [[SPMSBeaconAnnotationView alloc] initWithAnnotation:self
                                                        color:self.color
                                                      opacity:self.opacity
                                                       radius:self.radius
                                                    onMapView:mapView];
  ;
}

- (void)updatePosition {
  [self.view updatePosition];
}

@end
