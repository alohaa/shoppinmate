//
//  SPMSBeaconAnnotationCallOutView.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPMSBeaconAnnotation.h"
#import "NAMapView.h"

@interface SPMSBeaconAnnotationCallOutView : UIView

/// Create a new callout view on a map.
- (id)initOnMapView:(NAMapView*)mapView;

/// Recalculate position on map according to zoom level.
- (void)updatePosition;

/// Pin annotation.
@property(readwrite, nonatomic, strong) SPMSBeaconAnnotation* annotation;

@end
