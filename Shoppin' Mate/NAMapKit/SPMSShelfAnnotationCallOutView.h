//
//  SPMSShelfAnnotationCallOutView.h
//  Shoppin' Mate
//
//  Created by El Desperado on 8/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAMapView.h"
#import "SPMSShelfAnnotation.h"

@interface SPMSShelfAnnotationCallOutView : UIView

/// Create a new callout view on a map.
- (id)initOnMapView:(NAMapView*)mapView;

/// Recalculate position on map according to zoom level.
- (void)updatePosition;

/// Pin annotation.
@property(readwrite, nonatomic, strong) SPMSShelfAnnotation* annotation;

@end
