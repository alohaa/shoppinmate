//
//  SPMSShelfView.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/31/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSShelfAnnotation.h"
#import "NAMapView.h"
#import "SPMSShelfAnnotationView.h"

@interface SPMSShelfAnnotation ()

@property(nonatomic, readonly, weak) SPMSShelfAnnotationView* view;

@end

@implementation SPMSShelfAnnotation

- (id)annotationWithShapeRect:(CGRect)shapeRect {
  return [[[self class] alloc] initWithShapeRect:shapeRect];
}

- (id)initWithShapeRect:(CGRect)shapeRect {
  return [self initWithShapeRect:shapeRect labelString:nil image:nil];
}

- (id)initWithShapeRect:(CGRect)shapeRect
            labelString:(NSString*)labelString
                  image:(UIImage*)image {
  UIColor* borderColor =
      [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000];
  return [self initWithShapeRect:shapeRect
                     labelString:labelString
                           image:image
                     borderColor:borderColor];
}

- (id)initWithShapeRect:(CGRect)shapeRect
            labelString:(NSString*)labelString
                  image:(UIImage*)image
            borderColor:(UIColor*)borderColor {
  self = [super initWithPoint:shapeRect.origin];
  if (self) {
    _rect = shapeRect;
    if (labelString) {
      self.label = labelString;
    }

    if (image) {
      self.iconImage = image;
    }
    self.title = @"Shelf";
    self.subtitle = @"Tap Right Button to view detail";
    self.isHighlighted = NO;
    self.color = borderColor;
  }
  return self;
}

- (UIView*)createViewOnMapView:(NAMapView*)mapView {
  return [[SPMSShelfAnnotationView alloc] initWithAnnotation:self
                                                   onMapView:mapView];
}

- (void)updatePosition {
  [self.view updatePosition];
}

@end
