//
//  SPMSShelfView.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/31/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NAMapViewDelegate.h"
#import "NAAnnotation.h"

@class NAMapView;

@interface SPMSShelfAnnotation : NAAnnotation

/// Shelf's Rect.
@property(nonatomic, assign) CGRect rect;

@property(nonatomic, assign) NSString* label;
/// Shelf's title.
@property(nonatomic, copy) NSString* title;
/// Shelf's subtitle.
@property(nonatomic, copy) NSString* subtitle;
/// Callout view that appears when the annotation is tapped.
@property(nonatomic, strong) UIButton* rightCalloutAccessoryView;
@property(nonatomic, assign) UIImage* iconImage;
@property(nonatomic, assign) BOOL isHighlighted;
/// Border color.
@property(readwrite, nonatomic, strong) UIColor* color;

/// Create an annotation at a given point.
- (id)annotationWithShapeRect:(CGRect)shapeRect;
/// Create an annotation at a given point.
- (id)initWithShapeRect:(CGRect)shapeRect;
- (id)initWithShapeRect:(CGRect)shapeRect
            labelString:(NSString*)labelString
                  image:(UIImage*)image;
- (id)initWithShapeRect:(CGRect)shapeRect
            labelString:(NSString*)labelString
                  image:(UIImage*)image
            borderColor:(UIColor*)borderColor;
@end
