//
//  SPMSBeaconAnnotation.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "NAAnnotation.h"

@interface SPMSBeaconAnnotation : NAAnnotation

/// Pin title.
@property(nonatomic, copy) NSString* title;
/// Pin subtitle.
@property(nonatomic, copy) NSString* subtitle;
/// Pin subtitle.
@property(nonatomic, copy) NSString* productImageURLString;
/// Callout view that appears when the pin is tapped.
@property(nonatomic, strong) UIButton* rightCalloutAccessoryView;
/// Dot radius.
@property(readwrite, nonatomic, assign) CGFloat radius;
/// Dot color.
@property(readwrite, nonatomic, strong) UIColor* color;
/// Dot opacity.
@property(readwrite, nonatomic, assign) CGFloat opacity;

@end
