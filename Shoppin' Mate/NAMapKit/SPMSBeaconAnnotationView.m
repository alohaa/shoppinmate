//
//  SPMSBeaconAnnotationView.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSBeaconAnnotationView.h"
#import <UIImageView+WebCache.h>

const CGFloat NAMapViewBeaconAnnotationDotRadius = 40.0f;
const CGFloat NAMapViewBeaconAnnotationDotOpacity = 0.5f;

@interface SPMSBeaconAnnotationView () {
  UIImageView* imageView;
}
@property(nonatomic, weak) NAMapView* mapView;
@end

@implementation SPMSBeaconAnnotationView

- (id)initWithAnnotation:(SPMSBeaconAnnotation*)annotation
                   color:(UIColor*)color
                 opacity:(CGFloat)opacity
                  radius:(CGFloat)radius
               onMapView:(NAMapView*)mapView {
  self = [super initWithFrame:CGRectZero];
  if (self) {
    self.color = color;
    self.opacity = opacity;
    self.radius = radius;
    self.mapView = mapView;
    self.annotation = annotation;
    if (annotation.productImageURLString) {
      imageView = [[UIImageView alloc]
          initWithFrame:CGRectMake(0, 0, radius - 2.0, radius - 2.0)];
      [imageView setClipsToBounds:YES];
      [imageView sd_setImageWithURL:
                     [NSURL URLWithString:annotation.productImageURLString]];
      // Create Circular Image View
      imageView.layer.cornerRadius = imageView.frame.size.width / 2;

      // Add Border
      imageView.layer.borderWidth = 2.0f;
      imageView.layer.borderColor = self.color.CGColor;
      [self addSubview:imageView];
    } else {
      self.alpha = self.opacity ?: NAMapViewBeaconAnnotationDotOpacity;
      self.backgroundColor = self.color ?: [UIColor colorWithRed:0.906
                                                           green:0.298
                                                            blue:0.235
                                                           alpha:1.000];
    }
  }
  return self;
}

- (void)updatePosition {
  if (!self.mapView) {
    return;
  }

  CGFloat radius = (self.radius ?: NAMapViewBeaconAnnotationDotRadius) *
                   self.mapView.zoomScale;
  CGPoint point = [self.mapView zoomRelativePoint:self.annotation.point];
  if (!(self.annotation.productImageURLString.length > 0) || self.mapView.zoomScale <= kMAP_ZOOM_SCALE_DEFAULT_VALUE) {
      [imageView setHidden:YES];
    radius /= 2;
    self.backgroundColor = self.color ?: [UIColor redColor];
  } else {
    [imageView setHidden:NO];
    self.backgroundColor = [UIColor clearColor];
  }
  point.x -= radius;
  point.y -= radius;
  self.layer.cornerRadius = radius;
  self.frame = CGRectMake(point.x, point.y, radius * 2, radius * 2);
}

@end
