//
//  SPMSShelfAnnotationCallOutView.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSShelfAnnotationCallOutView.h"

const CGFloat NAMapViewShelfAnnotationCalloutTitleStandaloneLabelHeight = 22.0f;
const CGFloat NAMapViewShelfAnnotationCalloutTitleStandaloneFontSize = 18.0f;
const CGFloat NAMapViewShelfAnnotationCalloutTitleStandaloneTopOffset = 14.0f;
const CGFloat NAMapViewShelfAnnotationCalloutTitleTopOffset = 4.0f;
const CGFloat NAMapViewShelfAnnotationCalloutTitleLabelHeight = 20.0f;
const CGFloat NAMapViewShelfAnnotationCalloutTitleFontSize = 17.0f;
const CGFloat NAMapViewShelfAnnotationCalloutSubtitleTopOffset =
    0.0f + NAMapViewShelfAnnotationCalloutTitleLabelHeight;
const CGFloat NAMapViewShelfAnnotationCalloutSubtitleFontSize = 11.0f;
const CGFloat NAMapViewShelfAnnotationCalloutSubtitleLabelHeight = 25.0f;
const CGFloat NAMapViewShelfAnnotationCalloutRightAccessoryLeftOffset = 2.0f;
const CGFloat NAMapViewShelfAnnotationCalloutRightAccessoryTopOffset = 9.0f;
const CGFloat NAMapViewShelfAnnotationCalloutAnchorYOffset = 26.0f;
static NSString* NAMapViewShelfAnnotationCalloutImageLeft =
    @"/callout_left.png";
static NSString* NAMapViewShelfAnnotationCalloutImageRight =
    @"/callout_right.png";
static NSString* NAMapViewShelfAnnotationCalloutImageAnchor =
    @"/callout_anchor.png";
static NSString* NAMapViewShelfAnnotationCalloutImageBG = @"/callout_bg.png";

@interface SPMSShelfAnnotationCallOutView ()

@property(nonatomic, strong) UIImageView* calloutLeftCapView;
@property(nonatomic, strong) UIImageView* calloutRightCapView;
@property(nonatomic, strong) UIImageView* calloutAnchorView;
@property(nonatomic, strong) UIImageView* calloutLeftCenterView;
@property(nonatomic, strong) UIImageView* calloutRightCenterView;
@property(nonatomic, strong) UILabel* subtitleLabel;
@property(nonatomic, strong) UILabel* titleLabel;
@property(nonatomic, assign) CGPoint point;
@property(nonatomic, assign) CGPoint position;
@property(nonatomic, assign) CGRect rect;
@property(nonatomic, weak) NAMapView* mapView;

- (void)positionView:(UIView*)view posX:(float)x;
- (void)positionView:(UIView*)view posX:(float)x width:(float)width;

@end

@implementation SPMSShelfAnnotationCallOutView

- (id)initOnMapView:(NAMapView*)mapView {
  self = [super init];
  if (self) {
    UIImage* calloutBG = [[UIImage
        imageWithContentsOfFile:
            [[[NSBundle mainBundle] bundlePath]
                stringByAppendingString:NAMapViewShelfAnnotationCalloutImageBG]]
        stretchableImageWithLeftCapWidth:0
                            topCapHeight:0];
    self.calloutLeftCapView = [[UIImageView alloc]
        initWithImage:
            [UIImage imageWithContentsOfFile:
                         [[[NSBundle mainBundle] bundlePath]
                             stringByAppendingString:
                                 NAMapViewShelfAnnotationCalloutImageLeft]]];
    self.calloutRightCapView = [[UIImageView alloc]
        initWithImage:
            [UIImage imageWithContentsOfFile:
                         [[[NSBundle mainBundle] bundlePath]
                             stringByAppendingString:
                                 NAMapViewShelfAnnotationCalloutImageRight]]];
    self.calloutAnchorView = [[UIImageView alloc]
        initWithImage:
            [UIImage imageWithContentsOfFile:
                         [[[NSBundle mainBundle] bundlePath]
                             stringByAppendingString:
                                 NAMapViewShelfAnnotationCalloutImageAnchor]]];
    self.calloutLeftCenterView = [[UIImageView alloc] initWithImage:calloutBG];
    self.calloutRightCenterView = [[UIImageView alloc] initWithImage:calloutBG];
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.subtitleLabel.textColor = [UIColor whiteColor];
    self.subtitleLabel.backgroundColor = [UIColor clearColor];
    self.subtitleLabel.font = [UIFont
        systemFontOfSize:NAMapViewShelfAnnotationCalloutSubtitleFontSize];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.mapView = mapView;
    self.hidden = YES;
  }
  return self;
}

- (void)setAnnotation:(SPMSShelfAnnotation*)annotation {
  // --- RESET ---
  self.titleLabel.text = @"";
  self.subtitleLabel.text = @"";
  self.point = CGPointZero;
  self.rect = annotation.rect;

  for (UIView* view in self.subviews) {
    [view removeFromSuperview];
  }

  self.position = annotation.point;

  CGFloat leftCapWidth = self.calloutLeftCapView.image.size.width;
  CGFloat rightCapWidth = self.calloutRightCapView.image.size.width;
  CGFloat anchorWidth = self.calloutAnchorView.image.size.width;
  CGFloat anchorHeight = self.calloutAnchorView.image.size.height;
  CGFloat maxWidth = self.mapView.frame.size.width;

  // --- FRAME ---

  CGFloat middleWidth = anchorWidth;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
  if (annotation.subtitle) {
    CGSize subtitleSize = [annotation.subtitle
             sizeWithFont:
                 [UIFont boldSystemFontOfSize:
                             NAMapViewShelfAnnotationCalloutSubtitleFontSize]
        constrainedToSize:
            CGSizeMake(maxWidth,
                       NAMapViewShelfAnnotationCalloutSubtitleLabelHeight)
            lineBreakMode:NSLineBreakByTruncatingTail];

    middleWidth = MAX(subtitleSize.width, middleWidth);

    CGSize titleSize = [annotation.title
             sizeWithFont:[UIFont
                              boldSystemFontOfSize:
                                  NAMapViewShelfAnnotationCalloutTitleFontSize]
        constrainedToSize:CGSizeMake(
                              maxWidth,
                              NAMapViewShelfAnnotationCalloutTitleLabelHeight)
            lineBreakMode:NSLineBreakByTruncatingTail];

    middleWidth = MAX(titleSize.width, middleWidth);
  } else {
    CGSize titleSize = [annotation.title
             sizeWithFont:
                 [UIFont
                     boldSystemFontOfSize:
                         NAMapViewShelfAnnotationCalloutTitleStandaloneFontSize]
        constrainedToSize:
            CGSizeMake(
                maxWidth,
                NAMapViewShelfAnnotationCalloutTitleStandaloneLabelHeight)
            lineBreakMode:NSLineBreakByTruncatingTail];

    middleWidth = MAX(titleSize.width, middleWidth);
  }
#pragma clang diagnostic pop

  if (annotation.rightCalloutAccessoryView) {
    middleWidth += annotation.rightCalloutAccessoryView.frame.size.width +
                   NAMapViewShelfAnnotationCalloutRightAccessoryLeftOffset;
  }

  middleWidth = MIN(maxWidth, middleWidth);

  CGFloat totalWidth = middleWidth + leftCapWidth + rightCapWidth;

  self.point = annotation.point;

  self.frame = CGRectMake(0.0f, 0.0f, totalWidth, anchorHeight);
  [self updatePosition];

  // --- IMAGEVIEWS ---

  CGFloat centreOffsetWidth = (middleWidth - anchorWidth) / 2.0f;

  [self positionView:self.calloutLeftCapView posX:0.0f];
  [self positionView:self.calloutRightCapView
                posX:(totalWidth - rightCapWidth)];
  [self positionView:self.calloutAnchorView
                posX:(leftCapWidth + centreOffsetWidth)];

  [self addSubview:self.calloutLeftCapView];
  [self addSubview:self.calloutRightCapView];
  [self addSubview:self.calloutAnchorView];

  if (middleWidth > anchorWidth) {
    [self positionView:self.calloutLeftCenterView
                  posX:leftCapWidth
                 width:centreOffsetWidth];
    [self positionView:self.calloutRightCenterView
                  posX:(leftCapWidth + middleWidth - centreOffsetWidth)
                 width:centreOffsetWidth];

    [self addSubview:self.calloutLeftCenterView];
    [self addSubview:self.calloutRightCenterView];
  }

  CGFloat labelWidth = middleWidth;

  // --- RIGHT ACCESSORY VIEW ---

  if (annotation.rightCalloutAccessoryView) {
    CGFloat accesoryWidth =
        annotation.rightCalloutAccessoryView.frame.size.width;
    CGFloat x = middleWidth - accesoryWidth + leftCapWidth +
                NAMapViewShelfAnnotationCalloutRightAccessoryLeftOffset;

    CGRect frame = annotation.rightCalloutAccessoryView.frame;
    frame.origin.x = x;
    frame.origin.y = NAMapViewShelfAnnotationCalloutRightAccessoryTopOffset;
    annotation.rightCalloutAccessoryView.frame = frame;

    [self addSubview:annotation.rightCalloutAccessoryView];
    labelWidth -= accesoryWidth;
  }

  // --- LABELS ---

  CGFloat currentTitleTopOffset =
      NAMapViewShelfAnnotationCalloutTitleStandaloneTopOffset;
  CGFloat currentTitleLabelHeight =
      NAMapViewShelfAnnotationCalloutTitleStandaloneLabelHeight;
  CGFloat currentTitleFontSize =
      NAMapViewShelfAnnotationCalloutTitleStandaloneFontSize;

  // --- SUBTITLE ---

  if (annotation.subtitle) {
    currentTitleTopOffset = NAMapViewShelfAnnotationCalloutTitleTopOffset;
    currentTitleLabelHeight = NAMapViewShelfAnnotationCalloutTitleLabelHeight;
    currentTitleFontSize = NAMapViewShelfAnnotationCalloutTitleFontSize;
    self.subtitleLabel.text = annotation.subtitle;
    self.subtitleLabel.frame =
        CGRectMake(leftCapWidth,
                   NAMapViewShelfAnnotationCalloutSubtitleTopOffset,
                   labelWidth,
                   NAMapViewShelfAnnotationCalloutSubtitleLabelHeight);
    [self addSubview:self.subtitleLabel];
  }

  // --- TITLE ---

  self.titleLabel.text = annotation.title;
  self.titleLabel.font = [UIFont boldSystemFontOfSize:currentTitleFontSize];
  self.titleLabel.frame = CGRectMake(
      leftCapWidth, currentTitleTopOffset, labelWidth, currentTitleLabelHeight);

  [self addSubview:self.titleLabel];
}

#pragma - Private helpers

- (void)updatePosition {
  CGPoint point = [self.mapView zoomRelativePoint:self.position];
  CGFloat xPos = point.x - (self.frame.size.width / 2.0f) +
                 (self.rect.size.width / 2.0f * self.mapView.zoomScale);
  CGFloat offsetHeight;
  if (self.mapView.zoomScale <= kMAP_ZOOM_SCALE_DEFAULT_VALUE) {
    offsetHeight = self.rect.size.height * self.mapView.zoomScale;
  } else {
    offsetHeight = self.rect.size.height * self.mapView.zoomScale / 2.0f;
  }
  CGFloat yPos = point.y - (self.frame.size.height) -
                 NAMapViewShelfAnnotationCalloutAnchorYOffset + offsetHeight;
  self.frame = CGRectMake(
      floor(xPos), yPos, self.frame.size.width, self.frame.size.height);
}

- (void)positionView:(UIView*)view posX:(float)x width:(float)width {
  CGRect frame = view.frame;
  frame.origin.x = x;
  frame.size.width = width;
  view.frame = frame;
}

- (void)positionView:(UIView*)view posX:(float)x {
  [self positionView:view posX:x width:view.frame.size.width];
}

@end
