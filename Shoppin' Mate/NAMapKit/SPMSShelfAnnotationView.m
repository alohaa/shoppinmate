//
//  SPMSShelfAnnotationView.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSShelfAnnotationView.h"

@interface SPMSShelfAnnotationView ()

@property(nonatomic, weak) NAMapView* mapView;

@end

@implementation SPMSShelfAnnotationView

- (id)initWithAnnotation:(SPMSShelfAnnotation*)annotation
               onMapView:(NAMapView*)mapView {
  self = [super initWithFrame:CGRectZero];
  if (self) {
    self.mapView = mapView;
    self.annotation = annotation;
    if (self.annotation.label) {
      [self setTitle:self.annotation.label forState:UIControlStateNormal];
    }
    if (self.annotation.iconImage) {
      [self setImage:self.annotation.iconImage forState:UIControlStateNormal];
      [self setContentMode:UIViewContentModeCenter];
      [self setImageEdgeInsets:UIEdgeInsetsMake(20, 20, 20, 20)];
    }

    if (self.annotation.isHighlighted) {
      [self.layer setBorderWidth:2.0f];
      [self.layer setBorderColor:[self.annotation.color CGColor]];
    }
  }
  return self;
}

- (void)updatePosition {
  if (!self.mapView) {
    return;
  }

  CGPoint point = [self.mapView zoomRelativePoint:self.annotation.point];
  self.frame =
      CGRectMake(point.x,
                 point.y,
                 self.annotation.rect.size.width * self.mapView.zoomScale,
                 self.annotation.rect.size.height * self.mapView.zoomScale);
}

@end
