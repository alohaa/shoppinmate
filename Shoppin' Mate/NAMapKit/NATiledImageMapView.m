//
//  NATiledImageMapView.m
//  NAMapKit
//
//  Created by Daniel Doubrovkine on 3/10/14.
//  Copyright (c) 2010-14 neilang.com. All rights reserved.
//

#import "NATiledImageMapView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ARTiledImageView.h"
#import "ARTiledImageViewDataSource.h"
#import "SPMSBeaconAnnotation.h"
#import "SPMSBeaconAnnotationView.h"
#import "SPMSBeaconAnnotationCallOutView.h"
#import "SPMSShelfAnnotation.h"
#import "SPMSShelfAnnotationView.h"
#import "SPMSShelfAnnotationCallOutView.h"

const CGFloat NAMapViewBeaconAnnotationCalloutAnimationDuration = 0.1f;

@interface NATiledImageMapView ()
@property(nonatomic, weak, readonly)
    NSObject<ARTiledImageViewDataSource>* dataSource;
@property(nonatomic, readonly) ARTiledImageView* imageView;
@property(nonatomic, readonly) UIImageView* backgroundImageView;
@property(nonatomic, strong) SPMSBeaconAnnotationCallOutView* beaconCalloutView;
@property(nonatomic, strong) SPMSShelfAnnotationCallOutView* shelfCalloutView;

- (IBAction)showCallOut:(id)sender;
- (void)hideCallOut;
@end

@implementation NATiledImageMapView

- (id)initWithFrame:(CGRect)frame
    tiledImageDataSource:(NSObject<ARTiledImageViewDataSource>*)dataSource {
  self = [super initWithFrame:frame];
  if (self) {
    _dataSource = dataSource;
    [self setupMap];
  }
  return self;
}

- (void)createImageView {
  if (self.dataSource) {
    _imageView = [[ARTiledImageView alloc] initWithDataSource:self.dataSource];
    self.imageView.displayTileBorders = self.displayTileBorders;
    [self addSubview:self.imageView];
  }
}

- (void)setupMap {
  if (self.dataSource) {
    [super setupMap];
    [self setMaxMinZoomScalesForCurrentBounds];
    self.beaconCalloutView =
        [[SPMSBeaconAnnotationCallOutView alloc] initOnMapView:self];
    [self addSubview:self.beaconCalloutView];
    self.shelfCalloutView =
        [[SPMSShelfAnnotationCallOutView alloc] initOnMapView:self];
    [self addSubview:self.shelfCalloutView];
  }
}

- (void)addAnnotation:(NAAnnotation*)annotation animated:(BOOL)animate {
  [super addAnnotation:annotation animated:animate];
  if ([annotation.view isKindOfClass:SPMSBeaconAnnotationView.class]) {
    SPMSBeaconAnnotationView* annotationView =
        (SPMSBeaconAnnotationView*)annotation.view;
    [annotationView addTarget:self
                       action:@selector(showCallOut:)
             forControlEvents:UIControlEventTouchDown];
  }
  [self bringSubviewToFront:self.beaconCalloutView];
  if ([annotation.view isKindOfClass:SPMSShelfAnnotationView.class]) {
    SPMSShelfAnnotationView* annotationView =
        (SPMSShelfAnnotationView*)annotation.view;
    [annotationView addTarget:self
                       action:@selector(showCallOut:)
             forControlEvents:UIControlEventTouchDown];
  }
  [self bringSubviewToFront:self.shelfCalloutView];
}

- (void)setDisplayTileBorders:(BOOL)displayTileBorders {
  self.imageView.displayTileBorders = displayTileBorders;
  _displayTileBorders = displayTileBorders;
}

- (void)setMaxMinZoomScalesForCurrentBounds {
  CGSize boundsSize = self.bounds.size;
  CGSize imageSize = [self.dataSource imageSizeForImageView:nil];

  // calculate min/max zoomscale
  CGFloat xScale =
      boundsSize.width /
      imageSize
          .width;  // the scale needed to perfectly fit the image width-wise
  CGFloat yScale =
      boundsSize.height /
      imageSize
          .height;  // the scale needed to perfectly fit the image height-wise
  CGFloat minScale = MAX(xScale, yScale);  // use minimum of these to allow the
                                           // image to become fully visible

  CGFloat maxScale = 1.0;

  // don't let minScale exceed maxScale
  // if the image is smaller than the screen, we don't want to force it to be
  // zoomed
  if (minScale > maxScale) {
    minScale = maxScale;
  }

  self.maximumZoomScale = maxScale * 0.6;
  self.minimumZoomScale = minScale;

  self.originalSize = imageSize;
  self.contentSize = imageSize;
}

- (void)zoomToFit:(BOOL)animate {
  [self setZoomScale:self.minimumZoomScale animated:animate];
}

- (void)scrollViewDidZoom:(UIScrollView*)scrollView {
  self.backgroundImageView.frame = self.imageView.frame;
  [super scrollViewDidZoom:scrollView];
  NSInteger newZoomLevel = self.imageView.currentZoomLevel;
  if (newZoomLevel != self.tileZoomLevel) {
    // TODO: delegate that zoom level has changed
    _tileZoomLevel = self.imageView.currentZoomLevel;
  }
}

- (void)setBackgroundImageURL:(NSURL*)backgroundImageURL {
  UIImageView* backgroundImageView =
      [[UIImageView alloc] initWithFrame:self.imageView.frame];
  [self insertSubview:backgroundImageView belowSubview:self.imageView];
  [backgroundImageView sd_setImageWithURL:backgroundImageURL];
  _backgroundImageView = backgroundImageView;
  _backgroundImageURL = backgroundImageURL;
}

- (void)setBackgroundImage:(UIImage*)backgroundImage {
  UIImageView* backgroundImageView =
      [[UIImageView alloc] initWithFrame:self.imageView.frame];
  [self insertSubview:backgroundImageView belowSubview:self.imageView];
  backgroundImageView.image = backgroundImage;
  _backgroundImageView = backgroundImageView;
  _backgroundImage = backgroundImage;
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView*)scrollView {
  return self.imageView;
}

- (void)selectAnnotation:(NAAnnotation*)annotation animated:(BOOL)animate {
  [self hideCallOut];
  if ([annotation isKindOfClass:SPMSBeaconAnnotation.class]) {
    [self showCalloutForBeaconAnnotation:(SPMSBeaconAnnotation*)annotation
                                animated:animate];
  }
  if ([annotation isKindOfClass:SPMSShelfAnnotation.class]) {
    [self showCalloutForShelfAnnotation:(SPMSShelfAnnotation*)annotation
                               animated:animate];
  }
}

- (void)showCalloutForBeaconAnnotation:(SPMSBeaconAnnotation*)annotation
                              animated:(BOOL)animated {
  [self hideCallOut];

  self.beaconCalloutView.annotation = annotation;

  [self centerOnPoint:annotation.point animated:animated];

  CGFloat animationDuration =
      animated ? NAMapViewBeaconAnnotationCalloutAnimationDuration : 0.0f;

  self.beaconCalloutView.transform =
      CGAffineTransformScale(CGAffineTransformIdentity, 0.4f, 0.4f);
  self.beaconCalloutView.hidden = NO;

  __weak typeof(self) weakSelf = self;
  [UIView animateWithDuration:animationDuration
                   animations:^{
                       weakSelf.beaconCalloutView.transform =
                           CGAffineTransformIdentity;
                   }];
}

- (void)showCalloutForShelfAnnotation:(SPMSShelfAnnotation*)annotation
                             animated:(BOOL)animated {
  [self hideCallOut];

  self.shelfCalloutView.annotation = annotation;

  CGPoint point = CGPointMake(
      annotation.point.x + (annotation.rect.size.width * self.zoomScale),
      annotation.point.y + (annotation.rect.size.height * self.zoomScale));

  [self centerOnPoint:point animated:animated];

  CGFloat animationDuration =
      animated ? NAMapViewBeaconAnnotationCalloutAnimationDuration : 0.0f;

  self.shelfCalloutView.transform =
      CGAffineTransformScale(CGAffineTransformIdentity, 0.4f, 0.4f);
  self.shelfCalloutView.hidden = NO;

  __weak typeof(self) weakSelf = self;
  [UIView animateWithDuration:animationDuration
                   animations:^{
                       weakSelf.shelfCalloutView.transform =
                           CGAffineTransformIdentity;
                   }];
}

- (IBAction)showCallOut:(id)sender {
  if ([sender isKindOfClass:[SPMSBeaconAnnotationView class]]) {
    SPMSBeaconAnnotationView* annontationView =
        (SPMSBeaconAnnotationView*)sender;
    [self.mapViewDelegate mapView:self
               tappedOnAnnotation:annontationView.annotation];
    [self showCalloutForBeaconAnnotation:annontationView.annotation
                                animated:YES];
  }
  if ([sender isKindOfClass:[SPMSShelfAnnotationView class]]) {
    SPMSShelfAnnotationView* annontationView = (SPMSShelfAnnotationView*)sender;
    [self.mapViewDelegate mapView:self
               tappedOnAnnotation:annontationView.annotation];
    [self showCalloutForShelfAnnotation:annontationView.annotation
                               animated:YES];
  }
}

- (void)hideCallOut {
  self.beaconCalloutView.hidden = YES;
  self.shelfCalloutView.hidden = YES;
}

@end
