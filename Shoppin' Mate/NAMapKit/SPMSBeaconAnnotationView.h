//
//  SPMSBeaconAnnotationView.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAMapView.h"
#import "SPMSBeaconAnnotation.h"

@interface SPMSBeaconAnnotationView : UIButton

/// Associated NAPinAnnotation.
@property(readwrite, nonatomic, weak) SPMSBeaconAnnotation* annotation;
/// Dot radius.
@property(readwrite, nonatomic, assign) CGFloat radius;
/// Dot color.
@property(readwrite, nonatomic, strong) UIColor* color;
/// Dot opacity.
@property(readwrite, nonatomic, assign) CGFloat opacity;

/// Create a view for a pin annotation on a map.
- (id)initWithAnnotation:(SPMSBeaconAnnotation*)annotation
                   color:(UIColor*)color
                 opacity:(CGFloat)opacity
                  radius:(CGFloat)radius
               onMapView:(NAMapView*)mapView;
/// Update the pin position when the map is zoomed in or zoomed out.
- (void)updatePosition;

@end
