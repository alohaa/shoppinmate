//
//  SPMSBeaconAnnotationCallOutView.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSBeaconAnnotationCallOutView.h"
#import <UIImageView+WebCache.h>

const CGFloat NAMapViewBeaconAnnotationCalloutProductImageWidth = 32.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutProductImageTopOffset = 8.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutProductTextLeftPadding = 4.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutTitleStandaloneLabelHeight =
    22.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutTitleStandaloneFontSize = 18.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutTitleStandaloneTopOffset = 14.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutTitleTopOffset = 4.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutTitleLabelHeight = 20.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutTitleFontSize = 17.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutSubtitleTopOffset =
    0.0f + NAMapViewBeaconAnnotationCalloutTitleLabelHeight;
const CGFloat NAMapViewBeaconAnnotationCalloutSubtitleFontSize = 11.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutSubtitleLabelHeight = 25.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutRightAccessoryLeftOffset = 2.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutRightAccessoryTopOffset = 9.0f;
const CGFloat NAMapViewBeaconAnnotationCalloutAnchorYOffset = 4.0f;
static NSString* NAMapViewBeaconAnnotationCalloutImageLeft =
    @"/callout_left.png";
static NSString* NAMapViewBeaconAnnotationCalloutImageRight =
    @"/callout_right.png";
static NSString* NAMapViewBeaconAnnotationCalloutImageAnchor =
    @"/callout_anchor.png";
static NSString* NAMapViewBeaconAnnotationCalloutImageBG = @"/callout_bg.png";

@interface SPMSBeaconAnnotationCallOutView ()

@property(nonatomic, strong) UIImageView* calloutLeftCapView;
@property(nonatomic, strong) UIImageView* calloutRightCapView;
@property(nonatomic, strong) UIImageView* calloutAnchorView;
@property(nonatomic, strong) UIImageView* calloutLeftCenterView;
@property(nonatomic, strong) UIImageView* calloutRightCenterView;
@property(nonatomic, strong) UIImageView* productImageView;
@property(nonatomic, strong) UILabel* subtitleLabel;
@property(nonatomic, strong) UILabel* titleLabel;
@property(nonatomic, assign) CGPoint point;
@property(nonatomic, assign) CGPoint position;
@property(nonatomic, weak) NAMapView* mapView;

- (void)positionView:(UIView*)view posX:(float)x;
- (void)positionView:(UIView*)view posX:(float)x width:(float)width;

@end

@implementation SPMSBeaconAnnotationCallOutView

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
  }
  return self;
}

- (id)initOnMapView:(NAMapView*)mapView {
  self = [super init];
  if (self) {
    UIImage* calloutBG =
        [[UIImage imageWithContentsOfFile:
                      [[[NSBundle mainBundle] bundlePath]
                          stringByAppendingString:
                              NAMapViewBeaconAnnotationCalloutImageBG]]
            stretchableImageWithLeftCapWidth:0
                                topCapHeight:0];
    self.calloutLeftCapView = [[UIImageView alloc]
        initWithImage:
            [UIImage imageWithContentsOfFile:
                         [[[NSBundle mainBundle] bundlePath]
                             stringByAppendingString:
                                 NAMapViewBeaconAnnotationCalloutImageLeft]]];
    self.calloutRightCapView = [[UIImageView alloc]
        initWithImage:
            [UIImage imageWithContentsOfFile:
                         [[[NSBundle mainBundle] bundlePath]
                             stringByAppendingString:
                                 NAMapViewBeaconAnnotationCalloutImageRight]]];
    self.calloutAnchorView = [[UIImageView alloc]
        initWithImage:
            [UIImage imageWithContentsOfFile:
                         [[[NSBundle mainBundle] bundlePath]
                             stringByAppendingString:
                                 NAMapViewBeaconAnnotationCalloutImageAnchor]]];
    self.calloutLeftCenterView = [[UIImageView alloc] initWithImage:calloutBG];
    self.calloutRightCenterView = [[UIImageView alloc] initWithImage:calloutBG];
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.subtitleLabel.textColor = [UIColor whiteColor];
    self.subtitleLabel.backgroundColor = [UIColor clearColor];
    self.subtitleLabel.font = [UIFont
        systemFontOfSize:NAMapViewBeaconAnnotationCalloutSubtitleFontSize];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.mapView = mapView;
    self.hidden = YES;
  }
  return self;
}

- (void)setAnnotation:(SPMSBeaconAnnotation*)annotation {
  // --- RESET ---

  self.titleLabel.text = @"";
  self.subtitleLabel.text = @"";
  self.point = CGPointZero;

  for (UIView* view in self.subviews) {
    [view removeFromSuperview];
  }

  self.position = annotation.point;

  CGFloat leftCapWidth = self.calloutLeftCapView.image.size.width;
  CGFloat rightCapWidth = self.calloutRightCapView.image.size.width;
  CGFloat anchorWidth = self.calloutAnchorView.image.size.width;
  CGFloat anchorHeight = self.calloutAnchorView.image.size.height;
  CGFloat maxWidth = self.mapView.frame.size.width;

  // --- FRAME ---

  CGFloat middleWidth = anchorWidth;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
  if (annotation.subtitle) {
    CGSize subtitleSize = [annotation.subtitle
             sizeWithFont:
                 [UIFont boldSystemFontOfSize:
                             NAMapViewBeaconAnnotationCalloutSubtitleFontSize]
        constrainedToSize:
            CGSizeMake(maxWidth,
                       NAMapViewBeaconAnnotationCalloutSubtitleLabelHeight)
            lineBreakMode:NSLineBreakByTruncatingTail];

    middleWidth = MAX(subtitleSize.width, middleWidth);

    CGSize titleSize = [annotation.title
             sizeWithFont:[UIFont
                              boldSystemFontOfSize:
                                  NAMapViewBeaconAnnotationCalloutTitleFontSize]
        constrainedToSize:CGSizeMake(
                              maxWidth,
                              NAMapViewBeaconAnnotationCalloutTitleLabelHeight)
            lineBreakMode:NSLineBreakByTruncatingTail];

    middleWidth = MAX(titleSize.width, middleWidth);
  } else {
    CGSize titleSize = [annotation.title
             sizeWithFont:
                 [UIFont
                     boldSystemFontOfSize:
                         NAMapViewBeaconAnnotationCalloutTitleStandaloneFontSize]
        constrainedToSize:
            CGSizeMake(
                maxWidth,
                NAMapViewBeaconAnnotationCalloutTitleStandaloneLabelHeight)
            lineBreakMode:NSLineBreakByTruncatingTail];

    middleWidth = MAX(titleSize.width, middleWidth);
  }
#pragma clang diagnostic pop

  if (annotation.rightCalloutAccessoryView) {
    middleWidth += annotation.rightCalloutAccessoryView.frame.size.width +
                   NAMapViewBeaconAnnotationCalloutRightAccessoryLeftOffset;
  }

  if (annotation.productImageURLString) {
    middleWidth += NAMapViewBeaconAnnotationCalloutProductImageWidth +
                   NAMapViewBeaconAnnotationCalloutProductTextLeftPadding;
  }

  middleWidth = MIN(maxWidth, middleWidth);

  CGFloat totalWidth = middleWidth + leftCapWidth + rightCapWidth;

  self.point = annotation.point;

  self.frame = CGRectMake(0.0f, 0.0f, totalWidth, anchorHeight);
  [self updatePosition];

  // --- IMAGEVIEWS ---

  CGFloat centreOffsetWidth = (middleWidth - anchorWidth) / 2.0f;

  [self positionView:self.calloutLeftCapView posX:0.0f];
  [self positionView:self.calloutRightCapView
                posX:(totalWidth - rightCapWidth)];
  [self positionView:self.calloutAnchorView
                posX:(leftCapWidth + centreOffsetWidth)];

  [self addSubview:self.calloutLeftCapView];
  [self addSubview:self.calloutRightCapView];
  [self addSubview:self.calloutAnchorView];

  if (anchorWidth < middleWidth) {
    [self positionView:self.calloutLeftCenterView
                  posX:leftCapWidth
                 width:centreOffsetWidth];
    [self positionView:self.calloutRightCenterView
                  posX:(leftCapWidth + middleWidth - centreOffsetWidth)
                 width:centreOffsetWidth];

    [self addSubview:self.calloutLeftCenterView];
    [self addSubview:self.calloutRightCenterView];
  }

  CGFloat labelWidth = middleWidth;

  // --- RIGHT ACCESSORY VIEW ---

  if (annotation.rightCalloutAccessoryView) {
    CGFloat accesoryWidth =
        annotation.rightCalloutAccessoryView.frame.size.width;
    CGFloat x = middleWidth - accesoryWidth + leftCapWidth +
                NAMapViewBeaconAnnotationCalloutRightAccessoryLeftOffset;

    CGRect frame = annotation.rightCalloutAccessoryView.frame;
    frame.origin.x = x;
    frame.origin.y = NAMapViewBeaconAnnotationCalloutRightAccessoryTopOffset;
    annotation.rightCalloutAccessoryView.frame = frame;

    [self addSubview:annotation.rightCalloutAccessoryView];
    labelWidth -= accesoryWidth;
  }

  // --- PRODUCT'S IMAGE ---

  if (annotation.productImageURLString) {
    self.productImageView = [[UIImageView alloc]
        initWithFrame:CGRectMake(
                          leftCapWidth,
                          NAMapViewBeaconAnnotationCalloutProductImageTopOffset,
                          NAMapViewBeaconAnnotationCalloutProductImageWidth,
                          NAMapViewBeaconAnnotationCalloutProductImageWidth)];
    [self.productImageView setClipsToBounds:YES];
    [self.productImageView
        sd_setImageWithURL:[NSURL
                               URLWithString:annotation.productImageURLString]
          placeholderImage:[UIImage imageNamed:@"Photo"]];

    // Create Circular Image View
    self.productImageView.layer.cornerRadius =
        self.productImageView.frame.size.width / 2;

    // Add Border
    self.productImageView.layer.borderWidth = 2.0f;
    self.productImageView.layer.borderColor =
        [UIColor colorWithRed:0.925 green:0.941 blue:0.945 alpha:1.000].CGColor;
    [self addSubview:self.productImageView];
  }

  // --- LABELS ---

  CGFloat currentTitleTopOffset =
      NAMapViewBeaconAnnotationCalloutTitleStandaloneTopOffset;
  CGFloat currentTitleLabelHeight =
      NAMapViewBeaconAnnotationCalloutTitleStandaloneLabelHeight;
  CGFloat currentTitleFontSize =
      NAMapViewBeaconAnnotationCalloutTitleStandaloneFontSize;

  // --- SUBTITLE ---

  if (annotation.subtitle) {
    currentTitleTopOffset = NAMapViewBeaconAnnotationCalloutTitleTopOffset;
    currentTitleLabelHeight = NAMapViewBeaconAnnotationCalloutTitleLabelHeight;
    currentTitleFontSize = NAMapViewBeaconAnnotationCalloutTitleFontSize;
    self.subtitleLabel.text = annotation.subtitle;
    if (annotation.productImageURLString) {
      self.subtitleLabel.frame = CGRectMake(
          leftCapWidth + NAMapViewBeaconAnnotationCalloutProductImageWidth +
              NAMapViewBeaconAnnotationCalloutProductTextLeftPadding,
          NAMapViewBeaconAnnotationCalloutSubtitleTopOffset,
          labelWidth,
          NAMapViewBeaconAnnotationCalloutSubtitleLabelHeight);
    } else {
      self.subtitleLabel.frame =
          CGRectMake(leftCapWidth,
                     NAMapViewBeaconAnnotationCalloutSubtitleTopOffset,
                     labelWidth,
                     NAMapViewBeaconAnnotationCalloutSubtitleLabelHeight);
    }

    [self addSubview:self.subtitleLabel];
  }

  // --- TITLE ---

  self.titleLabel.text = annotation.title;
  self.titleLabel.font = [UIFont boldSystemFontOfSize:currentTitleFontSize];
  if (annotation.productImageURLString) {
    self.titleLabel.frame = CGRectMake(
        leftCapWidth + NAMapViewBeaconAnnotationCalloutProductImageWidth +
            NAMapViewBeaconAnnotationCalloutProductTextLeftPadding,
        currentTitleTopOffset,
        labelWidth,
        currentTitleLabelHeight);
  } else {
    self.titleLabel.frame = CGRectMake(leftCapWidth,
                                       currentTitleTopOffset,
                                       labelWidth,
                                       currentTitleLabelHeight);
  }

  [self addSubview:self.titleLabel];
}

#pragma - Private helpers

- (void)updatePosition {
  CGPoint point = [self.mapView zoomRelativePoint:self.position];
  CGFloat xPos = point.x - (self.frame.size.width / 2.0f);
  CGFloat yPos = point.y - (self.frame.size.height) -
                 NAMapViewBeaconAnnotationCalloutAnchorYOffset;
  self.frame = CGRectMake(
      floor(xPos), yPos, self.frame.size.width, self.frame.size.height);
}

- (void)positionView:(UIView*)view posX:(float)x width:(float)width {
  CGRect frame = view.frame;
  frame.origin.x = x;
  frame.size.width = width;
  view.frame = frame;
}

- (void)positionView:(UIView*)view posX:(float)x {
  [self positionView:view posX:x width:view.frame.size.width];
}

@end
