//
//  SPMSSWObjects.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#pragma once
#ifndef Shoppin__Mate_SPMSSWObjects_h
#define Shoppin__Mate_SPMSSWObjects_h

#import "SPMSSWAbstractSearchObject.h"
#import "SPMSSWCoreDataSearchObject.h"
#import "SPMSSWSearchObject.h"

#endif
