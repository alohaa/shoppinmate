//
//  MEDJsonGenerator.m
//  MapEditor
//
//  Created by Dat Truong on 2014-07-22.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "MEDJsonGenerator.h"
#import "MEDFurniture.h"
#import "MEDBeaconPoint.h"

#define kFUR_ID @"furId"
#define kFLIP_V @"flipV"
#define kFLIP_H @"flipH"
#define kSTENCIL_ID @"idStencil"
#define kZ_POS @"zPos"
#define kPOSITION @"position"
#define kSIZE @"size"
#define kPOINT_ID @"pointID"
#define kALIAS @"alias"

#define kP_POINTID @"pointId"
#define kP_POSITION @"position"
#define kP_MAJOR @"major"
#define kP_MINOR @"minor"
#define kP_UUID @"uuid"
#define kP_GRAPH @"graph"

#define kM_FURNITURES @"furnitures"
#define kM_POINTS @"points"

@interface MEDJsonGenerator () {
}

@end

@implementation MEDJsonGenerator

#pragma mark - Furnitures

- (NSString*)jsonFurnituresWithFurnituresArray:(NSArray*)furnitures {
  NSString* jsonFurnituresString;

  NSArray* processingJsonFurnitures;
  processingJsonFurnitures =
      [self jsonFurnitureDictArrayWithFurnitureArray:furnitures];

  NSData* jsonData =
      [NSJSONSerialization dataWithJSONObject:processingJsonFurnitures
                                      options:NSJSONWritingPrettyPrinted
                                        error:nil];

  jsonFurnituresString =
      [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

  return jsonFurnituresString;
}

- (NSArray*)jsonFurnitureDictArrayWithFurnitureArray:(NSArray*)furnitures {
  NSArray* jsonFurnitureDictArray;

  NSMutableArray* processingJsonFurnitures;
  processingJsonFurnitures = [NSMutableArray array];

  for (MEDFurniture* furniture in furnitures) {
    NSDictionary* jsonFuniture;
    jsonFuniture = [self jsonDictFromFurniture:furniture];
    [processingJsonFurnitures addObject:jsonFuniture];
  }

  jsonFurnitureDictArray = [processingJsonFurnitures copy];

  return jsonFurnitureDictArray;
}

- (NSDictionary*)jsonDictFromFurniture:(MEDFurniture*)furniture {
  NSDictionary* jsonFurniture;

  jsonFurniture = @{
    kFUR_ID : @(furniture.furnitureId),
    kFLIP_H : @(furniture.flippedHorizontal),
    kFLIP_V : @(furniture.flippedVertical),
    kSTENCIL_ID : @(furniture.stencilId),
    kZ_POS : @(furniture.zPos),
    kPOSITION : NSStringFromCGPoint(furniture.rect.origin),
    kSIZE : NSStringFromCGSize(furniture.rect.size),
    kPOINT_ID : @(furniture.pointId),
    kALIAS : furniture.name
  };

  return jsonFurniture;
}

- (NSArray*)furnituresWithJsonString:(NSString*)jsonString {
  NSArray* furnitures;

  NSMutableArray* processingFurnitures;
  processingFurnitures = [NSMutableArray array];

  NSArray* parsedArray;
  parsedArray = [NSJSONSerialization
      JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                 options:NSJSONReadingAllowFragments
                   error:nil];

  for (NSDictionary* furnitureDict in parsedArray) {
    MEDFurniture* furniture;
    furniture = [self furnitureFromDict:furnitureDict];
    [processingFurnitures addObject:furniture];
  }

  furnitures = [processingFurnitures copy];

  return furnitures;
}

- (MEDFurniture*)furnitureFromDict:(NSDictionary*)furnitureDict {
  MEDFurniture* furniture;

  furniture = [[MEDFurniture alloc] init];
  NSString* posString, *sizeString;
  posString = furnitureDict[kPOSITION];
  sizeString = furnitureDict[kSIZE];
  furniture.furnitureId = [furnitureDict[kFUR_ID] integerValue];
  furniture.stencilId = [furnitureDict[kSTENCIL_ID] integerValue];
  furniture.flippedHorizontal = [furnitureDict[kFLIP_H] boolValue];
  furniture.flippedVertical = [furnitureDict[kFLIP_V] boolValue];
  furniture.pointId = [furnitureDict[kPOINT_ID] integerValue];
  furniture.name = furnitureDict[kALIAS];

  CGPoint extractedPoint;
  extractedPoint = CGPointFromString(posString);
  CGSize extractedSize;
  extractedSize = CGSizeFromString(sizeString);
  furniture.rect = CGRectMake(extractedPoint.x,
                              extractedPoint.y,
                              extractedSize.width,
                              extractedSize.height);

  return furniture;
}

#pragma mark - Points

- (NSString*)jsonPointsWithPointsArray:(NSArray*)points {
  NSString* jsonPointsString;

  NSArray* processingJsonPoints;
  processingJsonPoints = [self jsonPointDictArrayFromPointsArray:points];

  NSData* jsonData =
      [NSJSONSerialization dataWithJSONObject:processingJsonPoints
                                      options:NSJSONWritingPrettyPrinted
                                        error:nil];

  jsonPointsString =
      [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

  return jsonPointsString;
}

- (NSArray*)jsonPointDictArrayFromPointsArray:(NSArray*)points {
  NSArray* jsonPointDictArray;

  NSMutableArray* processingJsonPoints;
  processingJsonPoints = [NSMutableArray array];

  for (MEDBeaconPoint* point in points) {
    NSDictionary* jsonPoint;
    jsonPoint = [self jsonDictFromPoint:point];
    [processingJsonPoints addObject:jsonPoint];
  }

  jsonPointDictArray = [processingJsonPoints copy];

  return jsonPointDictArray;
}

- (NSDictionary*)jsonDictFromPoint:(MEDBeaconPoint*)point {
  NSMutableDictionary* jsonPoint;
  jsonPoint = [NSMutableDictionary dictionary];
  jsonPoint = [NSMutableDictionary dictionaryWithDictionary:@{
    kP_POINTID : @(point.pointId),
    kP_POSITION : NSStringFromCGPoint(point.position)
  }];
  if (point.uuid) {
    [jsonPoint setValue:point.uuid forKey:kP_UUID];
    [jsonPoint setValue:@(point.minor) forKey:kP_MINOR];
    [jsonPoint setValue:@(point.major) forKey:kP_MAJOR];
  }

  [jsonPoint setValue:point.graphDict forKey:kP_GRAPH];

  return jsonPoint;
}

- (NSArray*)pointsWithJsonString:(NSString*)jsonString {
  NSArray* points;

  NSMutableArray* processingPoints;
  processingPoints = [NSMutableArray array];

  NSArray* parsedArray;
  parsedArray = [NSJSONSerialization
      JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                 options:NSJSONReadingAllowFragments
                   error:nil];

  for (NSDictionary* pointDict in parsedArray) {
    MEDBeaconPoint* point;
    point = [self pointFromDict:pointDict];
    [processingPoints addObject:point];
  }

  points = [processingPoints copy];

  return points;
}

- (MEDBeaconPoint*)pointFromDict:(NSDictionary*)pointDict {
  MEDBeaconPoint* point;

  point = [[MEDBeaconPoint alloc] init];
  point.pointId = [pointDict[kP_POINTID] integerValue];

  UInt16 minor, major;
  NSString* uuid;

  minor = [pointDict[kP_MINOR] integerValue];
  major = [pointDict[kP_MAJOR] integerValue];
  uuid = pointDict[kP_UUID];

  if (uuid) {
    point.minor = minor;
    point.major = major;
    point.uuid = uuid;
  }

  NSString* posString;
  posString = pointDict[kP_POSITION];
  CGPoint extractedPoint;
  extractedPoint = CGPointFromString(posString);

  point.position = extractedPoint;

  point.graphDict = pointDict[kP_GRAPH];

  return point;
}

@end
