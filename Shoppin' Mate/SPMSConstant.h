//
//  SPMSConstant.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#define kAddToListNotification @"addToListNotification"
#define kUpdatedUserProfileNotification @"updatedUserProfileNotification"
#define kSendSelectedCategoryFromFilterVCNotification \
  @"sendSelectedCategoryFromFilterVCNotification"
#define kSelectCategoryInCategoryListVCNotification \
  @"selectCategoryInCategoryListVCNotification"
#define kSendSelectedFloorFromFilterNotification \
  @"sendSelectedFloorFromFilterNotification"
#define kSelectFloorInFloorListVCNotification \
  @"selectFloorInFloorListVCNotification"
#define kStartOfflineOperationQueueNotification @"kOfflineOperationQueue"
#define NSUSERDEFAULTS_LOGGEDIN_KEY @"userLoggedIn"
#define NSUSERDEFAULTS_PROMOTION_COUNT_KEY @"promotionCountBadge"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define USER_GENDER_MALE 0
#define USER_GENDER_FEMALE 1
#define FB_API_READ_PERMISSIONS_NEEDED \
  @[ @"public_profile", @"email", @"user_birthday" ]
#define SW_SEARCH_DB_LOCAL_DATA_FILE @"SWSearchDB.json"
#define OFFLINE_REQUEST_QUEUE_DB_FILE @"Requests.db"
#define DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss.SS"
#define DATE_FORMAT_MAP @"yyyy-MM-dd HH:mm:ss"
#define DATE_FORMAT_PROMOTION_DATE @"MM-dd-yyyy"
#define WEB_SERVER_URL @"http://spms.sfone095.com"
//#define WEB_SERVER_URL @"http://10.12.1.8/web/"
#define ERROR_DOMAIN @"com.spms.ErrorDomain"
#define ERROR_CODE 381
#define ERROR_CODE_INVALID_GRANT 400
#define MAX_PRODUCT_PRICE_RANGE 5000
#define kOFFSET_FOR_KEYBOARD 216.0f
#define kMAP_ZOOM_SCALE_DEFAULT_VALUE 0.4f

#define kMAP_ZOOM_LEVEL 2

#define HTTP_CODE_NO401_UNAUTHORISED 401
