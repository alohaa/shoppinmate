//
//  SPMSScoringMatrix.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSScoringMatrix.h"

@implementation SPMSScoringMatrix
static SPMSScoringMatrix *scoringMatrix = nil;

/**
 *  Singleton Shared Object
 *
 *  @return ScoringMatrix
 */
+ (SPMSScoringMatrix *)sharedScoringMatrix
{
    if (scoringMatrix == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            scoringMatrix = [[self alloc] init];
        });
    }
    return scoringMatrix;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadDefaultValues];
        [self loadStructure];
    }
    return self;
}

- (void)loadDefaultValues
{
    _scorePerfectMatch = [self defaultValuesForEvent:ScoringEventPerfectMatch];
    _scoreNotPerfectMatchKeyboardAnalyseHelp = [self defaultValuesForEvent:ScoringEventNotPerfectMatchKeyboardAnalyseHelp];
    _scoreNotPerfectBecauseOfAccents = [self defaultValuesForEvent:ScoringEventNotPerfectBecauseOfAccents];
    _scoreLetterAddition = [self defaultValuesForEvent:ScoringEventLetterAddition];
}

- (void)loadStructure
{
    struct ScoringMatrixStruct scoringMatrixStruct;
    scoringMatrixStruct.scorePerfectMatch = (int)_scorePerfectMatch;
    scoringMatrixStruct.scoreNotPerfectBecauseOfAccents = (int)_scoreNotPerfectBecauseOfAccents;
    scoringMatrixStruct.scoreNotPerfectMatchKeyboardAnalyseHelp = (int)_scoreNotPerfectMatchKeyboardAnalyseHelp;
    scoringMatrixStruct.scoreLetterAddition = (int)_scoreLetterAddition;
    _structRepresentation = scoringMatrixStruct;
}

- (NSInteger)defaultValuesForEvent:(ScoringEvent)event
{
    switch (event) {
        case ScoringEventPerfectMatch:
            return 2;
            break;
            
        case ScoringEventNotPerfectMatchKeyboardAnalyseHelp:
            return 1;
            break;
            
        case ScoringEventNotPerfectBecauseOfAccents:
            return 2;
            break;
            
        case ScoringEventLetterAddition:
            return -2;
            break;
            
        default:
            break;
    }
    
    return NSNotFound;
}

@end
