//
//  NSString+Randomized.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/10/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "NSString+Randomized.h"
#include <stdlib.h>

#define DEFAULT_LENGTH 6

@implementation NSString (Randomized)

+ (NSString*)defaultAlphabet {
  return @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
}

+ (id)randomizedString {
  return [self randomizedStringWithAlphabet:[self defaultAlphabet]];
}

+ (id)randomizedStringWithAlphabet:(NSString*)alphabet {
  return [self randomizedStringWithAlphabet:alphabet length:DEFAULT_LENGTH];
}

+ (id)randomizedStringWithAlphabet:(NSString*)alphabet length:(NSUInteger)len {
  return [[self alloc] initWithAlphabet:alphabet length:len];
}

- (id)initWithDefaultAlphabet {
  return [self initWithAlphabet:[NSString defaultAlphabet]];
}

- (id)initWithAlphabet:(NSString*)alphabet {
  return [self initWithAlphabet:alphabet length:DEFAULT_LENGTH];
}

- (id)initWithAlphabet:(NSString*)alphabet length:(NSUInteger)len {
  NSMutableString* s = [NSMutableString stringWithCapacity:len];
  for (NSUInteger i = 0U; i < len; i++) {
    u_int32_t r = arc4random() % [alphabet length];
    unichar c = [alphabet characterAtIndex:r];
    [s appendFormat:@"%C", c];
  }
  return [s copy];
}

@end
