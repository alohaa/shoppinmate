//
//  SPMSUserProfileViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/26/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSUserProfileViewController.h"
#import <UIImageView+WebCache.h>
#import "SPMSUpdateProfileViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface SPMSUserProfileViewController ()

@end

@implementation SPMSUserProfileViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
  [super viewDidLoad];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(loadUserData:)
             name:kUpdatedUserProfileNotification
           object:nil];
  [self checkLoginAndUpdateViewIfNeccessary];
  [self configAppearance];
  [self loadUserData:nil];
}

#pragma mark - Load Data
- (void)loadUserData:(id)sender {
  dispatch_async(dispatch_get_main_queue(), ^{
      User* user = [User MR_findFirst];
      if (user) {
        [self.userAvatarImageView
            sd_setImageWithURL:[NSURL URLWithString:user.imageURL]
              placeholderImage:[UIImage imageNamed:@"UserDefault"]];
        self.userFullnameLabel.text = user.fullName;
        self.userEmailLabel.text =
            [NSString stringWithFormat:@"%@", user.email];
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        [formatter setDateStyle:NSDateFormatterLongStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
        self.userDOBLabel.text = [formatter stringFromDate:user.dateOfBirth];
      } else {
        [self.userAvatarImageView setImage:[UIImage imageNamed:@"UserDefault"]];
        self.userFullnameLabel.text = @"User's Fullname";
        self.userEmailLabel.text = @"User's Email";
        self.userDOBLabel.text = @"User's Date of Birth";
      }
  });
}

#pragma mark - Appearance Configuration
- (void)configAppearance {
  [self.userAvatarImageView setClipsToBounds:YES];
  // Create Circular User Avatar Image View
  self.userAvatarImageView.layer.cornerRadius =
      self.userAvatarImageView.frame.size.width / 2;
  self.userAvatarImageView.clipsToBounds = YES;

  // Add Border
  self.userAvatarImageView.layer.borderWidth = 2.0f;
  self.userAvatarImageView.layer.borderColor =
      [UIColor colorWithRed:0.925 green:0.941 blue:0.945 alpha:1.000].CGColor;
  // Config Sidebar
  [self configNavigationBar];
}

- (void)checkLoginAndUpdateViewIfNeccessary {
  // Check whether the user is logged-in or not to display corresponding view
  RACSignal* loggedInSignal = [[NSUserDefaults standardUserDefaults]
      rac_channelTerminalForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  [loggedInSignal subscribeNext:^(NSNumber* isLoggedIn) {
      [self loadUserData:nil];
      if (isLoggedIn) {
        [self.navigationItem.rightBarButtonItem
            setEnabled:[isLoggedIn boolValue]];
      }
  }];
}

#pragma mark - Sidebar Configuration
- (void)configNavigationBar {
  // "Show Menu" Button
  UIButton* showMenuBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  showMenuBTN.frame = CGRectMake(0, 0, 30, 30);
  [showMenuBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_ShowMenu_ICO"]
               forState:UIControlStateNormal];
  [showMenuBTN addTarget:self
                  action:@selector(presentLeftMenuViewController:)
        forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showMenuBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:showMenuBTN];

  [self.navigationItem setLeftBarButtonItem:showMenuBTNItem];
  // "Show Shopping List" Button
  UIButton* showShoppingListBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  showShoppingListBTN.frame = CGRectMake(0, 0, 30, 30);
  [showShoppingListBTN
      setImage:[UIImage imageNamed:@"UserProfile_EditProfile_ICO"]
      forState:UIControlStateNormal];
  [showShoppingListBTN addTarget:self
                          action:@selector(navigateToEditProfileVC:)
                forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showShoppingListBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:showShoppingListBTN];

  [self.navigationItem setRightBarButtonItem:showShoppingListBTNItem];
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)navigateToEditProfileVC:(id)sender {
  [self performSegueWithIdentifier:@"editUserProfile" sender:nil];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"editUserProfile"]) {
    SPMSUpdateProfileViewController* updateProfileVC =
        [segue destinationViewController];
    [updateProfileVC setUserFullname:self.userFullnameLabel.text];
    [updateProfileVC setUserDOB:self.userDOBLabel.text];
    [updateProfileVC setUserAvatarImage:self.userAvatarImageView.image];
  }
}

#pragma mark - Other
- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kUpdatedUserProfileNotification
              object:nil];
}
@end
