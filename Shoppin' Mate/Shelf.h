//
//  Shelf.h
//  Shoppin' Mate
//
//  Created by El Desperado on 8/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Map;

@interface Shelf : NSManagedObject

@property (nonatomic, retain) NSNumber * furnitureId;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * pointId;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * xCoordinate;
@property (nonatomic, retain) NSNumber * yCoordinate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Map *map;

@end
