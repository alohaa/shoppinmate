//
//  SPMSOAuthClient.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/15/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

typedef void (^SPMSSignInResponse)(BOOL);

@class SPMSOAuthAccessToken;

@interface SPMSOAuthClient : NSObject {
 @protected
  BOOL persistent;
  SPMSOAuthAccessToken* accessToken;
}
@property(nonatomic, strong) SPMSOAuthAccessToken* accessToken;
@property(nonatomic) BOOL isAuthenticated;
/*!
 * If set to NO, the access token is not stored any keychain, will be removed if
 * it was.
 * Defaults to YES
 */
@property(nonatomic, assign, readwrite, getter=isPersistent) BOOL persistent;

+ (id)sharedInstance;

- (id)initWithPersistent:(BOOL)shouldPersist;
- (BOOL)isHadAccessToken;
/*!
 * Authenticate with username & password (User Credentials Flow)
 */
- (RACSignal*)authenticateWithEmail:(NSString*)email
                           password:(NSString*)password;
- (RACSignal*)authenticateWithFacebookToken:(NSString*)token;
- (RACSignal*)doRequestAndRefreshTokenIfNecessary:(RACSignal*)requestSignal;

@end
