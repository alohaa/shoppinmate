//
//  MEDFurniture.h
//  MapEditor
//
//  Created by Dat Truong on 2014-07-22.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEDFurniture : NSObject

@property UInt16 furnitureId;

@property UInt16 pointId;
@property UInt16 stencilId;
@property UInt16 zPos;
@property BOOL flippedHorizontal;
@property BOOL flippedVertical;
@property(nonatomic, strong) NSString* name;

@property CGRect rect;

@end
