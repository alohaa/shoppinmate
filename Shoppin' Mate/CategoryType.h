//
//  CategoryType.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryType : NSObject

@property(nonatomic, strong) NSString* categoryID;
@property(nonatomic, strong) NSString* categoryName;

+ (CategoryType*)categoryTypeForDictionary:(NSDictionary*)dictionary;

@end
