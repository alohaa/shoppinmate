//
//  Map.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/23/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPMSMapOperator : NSObject

+ (SPMSMapOperator*)sharedService;
- (void)saveMapForDictionary:(NSDictionary*)dictionary;

@end
