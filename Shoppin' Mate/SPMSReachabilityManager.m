//
//  SPMSReachabilityManager.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/20/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSReachabilityManager.h"
#import "Reachability.h"

@implementation SPMSReachabilityManager

#pragma mark Default Manager
+ (SPMSReachabilityManager*)sharedManager {
  static SPMSReachabilityManager* _sharedManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{ _sharedManager = [[self alloc] init]; });

  return _sharedManager;
}

#pragma mark -
#pragma mark Memory Management
- (void)dealloc {
  // Stop Notifier
  if (_reachability) {
    [_reachability stopNotifier];
  }
}

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable {
  return [[[SPMSReachabilityManager sharedManager] reachability] isReachable];
}

+ (BOOL)isUnreachable {
  return ![[[SPMSReachabilityManager sharedManager] reachability] isReachable];
}

+ (BOOL)isReachableViaWWAN {
  return [[
      [SPMSReachabilityManager sharedManager] reachability] isReachableViaWWAN];
}

+ (BOOL)isReachableViaWiFi {
  return [[
      [SPMSReachabilityManager sharedManager] reachability] isReachableViaWiFi];
}

#pragma mark -
#pragma mark Private Initialization
- (id)init {
  self = [super init];

  if (self) {
    // Initialize Reachability
    self.reachability = [Reachability reachabilityForLocalWiFi];
    // Start Monitoring
    [self.reachability startNotifier];
  }

  return self;
}

@end
