//
//  Beacon.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/31/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Beacon.h"
#import "Map.h"


@implementation Beacon

@dynamic graphData;
@dynamic major;
@dynamic minor;
@dynamic pointId;
@dynamic uuid;
@dynamic xCoordinate;
@dynamic yCoordinate;
@dynamic map;

@end
