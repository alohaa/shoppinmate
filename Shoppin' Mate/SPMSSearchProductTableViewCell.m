//
//  SPMSSearchProductTableViewCell.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSSearchProductTableViewCell.h"

#import <UIImageView+WebCache.h>
#import "NSDictionary+Verified.h"
#import "SPMSCurrencyFormatter.h"

@implementation SPMSSearchProductTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString*)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    productNameLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(85, 10, 215, 20)];
    [productNameLabel
        setTextColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
    [productNameLabel
        setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f]];
    productDiscountLabel =
        [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(85, 35, 215, 20)];
    [productDiscountLabel
        setTextColor:
            [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000]];
    [productDiscountLabel
        setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:11.0f]];
    productPriceLabel =
        [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(85, 50, 215, 20)];
    [productPriceLabel
        setTextColor:
            [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000]];
    [productPriceLabel
        setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:11.0f]];

    productDescLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(85, 70, 215, 50)];
    [productDescLabel
        setTextColor:
            [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000]];
    [productDescLabel
        setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:11.0f]];
    [productDescLabel setNumberOfLines:3];
    [productDescLabel setLineBreakMode:NSLineBreakByWordWrapping];

    productImageView =
        [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 50, 50)];
    [productImageView setClipsToBounds:YES];
    // Create Circular User Avatar Image View
    productImageView.layer.cornerRadius = productImageView.frame.size.width / 2;

    // Add Border
    productImageView.layer.borderWidth = 2.0f;
    productImageView.layer.borderColor =
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000].CGColor;

    [self.contentView addSubview:productImageView];
    [self.contentView addSubview:productNameLabel];
    [self.contentView addSubview:productDiscountLabel];
    [self.contentView addSubview:productPriceLabel];
    [self.contentView addSubview:productDescLabel];
  }
  return self;
}

- (void)setSearchObject:(SPMSSWSearchObject*)searchObject {
  NSDictionary* data = [searchObject refencedObject];
  productNameLabel.text = [data verifiedObjectForKey:@"ProductName"];
  double productPrice = [[data verifiedObjectForKey:@"Price"] doubleValue];
  double discount = [[data verifiedObjectForKey:@"Discount"] doubleValue];
  BOOL hasPromotion = [[data verifiedObjectForKey:@"HasPromotion"] boolValue];
  NSString* discountString = [NSString
      stringWithFormat:@"Discount: %.0f%%", discount > 0 ? discount : 0];
  [self setProductDiscountLabelWithString:discountString];
  double newPrice = 0.0;
  BOOL isDiscounted = (hasPromotion && discount > 0);
  NSString* productPriceString;
  NSNumberFormatter* currencyFormatter = [SPMSCurrencyFormatter formatter];
  NSString* priceString = [currencyFormatter
      stringFromNumber:[NSNumber numberWithDouble:productPrice]];
  if (isDiscounted) {
    newPrice = productPrice * (100 - discount) / 100;
    NSString* newPriceString = [currencyFormatter
        stringFromNumber:[NSNumber numberWithDouble:newPrice]];
    productPriceString = [NSString
        stringWithFormat:@"Price:/%@/(%@)", priceString, newPriceString];
  } else {
    productPriceString = [NSString stringWithFormat:@"Price:/%@/", priceString];
  }

  [self setProductPriceLabelWithString:productPriceString];
  productDescLabel.text = [data verifiedObjectForKey:@"Description"];
  [productImageView
      sd_setImageWithURL:[NSURL
                             URLWithString:[data verifiedObjectForKey:@"Image"]]
        placeholderImage:[UIImage imageNamed:@"Photo"]];
}

- (void)setProductPriceLabelWithString:(NSString*)productPriceString {
  NSArray* stringArray = [productPriceString componentsSeparatedByString:@"/"];
  NSString* edittedString =
      [productPriceString stringByReplacingOccurrencesOfString:@"/"
                                                    withString:@" "];
  [productPriceLabel setText:edittedString
      afterInheritingLabelAttributesAndConfiguringWithBlock:
          ^NSMutableAttributedString *
          (NSMutableAttributedString * mutableAttributedString) {

              __block NSRange placeHolderTextRange;
              __block NSRange originalPriceRange;
              __block NSRange discountedPriceRange;
              [stringArray eachWithIndex:^(id object, NSUInteger index) {
                  NSString* string = (NSString*)object;
                  switch (index) {
                    case 0: {
                      placeHolderTextRange = [[mutableAttributedString string]
                          rangeOfString:string
                                options:NSCaseInsensitiveSearch];
                      break;
                    }
                    case 1: {
                      originalPriceRange = [[mutableAttributedString string]
                          rangeOfString:string
                                options:NSCaseInsensitiveSearch];
                      break;
                    }
                    case 2: {
                      discountedPriceRange = [[mutableAttributedString string]
                          rangeOfString:string
                                options:NSCaseInsensitiveSearch];
                      break;
                    }
                    default:
                      break;
                  }
              }];
              // Core Text APIs use C functions without a direct bridge to
              // UIFont. See Apple's "Core Text Programming Guide" to learn how
              // to configure string attributes.
              UIFont* boldSystemFont =
                  [UIFont fontWithName:@"HelveticaNeue-Bold" size:11.0f];
              CTFontRef boldFont = CTFontCreateWithName(
                  (__bridge CFStringRef)boldSystemFont.fontName,
                  boldSystemFont.pointSize,
                  NULL);
              if (boldFont) {
                if (discountedPriceRange.length == 0) {
                  [mutableAttributedString
                      addAttribute:(NSString*)kCTFontAttributeName
                             value:(__bridge id)boldFont
                             range:originalPriceRange];

                } else {
                  [mutableAttributedString
                      addAttribute:(NSString*)kCTFontAttributeName
                             value:(__bridge id)boldFont
                             range:discountedPriceRange];
                  [mutableAttributedString
                      addAttribute:kTTTStrikeOutAttributeName
                             value:[NSNumber numberWithBool:YES]
                             range:originalPriceRange];
                }
                CFRelease(boldFont);
              }
              [mutableAttributedString
                  addAttribute:(NSString*)kCTForegroundColorAttributeName
                         value:[UIColor colorWithRed:0.498
                                               green:0.549
                                                blue:0.553
                                               alpha:1.000]
                         range:placeHolderTextRange];

              return mutableAttributedString;
          }];
}

- (void)setProductDiscountLabelWithString:(NSString*)productDiscountString {
  [productDiscountLabel setText:productDiscountString
      afterInheritingLabelAttributesAndConfiguringWithBlock:
          ^NSMutableAttributedString *
          (NSMutableAttributedString * mutableAttributedString) {
              NSRange placeHolderRange = [[mutableAttributedString string]
                  rangeOfString:@"Discount: "
                        options:NSCaseInsensitiveSearch];
              NSRange boldRange = NSMakeRange(
                  placeHolderRange.location + placeHolderRange.length,
                  productDiscountString.length - placeHolderRange.length);

              UIFont* boldSystemFont =
                  [UIFont fontWithName:@"HelveticaNeue-Bold" size:11.0f];
              CTFontRef font = CTFontCreateWithName(
                  (__bridge CFStringRef)boldSystemFont.fontName,
                  boldSystemFont.pointSize,
                  NULL);
              if (font) {
                [mutableAttributedString
                    addAttribute:(NSString*)kCTFontAttributeName
                           value:(__bridge id)font
                           range:boldRange];
                CFRelease(font);
              }
              [mutableAttributedString
                  addAttribute:(NSString*)kCTForegroundColorAttributeName
                         value:[UIColor colorWithRed:0.498
                                               green:0.549
                                                blue:0.553
                                               alpha:1.000]
                         range:placeHolderRange];
              return mutableAttributedString;
          }];
}

@end
