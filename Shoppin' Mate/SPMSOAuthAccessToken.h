//
//  SPMSOAuthAccessToken.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/15/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

@interface SPMSOAuthAccessToken : NSObject <NSCoding>
{
@private
    NSString *accessToken;
    NSString *refreshToken;
    NSString *tokenType;
    NSDate *expiresAt;
    NSSet *scope;
    NSString *responseBody;
}
@property (nonatomic, readonly) NSString *accessToken;
@property (nonatomic, readonly) NSString *refreshToken;
@property (nonatomic, readonly) NSString *tokenType;
@property (nonatomic, readonly) NSDate *expiresAt;
@property (nonatomic, readonly) BOOL doesExpire;
@property (nonatomic, readonly) BOOL hasExpired;
@property (nonatomic, readonly) NSSet *scope;
@property (nonatomic, readonly) NSString *responseBody;

+ (id)tokenWithResponseBody:(NSDictionary *)responseBody;
+ (id)tokenWithResponseBody:(NSDictionary *)responseBody tokenType:(NSString *)tokenType;

- (id)initWithAccessToken:(NSString *)accessToken;
- (id)initWithAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken expiresAt:(NSDate *)expiryDate;
- (id)initWithAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken expiresAt:(NSDate *)expiryDate scope:(NSSet *)scope;
- (id)initWithAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken expiresAt:(NSDate *)expiryDate scope:(NSSet *)scope responseBody:(NSString *)responseBody;
- (id)initWithAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken expiresAt:(NSDate *)expiryDate scope:(NSSet *)scope responseBody:(NSString *)responseBody tokenType:(NSString*)tokenType;

- (void)restoreWithOldToken:(SPMSOAuthAccessToken *)oldToken;

#pragma mark Keychain Support
+ (id)tokenFromDefaultKeychain;
- (void)storeInDefaultKeychain;
- (void)removeFromDefaultKeychain;
@end
