//
//  SPMSScoringMatrix.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWAligmentMethod.h"

typedef enum {
    ScoringEventPerfectMatch = 0,
    ScoringEventNotPerfectMatchKeyboardAnalyseHelp,
    ScoringEventNotPerfectBecauseOfAccents,
    ScoringEventLetterAddition
} ScoringEvent;

@interface SPMSScoringMatrix : NSObject

@property (assign, nonatomic) NSInteger scorePerfectMatch;
@property (assign, nonatomic) NSInteger scoreNotPerfectMatchKeyboardAnalyseHelp;
@property (assign, nonatomic) NSInteger scoreNotPerfectBecauseOfAccents;
@property (assign, nonatomic) NSInteger scoreLetterAddition;
@property (assign, nonatomic) ScoringMatrixStruct structRepresentation;

+ (SPMSScoringMatrix *)sharedScoringMatrix;
- (void)loadDefaultValues;
- (void)loadStructure;
- (NSInteger)defaultValuesForEvent:(ScoringEvent)event;

@end
