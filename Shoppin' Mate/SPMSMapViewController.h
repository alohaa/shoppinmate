//
//  SPMSMapViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/9/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class SMDBeacon;

@interface SPMSMapViewController : UIViewController<CLLocationManagerDelegate>

@property(strong, nonatomic) SMDBeacon* currentBeacon;

@property(strong, nonatomic) NSArray* path;
@property(strong, nonatomic) NSMutableArray* beacons;
@property(assign, nonatomic) NSNumber* floor;

- (IBAction)detectBeaconButtonTapped:(id)sender;

@end
