//
//  NSString+Randomized.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/10/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Randomized)

+ (NSString*)defaultAlphabet;
+ (id)randomizedString;
+ (id)randomizedStringWithAlphabet:(NSString*)alphabet;
+ (id)randomizedStringWithAlphabet:(NSString*)alphabet length:(NSUInteger)len;
- (id)initWithDefaultAlphabet;
- (id)initWithAlphabet:(NSString*)alphabet;
- (id)initWithAlphabet:(NSString*)alphabet length:(NSUInteger)len;

@end
