//
//  MEDFurniture.m
//  MapEditor
//
//  Created by Dat Truong on 2014-07-22.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "MEDFurniture.h"

@implementation MEDFurniture

- (NSString*)name {
  if (!_name) {
    _name = @"";
  }
  return _name;
}

@end
