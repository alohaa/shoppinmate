//
//  SPMSFacebookServiceObject.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/9/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSFacebookService.h"
#import "User.h"
#import "NSDictionary+Verified.h"
#import "SPMSWebDataService.h"
#import "NSString+Randomized.h"
#import "SPMSOAuthClient.h"
#import <SDWebImageDownloader.h>

@implementation SPMSFacebookService

#pragma mark - Singleton Class Init
+ (id)sharedFBServiceInstance {
  static SPMSFacebookService* sharedFBServiceInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken,
                ^{ sharedFBServiceInstance = [[self alloc] init]; });
  return sharedFBServiceInstance;
}

#pragma mark - Session
/**
 *  Check for a cached session
 */
- (void)checkForCachedSession {
  // Whenever a person opens the app, check for a cached session
  if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
    // If there's one, just open the session silently, without showing the user
    // the login UI
    [FBSession
        openActiveSessionWithReadPermissions:FB_API_READ_PERMISSIONS_NEEDED
                                allowLoginUI:NO
                           completionHandler:^(FBSession* session,
                                               FBSessionState state,
                                               NSError* error) {}];
  }
}

// This method will handle ALL the session state changes in the app
- (void)sessionStateChanged:(FBSession*)session
                      state:(FBSessionState)state
                      error:(NSError*)error {
  // If the session was opened successfully
  if (!error && state == FBSessionStateOpen) {
    // Show the user the logged-in UI
    [[[self signInViaFacebookSignal] deliverOn:RACScheduler.mainThreadScheduler]
        subscribeNext:^(id x) { return; }];
  }
  if (state == FBSessionStateClosed ||
      state == FBSessionStateClosedLoginFailed) {
    // If the session is closed
    // Show the user the logged-out UI
    [[[self logoutFB] deliverOn:RACScheduler.mainThreadScheduler]
        subscribeCompleted:^{//
                           }];
  }

  // Handle errors
  if (error) {
    NSString* alertText;
    NSString* alertTitle;
    // If the error requires people using an app to make an action outside of
    // the app in order to recover
    if ([FBErrorUtility shouldNotifyUserForError:error] == YES) {
      alertTitle = @"Something went wrong";
      alertText = [FBErrorUtility userMessageForError:error];
      [self showMessage:alertText withTitle:alertTitle];
    } else {
      // If the user cancelled login, do nothing
      if ([FBErrorUtility errorCategoryForError:error] ==
          FBErrorCategoryUserCancelled) {
        // Handle session closures that happen outside of the app
      } else if ([FBErrorUtility errorCategoryForError:error] ==
                 FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertText =
            @"Your current session is no longer valid. Please log in again.";
        [self showMessage:alertText withTitle:alertTitle];

        // For simplicity, here we just show a generic message for all other
        // errors
        // You can learn how to handle other errors using our guide:
        // https://developers.facebook.com/docs/ios/errors
      } else {
        // Get more error information from the error
        NSDictionary* errorInformation = [[[error.userInfo
            objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"]
            objectForKey:@"body"] objectForKey:@"error"];

        // Show the user an error message
        alertTitle = @"Something went wrong";
        alertText = [NSString
            stringWithFormat:@"Please retry. \n\n If the problem persists "
                             @"contact us and mention this error code: %@",
                             [errorInformation objectForKey:@"message"]];
        [self showMessage:alertText withTitle:alertTitle];
      }
    }
    // Clear this token
    [FBSession.activeSession closeAndClearTokenInformation];
    // Show the user the logged-out UI
    [[[self logoutFB] deliverOn:RACScheduler.mainThreadScheduler]
        subscribeCompleted:^{//
                           }];
  }
}

// Show an alert message
- (void)showMessage:(NSString*)text withTitle:(NSString*)title {
  [[[UIAlertView alloc] initWithTitle:title
                              message:text
                             delegate:self
                    cancelButtonTitle:@"OK!"
                    otherButtonTitles:nil] show];
}

#pragma mark - Logout
- (RACSignal*)logoutFB {
  return
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          // Check FBLogin
          if ([self isLoggedIn]) {
            [[FBSession activeSession] closeAndClearTokenInformation];
          }
          // Truncate all User Information in "User" Entity in Core Date
          [User MR_truncateAll];
          [[NSManagedObjectContext
                  MR_defaultContext] MR_saveToPersistentStoreAndWait];
          // Remove Access Token
          [[SPMSOAuthClient sharedInstance] setAccessToken:nil];

          [[NSUserDefaults standardUserDefaults]
              setBool:NO
               forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
          [[NSUserDefaults standardUserDefaults] synchronize];
          [subscriber sendCompleted];
          return nil;
      }];
}

#pragma mark - Login & Authentication
- (RACSignal*)loginFBSignal {
  return [RACSignal createSignal:^RACDisposable *
                                 (id<RACSubscriber> subscriber) {
      [FBSession
          openActiveSessionWithReadPermissions:FB_API_READ_PERMISSIONS_NEEDED
                                  allowLoginUI:YES
                             completionHandler:^(FBSession* session,
                                                 FBSessionState state,
                                                 NSError* error) {
                                 [self sessionStateChanged:session
                                                     state:state
                                                     error:error];
                                 if (error) {
                                   [subscriber sendError:error];
                                 } else {
                                   [subscriber sendNext:@(YES)];
                                   [subscriber sendCompleted];
                                 }
                             }];
      return nil;
  }];
}

- (RACSignal*)signInViaFacebookSignal {
  RACSignal* signal = [self signalToGetUserProfileInformation];
  return [RACSignal createSignal:^RACDisposable *
                                 (id<RACSubscriber> subscriber) {
      [signal subscribeNext:^(id result) {
          NSString* email = [result verifiedObjectForKey:@"email"];
          // Check whether current FB Account with Email is registered or not
          [[[[SPMSWebDataService sharedService]
              checkExistedAccountWithEmail:email]
              deliverOn:
                  RACScheduler
                      .mainThreadScheduler] subscribeNext:^(NSNumber*
                                                                isSucceed) {
              BOOL isExistedUserAccount = [isSucceed boolValue];
              NSString* email = [result verifiedObjectForKey:@"email"];
              NSString* fullName = [result verifiedObjectForKey:@"name"];
              NSString* accessToken =
                  [[[FBSession activeSession] accessTokenData] accessToken];
              if (isExistedUserAccount) {
                // Get Access Token from Server with FB's Access Token
                [[[[SPMSOAuthClient sharedInstance]
                    authenticateWithFacebookToken:accessToken]
                    deliverOn:RACScheduler.mainThreadScheduler]
                    subscribeNext:^(NSNumber* isSucceed) {
                        if ([isSucceed boolValue]) {
                          // Save to NSUserDefaults
                          [[NSUserDefaults standardUserDefaults]
                              setBool:YES
                               forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
                          [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                    }];

              } else {
                // If not registered yet, register a new account
                NSString* randomPasswordString = [NSString randomizedString];
                [[[[SPMSWebDataService sharedService]
                    registerAccountWithEmail:email
                                    password:randomPasswordString
                                    fullName:fullName
                                 avatarImage:nil]
                    deliverOn:RACScheduler
                                  .mainThreadScheduler] subscribeNext:^(id x) {
                    // Get Access Token from Server with FB's Access Token
                    [[[[SPMSOAuthClient sharedInstance]
                        authenticateWithFacebookToken:accessToken]
                        deliverOn:RACScheduler.mainThreadScheduler]
                        subscribeNext:^(NSNumber* isSucceed) {
                            if ([isSucceed boolValue]) {
                              User* user =
                                  [User MR_findFirstByAttribute:@"email"
                                                      withValue:email];
                              NSString* avatarImageURL = [
                                  [self urlForProfilePictureSizeNormalForFBID:
                                            [result verifiedObjectForKey:
                                                        @"id"]] absoluteString];
                              user.imageURL = avatarImageURL;
                              [[NSManagedObjectContext
                                      MR_defaultContext] MR_saveToPersistentStoreAndWait];
                              if (user) {
                                // Update User Profile
                                [self updateUserProfileIfNeccessaryWithUser:
                                          user result:result];
                              }

                              // Save to NSUserDefaults
                              [[NSUserDefaults standardUserDefaults]
                                  setBool:YES
                                   forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
                              [[NSUserDefaults
                                      standardUserDefaults] synchronize];
                            }
                        }];
                }];
              }
          }];
      }];
      return nil;
  }];
}

- (void)updateUserProfileIfNeccessaryWithUser:(User*)user result:(id)result {
  NSString* avatarImageURL =
      [[self urlForProfilePictureSizeNormalForFBID:
                 [result verifiedObjectForKey:@"id"]] absoluteString];
  [SDWebImageDownloader.sharedDownloader
      downloadImageWithURL:[NSURL URLWithString:avatarImageURL]
                   options:0
                  progress:nil
                 completed:^(UIImage* image,
                             NSData* data,
                             NSError* error,
                             BOOL finished) {
                     if (image && finished) {
                       NSNumber* gender =
                           [[result verifiedObjectForKey:@"gender"]
                               isEqualToString:@"male"]
                               ? [NSNumber numberWithInt:USER_GENDER_MALE]
                               : [NSNumber numberWithInt:USER_GENDER_FEMALE];
                       NSString* dobString =
                           [result verifiedObjectForKey:@"birthday"];
                       NSDateFormatter* dateFormatter =
                           [[NSDateFormatter alloc] init];
                       [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                       dateFormatter.locale = [[NSLocale alloc]
                           initWithLocaleIdentifier:@"en_US_POSIX"];
                       NSDate* dateOfBirth =
                           [dateFormatter dateFromString:dobString];
                       [[[[SPMSWebDataService sharedService]
                           updateAccountWithFullName:nil
                                         avatarImage:image
                                              gender:gender
                                         dateOfBirth:dateOfBirth]
                           deliverOn:[RACScheduler scheduler]]
                           subscribeNext:^(id x) {//
                                         }];
                     }
                 }];
}

#pragma mark - Inquiries
- (BOOL)isSessionStateEffectivelyLoggedIn:(FBSessionState)state {
  BOOL effectivelyLoggedIn;

  switch (state) {
    case FBSessionStateOpen:
      effectivelyLoggedIn = YES;
      break;
    case FBSessionStateCreatedTokenLoaded:
      effectivelyLoggedIn = YES;
      break;
    case FBSessionStateOpenTokenExtended:
      effectivelyLoggedIn = YES;
      break;
    default:
      effectivelyLoggedIn = NO;
      break;
  }

  return effectivelyLoggedIn;
}

/**
 * Determines if the Facebook session has an authorized state. It might still
 * need to be opened if it is a cached
 * token, but the purpose of this call is to determine if the user is authorized
 * at least that they will not be
 * explicitly asked anything.
 */
- (BOOL)isLoggedIn {
  FBSession* activeSession = [FBSession activeSession];
  FBSessionState state = activeSession.state;
  BOOL isLoggedIn =
      activeSession && [self isSessionStateEffectivelyLoggedIn:state];
  return isLoggedIn;
}

/**
 * Attempts to silently open the Facebook session if we have a valid token
 * loaded (that perhaps needs a behind the scenes refresh).
 * After that attempt, we defer to the basic concept of the session being in one
 * of the valid authorized states.
 */
- (BOOL)isLoggedInAfterOpenAttempt {
  // If we don't have a cached token, a call to open here would cause UX for
  // login to
  // occur; we don't want that to happen unless the user clicks the login button
  // over in Settings, and so
  // we check here to make sure we have a token before calling open

  return [self isLoggedIn];
}

- (RACSignal*)signalToGetUserProfileInformation {
  return
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          [FBRequestConnection
              startForMeWithCompletionHandler:^(FBRequestConnection* connection,
                                                id result,
                                                NSError* error) {
                  if (error) {
                    [subscriber sendError:error];
                  } else {
                    [subscriber sendNext:result];
                  }
              }];
          return nil;
      }];
}

- (NSURL*)urlForProfilePictureSizeLargeForFBID:(NSString*)facebookID {
  return [self urlForProfilePictureOfSize:@"large" ForFBID:facebookID];
}

- (NSURL*)urlForProfilePictureSizeNormalForFBID:(NSString*)facebookID {
  return [self urlForProfilePictureOfSize:@"normal" ForFBID:facebookID];
}

- (NSURL*)urlForProfilePictureSizeSmallForFBID:(NSString*)facebookID {
  return [self urlForProfilePictureOfSize:@"small" ForFBID:facebookID];
}

- (NSURL*)urlForProfilePictureSizeSquareForFBID:(NSString*)facebookID {
  return [self urlForProfilePictureOfSize:@"square" ForFBID:facebookID];
}

- (NSURL*)urlForProfilePictureWithWidthClosestTo:(NSInteger)pixels
                                         ForFBID:(NSString*)facebookID {
  return [self
      urlForProfilePictureForFBID:facebookID
                        WithParam:[NSString stringWithFormat:@"width=%ld",
                                                             (long)pixels]];
}

- (NSURL*)urlForProfilePictureWithHeightClosestTo:(NSInteger)pixels
                                          ForFBID:(NSString*)facebookID {
  return [self
      urlForProfilePictureForFBID:facebookID
                        WithParam:[NSString stringWithFormat:@"height=%ld",
                                                             (long)pixels]];
}

- (NSURL*)urlForProfilePictureOfSize:(NSString*)size
                             ForFBID:(NSString*)facebookID {
  return [self
      urlForProfilePictureForFBID:facebookID
                        WithParam:[NSString stringWithFormat:@"type=%@", size]];
}

- (NSURL*)urlForProfilePictureForFBID:(NSString*)facebookID
                            WithParam:(NSString*)param {
  return [NSURL
      URLWithString:
          [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?%@",
                                     facebookID,
                                     param]];
}

- (void)requestURLForCoverPhotoSizeNormalForFBID:(NSString*)facebookID
                               completionHandler:
                                   (void (^)(NSURL* url,
                                             NSError* error))completion {
  NSString* urlAsString =
      [NSString stringWithFormat:@"http://graph.facebook.com/%@?fields=cover",
                                 facebookID];
  NSURL* url = [[NSURL alloc] initWithString:urlAsString];

  [NSURLConnection
      sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url]
                        queue:[[NSOperationQueue alloc] init]
            completionHandler:^(NSURLResponse* response,
                                NSData* data,
                                NSError* error) {
                NSURL* coverURL;
                if (data) {
                  NSDictionary* jsonDict =
                      [NSJSONSerialization JSONObjectWithData:data
                                                      options:0
                                                        error:&error];
                  coverURL =
                      [NSURL URLWithString:jsonDict[@"cover"][@"source"]];
                }
                completion(coverURL, error);
            }];
}

@end
