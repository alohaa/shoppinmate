//
//  SPMSShoppingListDataService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/21/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ShoppingList.h"
#import "Product.h"

@interface SPMSShoppingListDataService : NSObject

+ (SPMSShoppingListDataService*)sharedService;
- (RACSignal*)fetchShoppingListsWithPageNumber:(NSUInteger)pageNumber
                                       baseUrl:(NSURL*)baseUrl
                                        client:(NSURLSession*)client;
- (RACSignal*)shoppingListForSignal:(RACSignal*)signal;
- (RACSignal*)addProduct:(Product*)product
          toShoppingList:(ShoppingList*)shoppingList
                 baseUrl:(NSURL*)baseUrl
                  client:(NSURLSession*)client;
- (RACSignal*)removeProduct:(Product*)product
           fromShoppingList:(ShoppingList*)shoppingList
                    baseUrl:(NSURL*)baseUrl
                     client:(NSURLSession*)client;
- (RACSignal*)addNewShoppingListWithName:(NSString*)shoppingListName
                                 baseUrl:(NSURL*)baseUrl
                                  client:(NSURLSession*)client;
- (RACSignal*)editShoppingListWithShoppingListId:(NSNumber*)shoppingListId
                                shoppingListName:(NSString*)shoppingListName
                                         baseUrl:(NSURL*)baseUrl
                                          client:(NSURLSession*)client;
- (RACSignal*)deleteShoppingListWithShoppingListId:(NSNumber*)shoppingListId
                                           baseUrl:(NSURL*)baseUrl
                                            client:(NSURLSession*)client;
@end
