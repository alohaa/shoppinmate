//
//  SPMSOAuthAccessToken.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/15/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSOAuthAccessToken.h"
#import <SSKeychain/SSKeychain.h>
#import "User.h"
#import "NSDictionary+Verified.h"
#import "NSData+Base64.h"

@implementation SPMSOAuthAccessToken
#pragma mark Accessors
@synthesize accessToken;
@synthesize refreshToken;
@synthesize expiresAt;
@synthesize scope;
@synthesize responseBody;
@synthesize tokenType;

- (NSString*)tokenType {
  if (tokenType == nil || [tokenType isEqualToString:@""]) {
    // fall back on OAuth if token type not set
    return @"OAuth";
  } else if ([tokenType isEqualToString:@"bearer"]) {
    // this is for out case sensitive server
    // oauth server should be case insensitive so this should make no difference
    return @"Bearer";
  } else {
    return tokenType;
  }
}

- (BOOL)doesExpire;
{ return (expiresAt != nil); }

- (BOOL)hasExpired;
{ return ([[NSDate date] earlierDate:expiresAt] == expiresAt); }

- (NSString*)description;
{
  return
      [NSString stringWithFormat:@"<SPMSOAuth2Token token:%@ refreshToken:%@ "
                                 @"expiresAt:%@ tokenType: %@>",
                                 self.accessToken,
                                 self.refreshToken,
                                 self.expiresAt,
                                 self.tokenType];
}

#pragma mark - Transform to Token

+ (id)tokenWithResponseBody:(id)json {
  return [self tokenWithResponseBody:json tokenType:nil];
}

+ (id)tokenWithResponseBody:(id)json tokenType:(NSString*)tokenType;
{
  NSString* expiresIn = [json verifiedObjectForKey:@"expires_in"];
  NSString* anAccessToken = [json verifiedObjectForKey:@"access_token"];
  NSString* aRefreshToken = [json verifiedObjectForKey:@"refresh_token"];

  // if the response overrides token_type we take it from the response
  if ([json verifiedObjectForKey:@"token_type"]) {
    tokenType = [json verifiedObjectForKey:@"token_type"];
  }

  NSDate* expiryDate = nil;
  if (expiresIn != nil && [expiresIn isKindOfClass:[NSNull class]] == NO) {
    expiryDate = [NSDate dateWithTimeIntervalSinceNow:[expiresIn integerValue]];
  }
  return [[[self class] alloc] initWithAccessToken:anAccessToken
                                      refreshToken:aRefreshToken
                                         expiresAt:expiryDate
                                             scope:nil
                                      responseBody:json
                                         tokenType:tokenType];
}

#pragma mark - Initialize
- (id)initWithAccessToken:(NSString*)anAccessToken;
{
  return
      [self initWithAccessToken:anAccessToken refreshToken:nil expiresAt:nil];
}

- (id)initWithAccessToken:(NSString*)anAccessToken
             refreshToken:(NSString*)aRefreshToken
                expiresAt:(NSDate*)anExpiryDate;
{
  return [[[self class] alloc] initWithAccessToken:anAccessToken
                                      refreshToken:aRefreshToken
                                         expiresAt:anExpiryDate
                                             scope:nil
                                      responseBody:nil
                                         tokenType:nil];
}

- (id)initWithAccessToken:(NSString*)anAccessToken
             refreshToken:(NSString*)aRefreshToken
                expiresAt:(NSDate*)anExpiryDate
                    scope:(NSSet*)aScope;
{
  return [[[self class] alloc] initWithAccessToken:anAccessToken
                                      refreshToken:aRefreshToken
                                         expiresAt:anExpiryDate
                                             scope:aScope
                                      responseBody:nil];
}

- (id)initWithAccessToken:(NSString*)anAccessToken
             refreshToken:(NSString*)aRefreshToken
                expiresAt:(NSDate*)anExpiryDate
                    scope:(NSSet*)aScope
             responseBody:(NSString*)aResponseBody;
{
  return [[[self class] alloc] initWithAccessToken:anAccessToken
                                      refreshToken:aRefreshToken
                                         expiresAt:anExpiryDate
                                             scope:aScope
                                      responseBody:aResponseBody
                                         tokenType:nil];
}

- (id)initWithAccessToken:(NSString*)anAccessToken
             refreshToken:(NSString*)aRefreshToken
                expiresAt:(NSDate*)anExpiryDate
                    scope:(NSSet*)aScope
             responseBody:(NSString*)aResponseBody
                tokenType:(NSString*)aTokenType {
  // a token object without an actual token is not what we want!
  NSAssert1(anAccessToken, @"No token from token response: %@", aResponseBody);
  if (anAccessToken == nil) {
    return nil;
  }
  self = [super init];
  if (self) {
    accessToken = [anAccessToken copy];
    refreshToken = [aRefreshToken copy];
    expiresAt = [anExpiryDate copy];
    scope = aScope ? [aScope copy] : [[NSSet alloc] init];
    responseBody = [aResponseBody copy];
    tokenType = [aTokenType copy];
  }
  return self;
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder*)aCoder {
  [aCoder encodeObject:accessToken forKey:@"accessToken"];
  [aCoder encodeObject:refreshToken forKey:@"refreshToken"];
  [aCoder encodeObject:expiresAt forKey:@"expiresAt"];
  [aCoder encodeObject:scope forKey:@"scope"];
  [aCoder encodeObject:responseBody forKey:@"responseBody"];
  if (tokenType) {
    [aCoder encodeObject:tokenType forKey:@"tokenType"];
  }
}

- (id)initWithCoder:(NSCoder*)aDecoder {
  NSString* decodedAccessToken = [aDecoder decodeObjectForKey:@"accessToken"];

  // a token object without an actual token is not what we want!
  if (decodedAccessToken == nil) {
    return nil;
  }

  self = [super init];
  if (self) {
    accessToken = [decodedAccessToken copy];
    refreshToken = [[aDecoder decodeObjectForKey:@"refreshToken"] copy];
    expiresAt = [[aDecoder decodeObjectForKey:@"expiresAt"] copy];
    scope = [[aDecoder decodeObjectForKey:@"scope"] copy];
    responseBody = [[aDecoder decodeObjectForKey:@"responseBody"] copy];
    tokenType = [[aDecoder decodeObjectForKey:@"tokenType"] copy];
  }
  return self;
}

- (void)restoreWithOldToken:(SPMSOAuthAccessToken*)oldToken {
  if (self.refreshToken == nil) {
    refreshToken = oldToken.refreshToken;
  }
}

#pragma mark Keychain Support

+ (NSString*)serviceName;
{
  NSString* appName = [[NSBundle mainBundle] bundleIdentifier];

  return [NSString stringWithFormat:@"%@::OAuth2", appName];
}

+ (NSString*)accountName;
{
  NSString* name = nil;
  User* user = [User MR_findFirst];
  if (user != nil) {
    name = user.email;
  }
  return name;
}

+ (id)tokenFromDefaultKeychain {
  NSString* serviceName = [[self class] serviceName];
  NSString* account = [[self class] accountName];
  NSError* error;
  NSString* dataString =
      [SSKeychain passwordForService:serviceName account:account error:&error];
  if (error) {
    return nil;
  }
  NSData* data = [NSData dataFromBase64String:dataString];

  return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)storeInDefaultKeychain {
  NSString* serviceName = [[self class] serviceName];
  NSString* account = [[self class] accountName];
  NSData* data = [NSKeyedArchiver archivedDataWithRootObject:self];

  // Create a Base-64 encoded NSString from Token
  NSString* dataString = [data base64EncodedString];
  NSError* error;
  [SSKeychain setPassword:dataString
               forService:serviceName
                  account:account
                    error:&error];
  if (error) {
  }
}
- (void)removeFromDefaultKeychain {
  NSString* serviceName = [[self class] serviceName];
  NSString* account = [[self class] accountName];
  NSError* error;
  [SSKeychain deletePasswordForService:serviceName
                               account:account
                                 error:&error];
  if (error) {
    [error localizedDescription];
  }
}

@end
