//
//  SPMSDetailShelfViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSDetailShelfViewController.h"
#import "Product.h"
#import "SPMSSuggestedProductTableViewCell.h"
#import "SPMSWebDataService.h"
#import <SVPullToRefresh.h>
#import "RACEXTScope.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SVProgressHUD.h>
#import "SPMSDetailProductViewController.h"

@interface SPMSDetailShelfViewController ()

@property(strong, nonatomic) NSMutableArray* productList;
@property(assign, nonatomic) NSUInteger cellCount;
@property(assign, nonatomic) NSUInteger pageNumber;

@end

@implementation SPMSDetailShelfViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.productList = [[NSMutableArray alloc] init];
    self.cellCount = 0;
    self.pageNumber = 1;
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetDelegate = self;
  self.tableView.emptyDataSetSource = self;

  // Config Sidebar
  [self configNavigationBar];
  // Config TableView layout
  [self configTableViewLayout];

  // Register Custom UITableViewCell for SearchDisplayController tableview
  [self.tableView registerClass:[SPMSSuggestedProductTableViewCell class]
         forCellReuseIdentifier:@"productCell"];

  // Reload Data
  [self loadProductsWithPageNumber:self.pageNumber];
  // Config PullToRefresh
  [self configPullToRefresh];
}

#pragma mark - Load Data
- (void)loadProductsWithPageNumber:(NSUInteger)page {
  if (0 < self.furnitureId) {
    if (self.pageNumber == 1) {
      [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    }
    @weakify(self);
    [[[[SPMSWebDataService sharedService]
        fetchProductListOfShelfWithFurnitureId:self.furnitureId
                                    pageNumber:page]
        deliverOn:RACScheduler.mainThreadScheduler] subscribeNext:^(NSArray*
                                                                        list) {
        @strongify(self);
        [self.productList addObjectsFromArray:list];
        self.cellCount = [self.productList count];
        [self.tableView reloadData];
    } error:^(NSError* error) {
        [SVProgressHUD
            showErrorWithStatus:
                [NSString
                    stringWithFormat:@"%@", [error localizedDescription]]];
    } completed:^{
        self.pageNumber = self.pageNumber + 1;
        [SVProgressHUD dismiss];
    }];
  }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return [self.productList count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"productCell";
  SPMSSuggestedProductTableViewCell* cell = (SPMSSuggestedProductTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (void)configureCell:(SPMSSuggestedProductTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  Product* product = (Product*)[self.productList objectAtIndex:indexPath.row];
  cell.product = product;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  Product* product = [self.productList objectAtIndex:indexPath.row];
  [self performSegueWithIdentifier:@"viewDetailProduct" sender:product];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 130.0f;
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh {
  __weak SPMSDetailShelfViewController* weakSelf = self;

  [self.tableView.pullToRefreshView setTitle:@"Getting Products of Shelf"
                                    forState:SVPullToRefreshStateLoading];

  // Setup infinite scrolling
  [self.tableView
      addInfiniteScrollingWithActionHandler:^{ [weakSelf insertRowAtBottom]; }];
}

- (void)insertRowAtBottom {
  __weak SPMSDetailShelfViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      [self loadProductsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.infiniteScrollingView stopAnimating];
  });
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No Product in Shelf";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"This Shelf has no product";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"BrowseProduct_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView*)scrollView {
}

- (void)emptyDataSetDidTapButton:(UIScrollView*)scrollView {
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
// preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewDetailProduct"]) {
    Product* aProduct = (Product*)sender;
    SPMSDetailProductViewController* detailVC =
        [segue destinationViewController];
    [detailVC setDetailProduct:aProduct];
  }
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
  [self.tableView setBackgroundColor:[UIColor whiteColor]];
  // A little trick for removing the cell separators
  self.tableView.tableFooterView = [UIView new];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)dealloc {
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
}

@end
