//
//  SPMSRegisterViewController.m
//  Shoppin' Mate
//
//  Created by Hiro on 7/16/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSRegisterViewController.h"
#import "SPMSWebDataService.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSUserDataService.h"
#import "SPMSWebAccountService.h"
#import "SVProgressHUD.h"

@interface SPMSRegisterViewController ()<UINavigationControllerDelegate,
                                         UIImagePickerControllerDelegate,
                                         UIActionSheetDelegate>

@end

@implementation SPMSRegisterViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  self.emailTextField.delegate = self;
  self.passwordTextField.delegate = self;
  self.fullnameTextField.delegate = self;

  if (![UIImagePickerController
          isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    UIAlertView* alertView =
        [[UIAlertView alloc] initWithTitle:@"No Camera"
                                   message:@"There is no camera on this device"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alertView show];
  }
  [self checkForValidRegisterInput];

  [self.profileImage setClipsToBounds:YES];
  // Create Circular User Avatar Image View
  self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;

  // Add Border
  self.profileImage.layer.borderWidth = 2.0f;
  self.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
}

// Check valid textfield
- (void)checkForValidRegisterInput {
  RACSignal* validEmailSignal =
      [self.emailTextField.rac_textSignal map:^id(NSString* text) {
          return @([[SPMSWebAccountService sharedService] isValidEmail:text]);
      }];
  RACSignal* validPasswordSignal =
      [self.passwordTextField.rac_textSignal map:^id(NSString* text) {
          return
              @([[SPMSWebAccountService sharedService] isValidPassword:text]);
      }];
  RACSignal* validFullnameSignal =
      [self.fullnameTextField.rac_textSignal map:^id(NSString* text) {
          return
              @([[SPMSWebAccountService sharedService] isValidFullname:text]);
      }];

  // If String is not valid, text color is red, otherwise is green
  RAC(self.emailValidateStatusIndicatorImageView,
      highlighted) = [validEmailSignal
      map:^id(NSNumber* isValidEmail) { return @(![isValidEmail boolValue]); }];

  // If String is not valid, text color is red, otherwise is green
  RAC(self.passwordValidateStatusIndicatorImageView,
      highlighted) = [validPasswordSignal
      map:^id(NSNumber* isValidPass) { return @(![isValidPass boolValue]); }];
  // If String is not valid, text color is red, otherwise is green
  RAC(self.fullNameValidateStatusIndicatorImageView,
      highlighted) = [validFullnameSignal
      map:^id(NSNumber* isValidName) { return @(![isValidName boolValue]); }];

  // Signal to detect to enable/disable "Login" button whether the input is
  // valid or not
  RACSignal* isActiveRegisterBTNSignal = [RACSignal
      combineLatest:
          @[ validEmailSignal, validPasswordSignal, validFullnameSignal ]
             reduce:^id(NSNumber* isValidEmail,
                        NSNumber* isValidPass,
                        NSNumber* isValidFullname) {
                 return @([isValidEmail boolValue] && [isValidPass boolValue] &&
                          [isValidFullname boolValue]);
             }];
  // Enable/Disable "Login" button whether the input is valid or not
  [isActiveRegisterBTNSignal subscribeNext:^(NSNumber* isValidRegister) {
      self.registerButton.enabled = [isValidRegister boolValue];
  }];
}

// Register button action
- (IBAction)registerButtonTapped:(UIButton*)sender {
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];

  [[[[SPMSWebDataService sharedService]
      checkExistedAccountWithEmail:self.emailTextField.text]
      deliverOn:RACScheduler.mainThreadScheduler]
      subscribeNext:^(NSNumber* result) {
          if (result.intValue != 1) {
            [[[[SPMSWebDataService sharedService]
                registerAccountWithEmail:self.emailTextField.text
                                password:self.passwordTextField.text
                                fullName:self.fullnameTextField.text
                             avatarImage:self.profileImage.image]
                deliverOn:RACScheduler.mainThreadScheduler]
                subscribeNext:^(NSNumber* result) {
                    if (result) {
                      [SVProgressHUD
                          showSuccessWithStatus:@"Success! Please login"];
                      [self.presentingViewController
                          dismissViewControllerAnimated:YES
                                             completion:nil];
                    }
                }];
          } else {
            UIAlertView* alert = [[UIAlertView alloc]
                    initWithTitle:@"Email Address Error"
                          message:@"The email address you provided is "
                          @"associated with an existing account"
                         delegate:self
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil];
            [alert show];
          }
      }];
};

// Take photo action
- (IBAction)takePhoto:(UIButton*)sender {
  UIImagePickerController* picker = [[UIImagePickerController alloc] init];
  picker.delegate = self;
  picker.sourceType = UIImagePickerControllerSourceTypeCamera;
  picker.allowsEditing = YES;

  [self presentViewController:picker animated:YES completion:NULL];
}

// Choose photo from library action
- (IBAction)choosePhoto:(UIButton*)sender {
  // 1) Show status
  [SVProgressHUD showWithStatus:@"Loading..."];

  // 2) Get a concurrent queue form the system
  dispatch_queue_t concurrentQueue =
      dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

  // 3) Load picker in background
  dispatch_async(concurrentQueue, ^{
      UIImagePickerController* picker = [[UIImagePickerController alloc] init];
      picker.delegate = self;
      picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
      picker.allowsEditing = YES;
      dispatch_async(dispatch_get_main_queue(), ^{
          [self presentViewController:picker animated:YES completion:NULL];
          [SVProgressHUD dismiss];
      });
  });
}

// Background tap
- (IBAction)backgroundTapped:(id)sender {
  [self.view endEditing:YES];
}

- (IBAction)imageTappedAction:(UIButton*)sender {
  UIActionSheet* popup = [[UIActionSheet alloc]
               initWithTitle:@"How would you like to set your profile photo?"
                    delegate:self
           cancelButtonTitle:@"Cancel"
      destructiveButtonTitle:nil
           otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
  popup.tag = 1;
  [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)backToLoginViewAction:(id)sender {
  [self.presentingViewController dismissViewControllerAnimated:YES
                                                    completion:nil];
}

- (void)actionSheet:(UIActionSheet*)popup
    clickedButtonAtIndex:(NSInteger)buttonIndex {
  switch (popup.tag) {
    case 1: {
      switch (buttonIndex) {
        case 0:
          [self takePhoto:nil];
          break;
        case 1:
          [self choosePhoto:nil];
          break;
        default:
          break;
      }
      break;
    }
    default:
      break;
  }
}

- (void)imagePickerController:(UIImagePickerController*)picker
    didFinishPickingMediaWithInfo:(NSDictionary*)info {
  UIImage* chosenImage = info[UIImagePickerControllerEditedImage];

  self.profileImage.image = chosenImage;

  [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker {
  [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITextfield Delegates
- (void)textFieldDidBeginEditing:(UITextField*)textField {
  if (textField == self.emailTextField) {
    [self.emailValidateStatusIndicatorImageView setHidden:NO];
  }
  if (textField == self.passwordTextField) {
    [self.passwordValidateStatusIndicatorImageView setHidden:NO];
  }
  if (textField == self.fullnameTextField) {
    [self.fullNameValidateStatusIndicatorImageView setHidden:NO];
  }
}

- (void)textFieldDidEndEditing:(UITextField*)textField {
  if (textField == self.emailTextField) {
    [self.emailValidateStatusIndicatorImageView setHidden:YES];
  }
  if (textField == self.passwordTextField) {
    [self.passwordValidateStatusIndicatorImageView setHidden:YES];
  }
  if (textField == self.fullnameTextField) {
    [self.fullNameValidateStatusIndicatorImageView setHidden:YES];
  }
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  NSInteger nextTag = textField.tag + 1;
  // Try to find next responder to navigate through TextFields
  UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
  if (nextResponder) {
    // Found next responder, so set it.
    [nextResponder becomeFirstResponder];
  } else {
    // Not found, so remove keyboard.
    [textField resignFirstResponder];
    [[[[SPMSWebDataService sharedService]
        registerAccountWithEmail:self.emailTextField.text
                        password:self.passwordTextField.text
                        fullName:self.fullnameTextField.text
                     avatarImage:self.profileImage.image]
        deliverOn:RACScheduler.mainThreadScheduler]
        subscribeNext:^(NSNumber* result) {
            if (result) {
              [self.presentingViewController dismissViewControllerAnimated:YES
                                                                completion:nil];
            }
        }];
  }
  return NO;
}

- (void)dealloc {
  self.emailTextField.delegate = nil;
  self.passwordTextField.delegate = nil;
  self.fullnameTextField.delegate = nil;
}

@end
