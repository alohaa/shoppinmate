//
//  MEDJsonGenerator.h
//  MapEditor
//
//  Created by Dat Truong on 2014-07-22.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MEDBeaconPoint;
@class MEDFurniture;
@interface MEDJsonGenerator : NSObject

- (NSString *)jsonFurnituresWithFurnituresArray:(NSArray *)furnitures;
- (NSString *)jsonPointsWithPointsArray:(NSArray *)points;
- (NSArray *)furnituresWithJsonString:(NSString *)jsonString;
- (NSArray *)pointsWithJsonString:(NSString *)jsonString;


- (NSArray *)jsonPointDictArrayFromPointsArray:(NSArray *)points;
- (NSArray *)jsonFurnitureDictArrayWithFurnitureArray:(NSArray *)furnitures;

- (MEDBeaconPoint *)pointFromDict:(NSDictionary *)pointDict;
- (MEDFurniture *)furnitureFromDict:(NSDictionary *)furnitureDict;

@end
