//
//  SPMSBrowseProductViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu.h>
#import "SPMSAddProductToListViewController.h"
#import <UIScrollView+EmptyDataSet.h>

@interface SPMSBrowseProductViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       DZNEmptyDataSetDelegate,
                       DZNEmptyDataSetSource>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
- (IBAction)addProductToListAction:(id)sender;

@end
