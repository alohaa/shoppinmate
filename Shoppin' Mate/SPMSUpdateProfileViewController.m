//
//  SPMSUpdateProfileViewController.m
//  Shoppin' Mate
//
//  Created by Hiro on 7/21/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSUpdateProfileViewController.h"
#import "SPMSWebDataService.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSUserDataService.h"
#import "SPMSDatePicker.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

@interface SPMSUpdateProfileViewController ()<UINavigationControllerDelegate,
                                              UIImagePickerControllerDelegate,
                                              UITextFieldDelegate,
                                              UIActionSheetDelegate,
                                              UIImagePickerControllerDelegate>
@property(nonatomic, strong) SPMSDatePicker* datePicker;
@property(nonatomic, strong) NSNumber* chosenGender;
@property(nonatomic) BOOL isDatePickerValueChanged;
@end

@implementation SPMSUpdateProfileViewController

- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.datePicker =
        [[SPMSDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    [self.datePicker addTargetForDoneButton:self
                                     action:@selector(hideViewDatePicker)];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  _gender = @[ @"Male", @"Female" ];

  if (![UIImagePickerController
          isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    UIAlertView* alertView =
        [[UIAlertView alloc] initWithTitle:@"No Camera"
                                   message:@"There is no camera on this device"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alertView show];
  }
  [self configNavigationBar];
  [self configureView];
  [self loadUserData:nil];
}

#pragma Implementing Datasource for PickerView
// Implementing the DataSource Protocol for Picker View
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView {
  return 1;
}

- (NSInteger)pickerView:(UIPickerView*)pickerView
    numberOfRowsInComponent:(NSInteger)component {
  return _gender.count;
}

- (NSString*)pickerView:(UIPickerView*)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component {
  return _gender[row];
}

#pragma mark PickerView Delegate
- (IBAction)textFieldReturn:(id)sender {
  [sender resignFirstResponder];
}

#pragma Date Picker
- (void)showViewDatePicker {
  self.isDatePickerValueChanged = NO;
  [UIView animateWithDuration:1.0
                   animations:^{
                       self.datePicker.frame = CGRectMake(
                           0,
                           self.view.frame.size.height - kOFFSET_FOR_KEYBOARD,
                           320,
                           260);
                       [self.view addSubview:self.datePicker];
                   }];
  [self updateDateDatePicker];
}
- (void)hideViewDatePicker {
  self.isDatePickerValueChanged = YES;
  [UIView animateWithDuration:0.5
      animations:^{
          self.datePicker.frame =
              CGRectMake(0, self.view.frame.size.height, 320, 50);
      }
      completion:^(BOOL finished) {
          [self.datePicker removeFromSuperview];
          [self setViewMovedUp:NO];
      }];
  [self updateDateDatePicker];
}
#pragma mark - Gender Picker
- (void)updateDateDatePicker {
  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  [formatter setDateFormat:@"dd/MM/yyyy"];

  NSDate* date = [self.datePicker.picker date];
  NSString* dateString = [formatter stringFromDate:date];
  [self.dateOfBirthTextField setText:dateString];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
  if (textField == self.dateOfBirthTextField) {
    // move the main view, so that the keyboard does not hide it.
    if (self.view.frame.origin.y >= 0) {
      [self setViewMovedUp:YES];
    }
    [self showViewDatePicker];
    return NO;  // preventing keyboard from showing
  }
  if (textField == self.genderTextField) {
    [self showGenderPicker];
    return NO;  // preventing keyboard from showing
  }
  return YES;
}

- (void)textFieldDidEndEditing:(UITextField*)textField {
  if (textField == self.dateOfBirthTextField) {
    [self hideViewDatePicker];
  }
}

#pragma mark - Keyboard Helpers
// method to move the view up/down whenever the keyboard is shown/dismissed
- (void)setViewMovedUp:(BOOL)movedUp {
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationDuration:0.3];  // if you want to slide up the view

  CGRect rect = self.view.frame;
  if (movedUp) {
    // 1. move the view's origin up so that the text field that will be hidden
    // come above the keyboard
    // 2. increase the size of the view so that the area behind the keyboard is
    // covered up.
    rect.origin.y -= kOFFSET_FOR_KEYBOARD;
    rect.size.height += kOFFSET_FOR_KEYBOARD;
  } else {
    // revert back to the normal state.
    rect.origin.y += kOFFSET_FOR_KEYBOARD;
    rect.size.height -= kOFFSET_FOR_KEYBOARD;
  }
  self.view.frame = rect;

  [UIView commitAnimations];
}

- (void)keyboardWillShow {
  // Animate the current view out of the way
  if (self.view.frame.origin.y >= 0) {
    [self setViewMovedUp:YES];
  } else if (self.view.frame.origin.y < 0) {
    [self setViewMovedUp:NO];
  }
}

- (void)keyboardWillHide {
  if (self.view.frame.origin.y >= 0) {
    [self setViewMovedUp:YES];
  } else if (self.view.frame.origin.y < 0) {
    [self setViewMovedUp:NO];
  }
}

- (IBAction)updateButtonPressed:(id)sender {
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  BOOL genderBool = [self.genderTextField.text isEqualToString:@"Female"];
  NSNumber* gender = [NSNumber numberWithBool:genderBool];
  NSDate* dateOfBith;
  if (self.isDatePickerValueChanged) {
    dateOfBith = [self.datePicker.picker date];
  }

  [[[[SPMSWebDataService sharedService]
      updateAccountWithFullName:self.fullNameTextField.text
                    avatarImage:self.imageProfile.image
                         gender:gender
                    dateOfBirth:dateOfBith]
      deliverOn:RACScheduler.mainThreadScheduler]
      subscribeNext:^(NSNumber* result) {
          if (result) {
            [[NSNotificationCenter defaultCenter]
                postNotificationName:kUpdatedUserProfileNotification
                              object:nil
                            userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
          }
      }];
  [SVProgressHUD dismiss];
}

- (IBAction)imageTappedAction:(id)sender {
  [self showImageActionSheetPicker];
}

#pragma mark - Load Data
- (void)loadUserData:(id)sender {
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  [formatter setDateFormat:@"dd/MM/yyyy"];

  User* user = [User MR_findFirst];
  if (!user) {
    return;
  }
  [self.imageProfile sd_setImageWithURL:[NSURL URLWithString:user.imageURL]
                       placeholderImage:[UIImage imageNamed:@"UserDefault"]];
  self.fullNameTextField.text = user.fullName;
  self.dateOfBirthTextField.text =
      [formatter stringFromDate:[user dateOfBirth]];
  self.genderTextField.text = _gender[user.gender.intValue];
  [SVProgressHUD dismiss];
}

#pragma Gender Implementation
- (void)showGenderPicker {
  UIActionSheet* popup =
      [[UIActionSheet alloc] initWithTitle:@"Choose Gender?"
                                  delegate:self
                         cancelButtonTitle:nil
                    destructiveButtonTitle:nil
                         otherButtonTitles:@"Male", @"Female", nil];
  popup.tag = 1;

  [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)maleChosen {
  self.genderTextField.text = @"Male";
}

- (void)femaleChosen {
  self.genderTextField.text = @"Female";
}

- (void)actionSheet:(UIActionSheet*)popup
    clickedButtonAtIndex:(NSInteger)buttonIndex {
  switch (popup.tag) {
    case 1: {
      switch (buttonIndex) {
        case 0:
          [self maleChosen];
          break;
        case 1:
          [self femaleChosen];
          break;
        default:
          break;
      }
      break;
    }
    case 2: {
      switch (buttonIndex) {
        case 0:
          [self takePhoto:nil];
          break;
        case 1:
          [self choosePhoto:nil];
          break;
        default:
          break;
      }
    }
    default:
      break;
  }
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)configureView {
  self.datePicker.frame = CGRectMake(0,
                                     self.view.frame.size.height,
                                     self.datePicker.frame.size.width,
                                     self.datePicker.frame.size.height);
  [self.imageProfile setClipsToBounds:YES];

  // Create Circular User Avatar Image View
  self.imageProfile.layer.cornerRadius = self.imageProfile.frame.size.width / 2;

  // Add Border
  self.imageProfile.layer.borderWidth = 5.0f;
  self.imageProfile.layer.borderColor =
      [UIColor colorWithRed:0.925 green:0.941 blue:0.945 alpha:1.000].CGColor;
  if (self.userFullname) {
    self.fullNameTextField.text = self.userFullname;
  }
  if (self.userGender) {
    self.genderTextField.text = self.userGender;
  }
  if (self.userDOB) {
    self.dateOfBirthTextField.text = self.userDOB;
  }
  if (self.userAvatarImage) {
    [self.imageProfile setImage:self.userAvatarImage];
  }
}

// Choose photo from library action
- (IBAction)choosePhoto:(UIButton*)sender {
  // 1) Show status
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];

  // 2) Get a concurrent queue form the system
  dispatch_queue_t concurrentQueue =
      dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

  // 3) Load picker in background
  dispatch_async(concurrentQueue, ^{
      UIImagePickerController* picker = [[UIImagePickerController alloc] init];
      picker.delegate = self;
      picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
      picker.allowsEditing = YES;
      dispatch_async(dispatch_get_main_queue(), ^{
          [self presentViewController:picker animated:YES completion:NULL];
          [SVProgressHUD dismiss];
      });
  });
}

#pragma Photo Configuration

// Take photo action
- (IBAction)takePhoto:(UIButton*)sender {
  UIImagePickerController* picker = [[UIImagePickerController alloc] init];
  picker.delegate = self;
  picker.sourceType = UIImagePickerControllerSourceTypeCamera;
  picker.allowsEditing = YES;

  [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController*)picker
    didFinishPickingMediaWithInfo:(NSDictionary*)info {
  UIImage* chosenImage = info[UIImagePickerControllerEditedImage];

  self.imageProfile.image = chosenImage;

  [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker {
  [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)showImageActionSheetPicker {
  UIActionSheet* popup = [[UIActionSheet alloc]
               initWithTitle:@"How would you like to set your profile photo?"
                    delegate:self
           cancelButtonTitle:@"Cancel"
      destructiveButtonTitle:nil
           otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
  popup.tag = 2;
  [popup showInView:[UIApplication sharedApplication].keyWindow];
}

@end
