//
//  SPMSSWSearchObject.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSSWAbstractSearchObject.h"

@interface SPMSSWSearchObject : SPMSSWAbstractSearchObject

@property (strong, nonatomic) id refencedObject;

@end
