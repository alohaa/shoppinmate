//
//  SPMSSPMSDataPersistentOperation.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/27/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>

@class SPMSSPMSDataPersistentOperation;

@protocol SPMSDataPersistentOperationDelegate<NSObject>

@optional
- (void)SPMSDataPersistentOperationDidFinish:
        (SPMSSPMSDataPersistentOperation*)request;

@end

@interface SPMSSPMSDataPersistentOperation : NSObject

@property(nonatomic, strong) NSString* urlString;
@property(nonatomic, strong) NSString* headerParamString;
@property(nonatomic, strong) NSString* httpBodyParamString;
@property(nonatomic, strong) NSString* httpMethodString;

@property(nonatomic, weak) id<SPMSDataPersistentOperationDelegate> delegate;
- (instancetype)initWithRequest:(NSMutableURLRequest*)aRequest;
- (instancetype)initWithURL:(NSString*)aURLString
                 HTTPMethod:(NSString*)aHTTPMethodString
            HTTPHeaderParam:(NSString*)aHTTPHeaderParamString
                   HTTPBody:(NSString*)aHTTPBodyString;
- (RACSignal*)peformOfflineRequestWithClient:(NSURLSession*)client;
- (NSMutableURLRequest*)urlRequestFromPersistentOperaion;
@end
