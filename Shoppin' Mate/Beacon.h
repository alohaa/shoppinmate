//
//  Beacon.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/31/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Map;

@interface Beacon : NSManagedObject

@property (nonatomic, retain) NSData * graphData;
@property (nonatomic, retain) NSNumber * major;
@property (nonatomic, retain) NSNumber * minor;
@property (nonatomic, retain) NSNumber * pointId;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSNumber * xCoordinate;
@property (nonatomic, retain) NSNumber * yCoordinate;
@property (nonatomic, retain) Map *map;

@end
