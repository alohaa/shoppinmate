//
//  SPMSProductDataService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>

@interface SPMSProductDataService : NSObject

+ (SPMSProductDataService*)sharedService;
- (RACSignal*)fetchProductDetailWithProductId:(NSString*)productId
                                      baseUrl:(NSURL*)baseUrl
                                       client:(NSURLSession*)client;
- (RACSignal*)fetchProductListWithPageNumber:(NSUInteger)pageNumber
                                     baseUrl:(NSURL*)baseUrl
                                      client:(NSURLSession*)client;
- (RACSignal*)fetchProductListOfShelfWithFurnitureId:(NSUInteger)furnitureID
                                          pageNumber:(NSUInteger)pageNumber
                                             baseUrl:(NSURL*)baseUrl
                                              client:(NSURLSession*)client;
- (RACSignal*)productListForSignal:(RACSignal*)signal;

@end
