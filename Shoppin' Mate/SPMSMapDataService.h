//
//  SPMSMapDataService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/23/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>

@interface SPMSMapDataService : NSObject

+ (SPMSMapDataService*)sharedService;
- (RACSignal*)downloadMapDataListWithBaseUrl:(NSURL*)baseUrl
                                      client:(NSURLSession*)client;
- (RACSignal*)mapForSignal:(RACSignal*)signal;
- (RACSignal*)sendVisitedBeaconWithPointId:(NSNumber*)pointId
                                   baseUrl:(NSURL*)baseUrl
                                    client:(NSURLSession*)client;

@end
