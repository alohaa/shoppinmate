//
//  SPMSPromotionListTableViewCell.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Promotion.h"
#import <WCFastCell.h>

@interface SPMSPromotionListTableViewCell : WCFastCell

@property(assign, nonatomic) Promotion* promotion;

@property(strong, nonatomic) IBOutlet UIImageView* promotionImageView;
@property(strong, nonatomic) IBOutlet UILabel* promotionNameLabel;
@property(strong, nonatomic) IBOutlet UILabel* promotionDescriptionLabel;
@property(strong, nonatomic) IBOutlet UILabel* startDateLabel;
@property(strong, nonatomic) IBOutlet UILabel* endDateLabel;

@end
