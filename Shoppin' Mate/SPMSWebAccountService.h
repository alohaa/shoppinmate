//
//  SPMSWebAccountService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/15/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <Security/Security.h>

typedef void (^SPMSSignInResponse)(BOOL);

@interface SPMSWebAccountService : NSObject

+ (id)sharedService;

- (RACSignal *)signInWithEmail:(NSString *)emailString password:(NSString *)passwordString;
- (BOOL)isValidEmail:(NSString*)emailString;
- (BOOL)isValidPassword:(NSString*)passwordString;
- (BOOL)isValidFullname:(NSString*)fullnameString;
- (void)logout;

@end
