//
//  SPMSSuggestedProductTableViewCell.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/2/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import <TTTAttributedLabel.h>
#import <MCSwipeTableViewCell.h>

@interface SPMSSuggestedProductTableViewCell : MCSwipeTableViewCell {
 @private
  UIImageView* productImageView;
  UILabel* productNameLabel;
  UILabel* productDescLabel;
  UILabel* productCategoryLabel;
  TTTAttributedLabel* productDiscountLabel;
  TTTAttributedLabel* productPriceLabel;
}

@property(assign, nonatomic) Product* product;

@end
