//
//  SPMSMapDataService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/23/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSMapDataService.h"
#import "NSDictionary+Verified.h"
#import "SPMSMapOperator.h"
#import "SPMSOAuthClient.h"
#import "SPMSOAuthAccessToken.h"

@implementation SPMSMapDataService

+ (SPMSMapDataService*)sharedService {
  static SPMSMapDataService* sharedService;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

- (RACSignal*)downloadMapDataListWithBaseUrl:(NSURL*)baseUrl
                                      client:(NSURLSession*)client {
  NSString* relativeString = @"api/map/list";

  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {

      NSURL* relativeUrl =
          [NSURL URLWithString:relativeString relativeToURL:baseUrl];
      NSMutableURLRequest* request =
          [NSMutableURLRequest requestWithURL:relativeUrl];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[dict verifiedObjectForKey:@"Result"]];
                    [subscriber sendCompleted];
                  }
                }
            }];

      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

- (RACSignal*)mapForSignal:(RACSignal*)signal {
  return [signal map:^id(NSArray* dicts) {
      for (NSDictionary* mapDic in dicts) {
        [[SPMSMapOperator sharedService] saveMapForDictionary:mapDic];
      }
      return @(YES);
  }];
}

#pragma mark - Beacon Visit
- (RACSignal*)sendVisitedBeaconWithPointId:(NSNumber*)pointId
                                   baseUrl:(NSURL*)baseUrl
                                    client:(NSURLSession*)client {
  NSString* relativeString = @"api/beacon/visit";

  RACSignal* signal =
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSString* accessToken =
              [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];

          NSString* params = [NSString stringWithFormat:@"PointId=%@", pointId];
          NSURL* relativeUrl =
              [NSURL URLWithString:relativeString relativeToURL:baseUrl];
          NSMutableURLRequest* request =
              [NSMutableURLRequest requestWithURL:relativeUrl];
          if (accessToken) {
            NSString* headerParam =
                [NSString stringWithFormat:@"Bearer %@", accessToken];
            [request addValue:headerParam forHTTPHeaderField:@"Authorization"];
          }

          [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
          [request setHTTPMethod:@"POST"];

          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      NSError* aError = [self createErrorWithData:request];
                      [subscriber sendError:aError];
                    } else if (!data) {
                      NSError* error =
                          [self createErrorWithData:
                                    @"No data was received from the server."];
                      [subscriber sendError:error];
                    } else {
                      NSError* jsonError;
                      NSDictionary* dict = [NSJSONSerialization
                          JSONObjectWithData:data
                                     options:NSJSONReadingMutableContainers
                                       error:&jsonError];

                      if (jsonError) {
                        [subscriber sendError:jsonError];
                      } else {
                        NSUInteger responseCode =
                            [(NSHTTPURLResponse*)response statusCode];
                        if (responseCode == HTTP_CODE_NO401_UNAUTHORISED) {
                          NSDictionary* userInfo = @{
                            NSLocalizedDescriptionKey : @"Authorization has "
                            @"been denied. Please " @"login again."
                          };
                          NSError* error = [NSError
                              errorWithDomain:ERROR_DOMAIN
                                         code:HTTP_CODE_NO401_UNAUTHORISED
                                     userInfo:userInfo];
                          [subscriber sendError:error];
                        } else {
                          [subscriber
                              sendNext:[dict verifiedObjectForKey:@"Success"]];
                          [subscriber sendCompleted];
                        }
                      }
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
  return [[SPMSOAuthClient sharedInstance]
      doRequestAndRefreshTokenIfNecessary:signal];
}

#pragma mark - Generate Error containing Request
- (NSError*)createErrorWithData:(id)data {
  NSDictionary* userInfo = @{NSLocalizedDescriptionKey : data};
  NSError* error =
      [NSError errorWithDomain:ERROR_DOMAIN code:ERROR_CODE userInfo:userInfo];
  return error;
}

@end
