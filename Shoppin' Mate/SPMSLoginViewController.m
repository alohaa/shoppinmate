//
//  SPMSLoginViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSLoginViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSFacebookService.h"
#import "SPMSWebAccountService.h"
#import "SPMSResetPasswordViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface SPMSLoginViewController ()

@end

@implementation SPMSLoginViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
  [super viewDidLoad];
  // Config Signal for all Button
  [self configButtonAction];
  // Check state of Email & Password input to validate
  [self checkForValidLoginInput];
  // Swipe to close view
  UISwipeGestureRecognizer* swipeRecognizer =
      [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(userSwiped:)];
  swipeRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
  [self.bgImage addGestureRecognizer:swipeRecognizer];
}

#pragma mark - Input Validation
/**
 *  Check User Input when Logging-in to validate
 */
- (void)checkForValidLoginInput {
  RACSignal* validEmailSignal =
      [self.emailTextField.rac_textSignal map:^id(NSString* text) {
          return @([[SPMSWebAccountService sharedService] isValidEmail:text]);
      }];
  RACSignal* validPasswordSignal =
      [self.passwordTextField.rac_textSignal map:^id(NSString* text) {
          return
              @([[SPMSWebAccountService sharedService] isValidPassword:text]);
      }];

  // If String is not valid, text color is red, otherwise is green
  RAC(self.emailValidateStatusIndicatorImageView,
      highlighted) = [validEmailSignal
      map:^id(NSNumber* isValidEmail) { return @(![isValidEmail boolValue]); }];

  // If String is not valid, text color is red, otherwise is green
  RAC(self.passwordValidateStatusIndicatorImageView,
      highlighted) = [validPasswordSignal
      map:^id(NSNumber* isValidPass) { return @(![isValidPass boolValue]); }];

  // Signal to detect to enable/disable "Login" button whether the input is
  // valid or not
  RACSignal* isActiveLoginBTNSignal = [RACSignal
      combineLatest:@[ validEmailSignal, validPasswordSignal ]
             reduce:^id(NSNumber* isValidEmail, NSNumber* isValidPass) {
                 return @([isValidEmail boolValue] && [isValidPass boolValue]);
             }];
  // Enable/Disable "Login" button whether the input is valid or not
  [isActiveLoginBTNSignal subscribeNext:^(NSNumber* isValidLogin) {
      self.signInByEmailButton.enabled = [isValidLogin boolValue];
  }];
}

#pragma mark - Action
/**
 *  Configurate Buttons' Action
 */
- (void)configButtonAction {
  // Sign-in by Email
  [[[[self.signInByEmailButton
      rac_signalForControlEvents:UIControlEventTouchUpInside] doNext:^(id x) {
      self.signInByEmailButton.enabled = NO;
  }] flattenMap:^(id x) {
      /*
       Map the button touch event to a sign-in signal, but also flattens it by
       sending the events from the inner signal to the outer signal
       */
      return [[SPMSWebAccountService sharedService]
          signInWithEmail:self.emailTextField.text
                 password:self.passwordTextField.text];
  }] subscribeNext:^(NSNumber* signedIn) {
      self.signInByEmailButton.enabled = YES;
      BOOL isSucceedLogin = [signedIn boolValue];
      // Dimiss Login View if logged-in successfully
      if (isSucceedLogin) {
        [SVProgressHUD dismiss];
        [self.presentingViewController dismissViewControllerAnimated:YES
                                                          completion:nil];
      }
  } error:^(NSError* error) {
      [SVProgressHUD
          showErrorWithStatus:
              [NSString stringWithFormat:@"%@", [error localizedDescription]]];
  }];

  [[[self.loginViaFBButton
      rac_signalForControlEvents:UIControlEventTouchUpInside]
      doNext:^(id x) {}] subscribeNext:^(id x) {
      // Login
      [[[[SPMSFacebookService sharedFBServiceInstance] loginFBSignal]
          deliverOn:RACScheduler.mainThreadScheduler]
          subscribeNext:^(NSNumber* result) {
              BOOL isSuccess = [result boolValue];
              if (isSuccess) {
                [self.presentingViewController
                    dismissViewControllerAnimated:YES
                                       completion:nil];
              }
          }
          error:^(NSError* error) {
              [SVProgressHUD
                  showErrorWithStatus:
                      [NSString stringWithFormat:@"%@",
                                                 [error localizedDescription]]];
          }];
  }];
}

- (IBAction)backgroundTappedAction:(id)sender {
  [self.view endEditing:YES];
}

//- (IBAction)resetPressedAction:(UIButton*)sender {
//  [self performSegueWithIdentifier:@"resetPassword" sender:nil];
//}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  NSInteger nextTag = textField.tag + 1;
  // Try to find next responder to navigate through TextFields
  UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
  if (nextResponder) {
    // Found next responder, so set it.
    [nextResponder becomeFirstResponder];
  } else {
    // Not found, so remove keyboard.
    [textField resignFirstResponder];
    [[[[SPMSWebAccountService sharedService]
        signInWithEmail:self.emailTextField.text
               password:self.passwordTextField.text]
        deliverOn:RACScheduler.mainThreadScheduler]
        subscribeNext:^(NSNumber* signedIn) {
            self.signInByEmailButton.enabled = YES;
            BOOL isSucceedLogin = [signedIn boolValue];
            // Dimiss Login View if logged-in successfully
            if (isSucceedLogin) {
              [self.presentingViewController dismissViewControllerAnimated:YES
                                                                completion:nil];
            }
        }];
  }
  return NO;
}

- (void)textFieldDidBeginEditing:(UITextField*)textField {
  if (textField == self.emailTextField) {
    [self.emailValidateStatusIndicatorImageView setHidden:NO];
  }
  if (textField == self.passwordTextField) {
    [self.passwordValidateStatusIndicatorImageView setHidden:NO];
  }
}

- (void)textFieldDidEndEditing:(UITextField*)textField {
  if (textField == self.emailTextField) {
    [self.emailValidateStatusIndicatorImageView setHidden:YES];
  }
  if (textField == self.passwordTextField) {
    [self.passwordValidateStatusIndicatorImageView setHidden:YES];
  }
}

#pragma mark - Others
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}
#pragma mark - Close modal view
- (void)userSwiped:(UIGestureRecognizer*)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end
