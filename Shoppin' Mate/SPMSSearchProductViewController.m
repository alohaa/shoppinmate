//
//  SPMSSearchProductViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSSearchProductViewController.h"
#import "SPMSSearchProductTableViewCell.h"
#import "Product.h"
#import "NSDictionary+Verified.h"
#import "SPMSDetailProductViewController.h"
#import "SPMSFilterSearchProductViewController.h"
#import <UIViewController+KNSemiModal.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface SPMSSearchProductViewController () {
  Product* product;
}

@property(strong, nonatomic) NSMutableArray* allElements;
@property(strong, nonatomic) NSMutableArray* findedElements;
@property(assign, nonatomic) BOOL isFiltering;

@end

@implementation SPMSSearchProductViewController
#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.allElements = [[NSMutableArray alloc] init];
    self.findedElements = [[NSMutableArray alloc] init];
    product = [Product new];
    // Take note that you need to take ownership of the ViewController that is
    // being presented
    self.filterVC = [[SPMSFilterSearchProductViewController alloc] init];
    self.isFiltering = NO;
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self configSWSearchDB];
  self.tableView.emptyDataSetSource = self;
  self.tableView.emptyDataSetDelegate = self;

  // Register Custom UITableViewCell for SearchDisplayController tableview
  [self.searchDisplayController.searchResultsTableView
               registerClass:[SPMSSearchProductTableViewCell class]
      forCellReuseIdentifier:@"searchProductCell"];
  [self.tableView registerClass:[SPMSSearchProductTableViewCell class]
         forCellReuseIdentifier:@"searchProductCell"];
  [self configNavigationBar];
  [self.tableView setTableFooterView:[UIView new]];

  RACSignal* isNeedToFilterSignal = [RACSignal
      combineLatest:@[
                      RACObserve(self.filterVC, isAppliedFilter),
                      RACObserve(self.filterVC, maxPrice),
                      RACObserve(self.filterVC, seletedCategoryCount),
                      RACObserve(self.filterVC, seletedFloorCount),
                      RACObserve(self.filterVC, isHasPromotion)
                    ]
             reduce:^id(NSNumber* isAppliedFilter,
                        NSNumber* maxPrice,
                        NSNumber* selectedCategoryCount,
                        NSNumber* seletedFloorCount,
                        NSNumber* isHasPromotion) {
                 if (([maxPrice doubleValue] > 0 ||
                      ([selectedCategoryCount intValue] > 0) ||
                      [seletedFloorCount intValue] > 0 ||
                      [isHasPromotion boolValue]) &&
                     [isAppliedFilter boolValue]) {
                   return @(YES);
                 } else {
                   return @(NO);
                 }
             }];
  [isNeedToFilterSignal subscribeNext:^(NSNumber* isNeeded) {
      self.isFiltering = [isNeeded boolValue];
      if (0 < [self.allElements count]) {
        [self filterResults:self.allElements];
        [self.tableView reloadData];
      }
  }];
}

#pragma mark - Config Search Database
- (void)configSWSearchDB {
  [[SPMSSWSearchDatabase sharedDatabase] setDelegate:self];
  [[SPMSSWSearchDatabase sharedDatabase] setDatasource:self];
}

#pragma mark - Search Action
- (void)searchString:(NSString*)searchTerm {
  [[SPMSSWSearchDatabase sharedDatabase]
       searchString:searchTerm
      withOperation:ScoringOperationTypeHeuristic];
}

#pragma mark ScoringDatabaseDatasource
- (void)rebuildDatabase {
  NSArray* dirPaths = NSSearchPathForDirectoriesInDomains(
      NSDocumentDirectory, NSUserDomainMask, YES);
  NSString* docsDir = [dirPaths objectAtIndex:0];
  NSString* databasePath = [[NSString alloc]
      initWithString:[docsDir stringByAppendingPathComponent:
                                  SW_SEARCH_DB_LOCAL_DATA_FILE]];

  NSData* fileData = [NSData dataWithContentsOfFile:databasePath];
  if (fileData != nil) {
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:fileData
                                              options:kNilOptions
                                                error:&error];
    if (error) {
    }
    id products = [json objectForKey:@"Result"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SPMSSWSearchDatabase sharedDatabase] addObjects:products
                                              forKeyPaths:@[ @"ProductName" ]];
    });
  }
}

- (NSInteger)customCostForEvent:(ScoringEvent)event {
  switch (event) {
    case ScoringEventPerfectMatch:
      return 2;
      break;

    case ScoringEventNotPerfectMatchKeyboardAnalyseHelp:
      return 1;
      break;

    case ScoringEventNotPerfectBecauseOfAccents:
      return 2;
      break;

    case ScoringEventLetterAddition:
      return -1;
      break;

    default:
      break;
  }

  return NSNotFound;
}

#pragma mark - PermissiveResearchDelegate
- (void)searchCompletedWithResults:(NSArray*)results {
  dispatch_async(dispatch_get_main_queue(), ^{
      [self.findedElements removeAllObjects];
      [self.findedElements addObjectsFromArray:results];
      if (self.isFiltering && ([self.findedElements count] > 0)) {
        [self filterResults:self.findedElements];
      }
      [self.searchDisplayController.searchResultsTableView reloadData];
  });
}

#pragma mark - Search Filter Predicate
- (void)filterResults:(NSMutableArray*)data {
  if (self.filterVC && self.filterVC.isAppliedFilter) {
    NSPredicate* categoryPredicate = [NSPredicate
        predicateWithFormat:@"SELF.refencedObject.%K IN %@",
                            @"CategoryName",
                            [self.filterVC.selectedCategories allObjects]];
    NSPredicate* mapPredicate = [NSPredicate
        predicateWithFormat:@"SELF.refencedObject.%K IN %@",
                            @"MapId",
                            [self.filterVC.selectedMapIds allObjects]];
    NSPredicate* hasPromotionPredicate =
        [NSPredicate predicateWithFormat:@"SELF.refencedObject.%K == %@",
                                         @"HasPromotion",
                                         self.filterVC.isHasPromotion];
    NSNumber* maxPrice =
        [NSNumber numberWithFloat:(self.filterVC.maxPrice * 1000)];
    NSPredicate* pricePredicate;
    if (0 < self.filterVC.maxPrice &&
        self.filterVC.maxPrice < MAX_PRODUCT_PRICE_RANGE) {
      pricePredicate =
          [NSPredicate predicateWithFormat:@"SELF.refencedObject.%K <= %@",
                                           @"Price",
                                           maxPrice];
    } else if (0 < self.filterVC.maxPrice) {
      pricePredicate =
          [NSPredicate predicateWithFormat:@"SELF.refencedObject.%K => %@",
                                           @"Price",
                                           maxPrice];
    }

    if (self.filterVC.selectedCategories &&
        (self.filterVC.seletedCategoryCount > 0)) {
      [data filterUsingPredicate:categoryPredicate];
    }
    if (self.filterVC.selectedMapIds && (self.filterVC.seletedFloorCount > 0)) {
      [data filterUsingPredicate:mapPredicate];
    }
    if (self.filterVC.maxPrice > 0) {
      [data filterUsingPredicate:pricePredicate];
    }
    if (self.filterVC.isHasPromotion) {
      [data filterUsingPredicate:hasPromotionPredicate];
    }
  }
}

#pragma mark - SearchDisplayController Delegate
- (BOOL)searchDisplayController:(UISearchDisplayController*)controller
    shouldReloadTableForSearchString:(NSString*)searchString {
  [self searchString:searchString];
  return YES;
}

#pragma mark - UISearchbar Delegate
- (void)searchBarCancelButtonClicked:(UISearchBar*)searchBar {
  [self.allElements removeAllObjects];
  [self.allElements addObjectsFromArray:self.findedElements];
  [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  SPMSSWSearchObject* searchObject;
  if (tableView == self.searchDisplayController.searchResultsTableView) {
    searchObject = [self.findedElements objectAtIndex:indexPath.row];
  } else {
    searchObject = [self.allElements objectAtIndex:indexPath.row];
  }

  NSDictionary* data = [searchObject refencedObject];

  product.productID = [data verifiedObjectForKey:@"ProductId"];
  product.productName = [data verifiedObjectForKey:@"ProductName"];
  product.productDescription = [data verifiedObjectForKey:@"Description"];
  product.price = [data verifiedObjectForKey:@"Price"];
  product.discount = [data verifiedObjectForKey:@"Discount"];
  product.hasPromotion = [data verifiedObjectForKey:@"HasPromotion"];
  product.imageURL = [data verifiedObjectForKey:@"Image"];
  product.categoryName = [data verifiedObjectForKey:@"CategoryName"];
  product.createdDate = [data verifiedObjectForKey:@"CreatedDate"];
  [self performSegueWithIdentifier:@"viewDetailProduct" sender:product];
}
- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  if (tableView == self.searchDisplayController.searchResultsTableView) {
    return self.findedElements.count;
  } else {
    return self.allElements.count;
  }
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 130.0f;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  static NSString* cellIdentifier = @"searchProductCell";
  SPMSSearchProductTableViewCell* cell = (SPMSSearchProductTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

  if (cell == nil) {
    cell = [[SPMSSearchProductTableViewCell alloc]
          initWithStyle:UITableViewCellStyleDefault
        reuseIdentifier:cellIdentifier];
  }
  if (tableView == self.searchDisplayController.searchResultsTableView) {
    [self configureDisplayResultCell:cell atIndexPath:indexPath];
  } else {
    [self configureInitialDisplayCell:cell atIndexPath:indexPath];
  }
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (void)configureDisplayResultCell:(SPMSSearchProductTableViewCell*)cell
                       atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  SPMSSWSearchObject* obj = [self.findedElements objectAtIndex:indexPath.row];
  [cell setSearchObject:obj];
}

- (void)configureInitialDisplayCell:(SPMSSearchProductTableViewCell*)cell
                        atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  SPMSSWSearchObject* obj = [self.allElements objectAtIndex:indexPath.row];
  [cell setSearchObject:obj];
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No products founded";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"Tap search bar to search your desired products.";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"SearchProduct_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView*)scrollView {
}

- (void)emptyDataSetDidTapButton:(UIScrollView*)scrollView {
}

#pragma mark - Actions
- (void)showFilterSearchView:(id)sender {
  [self presentSemiViewController:self.filterVC
                      withOptions:@{
                        KNSemiModalOptionKeys.pushParentBack : @(YES),
                        KNSemiModalOptionKeys.animationDuration : @(0.4),
                        KNSemiModalOptionKeys.shadowOpacity : @(0.3),
                      }];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  // "Show Menu" Button
  UIButton* showFilterBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  showFilterBTN.frame = CGRectMake(0, 0, 50, 30);
  showFilterBTN.titleLabel.font =
      [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0];
  [showFilterBTN setTitle:@"Filter" forState:UIControlStateNormal];
  [showFilterBTN addTarget:self
                    action:@selector(showFilterSearchView:)
          forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showFilterBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:showFilterBTN];

  [self.navigationItem setRightBarButtonItem:showFilterBTNItem];
  [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
// preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewDetailProduct"]) {
    Product* detailProduct = (Product*)sender;
    SPMSDetailProductViewController* detailVC =
        [segue destinationViewController];
    [detailVC setDetailProduct:detailProduct];
  }
}

#pragma mark - Other
- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}
@end
