//
//  SPMSSWAbstractSearchObject.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPMSSWAbstractSearchObject : NSObject

@property (assign, nonatomic) char *flag;
@property (assign, nonatomic) short flagLenght;
@property (assign, nonatomic) short score;

@end
