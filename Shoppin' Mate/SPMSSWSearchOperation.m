//
//  SPMSSWSearchOperation.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSSWSearchOperation.h"
#import "SPMSSWAbstractSearchObject.h"
#include "SWAligmentMethod.h"
#import "SPMSScoringMatrix.h"
#import "SPMSSWSearchDatabase.h"

@implementation ScoringOperationQueue

+ (ScoringOperationQueue*)mainQueue {
  static ScoringOperationQueue* mainQueue = nil;
  if (mainQueue == nil) {
    mainQueue = [[ScoringOperationQueue alloc] init];
    mainQueue.maxConcurrentOperationCount = 1;
  }

  return mainQueue;
}

@end

@implementation ExactScoringOperation

#pragma mark -
#pragma mark - Main operation

- (BOOL)isConcurrent {
  return NO;
}

- (void)main {
  @autoreleasepool {
    int taille = (int)self.searchedString.length;
    int max = (int)MAX([[[SPMSSWSearchDatabase sharedDatabase].elements
                           valueForKeyPath:@"@max.flagLenght"] intValue],
                       [self.searchedString length]);
    int** alignementMatrix = allocate2D(max, max);

    JMOLog(@"Searching %@ in %d elements",
           self.searchedString,
           (int)[SPMSSWSearchDatabase sharedDatabase].elements.count);
    [[SPMSSWSearchDatabase sharedDatabase].elements
        enumerateObjectsUsingBlock:^(SPMSSWAbstractSearchObject* obj,
                                     BOOL* stop) {
            if (self.isCancelled)
              return;
            obj.score = score2Strings(
                self.searchedString.UTF8String,
                obj.flag,
                taille,
                obj.flagLenght,
                alignementMatrix,
                0,
                [SPMSScoringMatrix sharedScoringMatrix].structRepresentation);
        }];

    if (self.isCancelled)
      return;

    JMOLog(@"Searching -> Done ");

    // Sorting
    NSSortDescriptor* sortDescriptor =
        [[NSSortDescriptor alloc] initWithKey:@"score" ascending:NO];
    NSArray* findedElements = [[[SPMSSWSearchDatabase sharedDatabase].elements
        sortedArrayUsingDescriptors:@[ sortDescriptor ]]
        subarrayWithRange:NSMakeRange(0, 20)];

    // LOG MAX
    SPMSSWAbstractSearchObject* obj = [findedElements objectAtIndex:0];
    logCalculatedMatrix(
        [self.searchedString UTF8String],
        obj.flag,
        (int)taille,
        obj.flagLenght,
        [SPMSScoringMatrix sharedScoringMatrix].structRepresentation);

    if (self.customCompletionBlock) {
      self.customCompletionBlock(findedElements);
    }
  }
}

@end

@implementation HeuristicScoringOperation

#pragma mark -
#pragma mark - Main operation

- (BOOL)isConcurrent {
  return NO;
}

- (void)main {
  @autoreleasepool {
    if (self.searchedString.length < ScoringSegmentLenght) {
      if (self.customCompletionBlock) {
        JMOLog(@"Search operation aborded, search operation need more letters");
        self.customCompletionBlock(nil);
      }
      return;
    }

    JMOLog(@"Searching %@ in %d elements",
           self.searchedString,
           (int)[SPMSSWSearchDatabase sharedDatabase].elements.count);
    [[SPMSSWSearchDatabase sharedDatabase].elements
        enumerateObjectsUsingBlock:^(SPMSSWAbstractSearchObject* obj,
                                     BOOL* stop) { obj.score = 0; }];

    for (int i = 0; i <= self.searchedString.length - ScoringSegmentLenght;
         i++) {
      NSString* segment = [self.searchedString
          substringWithRange:NSMakeRange(i, ScoringSegmentLenght)];
      [[[SPMSSWSearchDatabase sharedDatabase] objectsForSegment:segment]
          enumerateObjectsUsingBlock:^(SPMSSWAbstractSearchObject* obj,
                                       BOOL* stop) { obj.score++; }];
    }

    if (self.isCancelled)
      return;

    JMOLog(@"Searching -> Done ");

    // Sorting
    NSSortDescriptor* sortDescriptor =
        [[NSSortDescriptor alloc] initWithKey:@"score" ascending:NO];
    NSArray* findedElements;
    NSUInteger numberOfElement =
        [[SPMSSWSearchDatabase sharedDatabase].elements count];
    if (20 <= numberOfElement) {
      findedElements = [[[SPMSSWSearchDatabase sharedDatabase].elements
          sortedArrayUsingDescriptors:@[ sortDescriptor ]]
          subarrayWithRange:NSMakeRange(0, 20)];
    } else {
      findedElements = [[[SPMSSWSearchDatabase sharedDatabase].elements
          sortedArrayUsingDescriptors:@[ sortDescriptor ]]
          subarrayWithRange:NSMakeRange(0, numberOfElement)];
    }

    // LOG MAX
    NSUInteger taille = self.searchedString.length;
    if (0 < [findedElements count]) {
      SPMSSWAbstractSearchObject* obj = [findedElements objectAtIndex:0];
      logCalculatedMatrix(
          [self.searchedString UTF8String],
          obj.flag,
          (int)taille,
          obj.flagLenght,
          [SPMSScoringMatrix sharedScoringMatrix].structRepresentation);
    }

    if (self.customCompletionBlock) {
      self.customCompletionBlock(findedElements);
    }
  }
}
@end

@implementation HeurexactScoringOperation

#pragma mark -
#pragma mark - Main operation

- (BOOL)isConcurrent {
  return NO;
}

- (void)main {
  @autoreleasepool {
    if (self.searchedString.length < ScoringSegmentLenght) {
      if (self.customCompletionBlock) {
        self.customCompletionBlock(nil);
      }
      return;
    }

    JMOLog(@"Searching %@ in %d elements",
           self.searchedString,
           (int)[SPMSSWSearchDatabase sharedDatabase].elements.count);
    [[SPMSSWSearchDatabase sharedDatabase].elements
        enumerateObjectsUsingBlock:^(SPMSSWAbstractSearchObject* obj,
                                     BOOL* stop) { obj.score = 0; }];

    for (int i = 0; i < self.searchedString.length - ScoringSegmentLenght;
         i++) {
      NSString* segment = [self.searchedString
          substringWithRange:NSMakeRange(i, ScoringSegmentLenght)];
      [[[SPMSSWSearchDatabase sharedDatabase] objectsForSegment:segment]
          enumerateObjectsUsingBlock:^(SPMSSWAbstractSearchObject* obj,
                                       BOOL* stop) { obj.score++; }];
    }

    if (self.isCancelled)
      return;

    JMOLog(@"Searching -> Done ");

    JMOLog(@"Start adjusting -> Done ");
    // Sorting
    NSSortDescriptor* sortDescriptor =
        [[NSSortDescriptor alloc] initWithKey:@"score" ascending:NO];
    NSArray* arrayOfElemets = [[SPMSSWSearchDatabase sharedDatabase].elements
        sortedArrayUsingDescriptors:@[ sortDescriptor ]];

    int taille = (int)self.searchedString.length;
    int max = (int)MAX([[[SPMSSWSearchDatabase sharedDatabase].elements
                           valueForKeyPath:@"@max.flagLenght"] intValue],
                       [self.searchedString length]);
    int** alignementMatrix = allocate2D(max, max);

    [arrayOfElemets
        enumerateObjectsUsingBlock:^(SPMSSWAbstractSearchObject* obj,
                                     NSUInteger idx,
                                     BOOL* stop) {
            if (self.isCancelled)
              return;

            if (idx == 50) {
              *stop = YES;
            }

            obj.score = score2Strings(
                self.searchedString.UTF8String,
                obj.flag,
                taille,
                obj.flagLenght,
                alignementMatrix,
                0,
                [SPMSScoringMatrix sharedScoringMatrix].structRepresentation);
        }];

    NSArray* findedElements =
        [[arrayOfElemets sortedArrayUsingDescriptors:@[ sortDescriptor ]]
            subarrayWithRange:NSMakeRange(0, 20)];

    // LOG MAX
    SPMSSWAbstractSearchObject* obj = [findedElements objectAtIndex:0];
    logCalculatedMatrix(
        [self.searchedString UTF8String],
        obj.flag,
        (int)taille,
        obj.flagLenght,
        [SPMSScoringMatrix sharedScoringMatrix].structRepresentation);

    if (self.customCompletionBlock) {
      self.customCompletionBlock(findedElements);
    }
  }
}

@end
