//
//  MEDBeaconPoint.m
//  MapEditor
//
//  Created by Dat Truong on 2014-07-23.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "MEDBeaconPoint.h"

@interface MEDBeaconPoint()

@property (strong, nonatomic) NSMutableDictionary *processingGraph;

@end

@implementation MEDBeaconPoint

- (instancetype)init
{
    self = [super init];
    if (self) {
        _processingGraph = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSDictionary *)graphDict
{
    return [_processingGraph copy];
}

- (void)setGraphDict:(NSDictionary *)graphDict
{
    _processingGraph = [NSMutableDictionary dictionaryWithDictionary:graphDict];
}

- (void)addToGraphWithPoint:(MEDBeaconPoint *)point
{
    float distance;
    distance = [MEDBeaconPoint lengthFrom:self.position to:point.position];
    [self.processingGraph setValue:@(distance) forKey:[NSString stringWithFormat:@"%d", point.pointId]];
    [point.processingGraph setValue:@(distance) forKey:[NSString stringWithFormat:@"%d", self.pointId]];
}

+ (float) lengthFrom:(CGPoint) a to:(CGPoint) b
{
    float length;
    
    length = sqrtf( powf((a.x - b.x), 2.0) +  powf((a.y - b.y), 2.0));
    
    return length;
}

@end
