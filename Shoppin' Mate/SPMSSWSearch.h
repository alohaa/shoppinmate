//
//  SPMSSWSearch.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#ifndef Shoppin__Mate_SPMSSWSearch_h
#define Shoppin__Mate_SPMSSWSearch_h

// Algorithms
#import "SPMSScoringMatrix.h"
#include "SWAligmentMethod.h"

// Database
#import "SPMSSWSearchDatabase.h"

// Operations
#import "SPMSSWSearchOperation.h"

#endif
