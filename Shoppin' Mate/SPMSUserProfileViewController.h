//
//  SPMSUserProfileViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/26/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import <RESideMenu.h>

@interface SPMSUserProfileViewController : UIViewController

@property(strong, nonatomic) IBOutlet UIImageView* userCoverImageView;
@property(strong, nonatomic) IBOutlet UIImageView* userAvatarImageView;
@property(strong, nonatomic) IBOutlet UILabel* userFullnameLabel;
@property(strong, nonatomic) IBOutlet UILabel* userEmailLabel;
@property(strong, nonatomic) IBOutlet UILabel* userDOBLabel;

@end
