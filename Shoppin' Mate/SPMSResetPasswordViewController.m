//
//  SPMSResetPasswordViewController.m
//  Shoppin' Mate
//
//  Created by Hiro on 7/14/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSResetPasswordViewController.h"
#import "SPMSWebDataService.h"
#import "SPMSWebAccountService.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSUserDataService.h"
#import "SVProgressHUD.h"

@interface SPMSResetPasswordViewController ()

@end

@implementation SPMSResetPasswordViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.emailTextField.delegate = self;
  // Check state of Email & Password input to validate
  [self checkForValidLResetInput];
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  NSInteger nextTag = textField.tag + 1;
  // Try to find next responder to navigate through TextFields
  UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
  if (nextResponder) {
    // Found next responder, so set it.
    [nextResponder becomeFirstResponder];
  } else {
    // Not found, so remove keyboard.
    [textField resignFirstResponder];
    [[[[SPMSWebAccountService sharedService]
        resetPasswordWithEmail:self.emailTextField.text]
        deliverOn:RACScheduler.mainThreadScheduler]
        subscribeNext:^(NSNumber* reset) {
            self.resetButton.enabled = YES;
            BOOL isResetLogin = [reset boolValue];
            // Dimiss Login View if logged-in successfully
            if (isResetLogin) {
              [self.presentingViewController dismissViewControllerAnimated:YES
                                                                completion:nil];
            }
        }];
  }
  return NO;
}

- (void)textFieldDidBeginEditing:(UITextField*)textField {
  if (textField == self.emailTextField) {
    [self.emailValidateStatusIndicatorImageView setHidden:NO];
  }
}

- (void)textFieldDidEndEditing:(UITextField*)textField {
  if (textField == self.emailTextField) {
    [self.emailValidateStatusIndicatorImageView setHidden:YES];
  }
}

- (void)checkForValidLResetInput {
  RACSignal* validEmailSignal =
      [self.emailTextField.rac_textSignal map:^id(NSString* text) {
          return @([[SPMSWebAccountService sharedService] isValidEmail:text]);
      }];
  // If String is not valid, text color is red, otherwise is green
  RAC(self.emailValidateStatusIndicatorImageView,
      highlighted) = [validEmailSignal
      map:^id(NSNumber* isValidEmail) { return @(![isValidEmail boolValue]); }];

  // Signal to detect to enable/disable "Reset" button whether the input is
  // valid or not
  RACSignal* isActiveResetBTNSignal =
      [RACSignal combineLatest:@[ validEmailSignal ]
                        reduce:^id(NSNumber* isValidEmail) {
                            return @([isValidEmail boolValue]);
                        }];
  // Enable/Disable "Login" button whether the input is valid or not
  [isActiveResetBTNSignal subscribeNext:^(NSNumber* isValidLogin) {
      self.resetButton.enabled = [isValidLogin boolValue];
  }];
}

- (IBAction)backToLoginButtonAction:(id)sender {
  [self.presentingViewController dismissViewControllerAnimated:YES
                                                    completion:nil];
}

- (IBAction)resetButtonPressed:(UIButton*)sender {
  // 1) Show status
  [SVProgressHUD showWithStatus:@"Sending Request"];

  // 2) Get a concurrent queue form the system
  dispatch_queue_t concurrentQueue =
      dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

  // 3) Load picker in background
  dispatch_async(concurrentQueue, ^{
      [[[[SPMSWebDataService sharedService]
          checkExistedAccountWithEmail:self.emailTextField.text]
          deliverOn:RACScheduler.mainThreadScheduler]
          subscribeNext:^(NSNumber* result) {
              if (result.intValue == 0) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc]
                        initWithTitle:@"Email Address Error"
                              message:@"The email address you provided is not "
                              @"associated with any existing account."
                             delegate:self
                    cancelButtonTitle:@"OK"
                    otherButtonTitles:nil];
                [alert show];
              } else {
                dispatch_async(dispatch_get_main_queue(), ^{

                    [[[[SPMSWebDataService sharedService]
                        resetPasswordWithEmail:self.emailTextField.text]
                        deliverOn:RACScheduler.mainThreadScheduler]
                        subscribeNext:^(NSNumber* result) {
                            if (result.intValue == 0) {
                            } else {
                              UIAlertView* alert = [[UIAlertView alloc]
                                      initWithTitle:@"Forgot your password?"
                                            message:@"Instructions to reset "
                                            @"your password have now "
                                            @"been sent to your email "
                                            @"address."
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
                              [alert show];
                            }
                            [self.presentingViewController
                                dismissViewControllerAnimated:YES
                                                   completion:nil];
                            [SVProgressHUD dismiss];
                        }];
                });
              };
          }];
  });
}
@end
