//
//  SPMSSWSearchDatabase.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "SPMSSWSearchDatasource.h"
#import "SPMSSWSearchDelegate.h"
#import "SPMSSWSearchOperation.h"
#import "SPMSScoringMatrix.h"
#import "SPMSSWObjects.h"

#define ScoringSegmentLenght 3

@interface SPMSSWSearchDatabase : NSObject

@property (strong, atomic) NSMutableSet *elements;
@property (weak, nonatomic) id <SPMSSWSearchDatasource> datasource;
@property (weak, nonatomic) id <SPMSSWSearchDelegate> delegate;

+ (SPMSSWSearchDatabase *)sharedDatabase;

- (void)fetchAndSaveToFileFromURL:(NSString *)url toFilePath:(NSString *)filePath;

- (void)addObject:(id)obj forKey:(NSString *)key;
- (void)addObjects:(NSArray *)obj forKey:(NSString *)key;
- (void)addObjects:(NSArray *)objs forKeys:(NSArray *)keys;
- (void)addObjects:(NSArray *)objs forKeyPaths:(NSArray *)KeyPaths;

- (void)addManagedObject:(NSManagedObject *)obj forKey:(NSString *)key;
- (void)addManagedObjects:(NSArray *)objs forKey:(NSString *)key;
- (void)addManagedObjects:(NSArray *)objs forKeys:(NSArray *)keys;
- (void)addManagedObjects:(NSArray *)objs forKeyPaths:(NSArray *)KeyPaths;

- (NSMutableSet *)objectsForSegment:(NSString *)key;
- (void)searchString:(NSString *)searchedString withOperation:(ScoringOperationType)operationType;

@end
