//
//  Product.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/7/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Product.h"
#import "Promotion.h"
#import "NSDictionary+Verified.h"
#import <RPJSONValidator.h>
#import <RPValidatorPredicate.h>

@implementation Product

+ (Product*)productForDictionary:(NSDictionary*)dictionary {
  if (![self isValidJSONWithDictionary:dictionary]) {
    return nil;
  }
  Product* aProduct = [[Product alloc] init];
  aProduct.categoryName = [dictionary verifiedObjectForKey:@"CategoryName"];
  NSDateFormatter* dateFormatter = [self dateFormatter];
  aProduct.createdDate = [dateFormatter
      dateFromString:[dictionary verifiedObjectForKey:@"CreatedDate"]];
  aProduct.discount = [dictionary verifiedObjectForKey:@"Discount"];
  aProduct.hasPromotion = [dictionary verifiedObjectForKey:@"HasPromotion"];
  aProduct.imageURL = [dictionary verifiedObjectForKey:@"Image"];
  aProduct.price = [dictionary verifiedObjectForKey:@"Price"];
  aProduct.productDescription =
      [dictionary verifiedObjectForKey:@"Description"];
  aProduct.productID = [dictionary verifiedObjectForKey:@"ProductId"];
  aProduct.productName = [dictionary verifiedObjectForKey:@"ProductName"];
  aProduct.mapId = [dictionary verifiedObjectForKey:@"MapId"];
  NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
  [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
  aProduct.pointId =
      [formatter numberFromString:[dictionary verifiedObjectForKey:@"PointId"]];
  aProduct.beaconXCoordinator =
      [dictionary verifiedObjectForKey:@"XCoordinator"];
  aProduct.beaconYCoordinator =
      [dictionary verifiedObjectForKey:@"YCoordinator"];
  aProduct.promotion = nil;

  return aProduct;
}

+ (NSMutableSet*)productsForDictionary:(NSDictionary*)dicts {
  NSMutableSet* products;
  NSMutableArray* array = [[NSMutableArray alloc] init];
  for (NSDictionary* productDict in dicts) {
    Product* aProduct = [self productForDictionary:productDict];
    if (aProduct) {
      [array addObject:aProduct];
    }
  }
  products = [[NSMutableSet alloc] initWithArray:array];
  return products;
}

+ (BOOL)isValidJSONWithDictionary:(NSDictionary*)dictionary {
  NSError* error;
  [RPJSONValidator
      validateValuesFrom:dictionary
        withRequirements:@{
          @"CategoryName" : RPValidatorPredicate.isOptional.isString,
          @"CreatedDate" : RPValidatorPredicate.isOptional,
          @"Discount" : [RPValidatorPredicate.isNumber.isOptional
              valueIsGreaterThanOrEqualTo:[NSNumber numberWithInt:0]],
          @"HasPromotion" : RPValidatorPredicate.isBoolean.isOptional,
          @"Image" : RPValidatorPredicate.isOptional.isString,
          @"Price" : [RPValidatorPredicate.isNumber.isOptional
              valueIsGreaterThanOrEqualTo:[NSNumber numberWithInt:0]],
          @"Description" : RPValidatorPredicate.isOptional.isString,
          @"ProductId" : RPValidatorPredicate.isNotNull.isString,
          @"ProductName" : RPValidatorPredicate.isOptional.isString,
          @"MapId" : RPValidatorPredicate.isNotNull.isString,
          @"PointId" : RPValidatorPredicate.isNotNull.isString,
          @"XCoordinator" : RPValidatorPredicate.isNotNull.isNumber,
          @"YCoordinator" : RPValidatorPredicate.isNotNull.isNumber
        } error:&error];
  if (error) {
    return NO;
  }
  return YES;
}

+ (NSDateFormatter*)dateFormatter {
  NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.locale =
      [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  dateFormatter.dateFormat = DATE_FORMAT;
  return dateFormatter;
}

@end
