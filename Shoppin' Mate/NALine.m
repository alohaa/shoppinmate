//
//  NALine.m
//  ShopinMateClient
//
//  Created by Dat Truong on 6/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "NALine.h"
#import "NAMapView.h"
#define POINT_SIZE 20.0f
@implementation NALine

+ (id)lineWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
  return [[[self class] alloc] initWithStartPoint:startPoint endPoint:endPoint];
}

- (id)initWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
  self = [super init];
  if (self) {
    _startPoint = startPoint;
    _endPoint = endPoint;
  }
  return self;
}

- (void)updatePosition {
  if (!self.mapView) {
    return;
  }

  CGPoint newStartPoint = [self.mapView zoomRelativePoint:self.startPoint];
  CGPoint newEndPoint = [self.mapView zoomRelativePoint:self.endPoint];

  CGPoint a, b;
  a = newStartPoint;
  b = newEndPoint;
  if (a.x == b.x && a.y == b.y) {
    return;
  }

  CGRect lineRect;
  lineRect = [self lineRectFrom:b a:a];

  NALine* lineView = [[NALine alloc] initWithFrame:lineRect];

  CGFloat angle = atan2(a.y - b.y, a.x - b.x);
  lineView.layer.anchorPoint = CGPointMake(0.0, 0.0);
  lineView.center = CGPointMake(b.x, b.y);
  lineView.transform = CGAffineTransformMakeRotation(angle);

  [self.view removeFromSuperview];

  self.view = lineView;
  [self.mapView addSubview:self.view];
}

- (void)addToMapView:(NAMapView*)mapView animated:(BOOL)animate {
  NSAssert(!self.mapView, @"Annotation already added to map.");

  if (!self.view) {
    _view = [self createViewOnMapView:mapView];
  }

  [mapView addSubview:self.view];
  _mapView = mapView;

  _mapViewDelegate = mapView.mapViewDelegate;

  [self updatePosition];
}

- (CGRect)lineRectFrom:(CGPoint)b a:(CGPoint)a {
  CGRect lineRect;
  float rectWidth = [self lengthFrom:a to:b];
  lineRect = CGRectMake(a.x + POINT_SIZE / 2,
                        a.y + POINT_SIZE / 2,
                        rectWidth - POINT_SIZE / 2,
                        1.0);
  return lineRect;
}

- (UIView*)createViewOnMapView:(NAMapView*)mapView {
  CGPoint a, b;
  a = self.startPoint;
  b = self.endPoint;

  if (a.x == b.x && a.y == b.y) {
    return nil;
  }

  CGRect lineRect;
  lineRect = [self lineRectFrom:b a:a];

  NALine* lineView = [[NALine alloc] initWithFrame:lineRect];

  CGFloat angle = atan2(a.y - b.y, a.x - b.x);
  lineView.layer.anchorPoint = CGPointMake(0.0, 0.0);
  lineView.center = CGPointMake(b.x, b.y);
  lineView.transform = CGAffineTransformMakeRotation(angle);

  return lineView;
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setBackgroundColor:[UIColor yellowColor]];
  }
  return self;
}
///*
- (void)drawRect:(CGRect)rect {
  CAShapeLayer* shapelayer = [CAShapeLayer layer];
  [self.layer addSublayer:shapelayer];

  // draw a line
  UIBezierPath* path = [UIBezierPath bezierPath];
  [path moveToPoint:CGPointMake(0, 0.0)];
  [path addLineToPoint:CGPointMake(rect.size.width, 0.0)];

  CGFloat dashPattern[] = {2, 6, 4, 2};
  [path setLineDash:dashPattern count:4 phase:3];

  UIColor* fill = [UIColor grayColor];
  shapelayer.strokeStart = 0.0;
  shapelayer.strokeColor = fill.CGColor;
  shapelayer.lineWidth = 2.0;
  shapelayer.lineJoin = kCALineJoinMiter;
  shapelayer.lineDashPattern = @[ @(10), @(7) ];
  shapelayer.lineDashPhase = 3.0f;
  shapelayer.path = path.CGPath;
}
// */

- (float)lengthFrom:(CGPoint)a to:(CGPoint)b {
  float length;
  length = sqrtf(powf((a.x - b.x), 2.0) + powf((a.y - b.y), 2.0));
  return length;
}

@end
