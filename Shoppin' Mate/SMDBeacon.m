//
//  SMDBeacon.m
//  ShopinMateClient
//
//  Created by Dat Truong on 2014-06-08.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "SMDBeacon.h"
#import <CoreLocation/CoreLocation.h>
#import "SPMSBeaconAnnotation.h"

@implementation SMDBeacon

- (instancetype)initWithPoint:(CGPoint)point {
  self = [super init];
  if (self) {
    _annotation = [SPMSBeaconAnnotation annotationWithPoint:point];
  }
  return self;
}

- (instancetype)initWithBeaconData:(Beacon*)beacon {
  self = [super init];
  if (self) {
    self.pointId = beacon.pointId;
    CGPoint point = CGPointMake([beacon.xCoordinate floatValue],
                                [beacon.yCoordinate floatValue]);
    self.annotation = [SPMSBeaconAnnotation annotationWithPoint:point];
    self.alias = [beacon.pointId stringValue];
    self.annotation.radius = 40.0f;
    self.annotation.color = [UIColor redColor];
    self.minor = [beacon.minor intValue];
    self.major = [beacon.major intValue];
  }
  return self;
}

- (NSString*)description {
  return [NSString stringWithFormat:@"Minor: %lu Major: %lu",
                                    (unsigned long)self.minor,
                                    (unsigned long)self.major];
}

@end
