//
//  PESGraph+Helpers.m
//  ShopinMateClient
//
//  Created by Dat Truong on 7/5/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "PESGraph+Helpers.h"

@implementation PESGraph (Helpers)

-(NSString *)description
{
    NSString *desc;
    
    NSMutableString *processingDesc;
    processingDesc = [NSMutableString string];
    [processingDesc appendString:[NSString stringWithFormat:@"Nodes: \n%@", self.nodes]];
    [processingDesc appendString:[NSString stringWithFormat:@"Edges: \n%@", self.edges]];
    
    desc = [processingDesc copy];
    
    return desc;
}

@end
