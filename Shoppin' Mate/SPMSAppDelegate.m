//
//  SPMSAppDelegate.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSAppDelegate.h"

#import "SPMSBrowseProductViewController.h"
#import <CoreData+MagicalRecord.h>
#import "SPMSLeftSidebarViewController.h"
#import "SPMSRightSidebarViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SPMSSWSearchDatabase.h"
#import "SPMSFacebookService.h"
#import <AFNetworking/AFNetworking.h>
#import "SPMSWebDataService.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSReachabilityManager.h"
#import <NewRelicAgent/NewRelic.h>
#import "User.h"
#import "SPMSWebAccountService.h"
#import "SPMSFacebookService.h"
#import "SPMSOAuthClient.h"

@interface SPMSAppDelegate () {
}

@end

@implementation SPMSAppDelegate

- (BOOL)application:(UIApplication*)application
    didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
  // Setup MagicalRecord
  [MagicalRecord setupAutoMigratingCoreDataStack];
  [self loadMaps];
  [self configAppApperance];
  [self configFacebookSDK];
  [self configNewRelicSDK];
  [self checkLoginAndLogoutIfNeccessaary];
  [self configReachability];
  [self loadNotification];
  [self loadDataForSWSearch];
  [[UIApplication sharedApplication] setStatusBarHidden:NO];
  return YES;
}

#pragma mark - Config Facebook SDK
- (void)configFacebookSDK {
  [[SPMSFacebookService sharedFBServiceInstance] checkForCachedSession];
}

#pragma mark - Config NewRelic SDK
- (void)configNewRelicSDK {
  [NewRelicAgent
      startWithApplicationToken:@"AAd33b4abec73b6583bfb1692478dea163417fc887"];
}

#pragma mark - Reachability
- (void)configReachability {
  [[AFNetworkReachabilityManager sharedManager] startMonitoring];
  // Instantiate Shared Manager
  //  [SPMSReachabilityManager sharedManager];
}

#pragma mark - Load Maps
- (void)loadMaps {
  [[[[SPMSWebDataService sharedService] getMaps]
      deliverOn:[RACScheduler
                    schedulerWithPriority:RACSchedulerPriorityBackground]]
      subscribeNext:^(id x) {}];
}

#pragma mark - Appearance
- (void)configAppApperance {
  // Set Light-theme UIVNavigationBar
  [[UIApplication sharedApplication]
      setStatusBarStyle:UIStatusBarStyleLightContent];
  [[UIBarButtonItem appearance]
      setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000)
                             forBarMetrics:UIBarMetricsDefault];
  // Set custom "Back" button
  [[UINavigationBar appearance]
      setBackIndicatorImage:[UIImage
                                imageNamed:@"Navigationbar_BackIndicator_BTN"]];
  [[UINavigationBar appearance]
      setBackIndicatorTransitionMaskImage:
          [UIImage imageNamed:@"Navigationbar_BackIndicator_BTN"]];
  [[UINavigationBar appearance]
      setTitleTextAttributes:
          [NSDictionary
              dictionaryWithObjectsAndKeys:
                  [UIColor whiteColor],
                  NSForegroundColorAttributeName,
                  [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0],
                  NSFontAttributeName,
                  nil]];
  // Change NavigationbarBar background color
  if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
    [[UINavigationBar appearance]
        setTintColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
  } else {
    [[UINavigationBar appearance]
        setBarTintColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
  }
}

#pragma mark - Load Notification
- (void)loadNotification {
  [[[[SPMSWebDataService sharedService]
      getTotalNumberofAvailablePromotionWithEndDate:[NSDate date]]
      deliverOn:[RACScheduler
                    schedulerWithPriority:RACSchedulerPriorityBackground]]
      subscribeNext:^(NSNumber* number) {
          [[NSUserDefaults standardUserDefaults]
              setInteger:[number integerValue]
                  forKey:NSUSERDEFAULTS_PROMOTION_COUNT_KEY];
          [[NSUserDefaults standardUserDefaults] synchronize];
      }];
}
#pragma mark - Load Data for SW Search Database
- (void)loadDataForSWSearch {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                 ^{
      NSString* docsDir;
      NSArray* dirPaths;

      dirPaths = NSSearchPathForDirectoriesInDomains(
          NSDocumentDirectory, NSUserDomainMask, YES);
      docsDir = [dirPaths objectAtIndex:0];
      NSString* databasePath = [[NSString alloc]
          initWithString:[docsDir stringByAppendingPathComponent:
                                      SW_SEARCH_DB_LOCAL_DATA_FILE]];

      [[SPMSSWSearchDatabase sharedDatabase]
          fetchAndSaveToFileFromURL:
              [NSString
                  stringWithFormat:@"%@/api/product/findall", WEB_SERVER_URL]
                         toFilePath:databasePath];
  });
}

#pragma mark - Check Login
- (void)checkLoginAndLogoutIfNeccessaary {
  User* currentUSer = [User MR_findFirst];
  BOOL isHadAccessToken = [[SPMSOAuthClient sharedInstance] isHadAccessToken];
  BOOL isSetUserDefault = [[NSUserDefaults standardUserDefaults]
      boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  if (!currentUSer || !isSetUserDefault || !isHadAccessToken) {
    if ([[SPMSFacebookService sharedFBServiceInstance] isLoggedIn]) {
      // Logout from FB Account
      [[[[SPMSFacebookService sharedFBServiceInstance] logoutFB]
          deliverOn:RACScheduler.mainThreadScheduler] subscribeCompleted:^{//
                                                                         }];
    } else {
      // Logout from Email Account
      [[SPMSWebAccountService sharedService] logout];
    }
  }
}

#pragma mark - Delegate
- (BOOL)application:(UIApplication*)application
              openURL:(NSURL*)url
    sourceApplication:(NSString*)sourceApplication
           annotation:(id)annotation {
  return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

- (void)applicationWillResignActive:(UIApplication*)application {
  // Sent when the application is about to move from active to inactive state.
  // This can occur for certain types of temporary interruptions (such as an
  // incoming phone call or SMS message) or when the user quits the application
  // and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down
  // OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication*)application {
  // Use this method to release shared resources, save user data, invalidate
  // timers, and store enough application state information to restore your
  // application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called
  // instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication*)application {
  // Called as part of the transition from the background to the inactive state;
  // here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication*)application {
  // Restart any tasks that were paused (or not yet started) while the
  // application was inactive. If the application was previously in the
  // background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication*)application {
  // Cleanup MagicalRecord
  [MagicalRecord cleanUp];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL*)applicationDocumentsDirectory {
  return [[[NSFileManager defaultManager]
      URLsForDirectory:NSDocumentDirectory
             inDomains:NSUserDomainMask] lastObject];
}

@end
