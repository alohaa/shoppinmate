//
//  SPMSReachabilityManager.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/20/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Reachability;

@interface SPMSReachabilityManager : NSObject

@property(nonatomic, strong) Reachability* reachability;

#pragma mark Shared Manager
+ (SPMSReachabilityManager*)sharedManager;

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable;
+ (BOOL)isUnreachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;

@end
