//
//  SPMSSPMSDataPersistentOperation.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/27/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSSPMSDataPersistentOperation.h"

@implementation SPMSSPMSDataPersistentOperation

#pragma mark - Initialization
- (instancetype)initWithRequest:(NSMutableURLRequest*)aRequest {
  if (self = [super init]) {
    self.headerParamString =
        [aRequest valueForHTTPHeaderField:@"Authorization"];
    self.httpMethodString = [aRequest HTTPMethod];
    self.httpBodyParamString =
        [[NSString alloc] initWithData:[aRequest HTTPBody]
                              encoding:NSUTF8StringEncoding];
    self.urlString = [[aRequest URL] absoluteString];
  }
  return self;
}

- (instancetype)initWithURL:(NSString*)aURLString
                 HTTPMethod:(NSString*)aHTTPMethodString
            HTTPHeaderParam:(NSString*)aHTTPHeaderParamString
                   HTTPBody:(NSString*)aHTTPBodyString {
  if (self = [super init]) {
    self.urlString = aURLString;
    self.headerParamString = aHTTPHeaderParamString;
    self.httpBodyParamString = aHTTPBodyString;
    self.httpMethodString = aHTTPMethodString;
  }
  return self;
}

#pragma mark - Action

- (RACSignal*)peformOfflineRequestWithClient:(NSURLSession*)client {
  NSMutableURLRequest* request = [self urlRequestFromPersistentOperaion];
  return
      [RACSignal createSignal:^RACDisposable * (id<RACSubscriber> subscriber) {
          NSURLSessionDataTask* task = [client
              dataTaskWithRequest:request
                completionHandler:^(NSData* data,
                                    NSURLResponse* response,
                                    NSError* error) {
                    if (error) {
                      NSError* aError = [self createErrorWithData:request];
                      [subscriber sendError:aError];
                    } else if (!data) {
                      NSError* error =
                          [self createErrorWithData:
                                    @"No data was received from the server."];
                      [subscriber sendError:error];
                    } else {
                      [subscriber sendCompleted];
                    }
                }];

          [task resume];

          return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
      }];
}

- (NSMutableURLRequest*)urlRequestFromPersistentOperaion {
  if (!self) {
    return nil;
  }
  NSURL* url = [NSURL URLWithString:self.urlString];
  NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
  [request addValue:self.headerParamString forHTTPHeaderField:@"Authorization"];
  [request setHTTPBody:[self.httpBodyParamString
                           dataUsingEncoding:NSUTF8StringEncoding]];
  [request setHTTPMethod:self.httpMethodString];
  return request;
}

#pragma mark - Generate Error containing Request
- (NSError*)createErrorWithData:(id)data {
  NSDictionary* userInfo = @{NSLocalizedDescriptionKey : data};
  NSError* error =
      [NSError errorWithDomain:ERROR_DOMAIN code:ERROR_CODE userInfo:userInfo];
  return error;
}

@end
