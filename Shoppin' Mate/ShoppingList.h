//
//  ShoppingList.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoppingList : NSObject

@property(nonatomic, strong) NSDate* createdDate;
@property(nonatomic, strong) NSNumber* shoppingListID;
@property(nonatomic, strong) NSString* shoppingListName;
@property(nonatomic, strong) NSString* userId;
@property(nonatomic, strong) NSMutableSet* products;

+ (ShoppingList*)shoppingListForDictionary:(NSDictionary*)dictionary;

@end
