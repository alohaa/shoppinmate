//
//  SPMSFacebookServiceObject.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/9/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface SPMSFacebookService : NSObject

+ (id)sharedFBServiceInstance;

- (BOOL)isLoggedIn;

// Check for a cached session
- (void)checkForCachedSession;

// Login
- (RACSignal*)loginFBSignal;

// Logout
- (RACSignal*)logoutFB;

// Get url of large facebook image for fbid/username specified
- (NSURL*)urlForProfilePictureSizeLargeForFBID:(NSString*)facebookID;

// Get url of normal facebook image for fbid/username specified
- (NSURL*)urlForProfilePictureSizeNormalForFBID:(NSString*)facebookID;

// Get url of small facebook image for fbid/username specified

- (NSURL*)urlForProfilePictureSizeSmallForFBID:(NSString*)facebookID;

// Get url of square facebook image for fbid/username specified
- (NSURL*)urlForProfilePictureSizeSquareForFBID:(NSString*)facebookID;

// Get url of the image that has a height closest to pixel height specified for
// fbid/username specified
- (NSURL*)urlForProfilePictureWithHeightClosestTo:(NSInteger)pixels
                                          ForFBID:(NSString*)facebookID;

// Get url of the image that has a width closest to pixel width specified for
// fbid/username specified
- (NSURL*)urlForProfilePictureWithWidthClosestTo:(NSInteger)pixels
                                         ForFBID:(NSString*)facebookID;

// Get url of the normal sized cover photo for fbid/username specified
- (void)requestURLForCoverPhotoSizeNormalForFBID:(NSString*)facebookID
                               completionHandler:
                                   (void (^)(NSURL* url,
                                             NSError* error))completion;

@end
