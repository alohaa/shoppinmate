//
//  Map.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/31/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Map.h"
#import "Beacon.h"
#import "Shelf.h"


@implementation Map

@dynamic createdDate;
@dynamic imageLink;
@dynamic lastUpdatedDate;
@dynamic level;
@dynamic mapId;
@dynamic mapName;
@dynamic beacons;
@dynamic shelves;

@end
