//
//  SPMSCategoryButton.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMSCategoryButton : UIButton

- (id)initWithTitleString:(NSString*)title
                     font:(UIFont*)font
                        x:(CGFloat)x
                        y:(CGFloat)y;

@end
