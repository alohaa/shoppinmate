//
//  SPMSWebDataService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>
#import "ShoppingList.h"
#import "Product.h"

@class RACSignal;

@interface SPMSWebDataService : NSObject<NSURLSessionDelegate>

@property(nonatomic, copy) NSURL* baseUrl;
@property(nonatomic, copy) NSURLSession* client;

+ (SPMSWebDataService*)sharedService;

- (RACSignal*)getProductListWithPageNumber:(NSUInteger)pageNumber;
- (RACSignal*)fetchProductListOfShelfWithFurnitureId:(NSUInteger)furnitureID
                                          pageNumber:(NSUInteger)pageNumber;

- (RACSignal*)checkExistedAccountWithEmail:(NSString*)email;
- (RACSignal*)registerAccountWithEmail:(NSString*)email
                              password:(NSString*)password
                              fullName:(NSString*)fullName
                           avatarImage:(UIImage*)avatarImage;
- (RACSignal*)updateAccountWithFullName:(NSString*)fullName
                            avatarImage:(UIImage*)avatarImage
                                 gender:(NSNumber*)gender
                            dateOfBirth:(NSDate*)dateOfBirth;
- (RACSignal*)getUserProfile;
- (RACSignal*)getUserProfileWithToken:(NSString*)accessToken;
- (RACSignal*)getShoppingListsWithPageNumber:(NSUInteger)pageNumber;
- (RACSignal*)addProduct:(Product*)product
     toShoppingListArray:(NSArray*)shoppingListArray;
- (RACSignal*)removeProduct:(Product*)product
      fromShoppingListArray:(NSArray*)shoppingListArray;
- (RACSignal*)addNewShoppingListWithName:(NSString*)shoppingListName;
- (RACSignal*)editShoppingListWithShoppingListId:(NSNumber*)shoppingListId
                                shoppingListName:(NSString*)shoppingListName;
- (RACSignal*)deleteShoppingListWithShoppingListId:(NSNumber*)shoppingListId;
- (RACSignal*)getPromotionListWithEndDate:(NSDate*)endDate
                               pageNumber:(NSUInteger)pageNumber;
- (RACSignal*)getTotalNumberofAvailablePromotionWithEndDate:(NSDate*)endDate;
- (RACSignal*)resetPasswordWithEmail:(NSString*)email;

- (RACSignal*)getCategoryTypes;
- (RACSignal*)fetchCategoryTypeListWithPageNumber:(NSUInteger)pageNumber;
- (RACSignal*)getMaps;
- (RACSignal*)sendVisitedBeaconWithPointId:(NSNumber*)pointId;
@end
