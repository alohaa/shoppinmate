//
//  SPMSAppDelegate.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu.h>

@interface SPMSAppDelegate
    : UIResponder<UIApplicationDelegate, RESideMenuDelegate>

@property(strong, nonatomic) UIWindow* window;

- (NSURL*)applicationDocumentsDirectory;

@end
