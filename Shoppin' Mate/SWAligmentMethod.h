//
//  SWAligmentMethod.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#pragma once
#ifndef Shoppin__Mate_SWAligmentMethod_h
#define Shoppin__Mate_SWAligmentMethod_h

struct ScoringMatrixStruct {
  int scorePerfectMatch;
  int scoreNotPerfectMatchKeyboardAnalyseHelp;
  int scoreNotPerfectBecauseOfAccents;
  int scoreLetterAddition;
};

typedef struct ScoringMatrixStruct ScoringMatrixStruct;

int score2Strings(const char* seq1,
                  const char* seq2,
                  int lenSeq1,
                  int lenSeq2,
                  int** scoring,
                  int logEnable,
                  ScoringMatrixStruct scoringStructure);

void logCalculatedMatrix(const char* seq1,
                         const char* seq2,
                         int lenSeq1,
                         int lenSeq2,
                         ScoringMatrixStruct scoringStructure);

int** allocate2D(int rows, int cols);

#endif
