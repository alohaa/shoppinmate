//
//  SPMSShoppingListsTableViewCell.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/22/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSShoppingListsTableViewCell.h"
#import <UIImageView+WebCache.h>
#import "Product.h"
#import <ObjectiveSugar.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface SPMSShoppingListsTableViewCell ()

@property(nonatomic, strong) NSNumber* cellShoppingListID;

@end

@implementation SPMSShoppingListsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString*)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Add Views
    self.shoppingListNameLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(78, 15, 222, 20)];
    [self.shoppingListNameLabel
        setTextColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
    [self.shoppingListNameLabel
        setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f]];

    self.productCountLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(78, 37, 222, 17)];
    [self.productCountLabel
        setTextColor:
            [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000]];
    [self.productCountLabel
        setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f]];
    [self.productCountLabel setNumberOfLines:2];

    self.shoppingListImageView =
        [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 50, 50)];

    [self.shoppingListImageView setClipsToBounds:YES];
    // Create Circular User Avatar Image View
    self.shoppingListImageView.layer.cornerRadius =
        self.shoppingListImageView.frame.size.width / 2;

    // Add Border
    self.shoppingListImageView.layer.borderWidth = 2.0f;
    self.shoppingListImageView.layer.borderColor =
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000].CGColor;

    [self.contentView addSubview:self.shoppingListImageView];
    [self.contentView addSubview:self.shoppingListNameLabel];
    [self.contentView addSubview:self.productCountLabel];

    RAC(self, accessoryType) = [RACSignal
        combineLatest:@[
                        RACObserve(self, hadInShoppingLists),
                        RACObserve(self, cellShoppingListID)
                      ]
               reduce:^id(NSArray* listArray, NSNumber* myShoppingListID) {
                   for (ShoppingList* containList in listArray) {
                     if (myShoppingListID &&
                         [containList.shoppingListID
                             isEqualToNumber:myShoppingListID]) {
                       return @(UITableViewCellAccessoryCheckmark);
                     }
                   }
                   return @(UITableViewCellAccessoryNone);
               }];
    [self.shoppingListImageView setClipsToBounds:YES];

    // Remove inset of iOS 7 separators
    if ([self respondsToSelector:@selector(setSelector:)]) {
      self.separatorInset = UIEdgeInsetsZero;
    }
    // Setting the background color of the cell
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
  }
  return self;
}

- (void)setShoppingList:(ShoppingList*)shoppingList {
  self.shoppingListNameLabel.text = shoppingList.shoppingListName;
  if ([shoppingList.products count] > 0) {
    Product* aProduct = [[shoppingList.products allObjects] lastObject];
    if (aProduct) {
      [self.shoppingListImageView
          sd_setImageWithURL:[NSURL URLWithString:aProduct.imageURL]
            placeholderImage:[UIImage imageNamed:@"Photo"]];
    }
    NSString* text =
        [shoppingList.products count] > 1 ? @"Products" : @"Product";
    self.productCountLabel.text =
        [NSString stringWithFormat:@"%lu %@",
                                   (unsigned long)[shoppingList.products count],
                                   text];
  } else {
    [self.shoppingListImageView setImage:[UIImage imageNamed:@"Photo"]];
    self.productCountLabel.text = @"0 Product";
  }
}

- (void)setCellDataWithShoppingList:(ShoppingList*)shoppingList
                 hadInShoppingLists:(NSMutableArray*)hadInShoppingLists {
  self.cellShoppingListID = shoppingList.shoppingListID;
  [self setShoppingList:shoppingList];
  [self setHadInShoppingLists:hadInShoppingLists];
}

@end
