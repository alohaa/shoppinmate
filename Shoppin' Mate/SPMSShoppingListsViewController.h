//
//  SPMSShoppingListsViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu.h>
#import <UIScrollView+EmptyDataSet.h>
#import <MCSwipeTableViewCell.h>

@interface SPMSShoppingListsViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       UITextFieldDelegate,
                       DZNEmptyDataSetDelegate,
                       DZNEmptyDataSetSource,
                       MCSwipeTableViewCellDelegate>

@property(strong, nonatomic) IBOutlet UITableView* tableView;

@end
