//
//  SPMSDataPersistentOperationQueue.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/27/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BAPersistentOperationQueue.h>
#import <ReactiveCocoa.h>

@interface SPMSDataPersistentOperationQueue
    : NSObject<BAPersistentOperationQueueDelegate>

@property(strong, nonatomic) BAPersistentOperationQueue* offlineTasksQueue;
@property(nonatomic, copy) NSURLSession* client;

+ (SPMSDataPersistentOperationQueue*)sharedService;
- (void)enqueueWithRequest:(NSMutableURLRequest*)request;

@end
