//
//  SPMSDetailShoppingListViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSDetailShoppingListViewController.h"
#import "Product.h"
#import "SPMSSuggestedProductTableViewCell.h"
#import "SPMSDetailProductViewController.h"
#import "SPMSMapPagerViewController.h"
#import "SPMSEditShoppingListViewController.h"
#import "ShoppingList.h"
#import "SPMSEditShoppingListViewController.h"
#import "SPMSWebDataService.h"
#import <TSMessage.h>

@interface SPMSDetailShoppingListViewController ()
@property(assign, nonatomic) NSUInteger cellCount;
@property(nonatomic) BOOL isDeletingList;

@end

@implementation SPMSDetailShoppingListViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.productList = [[NSMutableArray alloc] init];
    self.shoppingListID = [[NSNumber alloc] init];
    self.shoppingListName = [[NSString alloc] init];
      self.isDeletingList = NO;
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetDelegate = self;
  self.tableView.emptyDataSetSource = self;

  // Config Sidebar
  [self configNavigationBar];
  // Config TableView layout
  [self configTableViewLayout];

  // Register Custom UITableViewCell for SearchDisplayController tableview
  [self.tableView registerClass:[SPMSSuggestedProductTableViewCell class]
         forCellReuseIdentifier:@"detailShoppingListCell"];
    self.cellCount = [self.productList count];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return self.cellCount;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"detailShoppingListCell";
  SPMSSuggestedProductTableViewCell* cell = (SPMSSuggestedProductTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (void)configureCell:(SPMSSuggestedProductTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
    UIView* crossView =
    [self viewWithImageName:@"ShoppingListsCell_DeleteShoppingList_ICO"];
    UIColor* redColor =
    [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000];
    
    [cell setDefaultColor:self.tableView.backgroundView.backgroundColor];
    [cell setDelegate:self];
    [cell setSwipeGestureWithView:crossView
                            color:redColor
                             mode:MCSwipeTableViewCellModeExit
                            state:MCSwipeTableViewCellState3
                  completionBlock:^(MCSwipeTableViewCell* cell,
                                    MCSwipeTableViewCellState state,
                                    MCSwipeTableViewCellMode mode) {
                      [self deleteProductWithCell:cell];
                  }];
     
  Product* product = (Product*)[self.productList objectAtIndex:indexPath.row];
  cell.product = product;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  Product* product = [self.productList objectAtIndex:indexPath.row];
  [self performSegueWithIdentifier:@"viewDetailProduct" sender:product];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 130.0f;
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No Product in Shopping List";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"Try to add new Product to this Shopping List";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"BrowseProduct_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView*)scrollView {
}

- (void)emptyDataSetDidTapButton:(UIScrollView*)scrollView {
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
  [self.tableView setBackgroundColor:[UIColor whiteColor]];
  // A little trick for removing the cell separators
  self.tableView.tableFooterView = [UIView new];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  self.navigationItem.title = @"Detail Shopping List";
  [self.navigationController.navigationBar setTranslucent:NO];

  // "Search Product" Button
  UIButton* editShoppingListBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  editShoppingListBTN.frame = CGRectMake(5, 0, 30, 30);
  [editShoppingListBTN
      setImage:[UIImage imageNamed:@"DetailShoppingList_EdiShoppingList_ICO"]
      forState:UIControlStateNormal];
  [editShoppingListBTN addTarget:self
                          action:@selector(navigateToEditShoppingListVC:)
                forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showShoppingListBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:editShoppingListBTN];

  // "Find Shortest Path" Button
  UIButton* findShortestPathBTN = [UIButton buttonWithType:UIButtonTypeSystem];
  findShortestPathBTN.frame = CGRectMake(0, 0, 30, 30);
  [findShortestPathBTN
      setImage:[UIImage
                   imageNamed:@"DetailShoppingList_Nav_FindShortestPath_ICO"]
      forState:UIControlStateNormal];
  [findShortestPathBTN addTarget:self
                          action:@selector(showMapView:)
                forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* findShortestPathBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:findShortestPathBTN];
  [self.navigationItem
      setRightBarButtonItems:[NSArray arrayWithObjects:findShortestPathBTNItem,
                                                       showShoppingListBTNItem,
                                                       nil]];
}

- (void)navigateToEditShoppingListVC:(id)sender {
  [self performSegueWithIdentifier:@"editList" sender:nil];
}

- (void)showMapView:(id)sender {
  if ([self.productList count] > 0) {
    [self performSegueWithIdentifier:@"viewShortestPath" sender:sender];
  }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
// preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewDetailProduct"]) {
    Product* aProduct = (Product*)sender;
    SPMSDetailProductViewController* detailVC =
        [segue destinationViewController];
    [detailVC setDetailProduct:aProduct];
  }

  if ([[segue identifier] isEqualToString:@"viewShortestPath"]) {
    SPMSMapPagerViewController* mapPagerVC = [segue destinationViewController];
    mapPagerVC.productList = self.productList;
  }

  if ([[segue identifier] isEqualToString:@"editList"]) {
    SPMSEditShoppingListViewController* editShoppingList =
        [segue destinationViewController];
    editShoppingList.shoppingListName = self.shoppingListName;
    editShoppingList.shoppingListID = self.shoppingListID;
  }
}

#pragma mark - Utility
     - (UIView*)viewWithImageName:(NSString*)imageName {
         UIImage* image = [UIImage imageNamed:imageName];
         UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
         imageView.contentMode = UIViewContentModeCenter;
         return imageView;
     }
     
- (void)dealloc {
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
}

#pragma mark - Delete Shopping List
- (void)deleteProductWithCell:(MCSwipeTableViewCell*)cell {
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    Product* aProduct =
    [self.productList objectAtIndex:indexPath.row];
    if (!aProduct) {
        return;
    }
    [[[[SPMSWebDataService sharedService]
       removeProduct:aProduct fromShoppingListArray:@[self.shoppingList]]
      deliverOn:RACScheduler
      .mainThreadScheduler] subscribeNext:^(ShoppingList*
                                            newShoppingList) {
        self.cellCount--;
        [self.tableView deleteRowsAtIndexPaths:@[ indexPath ]
                              withRowAnimation:UITableViewRowAnimationFade];
        [TSMessage
         showNotificationInViewController:
         self title:NSLocalizedString(@"Selected Product Deleted",
                                      nil)
         subtitle:nil
         image:nil
         type:TSMessageNotificationTypeSuccess
         duration:
         TSMessageNotificationDurationAutomatic
         callback:nil
         buttonTitle:nil
         buttonCallback:nil
         atPosition:TSMessageNotificationPositionTop
         canBeDismissedByUser:YES];
    } error:^(NSError* error) {
        [cell swipeToOriginWithCompletion:nil];
        [TSMessage
         showNotificationInViewController:self
         title:NSLocalizedString(
                                 @"Delete selected Product"
                                 @"Failed. \nAdd to Offline "
                                 @"Queue to retry later",
                                 nil)
         subtitle:nil
         image:nil
         type:TSMessageNotificationTypeError
         duration:
         TSMessageNotificationDurationAutomatic
         callback:nil
         buttonTitle:nil
         buttonCallback:nil
         atPosition:TSMessageNotificationPositionTop
         canBeDismissedByUser:YES];
        self.isDeletingList = NO;
    } completed:^{ self.isDeletingList = NO; }];
}


@end
