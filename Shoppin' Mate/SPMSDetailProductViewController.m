//
//  SPMSDetailViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSDetailProductViewController.h"
#import "SPMSAddProductToListViewController.h"
#import "SPMSSuggestedProductTableViewCell.h"
#import <UIImageView+WebCache.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "RACEXTScope.h"
#import "SPMSWebDataService.h"
#import <SVPullToRefresh.h>
#import "SMDBeacon.h"
#import "SPMSBeaconAnnotation.h"
#import "SPMSMapViewController.h"
#import "SPMSCurrencyFormatter.h"
#import "Map.h"
#import <ObjectiveSugar.h>

@interface SPMSDetailProductViewController ()

@property(strong, nonatomic) NSMutableArray* suggestedProductList;
@property(assign, nonatomic) NSUInteger cellCount;
@property(assign, nonatomic) NSUInteger pageNumber;

@end

@implementation SPMSDetailProductViewController
#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.suggestedProductList = [[NSMutableArray alloc] init];
    self.cellCount = 0;
    self.pageNumber = 1;
  }
  return self;
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.dataSource = self;
  self.tableView.delegate = self;

  [self configureView];
  [self configNavigationBar];
  [self configureButton];
  // Register Custom UITableViewCell for SearchDisplayController tableview
  [self.tableView registerClass:[SPMSSuggestedProductTableViewCell class]
         forCellReuseIdentifier:@"suggestedProductCell"];

  // Reload Data
  [self loadProductsWithPageNumber:self.pageNumber];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self configureViewAppearance];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [self.detailProductImageView setClipsToBounds:YES];
}

#pragma mark - Load Data
- (void)setDetailProduct:(Product*)product {
  if (!product) {
    return;
  }
  self.product = product;
  [self configureView];
}

- (void)loadProductsWithPageNumber:(NSUInteger)page {
  @weakify(self);
  [[[[SPMSWebDataService sharedService] getProductListWithPageNumber:page]
      deliverOn:RACScheduler.mainThreadScheduler]
      subscribeNext:^(NSArray* products) {
          @strongify(self);
          [self.suggestedProductList addObjectsFromArray:products];
          self.cellCount = [self.suggestedProductList count];
          [self.tableView reloadData];
      }
      error:^(NSError* error) {}
      completed:^{ self.pageNumber = self.pageNumber + 1; }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return self.cellCount;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"suggestedProductCell";
  SPMSSuggestedProductTableViewCell* cell = (SPMSSuggestedProductTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];

  return cell;
}

- (void)configureCell:(SPMSSuggestedProductTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  Product* product =
      (Product*)[self.suggestedProductList objectAtIndex:indexPath.row];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  cell.product = product;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  Product* aProduct = [self.suggestedProductList objectAtIndex:indexPath.row];
  [self setDetailProduct:aProduct];
  [self.tableView setContentOffset:CGPointZero animated:YES];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 130.0f;
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh {
  __weak SPMSDetailProductViewController* weakSelf = self;

  // Setup pull-to-refresh
  [self.tableView
      addPullToRefreshWithActionHandler:^{ [weakSelf insertRowAtTop]; }];

  [self.tableView.pullToRefreshView setTitle:@"Getting New Suggested Products"
                                    forState:SVPullToRefreshStateLoading];

  // Setup infinite scrolling
  [self.tableView
      addInfiniteScrollingWithActionHandler:^{ [weakSelf insertRowAtBottom]; }];
}

- (void)insertRowAtTop {
}

- (void)insertRowAtBottom {
  __weak SPMSDetailProductViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      [self loadProductsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.infiniteScrollingView stopAnimating];
  });
}

#pragma mark - Add Product to List
- (void)showAddToListViewWithProduct:(Product*)product animated:(BOOL)animated {
  UIStoryboard* storyboard =
      [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  UIViewController* navListViewController = [storyboard
      instantiateViewControllerWithIdentifier:@"addToListNavViewController"];

  [navListViewController
      setModalPresentationStyle:UIModalPresentationFullScreen];
  [self presentViewController:navListViewController
                     animated:animated
                   completion:^{
                       [[NSNotificationCenter defaultCenter]
                           postNotificationName:kAddToListNotification
                                         object:nil
                                       userInfo:[[NSDictionary alloc]
                                                    initWithObjectsAndKeys:
                                                        product,
                                                        @"productInList",
                                                        nil]];
                   }];
}

#pragma mark - Configure View
- (void)configureView {
  if (self.product) {
    self.detailProductCategoryLabel.text =
        [self.product.categoryName uppercaseString];
    NSNumberFormatter* currencyFormatter = [SPMSCurrencyFormatter formatter];
    self.detailProductPriceLabel.text =
        [currencyFormatter stringFromNumber:self.product.price];
    self.detailProductNameLabel.text = self.product.productName;
    self.detailProductDescriptionTextView.text =
        self.product.productDescription;
    [self.detailProductImageView
        sd_setImageWithURL:[NSURL URLWithString:self.product.imageURL]
          placeholderImage:[UIImage imageNamed:@"Photo"]];
  }
}

- (void)configureViewAppearance {
  [self.detailProductCategoryLabel
      setTextColor:
          [UIColor colorWithRed:0.945 green:0.769 blue:0.059 alpha:1.000]];
  [self.detailProductCategoryLabel
      setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0f]];
  [self.detailProductPriceLabel
      setTextColor:
          [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
  [self.detailProductPriceLabel
      setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f]];
  [self.detailProductNameLabel
      setTextColor:
          [UIColor colorWithRed:0.204 green:0.286 blue:0.369 alpha:1.000]];
  [self.detailProductNameLabel
      setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:20.0f]];
  [self.detailProductDescriptionTextView
      setTextColor:
          [UIColor colorWithRed:0.584 green:0.647 blue:0.651 alpha:1.000]];
  [self.detailProductDescriptionTextView
      setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f]];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)configureButton {
  [[self.addToShoppingListButton
      rac_signalForControlEvents:UIControlEventTouchUpInside]
      subscribeNext:^(id x) {
          if (self.product) {
            [self showAddToListViewWithProduct:self.product animated:YES];
          }
      }];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewProductLocation"]) {
    Map* map =
        [Map MR_findFirstByAttribute:@"mapId" withValue:self.product.mapId];
    if (map) {
      if (0 < [map.beacons count]) {
        NSMutableArray* beaconArray = [[NSMutableArray alloc] init];
        [[map.beacons array] each:^(Beacon* beacon) {
            if ([beacon.pointId intValue] == [self.product.pointId intValue]) {
              SMDBeacon* smdBeacon =
                  [[SMDBeacon alloc] initWithBeaconData:beacon];
              smdBeacon.annotation.productImageURLString =
                  self.product.imageURL;
              smdBeacon.annotation.title = self.product.productName;
              smdBeacon.annotation.subtitle = self.detailProductPriceLabel.text;
              [beaconArray addObject:smdBeacon];
              return;
            }
        }];
        SPMSMapViewController* mapViewController =
            [segue destinationViewController];
        mapViewController.floor = map.level;
        mapViewController.beacons = [NSMutableArray arrayWithArray:beaconArray];
      }
    }
  }
}

- (void)dealloc {
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
}

@end
