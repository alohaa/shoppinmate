//
//  ShoppingList.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "ShoppingList.h"
#import "NSDictionary+Verified.h"
#import "Product.h"
#import "User.h"
#import <RPJSONValidator.h>
#import <RPValidatorPredicate.h>

@implementation ShoppingList

+ (ShoppingList*)shoppingListForDictionary:(NSDictionary*)dictionary {
  if (![self isValidJSONWithDictionary:dictionary]) {
    return nil;
  }
  ShoppingList* aShoppingList = [ShoppingList new];
  NSDateFormatter* dateFormatter = [self dateFormatter];
  aShoppingList.createdDate = [dateFormatter
      dateFromString:[dictionary verifiedObjectForKey:@"CreatedDate"]];
  aShoppingList.shoppingListID =
      [dictionary verifiedObjectForKey:@"ShoppingListId"];
  aShoppingList.shoppingListName =
      [dictionary verifiedObjectForKey:@"ShoppingListName"];
  aShoppingList.userId = [dictionary verifiedObjectForKey:@"UserId"] != nil
                             ? [dictionary verifiedObjectForKey:@"UserId"]
                             : [[User MR_findFirst] userID];
  aShoppingList.products = [Product
      productsForDictionary:[dictionary verifiedObjectForKey:@"Products"]];
  return aShoppingList;
}

+ (BOOL)isValidJSONWithDictionary:(NSDictionary*)dictionary {
  NSError* error;
  [RPJSONValidator
      validateValuesFrom:dictionary
        withRequirements:@{
          @"CreatedDate" : RPValidatorPredicate.isNotNull,
          @"UserId" : RPValidatorPredicate.isOptional.isString,
          @"ShoppingListId" : [RPValidatorPredicate.isNumber.isNotNull
              valueIsGreaterThanOrEqualTo:[NSNumber numberWithInt:0]],
          @"ShoppingListName" : RPValidatorPredicate.isOptional.isString
        } error:&error];
  if (error) {
    return NO;
  }
  return YES;
}

+ (NSDateFormatter*)dateFormatter {
  NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.locale =
      [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  dateFormatter.dateFormat = DATE_FORMAT;
  return dateFormatter;
}

@end
