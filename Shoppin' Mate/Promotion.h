//
//  Promotion.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Product;

@interface Promotion : NSObject

@property(nonatomic, strong) NSString* content;
@property(nonatomic, strong) NSDate* createdDate;
@property(nonatomic, strong) NSDate* endDate;
@property(nonatomic, strong) NSString* promotionDescription;
@property(nonatomic, strong) NSNumber* promotionID;
@property(nonatomic, strong) NSString* promotionName;
@property(nonatomic, strong) NSDate* startDate;
@property(nonatomic, strong) NSString* title;
@property(nonatomic, strong) NSMutableSet* products;

+ (Promotion*)promotionForDictionary:(NSDictionary*)dictionary;

@end
