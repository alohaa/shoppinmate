//
//  SPMSDetailShelfViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 8/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIScrollView+EmptyDataSet.h>

@interface SPMSDetailShelfViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       DZNEmptyDataSetDelegate,
                       DZNEmptyDataSetSource>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(assign, nonatomic) NSUInteger furnitureId;

@end
