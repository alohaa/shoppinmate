//
//  SPMSCategoryListViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 8/6/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSCategoryListViewController.h"
#import "SPMSWebDataService.h"
#import <SVPullToRefresh.h>
#import "RACEXTScope.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SVProgressHUD.h>
#import "SPMSCategoryListTableViewCell.h"
#import "CategoryType.h"
#import "NSDictionary+Verified.h"
#import <ObjectiveSugar.h>

@interface SPMSCategoryListViewController ()

@property(strong, nonatomic) NSMutableArray* categoryList;
@property(assign, nonatomic) NSUInteger cellCount;
@property(assign, nonatomic) NSUInteger pageNumber;

@end

@implementation SPMSCategoryListViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.categoryList = [[NSMutableArray alloc] init];
    self.cellCount = 0;
    self.pageNumber = 1;
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(setSelectedCategoriesInfo:)
               name:kSendSelectedCategoryFromFilterVCNotification
             object:nil];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetDelegate = self;
  self.tableView.emptyDataSetSource = self;

  // Config Sidebar
  [self configNavigationBar];
  // Config TableView layout
  [self configTableViewLayout];

  // Reload Data
  [self loadCategoriesWithPageNumber:self.pageNumber];
  // Config PullToRefresh
  [self configPullToRefresh];
}

#pragma mark - Load Data
- (void)loadCategoriesWithPageNumber:(NSUInteger)page {
  if (self.pageNumber == 1) {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  }
  @weakify(self);
  [[[[SPMSWebDataService sharedService]
      fetchCategoryTypeListWithPageNumber:page]
      deliverOn:RACScheduler.mainThreadScheduler] subscribeNext:^(NSArray*
                                                                      list) {
      @strongify(self);
      [self.categoryList addObjectsFromArray:list];
      self.cellCount = [self.categoryList count];
      [self.tableView reloadData];
  } error:^(NSError* error) {
      [SVProgressHUD
          showErrorWithStatus:
              [NSString stringWithFormat:@"%@", [error localizedDescription]]];
  } completed:^{
      self.pageNumber = self.pageNumber + 1;
      [SVProgressHUD dismiss];
  }];
}

- (void)setSelectedCategoriesInfo:(NSNotification*)notification {
  NSMutableArray* selectedCategoryArray =
      [[notification userInfo] verifiedObjectForKey:@"selectedCategories"];
  if (selectedCategoryArray &&
      (self.selectedCategories != selectedCategoryArray)) {
    self.selectedCategories = selectedCategoryArray;
    [self.tableView reloadData];
  }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return [self.categoryList count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"categoryCell";
  SPMSCategoryListTableViewCell* cell = (SPMSCategoryListTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (void)configureCell:(SPMSCategoryListTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  CategoryType* category =
      (CategoryType*)[self.categoryList objectAtIndex:indexPath.row];

  cell.accessoryType = UITableViewCellAccessoryNone;
  if (0 < [self.selectedCategories count]) {
    [self.selectedCategories each:^(NSString* selectedCatName) {
        if ([category.categoryName isEqualToString:selectedCatName]) {
          cell.accessoryType = UITableViewCellAccessoryCheckmark;
          return;
        }
    }];
  }

  cell.category = category;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  SPMSCategoryListTableViewCell* cell = (SPMSCategoryListTableViewCell*)
      [tableView cellForRowAtIndexPath:indexPath];
  CategoryType* category =
      (CategoryType*)[self.categoryList objectAtIndex:indexPath.row];

  [[NSNotificationCenter defaultCenter]
      postNotificationName:kSelectCategoryInCategoryListVCNotification
                    object:nil
                  userInfo:[[NSDictionary alloc]
                               initWithObjectsAndKeys:category.categoryName,
                                                      @"selectedCategory",
                                                      nil]];

  if (cell.accessoryType == UITableViewCellAccessoryNone) {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 60.0f;
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh {
  __weak SPMSCategoryListViewController* weakSelf = self;

  [self.tableView.pullToRefreshView setTitle:@"Getting Products of Shelf"
                                    forState:SVPullToRefreshStateLoading];

  // Setup infinite scrolling
  [self.tableView
      addInfiniteScrollingWithActionHandler:^{ [weakSelf insertRowAtBottom]; }];
}

- (void)insertRowAtBottom {
  __weak SPMSCategoryListViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      [self loadCategoriesWithPageNumber:self.pageNumber];
      [weakSelf.tableView.infiniteScrollingView stopAnimating];
  });
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No Category";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"There is no category";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"BrowseProduct_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView*)scrollView {
}

- (void)emptyDataSetDidTapButton:(UIScrollView*)scrollView {
}

#pragma mark - Actions
- (void)dimissView:(id)sender {
  [self.presentingViewController dismissViewControllerAnimated:YES
                                                    completion:nil];
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
  [self.tableView setBackgroundColor:[UIColor whiteColor]];
  // A little trick for removing the cell separators
  self.tableView.tableFooterView = [UIView new];
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
  // "Cancel" Button
  UIButton* cancelBTN = [UIButton buttonWithType:UIButtonTypeSystem];
  cancelBTN.frame = CGRectMake(0, 0, 50, 30);
  [cancelBTN setTitle:@"Done" forState:UIControlStateNormal];
  [cancelBTN addTarget:self
                action:@selector(dimissView:)
      forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* cancelBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:cancelBTN];

  [self.navigationItem setLeftBarButtonItem:cancelBTNItem];
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kSendSelectedCategoryFromFilterVCNotification
              object:nil];
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
}

@end
