//
//  SPMSSearchProductTableViewCell.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPMSSWSearchObject.h"
#import <TTTAttributedLabel.h>
#import <WCFastCell.h>

@interface SPMSSearchProductTableViewCell : WCFastCell {
 @private
  UIImageView* productImageView;
  UILabel* productNameLabel;
  UILabel* productDescLabel;
  TTTAttributedLabel* productDiscountLabel;
  TTTAttributedLabel* productPriceLabel;
}

@property(assign, nonatomic) SPMSSWSearchObject* searchObject;

@end
