//
//  Map.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/23/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSMapOperator.h"
#import "NSDictionary+Verified.h"
#import <RPJSONValidator.h>
#import <RPValidatorPredicate.h>
#import "MEDJsonGenerator.h"
#import "MEDBeaconPoint.h"
#import "MEDFurniture.h"
#import "Map.h"
#import "Beacon.h"
#import "Shelf.h"
#import <ObjectiveSugar.h>
#import "ADTiledMapSlicer.h"
#import <SDWebImageDownloader.h>

#define kM_FURNITURES @"furnitures"
#define kM_POINTS @"points"
#define kM_STENCILID 1

@interface SPMSMapOperator () {
  NSManagedObjectContext* managedObjectContext;
}
@end

@implementation SPMSMapOperator
#pragma mark - Initialization
+ (SPMSMapOperator*)sharedService {
  static SPMSMapOperator* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

- (id)init {
  if (self = [super init]) {
    managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
  }
  return self;
}

#pragma mark - Download Map
- (void)saveMapForDictionary:(NSDictionary*)dictionary {
  if (![self isValidJSONWithDictionary:dictionary]) {
    return;
  }
  NSDateFormatter* dateFormatter = [self dateFormatter];
  NSString* mapId = [dictionary verifiedObjectForKey:@"mapId"];
  NSDate* newLastUpdatedDate = [dateFormatter
      dateFromString:[dictionary verifiedObjectForKey:@"lastUpdatedDate"]];
  Map* existedMap = [Map MR_findFirstByAttribute:@"mapId" withValue:mapId];
  if (!existedMap) {
    // Create new Map
    Map* aMap = [Map MR_createInContext:managedObjectContext];
    aMap.createdDate = [dateFormatter
        dateFromString:[dictionary verifiedObjectForKey:@"createdDate"]];
    aMap.lastUpdatedDate = newLastUpdatedDate;
    aMap.mapId = mapId;
    aMap.mapName = [dictionary verifiedObjectForKey:@"alias"];
    aMap.imageLink = [dictionary verifiedObjectForKey:@"imageLink"];
    aMap.level = [dictionary verifiedObjectForKey:@"level"];
    NSString* mapData = [dictionary verifiedObjectForKey:@"mapData"];
    NSOrderedSet* beacons = [self beaconsFromMapData:mapData];
    aMap.beacons = beacons;
    NSSet* shelves = [self shelvesFromMapData:mapData];
    [aMap addShelves:shelves];

    // Slice Map
    [self sliceMapWithImageURLString:aMap.imageLink
                               floor:[aMap.level intValue]];
    [managedObjectContext MR_saveOnlySelfAndWait];
  } else {
    NSDate* oldLastUpdatedDate = existedMap.lastUpdatedDate;
    if (true) {
      // Update Map
      existedMap.createdDate = [dateFormatter
          dateFromString:[dictionary verifiedObjectForKey:@"createdDate"]];
      existedMap.lastUpdatedDate = newLastUpdatedDate;
      existedMap.mapId = mapId;
      existedMap.mapName = [dictionary verifiedObjectForKey:@"alias"];
      existedMap.imageLink = [dictionary verifiedObjectForKey:@"imageLink"];
      existedMap.level = [dictionary verifiedObjectForKey:@"level"];
      NSString* mapData = [dictionary verifiedObjectForKey:@"mapData"];
      NSOrderedSet* beacons = [self beaconsFromMapData:mapData];
      if (0 < [existedMap.beacons count]) {
        [[existedMap.beacons array] each:^(Beacon* beacon) {
            [managedObjectContext deleteObject:beacon];
        }];
      }
      existedMap.beacons = beacons;
      NSSet* shelves = [self shelvesFromMapData:mapData];
      if (0 < [existedMap.shelves count]) {
        [existedMap.shelves each:^(Shelf* shelf) {
            [managedObjectContext deleteObject:shelf];
        }];
      }
      [existedMap addShelves:shelves];
      // Slice Map
      [self sliceMapWithImageURLString:existedMap.imageLink
                                 floor:[existedMap.level intValue]];
      [managedObjectContext MR_saveOnlySelfAndWait];
    }
  }
}

- (NSMutableOrderedSet*)beaconsFromMapData:(NSString*)mapData {
  NSMutableOrderedSet* beacons = [[NSMutableOrderedSet alloc] init];
  if (mapData) {
    NSDictionary* mapDataDict;
    mapDataDict = [NSJSONSerialization
        JSONObjectWithData:[mapData dataUsingEncoding:NSUTF8StringEncoding]
                   options:0
                     error:nil];
    NSArray* jsonPoints = mapDataDict[kM_POINTS];
    MEDJsonGenerator* generator;
    generator = [[MEDJsonGenerator alloc] init];
    NSMutableArray* medBeaconPoints = [[NSMutableArray alloc] init];
    for (NSDictionary* pointDict in jsonPoints) {
      MEDBeaconPoint* point;
      point = [generator pointFromDict:pointDict];
      [medBeaconPoints addObject:point];
    }
    if (0 < [medBeaconPoints count]) {
      [medBeaconPoints each:^(MEDBeaconPoint* beaconPoint) {
          Beacon* beacon = [Beacon MR_createInContext:managedObjectContext];
          beacon.pointId = [NSNumber numberWithInt:beaconPoint.pointId];
          beacon.uuid = beaconPoint.uuid;
          beacon.major = [NSNumber numberWithInt:beaconPoint.major];
          beacon.minor = [NSNumber numberWithInt:beaconPoint.minor];
          beacon.xCoordinate =
              [NSNumber numberWithFloat:beaconPoint.position.x];
          beacon.yCoordinate =
              [NSNumber numberWithFloat:beaconPoint.position.y];
          beacon.graphData = [NSKeyedArchiver
              archivedDataWithRootObject:beaconPoint.graphDict];
          [managedObjectContext MR_saveOnlySelfAndWait];
          [beacons addObject:beacon];
      }];
    }
  }
  return beacons;
}

- (NSMutableSet*)shelvesFromMapData:(NSString*)mapData {
  NSMutableSet* shelves = [[NSMutableSet alloc] init];
  if (mapData) {
    NSDictionary* mapDataDict;
    mapDataDict = [NSJSONSerialization
        JSONObjectWithData:[mapData dataUsingEncoding:NSUTF8StringEncoding]
                   options:0
                     error:nil];
    NSArray* jsonFurnitures = mapDataDict[kM_FURNITURES];
    MEDJsonGenerator* generator;
    generator = [[MEDJsonGenerator alloc] init];
    NSMutableArray* medFurniturePoints = [[NSMutableArray alloc] init];
    for (NSDictionary* furnitureDict in jsonFurnitures) {
      MEDFurniture* furniture;
      furniture = [generator furnitureFromDict:furnitureDict];
      if (furniture.stencilId == kM_STENCILID) {
        [medFurniturePoints addObject:furniture];
      }
    }
    if (0 < [medFurniturePoints count]) {
      [medFurniturePoints each:^(MEDFurniture* furniture) {
          Shelf* shelf = [Shelf MR_createInContext:managedObjectContext];
          shelf.furnitureId = [NSNumber numberWithInt:furniture.furnitureId];
          shelf.name = furniture.name;
          shelf.pointId = [NSNumber numberWithInt:furniture.pointId];
          shelf.width = [NSNumber numberWithDouble:furniture.rect.size.width];
          shelf.height = [NSNumber numberWithDouble:furniture.rect.size.height];
          shelf.xCoordinate =
              [NSNumber numberWithFloat:furniture.rect.origin.x];
          shelf.yCoordinate =
              [NSNumber numberWithFloat:furniture.rect.origin.y];
          [managedObjectContext MR_saveOnlySelfAndWait];
          [shelves addObject:shelf];
      }];
    }
  }
  return shelves;
}

- (BOOL)isValidJSONWithDictionary:(NSDictionary*)dictionary {
  NSError* error;
  [RPJSONValidator
      validateValuesFrom:dictionary
        withRequirements:@{
          @"createdDate" : RPValidatorPredicate.isOptional.isNotNull,
          @"lastUpdatedDate" : RPValidatorPredicate.isOptional.isNotNull,
          @"mapId" : RPValidatorPredicate.isNotNull.isString,
          @"mapName" : RPValidatorPredicate.isOptional.isString,
          @"imageLink" : RPValidatorPredicate.isNotNull.isString,
          @"level" : [RPValidatorPredicate.isNotNull.isNumber
              valueIsGreaterThanOrEqualTo:[NSNumber numberWithInt:0]],
          @"mapData" : RPValidatorPredicate.isNotNull.isString
        } error:&error];
  if (error) {
    return NO;
  }
  return YES;
}

#pragma mark - Slice Map
- (void)sliceMapWithImageURLString:(NSString*)imageLink
                             floor:(NSUInteger)floor {
  [SDWebImageDownloader.sharedDownloader
      downloadImageWithURL:[NSURL URLWithString:imageLink]
                   options:0
                  progress:nil
                 completed:^(UIImage* image,
                             NSData* data,
                             NSError* error,
                             BOOL finished) {
                     if (image && finished) {
                       [self sliceMapWithImage:image floor:floor];
                     } else {
                       UIImage* imageTest = [UIImage imageNamed:@"Photo"];
                       [self sliceMapWithImage:imageTest floor:floor];
                     }
                 }];
}
- (void)sliceMapWithImage:(UIImage*)image floor:(NSUInteger)floor {
  ADTiledMapSlicer* slicer = [[ADTiledMapSlicer alloc] init];
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                 ^{
      NSString* folderName =
          [NSString stringWithFormat:@"%lu", (unsigned long)floor];
      [slicer generateSlicesForMapImage:image folderName:folderName];
  });
}

#pragma mark - Helpers
- (NSDateFormatter*)dateFormatter {
  NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.locale =
      [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  dateFormatter.dateFormat = DATE_FORMAT_MAP;
  return dateFormatter;
}

@end
