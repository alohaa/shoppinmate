//
//  MEDBeaconPoint.h
//  MapEditor
//
//  Created by Dat Truong on 2014-07-23.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEDBeaconPoint : NSObject

@property UInt16 pointId;
@property CGPoint position;

@property UInt16 minor;
@property UInt16 major;
@property (strong, nonatomic) NSString *uuid;

@property (strong, nonatomic) NSDictionary *graphDict;

// bi-direction
- (void)addToGraphWithPoint:(MEDBeaconPoint *)point;
@end
