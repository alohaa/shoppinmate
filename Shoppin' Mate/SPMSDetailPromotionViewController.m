//
//  SPMSDetailPromotionViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSDetailPromotionViewController.h"
#import "Product.h"
#import "SPMSSuggestedProductTableViewCell.h"
#import "SPMSDetailProductViewController.h"
#import "SPMSMapPagerViewController.h"

@interface SPMSDetailPromotionViewController ()
@end

@implementation SPMSDetailPromotionViewController
#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.productList = [[NSMutableArray alloc] init];
  }
  return self;
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
  [super viewDidLoad];

  self.tableView.dataSource = self;
  self.tableView.delegate = self;

  // Config Sidebar
  [self configNavigationBar];
  // Config TableView layout
  [self configTableViewLayout];

  // Register Custom UITableViewCell for SearchDisplayController tableview
  [self.tableView registerClass:[SPMSSuggestedProductTableViewCell class]
         forCellReuseIdentifier:@"detailPromotionCell"];

  if (!self.productList) {
    self.productList = [[NSMutableArray alloc] init];
  }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return [self.productList count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"detailPromotionCell";
  SPMSSuggestedProductTableViewCell* cell = (SPMSSuggestedProductTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (void)configureCell:(SPMSSuggestedProductTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  Product* product = (Product*)[self.productList objectAtIndex:indexPath.row];
  cell.product = product;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  Product* product = [self.productList objectAtIndex:indexPath.row];
  [self performSegueWithIdentifier:@"viewDetailProduct" sender:product];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 130.0f;
}

#pragma mark - Actions
- (void)showMapView:(id)sender {
  if ([self.productList count] > 0) {
    [self performSegueWithIdentifier:@"viewShortestPath" sender:sender];
  }
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
  [self.tableView setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark - Sidebar Configuration
- (void)configNavigationBar {
  self.navigationItem.title = @"Detail Promotion";
  // "Find Shortest Path" Button
  UIButton* findShortestPathBTN = [UIButton buttonWithType:UIButtonTypeSystem];
  findShortestPathBTN.frame = CGRectMake(0, 0, 30, 30);
  [findShortestPathBTN
      setImage:[UIImage
                   imageNamed:@"DetailShoppingList_Nav_FindShortestPath_ICO"]
      forState:UIControlStateNormal];
  [findShortestPathBTN addTarget:self
                          action:@selector(showMapView:)
                forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* findShortestPathBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:findShortestPathBTN];
  [self.navigationItem setRightBarButtonItem:findShortestPathBTNItem];
  [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
// preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewDetailProduct"]) {
    Product* aProduct = (Product*)sender;
    SPMSDetailProductViewController* detailVC =
        [segue destinationViewController];
    [detailVC setDetailProduct:aProduct];
  }
  if ([[segue identifier] isEqualToString:@"viewShortestPath"]) {
    SPMSMapPagerViewController* mapPagerVC = [segue destinationViewController];
    mapPagerVC.productList = self.productList;
  }
}

- (void)dealloc {
  self.tableView.delegate = nil;
}

@end
