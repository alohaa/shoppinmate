//
//  SPMSDatePicker.m
//  Shoppin' Mate
//
//  Created by Hiro on 7/24/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSDatePicker.h"

#define MyDateTimePickerHeight 260
#define MyDateTimePickerPickerHeight 216
#define MyDateTimePickerToolbarHeight 44
#define MyConstantsElementAnimationLength 0.3

@interface SPMSDatePicker ()

@property(nonatomic, assign, readwrite) UIDatePicker* picker;
@property(nonatomic, assign) CGRect originalFrame;

@property(nonatomic, assign) id doneTarget;
@property(nonatomic, assign) SEL doneSelector;

- (void)donePressed;

@end

@implementation SPMSDatePicker
- (id)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame])) {
    self.originalFrame = frame;
    self.backgroundColor = [UIColor clearColor];

    CGFloat width = self.bounds.size.width;

    UIToolbar* toolbar = [[UIToolbar alloc]
        initWithFrame:CGRectMake(0, 0, width, MyDateTimePickerToolbarHeight)];
    toolbar.barStyle = UIBarStyleBlackOpaque;

    UIBarButtonItem* doneButton =
        [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                         style:UIBarButtonItemStyleBordered
                                        target:self
                                        action:@selector(donePressed)];
    doneButton.width = width - 20;
    toolbar.items = [NSArray arrayWithObject:doneButton];
    [self addSubview:toolbar];

    UIDatePicker* picker = [[UIDatePicker alloc]
        initWithFrame:CGRectMake(0,
                                 toolbar.frame.origin.y +
                                     MyDateTimePickerToolbarHeight,
                                 width,
                                 MyDateTimePickerPickerHeight)];
      picker.datePickerMode = UIDatePickerModeDate;
    [self addSubview:picker];

    self.picker = picker;
  }
  return self;
}

- (void)setMode:(UIDatePickerMode)mode {
  self.picker.datePickerMode = mode;
}

- (void)donePressed {
  if (self.doneTarget) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.doneTarget performSelector:self.doneSelector];
#pragma clang diagnostic pop
  }
}

- (void)addTargetForDoneButton:(id)target action:(SEL)action {
  self.doneTarget = target;
  self.doneSelector = action;
}

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated {
  CGRect newFrame = self.originalFrame;
  newFrame.origin.y += hidden ? MyDateTimePickerHeight : 0;
  if (animated) {
    [UIView beginAnimations:@"animateDateTimePicker" context:nil];
    [UIView setAnimationDuration:MyConstantsElementAnimationLength];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];

    self.frame = newFrame;

    [UIView commitAnimations];
  } else {
    self.frame = newFrame;
  }
}
@end
