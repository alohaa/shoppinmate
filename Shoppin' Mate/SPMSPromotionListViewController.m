//
//  SPMSPromotionListViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/25/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSPromotionListViewController.h"
#import "SPMSPromotionListTableViewCell.h"
#import "SPMSWebDataService.h"
#import "Promotion.h"
#import "RACEXTScope.h"
#import <SVPullToRefresh.h>
#import "SPMSDetailPromotionViewController.h"
#import <SVProgressHUD.h>

@interface SPMSPromotionListViewController ()

@property(strong, nonatomic) NSMutableArray* promotionList;
@property(assign, nonatomic) NSUInteger cellCount;
@property(assign, nonatomic) NSUInteger pageNumber;

@end

@implementation SPMSPromotionListViewController
#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.promotionList = [[NSMutableArray alloc] init];
    self.cellCount = 0;
    self.pageNumber = 1;
  }
  return self;
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
  [super viewDidLoad];

  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetSource = self;
  self.tableView.emptyDataSetDelegate = self;

  // Config Sidebar
  [self configNavigationBar];
  // Config TableView
  [self configTableViewLayout];

  // Reload Data
  [self loadPromotionsWithPageNumber:self.pageNumber];

  // Config PullToRefresh
  [self configPullToRefresh];
  // A little trick for removing the cell separators
  self.tableView.tableFooterView = [UIView new];
}

#pragma mark - Load Data
- (void)loadPromotionsWithPageNumber:(NSUInteger)page {
  if (self.pageNumber == 1) {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  }
  @weakify(self);
  [[[[SPMSWebDataService sharedService]
      getPromotionListWithEndDate:[NSDate date]
                       pageNumber:page]
      deliverOn:RACScheduler
                    .mainThreadScheduler] subscribeNext:^(NSArray* promotions) {
      @strongify(self);
      [self.promotionList addObjectsFromArray:promotions];
      self.cellCount = [self.promotionList count];
      [self.tableView reloadData];
  } error:^(NSError* error) {
      [SVProgressHUD
          showErrorWithStatus:
              [NSString stringWithFormat:@"%@", [error localizedDescription]]];
  } completed:^{
      self.pageNumber = self.pageNumber + 1;
      [SVProgressHUD dismiss];
  }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return self.cellCount;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"promotionCell";
  SPMSPromotionListTableViewCell* cell = (SPMSPromotionListTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];

  // Create Circular User Avatar Image View
  cell.promotionImageView.layer.cornerRadius =
      cell.promotionImageView.frame.size.width / 2;
  cell.promotionImageView.clipsToBounds = YES;

  // Add Border
  cell.promotionImageView.layer.borderWidth = 2.0f;
  cell.promotionImageView.layer.borderColor =
      [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000].CGColor;

  return cell;
}

- (void)configureCell:(SPMSPromotionListTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  Promotion* promotion =
      (Promotion*)[self.promotionList objectAtIndex:indexPath.row];
  cell.promotion = promotion;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  Promotion* promotion =
      (Promotion*)[self.promotionList objectAtIndex:indexPath.row];
  SPMSDetailPromotionViewController* detailPromotionVC = [self.storyboard
      instantiateViewControllerWithIdentifier:@"detailPromotionController"];
  NSMutableArray* products =
      [NSMutableArray arrayWithArray:[promotion.products allObjects]];
  detailPromotionVC.productList = products;
  [self.navigationController pushViewController:detailPromotionVC animated:YES];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 120.0f;
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No promotions loaded";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"Promotion is loading or something goes wrong!";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"Promotion_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView*)scrollView {
}

- (void)emptyDataSetDidTapButton:(UIScrollView*)scrollView {
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh {
  __weak SPMSPromotionListViewController* weakSelf = self;

  [self.tableView.pullToRefreshView setTitle:@"Getting New Promotions"
                                    forState:SVPullToRefreshStateLoading];
  // Setup pull-to-refresh
  [self.tableView
      addPullToRefreshWithActionHandler:^{ [weakSelf insertRowAtTop]; }];
  // Setup infinite scrolling

  [self.tableView
      addInfiniteScrollingWithActionHandler:^{ [weakSelf insertRowAtBottom]; }];
}

- (void)insertRowAtBottom {
  __weak SPMSPromotionListViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      [self loadPromotionsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.infiniteScrollingView stopAnimating];
  });
}

- (void)insertRowAtTop {
  __weak SPMSPromotionListViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      weakSelf.pageNumber = 1;
      [weakSelf.promotionList removeAllObjects];
      [weakSelf loadPromotionsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.pullToRefreshView stopAnimating];
  });
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout {
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  self.navigationItem.title = @"Promotions";
  // "Show Menu" Button
  UIButton* showMenuBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  showMenuBTN.frame = CGRectMake(0, 0, 30, 30);
  [showMenuBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_ShowMenu_ICO"]
               forState:UIControlStateNormal];
  [showMenuBTN addTarget:self
                  action:@selector(presentLeftMenuViewController:)
        forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showMenuBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:showMenuBTN];

  [self.navigationItem setLeftBarButtonItem:showMenuBTNItem];
  // "Search Product" Button
  UIButton* searchBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  searchBTN.frame = CGRectMake(5, 0, 30, 30);
  [searchBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_Search_ICO"]
             forState:UIControlStateNormal];
  [searchBTN addTarget:self
                action:@selector(showSearchProductVC:)
      forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* searchBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:searchBTN];

  [self.navigationItem setRightBarButtonItem:searchBTNItem];
  [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark - Action
- (void)showSearchProductVC:(id)sender {
  [self performSegueWithIdentifier:@"searchProduct" sender:self];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)dealloc {
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
}

@end
