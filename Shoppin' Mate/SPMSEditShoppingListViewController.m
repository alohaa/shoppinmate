//
//  SPMSEditShoppingListViewController.m
//  Shoppin' Mate
//
//  Created by Hiro on 8/9/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//
#import "SVProgressHUD.h"
#import "SPMSWebDataService.h"
#import "SPMSEditShoppingListViewController.h"

@interface SPMSEditShoppingListViewController ()

@end

@implementation SPMSEditShoppingListViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.shoppingListNameTextField.text = self.shoppingListName;
  [self configNavigationBar];
  // Do any additional setup after loading the view.
}

- (IBAction)saveButtonTapped:(id)sender {
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];

  [[[[SPMSWebDataService sharedService]
      editShoppingListWithShoppingListId:self.shoppingListID
                        shoppingListName:self.shoppingListNameTextField.text]
      deliverOn:RACScheduler.mainThreadScheduler] subscribeNext:^(NSNumber*
                                                                      result) {
      if (result.intValue == 1) {
        [SVProgressHUD showSuccessWithStatus:@"Shopping list's name changed"];
      }

      [self.navigationController popViewControllerAnimated:YES];
  } error:^(NSError* error) {
      [SVProgressHUD
          showErrorWithStatus:
              [NSString stringWithFormat:@"%@", [error localizedDescription]]];
  }];
}
- (void)configNavigationBar {
  self.navigationItem.title = @"Edit Shopping List";
  [self.navigationController.navigationBar setTranslucent:NO];
}
@end
