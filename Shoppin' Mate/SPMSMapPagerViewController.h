//
//  SPMSMapPagerViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 8/1/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "ViewPagerController.h"
#import <RESideMenu.h>

@interface SPMSMapPagerViewController : ViewPagerController

@property(strong, nonatomic) NSMutableArray* productList;

@end
