//
//  SPMSMapViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/9/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSMapViewController.h"
#import "NATiledImageMapView.h"
#import "ARLocalTiledImageDataSource.h"
#import "SPMSBeaconAnnotation.h"
#import "SMDBeacon.h"
#import "NALine.h"
#import "Shelf.h"
#import "Map.h"
#import "SPMSShelfAnnotation.h"
#import "NSMutableArray+Queue.h"
#import "NSMutableArray+Stack.h"
#import <ObjectiveSugar.h>
#import <SVProgressHUD.h>
#import "SPMSDetailShelfViewController.h"

@interface SPMSMapViewController () {
  CLLocationManager* _locationManager;
  NSMutableArray* _rangedRegions;
}
@property(readonly, nonatomic) NATiledImageMapView* mapView;
@property(readonly, nonatomic) ARLocalTiledImageDataSource* dataSource;
@property(strong, nonatomic) NSMutableArray* calculatingBeacons;
@property(strong, nonatomic) NSArray* allBeacons;
@property(strong, nonatomic) SPMSBeaconAnnotation *userAnnotation;
@property BOOL isRouting;

@end

@implementation SPMSMapViewController

#pragma mark - Initialization
- (instancetype)initWithCoder:(NSCoder*)coder {
  self = [super initWithCoder:coder];
  if (self) {
    self.beacons = [NSMutableArray array];

    // This location manager will be used to demonstrate how to range beacons.
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    self.calculatingBeacons = [NSMutableArray array];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    self.edgesForExtendedLayout = UIRectEdgeNone;

  [self configNavigationBar];
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  [self configureBeacon];
  [self setupMapView];

  [_locationManager startUpdatingHeading];
}

- (void)viewDidAppear:(BOOL)animated {
  // Start ranging when the view appears.
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [self stopRanging];
  [_locationManager stopUpdatingHeading];
}

#pragma mark - Configure Beacon
- (void)configureBeacon {
  _rangedRegions = [NSMutableArray array];
  NSArray* supportedProximityUUIDs = @[
    [[NSUUID alloc]
        initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"]
  ];
  [supportedProximityUUIDs enumerateObjectsUsingBlock:^(id uuidObj,
                                                        NSUInteger uuidIdx,
                                                        BOOL* uuidStop) {
      NSUUID* uuid = (NSUUID*)uuidObj;
      CLBeaconRegion* region =
          [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                             identifier:[uuid UUIDString]];
      [_rangedRegions addObject:region];
  }];
}

#pragma mark - Core Location Manager Delegate
- (void)locationManager:(CLLocationManager*)manager
        didRangeBeacons:(NSArray*)beacons
               inRegion:(CLBeaconRegion*)region {
  // CoreLocation will call this delegate method at 1 Hz with updated range
  // information.
  // Beacons will be categorized and displayed by proximity.
  NSArray* eligbleBeacons = [beacons
      filteredArrayUsingPredicate:
          [NSPredicate predicateWithFormat:@"proximity = %d OR proximity = %d",
                                           CLProximityImmediate,
                                           CLProximityNear]];

  if (eligbleBeacons.count) {
    NSString* consoleText;
    consoleText = @"";
    for (CLBeacon* beacon in eligbleBeacons) {
      consoleText = [consoleText
          stringByAppendingString:
              [NSString stringWithFormat:@"Major: %@, Minor: %@, Acc: %.2fm\n",
                                         beacon.major,
                                         beacon.minor,
                                         beacon.accuracy]];

      SMDBeacon* beaconToCalculate;
      beaconToCalculate = [[SMDBeacon alloc] init];
      beaconToCalculate.major = [beacon.major integerValue];
      beaconToCalculate.minor = [beacon.minor integerValue];
      beaconToCalculate.accuracy = beacon.accuracy;
      [self.calculatingBeacons addObject:beaconToCalculate];
    }
  }
}

- (void)locationManager:(CLLocationManager*)manager
       didUpdateHeading:(CLHeading*)newHeading {
}

- (void)calculatePosition {
    [_userAnnotation removeFromMapView];
  [self stopRanging];

  // Take the first beacon as the nearest one
  for (SMDBeacon* beacon in self.calculatingBeacons) {
    if (0 < beacon.accuracy) {
      self.currentBeacon = beacon;
      break;
    }
  }

  // Comapare and set currentBeacon to the beacon with smallest accurary value
  for (SMDBeacon* beacon in self.calculatingBeacons) {
    if (0 < beacon.accuracy && beacon.accuracy < self.currentBeacon.accuracy) {
      self.currentBeacon = beacon;
    }
  }

  if (self.currentBeacon) {
    CGPoint currentPoint;
    currentPoint = CGPointZero;

    UInt16 currentMajor, currentMinor;
    currentMajor = self.currentBeacon.major;
    currentMinor = self.currentBeacon.minor;
    NSLog(@"current major %d current minor %d", currentMajor, currentMinor);

    for (SMDBeacon* beacon in self.allBeacons) {
      UInt16 major, minor;
      major = beacon.major;
      minor = beacon.minor;

      NSLog(@"major %d minor %d", major, minor);
      BOOL equalMajor, equalMinor;

      equalMajor = major == currentMajor;
      equalMinor = minor == currentMinor;

      if (equalMajor && equalMinor) {
        currentPoint = CGPointMake(beacon.annotation.point.x * kMAP_ZOOM_LEVEL,
                                   beacon.annotation.point.y * kMAP_ZOOM_LEVEL);
        break;
      }
    }
      _userAnnotation = [[SPMSBeaconAnnotation alloc] initWithPoint:currentPoint];
      [self.mapView addAnnotation:_userAnnotation animated:YES];
    [self.mapView centerOnPoint:currentPoint animated:YES];
      
    [SVProgressHUD dismiss];
  } else {
    [SVProgressHUD showErrorWithStatus:@"Cannot detect beacon!"];
  }

  [self.calculatingBeacons removeAllObjects];
  self.currentBeacon = nil;
}

- (void)startRanging {
  [_rangedRegions
      enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop) {
          CLBeaconRegion* region = obj;
          [_locationManager startRangingBeaconsInRegion:region];
      }];
}

- (void)stopRanging {
  // Stop ranging when the view goes away.
  [_rangedRegions
      enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop) {
          CLBeaconRegion* region = obj;
          [_locationManager stopRangingBeaconsInRegion:region];
      }];
}

#pragma mark - View Helpers Functions
- (NSString*)configureMapPath {
  NSString* mapPath;
  if (self.floor) {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(
        NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentPath = [paths objectAtIndex:0];
    mapPath =
        [NSString stringWithFormat:@"%@/Maps/%@", documentPath, self.floor];
  }

  return mapPath;
}

- (void)setupMapView {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                 ^{
      ARLocalTiledImageDataSource* dataSource =
          [[ARLocalTiledImageDataSource alloc] init];
      dataSource.maxTiledHeight = 1408;
      dataSource.maxTiledWidth = 2048;
      dataSource.minTileLevel = 3;
      dataSource.maxTileLevel = 13;
      dataSource.tileSize = 512;
      dataSource.tileFormat = @"png";
      dataSource.tileBasePath = [self configureMapPath];
      _dataSource = dataSource;

      NATiledImageMapView* mapView =
          [[NATiledImageMapView alloc] initWithFrame:self.view.bounds
                                tiledImageDataSource:dataSource];
      mapView.backgroundColor = [UIColor whiteColor];
      mapView.autoresizingMask =
          UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
      mapView.displayTileBorders = NO;
      mapView.zoomStep = 3.0f;
      _mapView = mapView;
      dispatch_async(dispatch_get_main_queue(),
                     ^{ [self.view addSubview:mapView]; });
      Map* map = [Map MR_findFirstByAttribute:@"level" withValue:self.floor];
      if (map) {
        // Add Beacons
        [self addBeaconsWithMap:map];
        // Add Shelves
        [self addShelvesWithMap:map];
        // Add Annotations
        [self addAnnotations];
        // Add Shelves
        [self addShelvesWithMap:map];
        // Draw the Shortest Path's Lines
        [self drawLinesWithMap:map];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self addZoomMapToFitBTN];
            // Add Controls UI
            [SVProgressHUD dismiss];
            [self.mapView zoomToFit:YES];
        });
      } else {
        [SVProgressHUD showErrorWithStatus:
                           @"Cannot display Map. Reload Map data and retry."];
      }
  });
}

- (void)addBeaconsWithMap:(Map*)map {
  NSArray* beaconArray = [map.beacons array];
  NSMutableArray* smdBeaconArray = [[NSMutableArray alloc] init];
  if (beaconArray) {
    [beaconArray each:^(Beacon* beacon) {
        SMDBeacon* smdBeacon = [[SMDBeacon alloc] initWithBeaconData:beacon];
        [smdBeaconArray addObject:smdBeacon];
    }];
    self.allBeacons = [[NSArray alloc] initWithArray:smdBeaconArray];
  }
}

- (void)addAnnotations {
  for (SMDBeacon* beacon in self.beacons) {
    beacon.annotation.point =
        CGPointMake(beacon.annotation.point.x * kMAP_ZOOM_LEVEL,
                    beacon.annotation.point.y * kMAP_ZOOM_LEVEL);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mapView addAnnotation:beacon.annotation animated:YES];
    });
  }
}

- (void)addShelvesWithMap:(Map*)map {
  if (self.floor) {
    if (map && 0 < [map.shelves count]) {
      [map.shelves each:^(Shelf* shelf) {
          CGRect rect =
              CGRectMake([shelf.xCoordinate floatValue] * kMAP_ZOOM_LEVEL,
                         [shelf.yCoordinate floatValue] * kMAP_ZOOM_LEVEL,
                         [shelf.width floatValue] * kMAP_ZOOM_LEVEL,
                         [shelf.height floatValue] * kMAP_ZOOM_LEVEL);
          SPMSShelfAnnotation* annotation =
              [[SPMSShelfAnnotation alloc] initWithShapeRect:rect
                                                 labelString:shelf.name
                                                       image:nil];
          annotation.title = shelf.name;

          UIButton* viewDetailShelfBTN =
              [UIButton buttonWithType:UIButtonTypeInfoDark];
          [viewDetailShelfBTN setTintColor:[UIColor whiteColor]];

          // Assign ShelfId to Corresponding Button's Tag to store value to
          // fetch all products were contained in this Shelf
          viewDetailShelfBTN.tag = [shelf.furnitureId integerValue];
          [viewDetailShelfBTN addTarget:self
                                 action:@selector(viewDetailShelfBTNTapped:)
                       forControlEvents:UIControlEventTouchUpInside];
          annotation.rightCalloutAccessoryView = viewDetailShelfBTN;
          [self.beacons each:^(SMDBeacon* smdBeacon) {
              if (smdBeacon.pointId == shelf.pointId) {
                annotation.isHighlighted = YES;
                annotation.color = smdBeacon.annotation.color;
                return;
              }
          }];
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.mapView addAnnotation:annotation animated:YES];
          });
      }];
    }
  }
}

- (void)drawLinesWithMap:(Map*)map {
#warning OVERKILL???
    // What the hell? Create a temporary mutable array just to use dequeue?
  NSMutableArray* pathStack;
  pathStack = [NSMutableArray arrayWithArray:self.path];

  NSString* currentAlias, *nextAlias;
  currentAlias = [pathStack dequeue];

  while (0 < pathStack.count) {
    nextAlias = [pathStack dequeue];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self drawLineFromBeacon:[self beaconWithAlias:currentAlias map:map]
                        toBeacon:[self beaconWithAlias:nextAlias map:map]];
    });

    currentAlias = nextAlias;
  }
}

- (SMDBeacon*)beaconWithAlias:(NSString*)alias map:(Map*)map {
  SMDBeacon* foundBeacon;
  NSMutableArray* beaconArray = [[NSMutableArray alloc] init];
  [[map.beacons array] each:^(Beacon* beacon) {
      SMDBeacon* smdBeacon = [[SMDBeacon alloc] initWithBeaconData:beacon];
      [beaconArray addObject:smdBeacon];
  }];
  NSPredicate* bPredicate =
      [NSPredicate predicateWithFormat:@"SELF.alias like %@", alias];
  NSArray* filteredArray = [beaconArray filteredArrayUsingPredicate:bPredicate];

  foundBeacon = [filteredArray firstObject];
  return foundBeacon;
}

- (void)drawLineFromBeacon:(SMDBeacon*)beaconA toBeacon:(SMDBeacon*)beaconB {
  beaconA.annotation.point =
      CGPointMake(beaconA.annotation.point.x * kMAP_ZOOM_LEVEL,
                  beaconA.annotation.point.y * kMAP_ZOOM_LEVEL);
  beaconB.annotation.point =
      CGPointMake(beaconB.annotation.point.x * kMAP_ZOOM_LEVEL,
                  beaconB.annotation.point.y * kMAP_ZOOM_LEVEL);
  [self drawLineFromPoint:beaconA.annotation.point
                  toPoint:beaconB.annotation.point];
}

- (void)drawLineFromPoint:(CGPoint)pointA toPoint:(CGPoint)pointB {
  NALine* line = [[NALine alloc] initWithStartPoint:pointA endPoint:pointB];
  [line addToMapView:self.mapView animated:NO];
  [self.mapView.lines addObject:line];
}

#pragma mark - Action
- (IBAction)detectBeaconButtonTapped:(id)sender {
  [SVProgressHUD showWithStatus:@"Locating Beacon..."
                       maskType:SVProgressHUDMaskTypeBlack];
  [self startRanging];

  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)),
                 dispatch_get_main_queue(),
                 ^{ [self calculatePosition]; });
}

- (void)viewDetailShelfBTNTapped:(id)sender {
  if ([sender isKindOfClass:[UIButton class]]) {
    UIButton* button = (UIButton*)sender;
    [self performSegueWithIdentifier:@"viewDetailShelf" sender:button];
  }
}

- (void)zoomMapToFit:(id)sender {
  [self.mapView zoomToFit:YES];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewDetailShelf"]) {
    UIButton* button = (UIButton*)sender;
    SPMSDetailShelfViewController* detailShelfVC =
        [segue destinationViewController];
    detailShelfVC.furnitureId = button.tag;
  }
}

#pragma mark - NavigationBar Configuration
- (void)configNavigationBar {
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)addZoomMapToFitBTN {
  // "Locate Beacon" Button
  UIButton* zoomMapToFitBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  CGFloat padding = 10.0f;
  CGFloat yCoordinate = self.view.bounds.size.height - padding - 40;
  CGFloat xCoordinate = self.view.bounds.size.width - padding - 40;
  zoomMapToFitBTN.frame = CGRectMake(xCoordinate, yCoordinate, 40, 40);
  [zoomMapToFitBTN setImage:[UIImage imageNamed:@"Map_ZoomToFit_ICO"]
                   forState:UIControlStateNormal];
  [zoomMapToFitBTN
      setImage:[UIImage imageNamed:@"Map_ZoomToFit_Highlighted_ICO"]
      forState:UIControlStateHighlighted | UIControlStateSelected];
  [zoomMapToFitBTN addTarget:self
                      action:@selector(zoomMapToFit:)
            forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:zoomMapToFitBTN];
}

@end
