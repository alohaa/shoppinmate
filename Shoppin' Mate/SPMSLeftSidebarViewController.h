//
//  SPMSLeftSidebarViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu.h>

@interface SPMSLeftSidebarViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       RESideMenuDelegate>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(strong, nonatomic) IBOutlet UIImageView* userAvatarImageView;
@property(strong, nonatomic) IBOutlet UIView* userProfileView;
@property(strong, nonatomic) IBOutlet UILabel* userFullNameLabel;
@property(strong, nonatomic) IBOutlet UILabel* viewAndEditProfileLabel;
@property(strong, nonatomic) IBOutlet UIImageView* askToLoginImageView;

@end
