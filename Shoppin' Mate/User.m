//
//  User.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic dateOfBirth;
@dynamic email;
@dynamic fullName;
@dynamic gender;
@dynamic imageURL;
@dynamic password;
@dynamic userID;

@end
