//
//  SPMSCategoryTypeDataService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/13/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSCategoryTypeDataService.h"
#import "NSDictionary+Verified.h"
#import "CategoryType.h"

@implementation SPMSCategoryTypeDataService
#pragma mark - Singleton Object
+ (SPMSCategoryTypeDataService*)sharedService {
  static SPMSCategoryTypeDataService* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

#pragma mark - Get CategoryType List
- (RACSignal*)fetchCategoryTypeWithBaseUrl:(NSURL*)baseUrl
                                    client:(NSURLSession*)client {
  NSString* relativeString = @"api/category/findall";

  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSURL* relativeUrl =
          [NSURL URLWithString:relativeString relativeToURL:baseUrl];
      NSURLRequest* request = [NSURLRequest requestWithURL:relativeUrl];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[dict verifiedObjectForKey:@"Result"]];
                    [subscriber sendCompleted];
                  }
                }
            }];

      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

#pragma mark - Get Category List With Page
- (RACSignal*)fetchCategoryTypeListWithPageNumber:(NSUInteger)pageNumber
                                          baseUrl:(NSURL*)baseUrl
                                           client:(NSURLSession*)client {
  NSString* relativeString = [NSString
      stringWithFormat:@"api/category/get/%lu", (unsigned long)pageNumber];
  RACSignal* signal = [RACSignal createSignal:^RACDisposable *
                                              (id<RACSubscriber> subscriber) {
      NSMutableURLRequest* request = [NSMutableURLRequest
          requestWithURL:[NSURL URLWithString:relativeString
                                relativeToURL:baseUrl]];

      NSURLSessionDataTask* task = [client
          dataTaskWithRequest:request
            completionHandler:^(NSData* data,
                                NSURLResponse* response,
                                NSError* error) {
                if (error) {
                  [subscriber sendError:error];
                } else if (!data) {
                  NSDictionary* userInfo = @{
                    NSLocalizedDescriptionKey :
                        @"No data was received from the server."
                        };
                  NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                       code:ERROR_CODE
                                                   userInfo:userInfo];
                  [subscriber sendError:error];
                } else {
                  NSError* jsonError;
                  NSDictionary* dict = [NSJSONSerialization
                      JSONObjectWithData:data
                                 options:NSJSONReadingMutableContainers
                                   error:&jsonError];

                  if (jsonError) {
                    [subscriber sendError:jsonError];
                  } else {
                    [subscriber sendNext:[[dict verifiedObjectForKey:@"Result"]
                                             verifiedObjectForKey:@"Data"]];
                    [subscriber sendCompleted];
                  }
                }
            }];
      [task resume];

      return [RACDisposable disposableWithBlock:^{ [task cancel]; }];
  }];
  return signal;
}

- (RACSignal*)categoryTypeForSignal:(RACSignal*)signal {
  return [signal map:^id(NSArray* dicts) {
      NSMutableArray* categories = [[NSMutableArray alloc] init];
      for (NSDictionary* categoryTypeDic in dicts) {
        CategoryType* new_category =
            [CategoryType categoryTypeForDictionary:categoryTypeDic];
        [categories addObject:new_category];
      }
      return categories;
  }];
}

@end
