//
//  SPMSUserDataService.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>

@interface SPMSUserDataService : NSObject

+ (SPMSUserDataService*)sharedService;
- (RACSignal*)fetchUserProfileWithToken:(NSString*)tokenString
                                baseUrl:(NSURL*)baseUrl
                                 client:(NSURLSession*)client;
- (RACSignal*)userForSignal:(RACSignal*)signal;
- (RACSignal*)checkExistedAccountWithEmail:(NSString*)email
                                   baseUrl:(NSURL*)baseUrl
                                    client:(NSURLSession*)client;
- (RACSignal*)registerAccountWithEmail:(NSString*)email
                              password:(NSString*)password
                              fullName:(NSString*)fullName
                           avatarImage:(UIImage*)avatarImage
                               baseUrl:(NSURL*)baseUrl
                                client:(NSURLSession*)client;
- (RACSignal*)resetPasswordWithEmail:(NSString*)email
                             baseUrl:(NSURL*)baseUrl
                              client:(NSURLSession*)client;

- (RACSignal*)updateAccountWithFullName:(NSString*)fullName
                            avatarImage:(UIImage*)avatarImage
                                 gender:(NSNumber*)gender
                            dateOfBirth:(NSDate*)dateOfBirth
                                baseUrl:(NSURL*)baseUrl
                                 client:(NSURLSession*)client;
- (RACSignal*)userForUpdateProfileSignal:(RACSignal*)signal;
@end
