//
//  SPMSDataPersistentOperationQueue.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/27/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSDataPersistentOperationQueue.h"
#import "AFNetworkReachabilityManager+RAC.h"
#import "SPMSSPMSDataPersistentOperation.h"
#import "NSDictionary+Verified.h"
#import "SPMSWebDataService.h"
#import <TSMessages/TSMessage.h>

@interface SPMSDataPersistentOperationQueue () {
  NSMutableArray* requests;
}

@end

@implementation SPMSDataPersistentOperationQueue

+ (SPMSDataPersistentOperationQueue*)sharedService {
  static SPMSDataPersistentOperationQueue* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

- (id)init {
  if (self = [super init]) {
    NSArray* dirPaths = NSSearchPathForDirectoriesInDomains(
        NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docsDir = [dirPaths objectAtIndex:0];
    NSString* databasePath = [[NSString alloc]
        initWithString:[docsDir stringByAppendingPathComponent:
                                    OFFLINE_REQUEST_QUEUE_DB_FILE]];

    self.offlineTasksQueue =
        [[BAPersistentOperationQueue alloc] initWithDatabasePath:databasePath];
    self.offlineTasksQueue.delegate = self;

    [[[[AFNetworkReachabilityManager
            sharedManager] rac_reachabilityStatusChangeSignal]
        deliverOn:[RACScheduler scheduler]] subscribeNext:^(id x) {
        BOOL isReachable =
            [AFNetworkReachabilityManager sharedManager].reachable;
        if (isReachable) {
          [self.offlineTasksQueue startWorking];
          [TSMessage
              showNotificationInViewController:[UIApplication sharedApplication]
                                                   .keyWindow.rootViewController
                                         title:NSLocalizedString(
                                                   @"Offline Queue "
                                                   @"Synchronization",
                                                   nil)
                                      subtitle:nil
                                         image:nil
                                          type:TSMessageNotificationTypeSuccess
                                      duration:
                                          TSMessageNotificationDurationEndless
                                      callback:nil
                                   buttonTitle:nil
                                buttonCallback:nil
                                    atPosition:TSMessageNotificationPositionTop
                          canBeDismissedByUser:YES];
        } else {
          [self.offlineTasksQueue stopWorking];
        }
    }];
  }
  return self;
}

#pragma mark - BAPersistentOperationQueueDelegate
- (NSDictionary*)persistentOperationQueueSerializeObject:(id)object {
  SPMSSPMSDataPersistentOperation* operation =
      (SPMSSPMSDataPersistentOperation*)object;
  NSDictionary* data = @{
    @"operation" : @{
      @"urlString" : operation.urlString,
      @"headerParamString" : operation.headerParamString,
      @"httpBodyParamString" : operation.httpBodyParamString,
      @"httpMethodString" : operation.httpMethodString
    }
  };
  return data;
}

- (void)persistentOperationQueueStartedOperation:
            (BAPersistentOperation*)operation {
  if (!self.client) {
    self.client = [[SPMSWebDataService sharedService] client];
  }
  SPMSSPMSDataPersistentOperation* persistentOperation;
  NSDictionary* dictionary = operation.data[@"operation"];
  NSString* urlString = [dictionary verifiedObjectForKey:@"urlString"];
  NSString* headerParamString =
      [dictionary verifiedObjectForKey:@"headerParamString"];
  NSString* httpBodyParamString =
      [dictionary verifiedObjectForKey:@"httpBodyParamString"];
  NSString* httpMethodString =
      [dictionary verifiedObjectForKey:@"httpMethodString"];
  persistentOperation =
      [[SPMSSPMSDataPersistentOperation alloc] initWithURL:urlString
                                                HTTPMethod:httpMethodString
                                           HTTPHeaderParam:headerParamString
                                                  HTTPBody:httpBodyParamString];
  [[[persistentOperation peformOfflineRequestWithClient:self.client]
      deliverOn:[RACScheduler scheduler]]
      subscribeCompleted:^{
          [operation finish];
      }];
}

#pragma mark - Action
- (void)enqueueWithRequest:(NSMutableURLRequest*)request {
  if (!request) {
    return;
  }
  SPMSSPMSDataPersistentOperation* operation =
      [[SPMSSPMSDataPersistentOperation alloc] initWithRequest:request];
  [self.offlineTasksQueue addObject:operation];
}

@end
