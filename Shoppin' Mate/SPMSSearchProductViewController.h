//
//  SPMSSearchProductViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPMSSWSearch.h"
#import <UIScrollView+EmptyDataSet.h>

@class SPMSFilterSearchProductViewController;

@interface SPMSSearchProductViewController
    : UIViewController<SPMSSWSearchDelegate,
                       SPMSSWSearchDatasource,
                       UISearchDisplayDelegate,
                       UISearchBarDelegate,
                       DZNEmptyDataSetSource,
                       DZNEmptyDataSetDelegate>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(strong, nonatomic) IBOutlet UISearchBar* searchBar;
@property(strong, nonatomic) SPMSFilterSearchProductViewController* filterVC;
@end
