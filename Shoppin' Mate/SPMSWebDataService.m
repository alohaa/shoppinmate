//
//  SPMSWebDataService.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSWebDataService.h"
#import "SPMSProductDataService.h"
#import "SPMSUserDataService.h"
#import "SPMSShoppingListDataService.h"
#import "SPMSPromotionDataService.h"
#import "SPMSMapDataService.h"
#import "RACEXTScope.h"
#import "SPMSCategoryTypeDataService.h"
#import "SPMSDataPersistentOperationQueue.h"
#import "SPMSOAuthClient.h"
#import "SPMSOAuthAccessToken.h"

@interface SPMSWebDataService ()

@end

@implementation SPMSWebDataService

+ (SPMSWebDataService*)sharedService {
  static SPMSWebDataService* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
    sharedService.client = [NSURLSession
        sessionWithConfiguration:[NSURLSessionConfiguration
                                         defaultSessionConfiguration]
                        delegate:sharedService
                   delegateQueue:nil];
    sharedService.baseUrl = [NSURL URLWithString:WEB_SERVER_URL];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

- (id)init {
  if (self = [super init]) {
  }
  return self;
}

#pragma mark - Product
- (RACSignal*)getProductDetailWithProductId:(NSString*)productId {
  RACSignal* signal = [[SPMSProductDataService sharedService]
      fetchProductDetailWithProductId:productId
                              baseUrl:self.baseUrl
                               client:self.client];
  return [[SPMSProductDataService sharedService] productListForSignal:signal];
}

- (RACSignal*)getProductListWithPageNumber:(NSUInteger)pageNumber {
  RACSignal* signal = [[SPMSProductDataService sharedService]
      fetchProductListWithPageNumber:pageNumber
                             baseUrl:self.baseUrl
                              client:self.client];

  return [[SPMSProductDataService sharedService] productListForSignal:signal];
}

- (RACSignal*)fetchProductListOfShelfWithFurnitureId:(NSUInteger)furnitureID
                                          pageNumber:(NSUInteger)pageNumber {
  RACSignal* signal = [[SPMSProductDataService sharedService]
      fetchProductListOfShelfWithFurnitureId:furnitureID
                                  pageNumber:pageNumber
                                     baseUrl:self.baseUrl
                                      client:self.client];

  return [[SPMSProductDataService sharedService] productListForSignal:signal];
}

#pragma mark - User
- (RACSignal*)checkExistedAccountWithEmail:(NSString*)email {
  return [[SPMSUserDataService sharedService]
      checkExistedAccountWithEmail:email
                           baseUrl:self.baseUrl
                            client:self.client];
}
- (RACSignal*)resetPasswordWithEmail:(NSString*)email {
  return
      [[SPMSUserDataService sharedService] resetPasswordWithEmail:email
                                                          baseUrl:self.baseUrl
                                                           client:self.client];
}

- (RACSignal*)registerAccountWithEmail:(NSString*)email
                              password:(NSString*)password
                              fullName:(NSString*)fullName
                           avatarImage:(UIImage*)avatarImage {
  return [[SPMSUserDataService sharedService]
      registerAccountWithEmail:email
                      password:password
                      fullName:fullName
                   avatarImage:avatarImage
                       baseUrl:self.baseUrl
                        client:self.client];
}

- (RACSignal*)updateAccountWithFullName:(NSString*)fullName
                            avatarImage:(UIImage*)avatarImage
                                 gender:(NSNumber*)gender
                            dateOfBirth:(NSDate*)dateOfBirth {
  RACSignal* signal = [[SPMSUserDataService sharedService]
      updateAccountWithFullName:fullName
                    avatarImage:avatarImage
                         gender:gender
                    dateOfBirth:dateOfBirth
                        baseUrl:self.baseUrl
                         client:self.client];
  return
      [[SPMSUserDataService sharedService] userForUpdateProfileSignal:signal];
}

- (RACSignal*)getUserProfile {
  NSString* accessToken =
      [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];
  RACSignal* signal = [[SPMSUserDataService sharedService]
      fetchUserProfileWithToken:accessToken
                        baseUrl:self.baseUrl
                         client:self.client];
  return [[SPMSUserDataService sharedService] userForSignal:signal];
}

- (RACSignal*)getUserProfileWithToken:(NSString*)tokenString {
  RACSignal* signal = [[SPMSUserDataService sharedService]
      fetchUserProfileWithToken:tokenString
                        baseUrl:self.baseUrl
                         client:self.client];
  return [[SPMSUserDataService sharedService] userForSignal:signal];
}

#pragma mark - ShoppingList
- (RACSignal*)getShoppingListsWithPageNumber:(NSUInteger)pageNumber {
  RACSignal* signal = [[SPMSShoppingListDataService sharedService]
      fetchShoppingListsWithPageNumber:pageNumber
                               baseUrl:self.baseUrl
                                client:self.client];
  return [[SPMSShoppingListDataService sharedService]
      shoppingListForSignal:signal];
}

- (RACSignal*)addProduct:(Product*)product
     toShoppingListArray:(NSArray*)shoppingListArray {
  // Check for login
  BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults]
      boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  NSString* accessToken =
      [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];

  if (!product || !shoppingListArray || !isUserLoggedIn || !accessToken) {
    return [RACSignal return:[NSError errorWithDomain:ERROR_DOMAIN
                                                 code:ERROR_CODE
                                             userInfo:nil]];
  }
  NSMutableArray* signalArray = [[NSMutableArray alloc] init];
  [shoppingListArray each:^(ShoppingList* shoppingList) {
      // Insert Product which belongs to Shopping List
      [shoppingList.products addObject:product];
      // Push to Server
      RACSignal* signal =
          [[SPMSShoppingListDataService sharedService] addProduct:product
                                                   toShoppingList:shoppingList
                                                          baseUrl:self.baseUrl
                                                           client:self.client];
      [signalArray addObject:signal];
  }];
  return [[[RACSignal combineLatest:signalArray]
      catch:^RACSignal * (NSError * error) {
          if ([[error localizedDescription]
                  isKindOfClass:[NSMutableURLRequest class]]) {
            NSMutableURLRequest* request =
                (NSMutableURLRequest*)[error localizedDescription];
            // If error, Add to Offline Persistent Operation Queue
            [[SPMSDataPersistentOperationQueue sharedService]
                enqueueWithRequest:request];
          }
          // Return Error to display Error Message
          return [RACSignal error:error];
      }] map:^(RACTuple* signalValues) {
      return @([signalValues.rac_sequence
          all:^BOOL(NSNumber* value) { return [value boolValue]; }]);
  }];
}

- (RACSignal*)removeProduct:(Product*)product
      fromShoppingListArray:(NSArray*)shoppingListArray {
  // Check for login
  BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults]
      boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  NSString* accessToken =
      [[[SPMSOAuthClient sharedInstance] accessToken] accessToken];

  if (!product || !shoppingListArray || !isUserLoggedIn || !accessToken) {
    return [RACSignal return:[NSError errorWithDomain:ERROR_DOMAIN
                                                 code:ERROR_CODE
                                             userInfo:nil]];
  }
  NSMutableArray* signalArray = [[NSMutableArray alloc] init];
  [shoppingListArray each:^(ShoppingList* shoppingList) {
      // Remove Product which belongs to Shopping List
      [shoppingList.products each:^(Product* productOfShoppingList) {
          if ([productOfShoppingList.productID
                  isEqualToString:product.productID]) {
            [shoppingList.products removeObject:productOfShoppingList];
            return;
          }
      }];

      // Push to Server
      RACSignal* signal = [[SPMSShoppingListDataService sharedService]
             removeProduct:product
          fromShoppingList:shoppingList
                   baseUrl:self.baseUrl
                    client:self.client];
      [signalArray addObject:signal];
  }];
  return [[[RACSignal combineLatest:signalArray]
      catch:^RACSignal * (NSError * error) {
          if ([[error localizedDescription]
                  isKindOfClass:[NSMutableURLRequest class]]) {
            NSMutableURLRequest* request =
                (NSMutableURLRequest*)[error localizedDescription];
            // If error, Add to Offline Persistent Operation Queue
            [[SPMSDataPersistentOperationQueue sharedService]
                enqueueWithRequest:request];
          }
          // Return Error to display Error Message
          return [RACSignal error:error];
      }] map:^(RACTuple* signalValues) {
      return @([signalValues.rac_sequence
          all:^BOOL(NSNumber* value) { return [value boolValue]; }]);
  }];
}

- (RACSignal*)addNewShoppingListWithName:(NSString*)shoppingListName {
  RACSignal* signal = [[SPMSShoppingListDataService sharedService]
      addNewShoppingListWithName:shoppingListName
                         baseUrl:self.baseUrl
                          client:self.client];
  return [[[SPMSShoppingListDataService sharedService]
      shoppingListForSignal:signal] catch:^RACSignal * (NSError * error) {
      if ([[error localizedDescription]
              isKindOfClass:[NSMutableURLRequest class]]) {
        NSMutableURLRequest* request =
            (NSMutableURLRequest*)[error localizedDescription];
        // If error, Add to Offline Persistent Operation Queue
        [[SPMSDataPersistentOperationQueue sharedService]
            enqueueWithRequest:request];
      }
      // Return Error to display Error Message
      return [RACSignal error:error];
  }];
}
- (RACSignal*)editShoppingListWithShoppingListId:(NSNumber*)shoppingListId
                                shoppingListName:(NSString*)shoppingListName {
  return [[SPMSShoppingListDataService sharedService]
      editShoppingListWithShoppingListId:shoppingListId
                        shoppingListName:shoppingListName
                                 baseUrl:self.baseUrl
                                  client:self.client];
}

- (RACSignal*)deleteShoppingListWithShoppingListId:(NSNumber*)shoppingListId {
  RACSignal* signal = [[SPMSShoppingListDataService sharedService]
      deleteShoppingListWithShoppingListId:shoppingListId
                                   baseUrl:self.baseUrl
                                    client:self.client];

  return [signal catch:^RACSignal * (NSError * error) {
      if ([[error localizedDescription]
              isKindOfClass:[NSMutableURLRequest class]]) {
        NSMutableURLRequest* request =
            (NSMutableURLRequest*)[error localizedDescription];
        // If error, Add to Offline Persistent Operation Queue
        [[SPMSDataPersistentOperationQueue sharedService]
            enqueueWithRequest:request];
      }
      // Return Error to display Error Message
      return [RACSignal error:error];
  }];
}

#pragma mark - Promotion
- (RACSignal*)getPromotionListWithEndDate:(NSDate*)endDate
                               pageNumber:(NSUInteger)pageNumber {
  RACSignal* signal = [[SPMSPromotionDataService sharedService]
      fetchPromotionListWithEndDate:endDate
                         pageNumber:pageNumber
                            baseUrl:self.baseUrl
                             client:self.client];
  return [[SPMSPromotionDataService sharedService] promotionForSignal:signal];
}

- (RACSignal*)getTotalNumberofAvailablePromotionWithEndDate:(NSDate*)endDate {
  return [[SPMSPromotionDataService sharedService]
      getTotalNumberofAvailablePromotionWithEndDate:endDate
                                            baseUrl:self.baseUrl
                                             client:self.client];
}

#pragma mark - CategoryType
- (RACSignal*)getCategoryTypes {
  RACSignal* signal = [[SPMSCategoryTypeDataService sharedService]
      fetchCategoryTypeWithBaseUrl:self.baseUrl
                            client:self.client];
  return [[SPMSCategoryTypeDataService sharedService]
      categoryTypeForSignal:signal];
}

- (RACSignal*)fetchCategoryTypeListWithPageNumber:(NSUInteger)pageNumber {
  RACSignal* signal = [[SPMSCategoryTypeDataService sharedService]
      fetchCategoryTypeListWithPageNumber:pageNumber
                                  baseUrl:self.baseUrl
                                   client:self.client];
  return [[SPMSCategoryTypeDataService sharedService]
      categoryTypeForSignal:signal];
}

#pragma mark - Map
- (RACSignal*)getMaps {
  RACSignal* signal = [[SPMSMapDataService sharedService]
      downloadMapDataListWithBaseUrl:self.baseUrl
                              client:self.client];
  return [[SPMSMapDataService sharedService] mapForSignal:signal];
}

- (RACSignal*)sendVisitedBeaconWithPointId:(NSNumber*)pointId {
  RACSignal* signal = [[SPMSMapDataService sharedService]
      sendVisitedBeaconWithPointId:pointId
                           baseUrl:self.baseUrl
                            client:self.client];

  return [signal catch:^RACSignal * (NSError * error) {
      if ([[error localizedDescription]
              isKindOfClass:[NSMutableURLRequest class]]) {
        NSMutableURLRequest* request =
            (NSMutableURLRequest*)[error localizedDescription];
        // If error, Add to Offline Persistent Operation Queue
        [[SPMSDataPersistentOperationQueue sharedService]
            enqueueWithRequest:request];
      }
      // Return Error to display Error Message
      return [RACSignal error:error];
  }];
}

@end
