//
//  SPMSCategoryListViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 8/6/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIScrollView+EmptyDataSet.h>

@interface SPMSCategoryListViewController
    : UIViewController<UITableViewDataSource,
                       UITableViewDelegate,
                       DZNEmptyDataSetDelegate,
                       DZNEmptyDataSetSource>

@property(strong, nonatomic) IBOutlet UITableView* tableView;
@property(assign, nonatomic) NSMutableArray* selectedCategories;

@end
