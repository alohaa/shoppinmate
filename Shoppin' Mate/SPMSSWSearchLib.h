//
//  SPMSSWSearch.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/12/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#pragma once
#ifndef Shoppin__Mate_SPMSSWSearch_h
#define Shoppin__Mate_SPMSSWSearch_h

// Delegate and Datasource
#import "SPMSSWSearchDelegate.h"
#import "SPMSSWSearchDatasource.h"

// Operation
#import "SPMSSWSearchOperation.h"

// Algorithm
#import "SPMSScoringMatrix.h"

// Objects
#import "SPMSSWObjects.h"

#endif
