//
//  SPMSShoppingListsViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 7/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSShoppingListsViewController.h"
#import "SPMSShoppingListsTableViewCell.h"
#import "SPMSWebDataService.h"
#import "ShoppingList.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "NSDictionary+Verified.h"
#import "RACEXTScope.h"
#import "SPMSDetailShoppingListViewController.h"
#import <SVPullToRefresh.h>
#import <TSMessage.h>
#import <SVProgressHUD.h>
#import "UIView+Border.h"
#import "SPMSPaddingTextField.h"

@interface SPMSShoppingListsViewController () {
  SPMSPaddingTextField* addNewShoppingListTextField;
}

@property(strong, nonatomic) NSMutableArray* listOfShoppingList;
@property(nonatomic) BOOL isDeletingList;
@property(nonatomic) BOOL isCreatingNewList;
@property(assign, nonatomic) NSUInteger cellCount;
@property(assign, nonatomic) NSUInteger pageNumber;

@end

@implementation SPMSShoppingListsViewController
#pragma mark - Initialization
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    self.listOfShoppingList = [[NSMutableArray alloc] init];
    self.cellCount = 0;
    self.pageNumber = 1;
    self.isDeletingList = NO;
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.dataSource = self;
  self.tableView.delegate = self;
  self.tableView.emptyDataSetDelegate = self;
  self.tableView.emptyDataSetSource = self;
  // Config Sidebar
  [self configNavigationBar];
  // Config Sidebar
  [self configTableViewLayout];
  // Register Custom UITableViewCell for SearchDisplayController tableview
  [self.tableView registerClass:[SPMSShoppingListsTableViewCell class]
         forCellReuseIdentifier:@"shoppingListsCell"];
  [self configureAddNewShoppingListTextField];
  // Reload Data
  [self loadShoppingListsWithPageNumber:self.pageNumber];
  // Config PullToRefresh
  [self configPullToRefresh];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  // register for keyboard notifications
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWillShow)
             name:UIKeyboardWillShowNotification
           object:nil];

  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWillHide)
             name:UIKeyboardWillHideNotification
           object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  // unregister for keyboard notifications while not visible.
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillShowNotification
              object:nil];

  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillHideNotification
              object:nil];
}

#pragma mark - Load Data
- (void)loadShoppingListsWithPageNumber:(NSUInteger)page {
  if (self.pageNumber == 1) {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  }
  @weakify(self);
  [[[[SPMSWebDataService sharedService] getShoppingListsWithPageNumber:page]
      deliverOn:RACScheduler.mainThreadScheduler] subscribeNext:^(NSArray*
                                                                      list) {
      @strongify(self);
      [self.listOfShoppingList addObjectsFromArray:list];
      self.cellCount = [self.listOfShoppingList count];
      [self.tableView reloadData];
  } error:^(NSError* error) {
      [SVProgressHUD
          showErrorWithStatus:
              [NSString stringWithFormat:@"%@", [error localizedDescription]]];
  } completed:^{
      self.pageNumber = self.pageNumber + 1;
      [SVProgressHUD dismiss];
  }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return self.cellCount;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell...
  static NSString* CellIdentifier = @"shoppingListsCell";
  SPMSShoppingListsTableViewCell* cell = (SPMSShoppingListsTableViewCell*)
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  [self configureCell:cell atIndexPath:indexPath];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (void)configureCell:(SPMSShoppingListsTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath {
  // Configure the cell
  ShoppingList* shoppingList =
      (ShoppingList*)[self.listOfShoppingList objectAtIndex:indexPath.row];

  UIView* crossView =
      [self viewWithImageName:@"ShoppingListsCell_DeleteShoppingList_ICO"];
  UIColor* redColor =
      [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000];

  [cell setDefaultColor:self.tableView.backgroundView.backgroundColor];
  [cell setDelegate:self];
  [cell setSwipeGestureWithView:crossView
                          color:redColor
                           mode:MCSwipeTableViewCellModeExit
                          state:MCSwipeTableViewCellState3
                completionBlock:^(MCSwipeTableViewCell* cell,
                                  MCSwipeTableViewCellState state,
                                  MCSwipeTableViewCellMode mode) {
                    [self deleteShoppingListWithCell:cell];
                }];
  [cell setCellDataWithShoppingList:shoppingList hadInShoppingLists:nil];
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
  return 70.0f;
}

- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  ShoppingList* aShoppingList =
      [self.listOfShoppingList objectAtIndex:indexPath.row];
  [self performSegueWithIdentifier:@"viewDetailShoppingList"
                            sender:aShoppingList];
}

#pragma mark - DZNEmptyDataSetSource Methods
- (NSAttributedString*)titleForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"No Shopping List";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0],
    NSForegroundColorAttributeName :
        [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)descriptionForEmptyDataSet:(UIScrollView*)scrollView {
  NSString* text = @"Try to add new Shopping List";

  NSMutableParagraphStyle* paragraphStyle = [NSMutableParagraphStyle new];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;

  NSDictionary* attributes = @{
    NSFontAttributeName : [UIFont systemFontOfSize:15.0],
    NSForegroundColorAttributeName : [UIColor colorWithWhite:0.820 alpha:1.000],
    NSParagraphStyleAttributeName : paragraphStyle
  };

  return [[NSMutableAttributedString alloc] initWithString:text
                                                attributes:attributes];
}

- (NSAttributedString*)buttonTitleForEmptyDataSet:(UIScrollView*)scrollView
                                         forState:(UIControlState)state {
  return nil;
}

- (UIImage*)imageForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIImage imageNamed:@"AddToList_EmptyState_ICO"];
}

- (UIColor*)backgroundColorForEmptyDataSet:(UIScrollView*)scrollView {
  return [UIColor whiteColor];
}

- (UIView*)customViewForEmptyDataSet:(UIScrollView*)scrollView {
  return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView*)scrollView {
  return 0;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView*)scrollView {
  return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView*)scrollView {
  return YES;
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh {
  __weak SPMSShoppingListsViewController* weakSelf = self;

  [self.tableView.pullToRefreshView setTitle:@"Getting New Shopping Lists"
                                    forState:SVPullToRefreshStateLoading];

  // Setup pull-to-refresh
  [self.tableView
      addPullToRefreshWithActionHandler:^{ [weakSelf insertRowAtTop]; }];
  // Setup infinite scrolling
  [self.tableView
      addInfiniteScrollingWithActionHandler:^{ [weakSelf insertRowAtBottom]; }];
}

- (void)insertRowAtBottom {
  __weak SPMSShoppingListsViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      [self loadShoppingListsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.infiniteScrollingView stopAnimating];
  });
}

- (void)insertRowAtTop {
  __weak SPMSShoppingListsViewController* weakSelf = self;

  int64_t delayInSeconds = 2.0;
  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      weakSelf.pageNumber = 1;
      [weakSelf.listOfShoppingList removeAllObjects];
      [weakSelf loadShoppingListsWithPageNumber:self.pageNumber];
      [weakSelf.tableView.pullToRefreshView stopAnimating];
  });
}

#pragma mark - Action
- (void)addNewListAction {
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
  [[[[SPMSWebDataService sharedService]
      addNewShoppingListWithName:addNewShoppingListTextField.text]
      deliverOn:RACScheduler
                    .mainThreadScheduler] subscribeNext:^(NSArray*
                                                              resultArray) {
      if (0 < [resultArray count]) {
        ShoppingList* newShoppingList = [resultArray firstObject];
        self.cellCount++;
        [self.listOfShoppingList insertObject:newShoppingList atIndex:0];
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        if (0 < self.cellCount) {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.tableView
                  scrollToRowAtIndexPath:indexPath
                        atScrollPosition:UITableViewScrollPositionTop
                                animated:YES];
          });
        }
        // Remove Keyboard
        [addNewShoppingListTextField resignFirstResponder];
        // Update Tableview
        [self.tableView insertRowsAtIndexPaths:@[ indexPath ]
                              withRowAnimation:UITableViewRowAnimationFade];
        [TSMessage
            showNotificationInViewController:self
                                       title:NSLocalizedString(
                                                 @"New Shopping List Added",
                                                 nil)
                                    subtitle:nil
                                       image:nil
                                        type:TSMessageNotificationTypeSuccess
                                    duration:
                                        TSMessageNotificationDurationAutomatic
                                    callback:nil
                                 buttonTitle:nil
                              buttonCallback:nil
                                  atPosition:TSMessageNotificationPositionTop
                        canBeDismissedByUser:YES];
      }
  } error:^(NSError* error) {
      [TSMessage
          showNotificationInViewController:self
                                     title:NSLocalizedString(
                                               @"Add new Shopping List "
                                               @"Failed. \nAdd to Offline "
                                               @"Queue to retry later",
                                               nil)
                                  subtitle:nil
                                     image:nil
                                      type:TSMessageNotificationTypeError
                                  duration:
                                      TSMessageNotificationDurationAutomatic
                                  callback:nil
                               buttonTitle:nil
                            buttonCallback:nil
                                atPosition:TSMessageNotificationPositionTop
                      canBeDismissedByUser:YES];
      self.isCreatingNewList = NO;
      addNewShoppingListTextField.text = @"";
      [SVProgressHUD dismiss];
  } completed:^{
      [self.tableView reloadData];
      addNewShoppingListTextField.text = @"";
      self.isCreatingNewList = NO;
      [SVProgressHUD dismiss];
  }];
}

- (void)showSearchProductVC:(id)sender {
  [self performSegueWithIdentifier:@"searchProduct" sender:self];
}

#pragma mark - Delete Shopping List
- (void)deleteShoppingListWithCell:(MCSwipeTableViewCell*)cell {
  NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
  ShoppingList* aShoppingList =
      [self.listOfShoppingList objectAtIndex:indexPath.row];
  if (!aShoppingList) {
    return;
  }
  [[[[SPMSWebDataService sharedService]
      deleteShoppingListWithShoppingListId:aShoppingList.shoppingListID]
      deliverOn:RACScheduler.mainThreadScheduler]
      subscribeNext:^(ShoppingList* newShoppingList) {
          self.cellCount--;
          [self.tableView deleteRowsAtIndexPaths:@[ indexPath ]
                                withRowAnimation:UITableViewRowAnimationFade];
          [TSMessage
              showNotificationInViewController:
                  self title:NSLocalizedString(
                                 @"Selected Shopping List Deleted", nil)
                                      subtitle:nil
                                         image:nil
                                          type:TSMessageNotificationTypeSuccess
                                      duration:
                                          TSMessageNotificationDurationAutomatic
                                      callback:nil
                                   buttonTitle:nil
                                buttonCallback:nil
                                    atPosition:TSMessageNotificationPositionTop
                          canBeDismissedByUser:YES];
      }
      error:^(NSError* error) {
          [cell swipeToOriginWithCompletion:nil];
          [TSMessage
              showNotificationInViewController:
                  self title:NSLocalizedString(@"Delete selected Shopping List "
                                               @"Failed. \nAdd to Offline "
                                               @"Queue to retry later",
                                               nil)
                                      subtitle:nil
                                         image:nil
                                          type:TSMessageNotificationTypeError
                                      duration:
                                          TSMessageNotificationDurationAutomatic
                                      callback:nil
                                   buttonTitle:nil
                                buttonCallback:nil
                                    atPosition:TSMessageNotificationPositionTop
                          canBeDismissedByUser:YES];
          self.isDeletingList = NO;
      }
      completed:^{ self.isDeletingList = NO; }];
}

#pragma mark - Buttons
- (void)configureAddNewShoppingListTextField {
  addNewShoppingListTextField = [[SPMSPaddingTextField alloc]
      initWithFrame:CGRectMake(
                        0, self.view.bounds.size.height - 50 - 64, 320, 50)];
  addNewShoppingListTextField.delegate = self;
  [addNewShoppingListTextField
      setBackgroundColor:
          [UIColor colorWithRed:0.925 green:0.941 blue:0.945 alpha:1.000]];
  addNewShoppingListTextField.placeholder = @"Add New Shopping List";
  UIButton* addNewButton =
      [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
  [addNewButton setImage:[UIImage imageNamed:@"AddToList_AddNewList_BTN"]
                forState:UIControlStateNormal];
  [addNewButton
      setImage:[UIImage imageNamed:@"AddToList_AddNewList_Highlighted_BTN"]
      forState:UIControlStateHighlighted];
  [addNewButton
      setImage:[UIImage imageNamed:@"AddToList_AddNewList_Highlighted_BTN"]
      forState:UIControlStateSelected];
  [addNewButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
  RAC(addNewButton, enabled) = [RACSignal
      combineLatest:@[
                      addNewShoppingListTextField.rac_textSignal,
                      RACObserve(self, isCreatingNewList)
                    ]
             reduce:^(NSString* newShoppingListName, NSNumber* isCreating) {
                 return
                     @(0 < newShoppingListName.length && !isCreating.boolValue);
             }];
  [[addNewButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      subscribeNext:^(id x) {
          self.isCreatingNewList = YES;
          [self addNewListAction];
      }];
  [addNewShoppingListTextField setRightView:addNewButton];
  [addNewShoppingListTextField setRightViewMode:UITextFieldViewModeAlways];
  [addNewShoppingListTextField
      addTopBorderWithHeight:0.5f
                    andColor:[UIColor colorWithRed:0.741
                                             green:0.765
                                              blue:0.780
                                             alpha:1.000]];
  [self.view addSubview:addNewShoppingListTextField];
}

#pragma mark - View Layout Configuration
- (void)configNavigationBar {
  self.navigationItem.title = @"Shopping Lists";
  // "Show Menu" Button
  UIButton* showMenuBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  showMenuBTN.frame = CGRectMake(0, 0, 30, 30);
  [showMenuBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_ShowMenu_ICO"]
               forState:UIControlStateNormal];
  [showMenuBTN addTarget:self
                  action:@selector(presentLeftMenuViewController:)
        forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* showMenuBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:showMenuBTN];

  [self.navigationItem setLeftBarButtonItem:showMenuBTNItem];

  // "Search Product" Button
  UIButton* searchBTN = [UIButton buttonWithType:UIButtonTypeCustom];
  searchBTN.frame = CGRectMake(5, 0, 30, 30);
  [searchBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_Search_ICO"]
             forState:UIControlStateNormal];
  [searchBTN addTarget:self
                action:@selector(showSearchProductVC:)
      forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* searchBTNItem =
      [[UIBarButtonItem alloc] initWithCustomView:searchBTN];

  [self.navigationItem setRightBarButtonItem:searchBTNItem];
  [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)configTableViewLayout {
  [self.tableView setTableFooterView:[UIView new]];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  [textField resignFirstResponder];
  return NO;
}

#pragma mark - Keyboard Helpers
// method to move the view up/down whenever the keyboard is shown/dismissed
- (void)setViewMovedUp:(BOOL)movedUp {
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationDuration:0.3];  // if you want to slide up the view

  CGRect rect = self.view.frame;
  if (movedUp) {
    // 1. move the view's origin up so that the text field that will be hidden
    // come above the keyboard
    // 2. increase the size of the view so that the area behind the keyboard is
    // covered up.
    rect.origin.y -= kOFFSET_FOR_KEYBOARD;
    rect.size.height += kOFFSET_FOR_KEYBOARD;
  } else {
    // revert back to the normal state.
    rect.origin.y += kOFFSET_FOR_KEYBOARD;
    rect.size.height -= kOFFSET_FOR_KEYBOARD;
  }
  self.view.frame = rect;

  [UIView commitAnimations];
}

- (void)keyboardWillShow {
  // Animate the current view out of the way
  if (self.view.frame.origin.y >= 0) {
    [self setViewMovedUp:YES];
  } else if (self.view.frame.origin.y < 0) {
    [self setViewMovedUp:NO];
  }
}

- (void)keyboardWillHide {
  if (self.view.frame.origin.y >= 0) {
    [self setViewMovedUp:YES];
  } else if (self.view.frame.origin.y < 0) {
    [self setViewMovedUp:NO];
  }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"viewDetailShoppingList"]) {
    ShoppingList* aShoppingList = (ShoppingList*)sender;
    SPMSDetailShoppingListViewController* detailVC =
        [segue destinationViewController];
    NSMutableArray* array = [[NSMutableArray alloc]
        initWithArray:[aShoppingList.products allObjects]];
    [detailVC setProductList:array];
    [detailVC setShoppingListID:aShoppingList.shoppingListID];
    [detailVC setShoppingList:aShoppingList];
    [detailVC setShoppingListName:aShoppingList.shoppingListName];
  }
}

#pragma mark - Utility
- (UIView*)viewWithImageName:(NSString*)imageName {
  UIImage* image = [UIImage imageNamed:imageName];
  UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
  imageView.contentMode = UIViewContentModeCenter;
  return imageView;
}

- (void)dealloc {
  self.tableView.delegate = nil;
  self.tableView.emptyDataSetDelegate = nil;
  addNewShoppingListTextField.delegate = nil;
}

@end
