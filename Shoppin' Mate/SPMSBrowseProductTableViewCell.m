//
//  HomeFeedTableViewCell.m
//  Ohyeap
//
//  Created by El Desperado on 7/15/13.
//  Copyright (c) 2013 El Desperado. All rights reserved.
//

#import "SPMSBrowseProductTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <UIImageView+WebCache.h>
#import "SPMSCurrencyFormatter.h"

@implementation SPMSBrowseProductTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString*)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)setProduct:(Product*)product {
  self.productNameLabel.text = product.productName;
  self.productDescLabel.text = product.productDescription;
  NSNumberFormatter* currencyFormatter = [SPMSCurrencyFormatter formatter];
  NSString* priceString = [currencyFormatter stringFromNumber:product.price];
  self.productPriceLabel.text = [NSString stringWithFormat:@"%@", priceString];
  [self.productImageView
      sd_setImageWithURL:[NSURL URLWithString:product.imageURL]
        placeholderImage:[UIImage imageNamed:@"Photo"]];
}
@end
