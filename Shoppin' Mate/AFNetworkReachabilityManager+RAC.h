//
//  AFNetworkReachabilityManager+RAC.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/27/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "AFNetworkReachabilityManager.h"

@class RACSignal;

@interface AFNetworkReachabilityManager (RAC)

- (RACSignal *)rac_reachabilityStatusChangeSignal;

@end
