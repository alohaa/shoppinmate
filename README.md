![AppIcon76x76@2x.png](https://bitbucket.org/repo/7nExrz/images/3414945409-AppIcon76x76%402x.png)

Shoppin' Mate iOS Application

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Pods ###

```

platform :ios, '7.0'
pod 'MagicalRecord', '~> 2.2'
pod 'AFNetworking', '~> 2.2'
pod 'SVPullToRefresh'
pod 'SVProgressHUD'
pod 'RESideMenu', '~> 4.0.4'
pod 'TTTAttributedLabel'
pod 'SDWebImage', '~> 3.6'
pod 'HPGrowingTextView', '~> 1.1'
pod 'ObjectiveSugar', '~> 1.1'
pod 'SSKeychain', '~> 1.2'
pod 'MCSwipeTableViewCell', '~> 2.1'
pod "JLPermissions"
pod "RQShineLabel"
pod 'ReactiveCocoa', '~> 2.3'
pod 'NewRelicAgent', '~> 3.324'
```